import { generateWALinks } from '../tools/WaIntegration.js';
import { AEItemSheetBaseImplem } from './ItemSheetBaseImplem.js';
import { AEWeaponItemSheet } from './WeaponItemSheet.js';
import { AEArmorItemSheet } from './ArmorItemSheet.js';
import { AEMiscellaneousItemSheet } from './MiscellaneousItemSheet.js';

const chooseImpl = (itemSheet) => {

    if( itemSheet.item.isWeapon ) { return new AEWeaponItemSheet(itemSheet); }

    if( itemSheet.item.isArmor ) { return new AEArmorItemSheet(itemSheet); }

    if( itemSheet.item.isMisc ) { return new AEMiscellaneousItemSheet(itemSheet); }
}

/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
export class AEItemSheet extends ItemSheet {

    /** @override */
	static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
              classes: ["acariaempire", "sheet", "item"],
              width: 'auto',
              height: 'auto'
        });
    }
  
    constructor(...args) {
        super(...args);
        this._impl = chooseImpl(this, this.item);
    }

    /** @override */
    get title() {
        const customTitle = this._impl.customTitle;
        return customTitle ?? super.title;
    }

    /**
     * @returns {AEItemSheetBaseImplem}
     */
    get impl() {
        return this._impl;
    }

    /* -------------------------------------------- */

    /** @override */
    getData() {
        const data = super.getData();
        this.impl.fillData(data);

        return data;
    }

    /** @override */
    _getHeaderButtons() {
        let buttons = super._getHeaderButtons();
        this.impl.fillHeaderButtons(buttons);

        return buttons;
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);
        generateWALinks(html);
        this.impl.activateListeners(html);
    }

    /** @override */
    get isEditable() {
        return super.isEditable && !!this.item.id;
    }
}
