import { commonMiscList, miscAffixList, miscSlotList } from '../tools/WaIntegration.js';
import { AEItemSheetBaseImplem } from './ItemSheetBaseImplem.js';

/**
 * Item sheet data for Power items
 * @extends {AEItemSheetBaseImplem}
 */
 export class AEMiscellaneousItemSheet extends AEItemSheetBaseImplem {

    constructor(itemSheet) {
        super(itemSheet);

        this._misc = this.item.asMisc();
        this.sheet.options.classes.push('misc');
        this.sheet.options.template = "systems/acariaempire/resources/items/sheet/misc-sheet.hbs";
        this.sheet.options.width = 400;
        this.sheet.options.resizable = false;

        this._displayData = {
            chooseMiscOpenedSections: []
        };
    }

    get misc() {
        return this._misc;
    }

    /** @override */
    fillData(updatedData) {

        updatedData.misc = this.misc;
        updatedData.upgradable = game.user.isGM && !!this.misc.item.id;

        const miscBaseSet = this.misc.basedOn != null;
        updatedData.chooseBaseMisc = updatedData.upgradable && ! miscBaseSet;
        if(updatedData.chooseBaseMisc) {

            const openedSections = this._displayData.chooseMiscOpenedSections;
            updatedData.allSlots =  miscSlotList().filter( s => {
                return s.key != 'unset';
            }).map(slot => {
                const sectionId = slot.key;
                const data = {
                    sectionId: sectionId,
                    miscs: commonMiscList().filter(m => m.slot.key === slot.key),
                    showContent: openedSections.includes(sectionId)
                };
                data.css = data.showContent ? 'opened' : '';
                return mergeObject(data, slot);
            });
        }

        updatedData.displayDetails = miscBaseSet;
        if(updatedData.displayDetails) {

            //Affix display
            if( updatedData.displayDetails ) {
                updatedData.miscAffixes = this.misc.affixes.map(a => {
                    return mergeObject( {upgradable: updatedData.upgradable }, a);
                });
        
                updatedData.affixMenus = this.misc.availableAffixes.reduce( (result, affix) => {
                    const menu = affix.menu;
                    const section = result.find( s => s.menu.key === menu.key );
                    if( section ) {
                        section.affixes.push( affix );
                    } else {
                        result.push({
                            menu: menu,
                            affixes: [affix]
                        });
                    }
                    return result;
                }, []);
    
                updatedData.affixMenus.sort( (a,b) => a.menu.displayOrder - b.menu.displayOrder);
                updatedData.affixMenus.forEach( section => {
                    section.affixes.sort( (a,b) => a.name.localeCompare(b.name) );
                });
            }
        }
    }

    /** @override */
    activateListeners(html) {

        // Everything below here is only needed if the sheet is editable
        if (!this.sheet.options.editable) return;

        html.find(".choosing-misc").on("click", ".choose-menu",  this._onClickToggleChoosingMenu.bind(this));

        html.find(".choose-misc-panel").on("click", ".misc-name",  this._onClickSelectBaseMisc.bind(this));
        html.find(".current-misc-base").on("click", ".current-misc-delete",  this._onClickDropBaseMisc.bind(this));
        html.find(".misc-affix-list").on("click", ".affix-delete",  this._onClickDropAffix.bind(this));
        html.find(".new-affixes").on("click", ".affix-add",  this._onClickAddAffix.bind(this));
    }

    async _onClickToggleChoosingMenu(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const sectionId = a.dataset.section;

        const openedSections = this._displayData.chooseMiscOpenedSections;
        const index = openedSections.indexOf(sectionId);
        if( index != -1 ) {
            openedSections.splice(index, 1);
        } else {
            openedSections.push(sectionId);
        }
        this.sheet.render();
    }

    async _onClickSelectBaseMisc(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const ref = a.parentElement.parentElement.dataset.ref;

        const commonMisc = commonMiscList().find(m => m.key === ref);
        if( !commonMisc ) { return ui.notifications.warn(`Can't find common misc named ${ref}`); }

        await this.misc.setBaseMisc(ref);
        this.sheet.render();
    }

    async _onClickDropBaseMisc(event) {
        event.preventDefault();

        await this.misc.setBaseMisc(null);
        this.sheet.render();
    }


    async _onClickDropAffix(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const ref = a.parentElement.dataset.ref;

        this.misc.clearCache();
        await this.misc.removeAffix(ref);
        this.sheet.render();
    }

    async _onClickAddAffix(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const ref = a.parentElement.dataset.ref;

        this.misc.clearCache();
        await this.misc.addAffix(ref);
        this.sheet.render();
    }

}
