import { commonWeaponList, elementList, weaponAffixList, weaponMaxRarity, weaponRarityList, weaponSizeList } from '../tools/WaIntegration.js';
import { mergeOnAttackSucessAffixes, modifyElementWithAffixes, modifyDamageWithAffixes, modifySpeedWithAffixes } from '../tools/affixes.js';
import { AEEquipmentItem } from './EquipmentItem.js';
import { ALL_ITEMS_TYPES } from '../tools/constants.js';

const checkIntegrity = (item) => {

    let isOk = true;
    if(item.type != ALL_ITEMS_TYPES.weapon) {
        isOk = false;
        console.error(item.name + ' is not a weapon. (' + item.type + ');');

    } else if( item.system.basedOn ) { // Weapon can be in preparation. If that's the cse, no further checks

        if( ! item.system.size ) {
            isOk = false;
            console.error(item.name + ' has no weapon size');

        } else if( weaponSizeList() && ! weaponSizeList().find(s => s.size === item.system.size) ) {
            isOk = false;
            console.error(item.name + ' has an invalid weapon size : ' + item.system.size);
        }
    }
    
    if(! isOk ) {
        throw 'Invalid weapon item';
    }
}

export class AEWeaponItem extends AEEquipmentItem{

    constructor(item) {
        super(item);
        checkIntegrity(this._item);
        this._basedOn = null;
    }

    get basedOn() {
        
        if( !this._basedOn && this.itemData.basedOn ) {
            this._basedOn = commonWeaponList().find(w => w.weapon === this.itemData.basedOn) ?? null;
            if(!this._basedOn) { console.error(`Can't find common weapon: ${this.itemData.basedOn}`); }
        }
        
        return this._basedOn;
    }


    get size() {
        if(!this._size) { this._size = mergeObject( {}, weaponSizeList().find(s => s.size === this.itemData.size) ); }
        return this._size;
    }

    get element() {
        const currentElementKey = modifyElementWithAffixes(this.itemData.element, this.affixes)
        return mergeObject( {}, elementList().find(e => e.key === currentElementKey) ); 
    }

    get damage() {
        return modifyDamageWithAffixes(this.itemData.baseDamage, this.affixes);
    }

    get speed() {
        return modifySpeedWithAffixes(this.itemData.baseSpeed, this.affixes);
    }

    get onAttackSuccess() {
        return mergeOnAttackSucessAffixes( this.affixes );
    }

    get category() {
        return this.basedOn?.category ?? null;
    }

    get isMeleeWeapon() {
        return this.category == 'melee';
    }

    get isRangedWeapon() {
        return this.category == 'ranged';
    }

    get isImprovisedWeapon() {
        return this.category == 'improvised';
    }

    get isAllowedOnLeftHand() {
        const affix = this.affixes.find(a => a.weaponUpgrade.leftHand) ?? null;
        return affix != null;
    }

    /** @override */
    get rarity() {
        if(!this._rarity) {
            const count = this.baseAffixes.reduce( (result, affix) => {
                return Math.min( result + (affix.weaponUpgrade?.cost ?? 0), weaponMaxRarity() );
            }, 0);
            this._rarity = weaponRarityList().find( r => r.affixCost == count );
        }
        return this._rarity;
    }

    /** @override */
    get cost() {
        return this.size.basePrice + this.rarity.additionalCost;
    }

    /** @override */
    get slot() {
        return {
            key: 'weapon',
            name: this.size?.name ?? '',
            icon: 'systems/acariaempire/resources/items/slots/weapon.png',
            shortcut: 'item.weapon.definition',
            sortValue : '030'
        };
    }

    /** @override */
    clearCache() {
        super.clearCache();
        // Attribute based on this.affixes can't be cached since affixes can be altered by spells
        // Only those based on this.baseAffixes can be cached.
        this._basedOn = null;
        this._size = null;
        this._rarity = null;
    }

    /** Which other affixes the weapon can acquire */
    get availableAffixes() {


        const currentAffixCost = this.rarity.affixCost;
        const weaponAffixNames = this.baseAffixes.map(a => a.key);
        return weaponAffixList().filter(a => {
            if( weaponAffixNames.includes(a.key) ) { return false; }

            const weaponUpgrade = a.weaponUpgrade;
            if( !weaponUpgrade ) { return false; }

            if( currentAffixCost + weaponUpgrade.cost > weaponMaxRarity() ) { return false; }
            if( !weaponUpgrade.sizes.includes( this.size.key ) ) { return false; }
            if( !weaponUpgrade.categories.includes( this.category ) ) { return false; }
            return true;
        });
    }    

    /**
     * Change current weapon base weapon and update cached data
     * @param {String} baseWeaponRef : If null, will unset base weapon
     */
    async setBaseWeapon(baseWeaponRef) {
        
        this.clearCache();
        const weapon = commonWeaponList().find(w => w.weapon === baseWeaponRef) ?? null;

        const smallWeaponRef = weaponSizeList().find( s => s.key === 'small_weapon');
        const sizeRef = weapon?.size ?? smallWeaponRef;

        const updateData = {};
        updateData['system.basedOn'] = baseWeaponRef;
        updateData['system.element'] = weapon?.element.key ?? 'metal';
        updateData['system.affixes'] = weapon?.affixes?.map( a => a.key ) ?? [];

        updateData['system.size'] = sizeRef.size;
        updateData['system.baseDamage'] = sizeRef.baseDamage;
        updateData['system.baseSpeed'] = sizeRef.baseSpeed;
        
        updateData['img'] = weapon?.icon ?? this.item.img;

        await this.item.update(updateData);
    }

    /**
     * Change current weapon attack element
     */
     async changeElement(elementKey) {
        
        const element = elementList().find( e => e.key === elementKey );
        if( !element ) {
            return ui.notifications.error( 'Invalid element key ' + elementKey );
        }

        this.clearCache();
        const updateData = {};
        updateData['system.element'] = elementKey;
        await this.item.update(updateData);
    }
}