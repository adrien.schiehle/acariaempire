import {translate} from '../tools/Translator.js';
import { commonWeaponList, elementList, weaponAffixList, weaponSizeList } from '../tools/WaIntegration.js';
import { AEItemSheetBaseImplem } from './ItemSheetBaseImplem.js';

const CSS_VALUE_BUFFED = 'buffed-value';
const CSS_VALUE_DEBUFFED = 'debuffed-value';


const alteredValue = (base, smallIsBetter=false) => {
    let diff = base.value - base.initialValue;
    if( smallIsBetter ) { diff = -1 * diff; }

    return {
        value: diff == 0 ? base.value : '' + base.initialValue + '⇒ ' + base.value,
        css: diff < 0 ? CSS_VALUE_DEBUFFED : ( diff > 0 ? CSS_VALUE_BUFFED : '')
    };
}

/**
 * Item sheet data for Power items
 * @extends {AEItemSheetBaseImplem}
 */
 export class AEWeaponItemSheet extends AEItemSheetBaseImplem {

    constructor(itemSheet) {
        super(itemSheet);

        this._weapon = this.item.asWeapon();
        this.sheet.options.classes.push('weapon');
        this.sheet.options.template = "systems/acariaempire/resources/items/sheet/weapon-sheet.hbs";
        this.sheet.options.width = 400;
        this.sheet.options.resizable = false;

        this._displayData = {
            changingElement: false
        };
    }

    get weapon() {
        return this._weapon;
    }

    /** @override */
    fillData(updatedData) {

        updatedData.weapon = this.weapon;
        updatedData.upgradable = game.user.isGM && !!this.weapon.item.id;

        // Choosing base weapon
        const weaponBaseSet = this.weapon.basedOn != null;
        updatedData.chooseBaseWeapon = updatedData.upgradable && ! weaponBaseSet;
        if(updatedData.chooseBaseWeapon) {
            updatedData.baseWeapons = ['none', 'small_weapon', 'medium_weapon', 'large_weapon'].map(weaponSize => {
                return {
                    size: weaponSizeList().find(s => s.size === weaponSize),
                    weapons: commonWeaponList().filter(w => w.size.size === weaponSize)
                };
            });
        }

        // Detail display (when not choosing weapon)
        updatedData.displayDetails = weaponBaseSet;
        if(updatedData.displayDetails) {

            updatedData.weaponSpeed = alteredValue(this.weapon.speed, true);

            // Element display
            updatedData.changingElement = this._displayData.changingElement;

            updatedData.currentElementTitle = translate('AESYSTEM.item.weapon.sheet.element.current').replace( 'ELEMENT', this.weapon.element.name );
            updatedData.availableElements = elementList().filter(e => ['metal', 'earth', 'plant', 'animal'].includes(e.key) );
            
            // Damage display
            //--------------------
            updatedData.weaponDamage = [];
            const grid = updatedData.weaponDamage;
            // - First line : armor protections
            grid.push({
                value: translate('AESYSTEM.item.weapon.sheet.opponentArmor'), 
                css:'title'
            });
            const armorRange = this.weapon.damage[0].map( d => d.armorLevel );
            armorRange.forEach( armorLevel => {
                grid.push( {value: armorLevel, css:'header'} );
            })

            // - Other lines : One for each success level
            this.weapon.damage.forEach(successLine => {
                const level = successLine[0].successLevel;
                grid.push({
                    value: translate('AESYSTEM.item.weapon.sheet.successLevel.' + level), 
                    css:'title'
                });
                successLine.map(dmg => alteredValue(dmg)).forEach( dmg => {
                    grid.push( dmg );
                })
            });

            //Affix display
            updatedData.weaponAffixes = this.weapon.affixes.map(a => {
                const data = {
                    // Some affixes can be directly retrieved from actor. Those ones aren't upgradable
                    upgradable: updatedData.upgradable && !a.fromSpells 
                };
                return mergeObject( data, a);
            });

            updatedData.affixMenus = this.weapon.availableAffixes.reduce( (result, affix) => {
                const menu = affix.menu;
                const section = result.find( s => s.menu.key === menu.key );
                if( section ) {
                    section.affixes.push( affix );
                } else {
                    result.push({
                        menu: menu,
                        affixes: [affix]
                    });
                }
                return result;
            }, []);

            updatedData.affixMenus.sort( (a,b) => a.menu.displayOrder - b.menu.displayOrder);
            updatedData.affixMenus.forEach( section => {
                section.affixes.sort( (a,b) => a.name.localeCompare(b.name) );
            });

        }

    }

    /** @override */
    activateListeners(html) {

        // Everything below here is only needed if the sheet is editable
        if (!this.sheet.options.editable) return;

        html.find(".choose-weapon-panel").on("click", ".weapon-name",  this._onClickSelectBaseWeapon.bind(this));
        html.find(".current-weapon-base").on("click", ".current-weapon-delete",  this._onClickDropBaseWeapon.bind(this));
        html.find(".weapon-base-data").on("click", ".new-elem",  this._onClickChooseNewElement.bind(this));
        html.find(".weapon-base-data").on("click", ".change-elem",  this._onClickToggleElementChange.bind(this));
        html.find(".weapon-affix-list").on("click", ".affix-delete",  this._onClickDropAffix.bind(this));
        html.find(".new-affixes").on("click", ".affix-add",  this._onClickAddAffix.bind(this));
    }

    async _onClickSelectBaseWeapon(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const ref = a.parentElement.parentElement.dataset.ref;

        const commonWeapon = commonWeaponList().find(w => w.weapon === ref);
        if( !commonWeapon ) { return ui.notifications.warn(`Can't find common weapon named ${ref}`); }

        await this.weapon.setBaseWeapon(ref);
        this.sheet.render();
    }

    async _onClickDropBaseWeapon(event) {
        event.preventDefault();

        await this.weapon.setBaseWeapon(null);
        this.sheet.render();
    }

    async _onClickChooseNewElement(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const elementKey = a.dataset.elem;

        this._displayData.changingElement = false;
        await this.weapon.changeElement(elementKey);
        this.sheet.render();
    }

    async _onClickToggleElementChange(event) {
        event.preventDefault();
        this._displayData.changingElement = true;
        this.sheet.render();
    }

    async _onClickDropAffix(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const ref = a.parentElement.dataset.ref;

        this.weapon.clearCache();
        await this.weapon.removeAffix(ref);
        this.sheet.render();
    }

    async _onClickAddAffix(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const ref = a.parentElement.dataset.ref;

        this.weapon.clearCache();
        await this.weapon.addAffix(ref);
        this.sheet.render();
    }

}
