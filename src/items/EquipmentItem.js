import { affixList } from '../tools/WaIntegration.js';

const comparisonName = (eqt) => {
    let name = eqt.isEquipped ? '0' : '1';
    name += eqt.slot.sortValue;
    name += eqt.item.name;
    return name;
}

export class AEEquipmentItem {

    constructor(item) {
        this._item = item;
    }

    compare(otherEqtItem) {
        return comparisonName(this).localeCompare( comparisonName(otherEqtItem) );
    }

    get item() {
        return this._item;
    }

    get itemData() {
        return this._item.system;
    }

    get description() {
        return this.itemData.description ?? '';
    }

    get affixes() {

        // Add potential affixes due to spell effects (Not inside cache, since handled outside item)
        const owner = this.item.actor;
        const spellsOnEquipment = owner?.system.spellsOnEquipment;
        const allAffixes = duplicate(this.baseAffixes);
        if( spellsOnEquipment ) {
            const keys = spellsOnEquipment[this.slot.key] ?? [];
            keys.forEach( affixKey => {
                if( allAffixes.find( a => a.key === affixKey ) ) { return; }

                const affix = affixList().find(a => a.key === affixKey);
                if( !affix ) { console.warn(`No affix named ${affixKey}`); }

                const spellAffix = duplicate(affix);
                spellAffix.fromSpells = true;
                allAffixes.push( spellAffix );
            });
        }
        return allAffixes;
    }

    /** Filter affixes which resulted from spell effects */
    get baseAffixes() {
        if( ! this._baseAffixes ) {
            const base = this.itemData.affixes ?? [];
            this._baseAffixes = base.map(itemAffix => {
                const affix = affixList().find(a => a.key === itemAffix);
                if( !affix ) { console.warn(`No affix named ${itemAffix}`); }
                return affix ?? null;
            }).filter( a => {
                return a != null;
            });
        }
        return this._baseAffixes;
    }

    get isEquipped() {
        return this.itemData.equipped ?? false;
    }

    get canBeEquipped() {
        return true;
    }

    /** Should be overriden */
    get slot() {}

    clearCache() {
        this._baseAffixes = null;
    }
    
    async _equip(toEquip = true) {
        this.clearCache(); // Some data depends on actor current buffs. When equipping / unequipping => reset those data
        const updateData = {};
        updateData['system.equipped'] = toEquip;
        await this.item.update(updateData);
    }

    async addAffix(affix) {
        const newAffixes = duplicate(this.itemData.affixes);
        if( newAffixes.includes(affix) ) { throw `Item ${this.item.name} already has the affix ${affix}`; }

        const affixDef = affixList().find(a => a.key === affix);
        if( !affixDef ) { throw `No affix named ${affix}`; }

        const updateData = {};
        newAffixes.push(affix);
        updateData['system.affixes'] = newAffixes;
        await this.item.update(updateData);
    }

    async removeAffix(affix) {
        const newAffixes = this.itemData.affixes.filter(a => a != affix);
        const updateData = {};
        updateData['system.affixes'] = newAffixes;
        await this.item.update(updateData);
    }

    async showOnChat() {
        const html = await renderTemplate('systems/acariaempire/resources/items/sheet/show-on-chat.hbs', {item: this.item, eqt: this});
        return this.item.actor.sendMessage({ 
            flags: {
                acariaempire: {
                    item: {
                        eqtId: this.item.id,
                        tokenId: this.item.actor.tokenId,
                        actorId: this.item.actor.id
                    }
                }
            },
            content: html 
        }, {});
    }
}