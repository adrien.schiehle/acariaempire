import { ALL_ITEMS_TYPES } from '../tools/constants.js';
import { AEArmorItem } from './ArmorItem.js';
import { AEWeaponItem } from './WeaponItem.js';
import { AEMiscellaneousItem } from './MiscellaneousItem.js';

export class AEItem extends Item {

    constructor(...args) {
        super(...args);
    }

    get isWeapon() {
        return this.type === ALL_ITEMS_TYPES.weapon;
    }

    asWeapon() {
        return new AEWeaponItem(this);
    }

    get isArmor() {
        return this.type === ALL_ITEMS_TYPES.armor;
    }

    asArmor() {
        return new AEArmorItem(this);
    }

    get isMisc() {
        return this.type === ALL_ITEMS_TYPES.misc;
    }

    asMisc() {
        return new AEMiscellaneousItem(this);
    }

    get isEquipment() {
        const equipmentTypes = [ALL_ITEMS_TYPES.weapon, ALL_ITEMS_TYPES.armor, ALL_ITEMS_TYPES.misc];
        return equipmentTypes.includes(this.type);
    }

    asEquipement() {
        if( this.isWeapon ) { return this.asWeapon(); }
        if( this.isArmor ) { return this.asArmor(); }
        return this.asMisc();
    }

}
