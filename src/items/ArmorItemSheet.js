import { armorAffixList, armorProtectionLevels, armorSlotList, attributeList, commonArmorList, elementList } from '../tools/WaIntegration.js';
import { AEItemSheetBaseImplem } from './ItemSheetBaseImplem.js';

const CSS_VALUE_BUFFED = 'buffed-value';
const CSS_VALUE_DEBUFFED = 'debuffed-value';


const alteredValue = (base, smallIsBetter=false) => {
    let diff = base.value - base.initialValue;
    if( smallIsBetter ) { diff = -1 * diff; }

    return {
        value: base.value,
        css: diff < 0 ? CSS_VALUE_DEBUFFED : ( diff > 0 ? CSS_VALUE_BUFFED : '')
    };
}

const mapAvailableProtectionLevelForSlot = (armorSlot, openedSections) => {
    
    const commonArmors = commonArmorList().filter(a => (a.slot.key === armorSlot.key) );
    const protectionLevels = armorProtectionLevels().filter( p => {
        return commonArmors.find( a => a.protectionLevel?.key === p.key );
    });
    return protectionLevels.map( p => {
        const sectionId = armorSlot.key + '.' + p.key;
        const data = {
            sectionId: sectionId,
            armors: commonArmors.filter( a => a.protectionLevel.key === p.key ),
            showContent: openedSections.includes(sectionId)
        };
        data.css = data.showContent ? 'opened' : '';
        
        return mergeObject( data, p );
    });
}

/**
 * Item sheet data for Power items
 * @extends {AEItemSheetBaseImplem}
 */
 export class AEArmorItemSheet extends AEItemSheetBaseImplem {

    constructor(itemSheet) {
        super(itemSheet);

        this._armor = this.item.asArmor();
        this.sheet.options.classes.push('armor');
        this.sheet.options.template = "systems/acariaempire/resources/items/sheet/armor-sheet.hbs";
        this.sheet.options.width = 400;
        this.sheet.options.resizable = false;

        this._displayData = {
            chooseArmorOpenedSections: []
        };
    }

    get armor() {
        return this._armor;
    }

    /** @override */
    fillData(updatedData) {

        updatedData.armor = this.armor;
        updatedData.upgradable = game.user.isGM && !!this.armor.item.id;

        const armorBaseSet = this.armor.basedOn != null;
        updatedData.chooseBaseArmor = updatedData.upgradable && ! armorBaseSet;
        if(updatedData.chooseBaseArmor) {

            const openedSections = this._displayData.chooseArmorOpenedSections;
            updatedData.allSlots = armorSlotList().filter( s => {
                return s.key != 'unset';
            }).map(slot => {
                const sectionId = slot.key;
                const data = {
                    sectionId: sectionId,
                    sublevel: {
                        needed: slot.allowProtection ? true : false
                    },
                    showContent: openedSections.includes(sectionId)
                };
                if( data.sublevel.needed ) {
                    data.sublevel.protections = mapAvailableProtectionLevelForSlot(slot, openedSections);
                } else {
                    data.armors = commonArmorList().filter( a => a.slot.key === slot.key );
                }
                data.css = data.showContent ? 'opened' : '';
                return mergeObject(data, slot);
            });
        }

        updatedData.displayDetails = armorBaseSet;
        if(updatedData.displayDetails) {

            updatedData.details = {};
            const details = updatedData.details;

            // Base protection level
            details.protection = {
                displayed: this.armor.slot.allowProtection ? true : false,
                definition: this.armor.basedOn?.protectionLevel
            };
            
            // Limiter display
            const limiter = this.armor.limiter;
            details.limiter = {
                displayed: limiter.apply
            };
            if( details.limiter.displayed ) {
                if( !limiter.apply ) {
                    details.limiter.text= '-';
                } else {
                    const attribute = attributeList().find( a => a.key === limiter.attribute );
                    details.limiter.text= attribute?.name + ' ≥ ' + limiter.step;
                }
            }
            
            // Loop on elements for displaying protection level
            updatedData.protectionLevels = this.armor.protections.map( p => {
                return {
                    on: elementList().find(e => e.element === p.element)?.icon,
                    protection: alteredValue(p)
                };
            });

            // Loop on elements for displaying elemental resistances
            updatedData.elementResistances = this.armor.elementResistances.map( r => {
                return {
                    on: elementList().find(e => e.element === r.element)?.icon,
                    resistance: r.value
                };
            });



            //Affix display
            updatedData.armorAffixes = this.armor.affixes.map(a => {
                return mergeObject( {upgradable: updatedData.upgradable }, a);
            });

            updatedData.affixMenus = this.armor.availableAffixes.reduce( (result, affix) => {
                const menu = affix.menu;
                const section = result.find( s => s.menu.key === menu.key );
                if( section ) {
                    section.affixes.push( affix );
                } else {
                    result.push({
                        menu: menu,
                        affixes: [affix]
                    });
                }
                return result;
            }, []);

            updatedData.affixMenus.sort( (a,b) => a.menu.displayOrder - b.menu.displayOrder);
            updatedData.affixMenus.forEach( section => {
                section.affixes.sort( (a,b) => a.name.localeCompare(b.name) );
            });

        }

    }

    /** @override */
    activateListeners(html) {

        // Everything below here is only needed if the sheet is editable
        if (!this.sheet.options.editable) return;

        html.find(".choosing-armor").on("click", ".choose-menu",  this._onClickToggleChoosingMenu.bind(this));

        html.find(".choose-armor-panel").on("click", ".armor-name",  this._onClickSelectBaseArmor.bind(this));
        html.find(".current-armor-base").on("click", ".current-armor-delete",  this._onClickDropBaseArmor.bind(this));
        html.find(".armor-affix-list").on("click", ".affix-delete",  this._onClickDropAffix.bind(this));
        html.find(".new-affixes").on("click", ".affix-add",  this._onClickAddAffix.bind(this));
    }

    async _onClickToggleChoosingMenu(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const sectionId = a.dataset.section;

        const openedSections = this._displayData.chooseArmorOpenedSections;
        const index = openedSections.indexOf(sectionId);
        if( index != -1 ) {
            openedSections.splice(index, 1);
        } else {
            openedSections.push(sectionId);
        }
        this.sheet.render();
    }

    async _onClickSelectBaseArmor(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const ref = a.parentElement.parentElement.dataset.ref;

        const commonArmor = commonArmorList().find(a => a.key === ref);
        if( !commonArmor ) { return ui.notifications.warn(`Can't find common armor named ${ref}`); }

        await this.armor.setBaseArmor(ref);
        this.sheet.render();
    }

    async _onClickDropBaseArmor(event) {
        event.preventDefault();

        await this.armor.setBaseArmor(null);
        this.sheet.render();
    }

    async _onClickDropAffix(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const ref = a.parentElement.dataset.ref;

        this.armor.clearCache();
        await this.armor.removeAffix(ref);
        this.sheet.render();
    }

    async _onClickAddAffix(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const ref = a.parentElement.dataset.ref;

        this.armor.clearCache();
        await this.armor.addAffix(ref);
        this.sheet.render();
    }

}
