import { armorAffixList, armorMaxRarity, armorProtectionLevels, armorRarityList, armorSlotList, commonArmorList } from '../tools/WaIntegration.js';
import { AEEquipmentItem } from './EquipmentItem.js';
import { ALL_ITEMS_TYPES } from '../tools/constants.js';

const checkIntegrity = (item) => {

    let isOk = true;
    if(item.type != ALL_ITEMS_TYPES.armor) {
        isOk = false;
        console.error(item.name + ' is not an armor. (' + item.type + ');');

    }
    
    if(! isOk ) {
        throw 'Invalid armor item';
    }
}

export class AEArmorItem extends AEEquipmentItem{

    constructor(item) {
        super(item);
        checkIntegrity(this._item);
    }

    get basedOn() {
        
        if( !this._basedOn && this.itemData.basedOn ) {
            this._basedOn = commonArmorList().find(a => a.key === this.itemData.basedOn) ?? null;
            if(!this._basedOn) { console.error(`Can't find common armor: ${this.itemData.basedOn}`); }
        }
        
        return this._basedOn;
    }

    /** @override */
    get slot() {
        if( !this.basedOn ) {
            return armorSlotList().find( s => s.key === 'unset');
        }
        return this.basedOn.slot;
    }

    get protectionLevel() {
        if( !this.basedOn ) {
            return armorProtectionLevels().find( s => s.key === 'none');
        }
        return this.basedOn.protectionLevel;
    }

    get limiter() {

        if( !this._limiter ) {
            const protectionLevel = this.basedOn?.protectionLevel?.value ?? 0;
            const hasLimiter = !!this.slot.allowProtection?.limiter;

            if( hasLimiter ) {
                // Affix managment: armorUpgrade.cost, armorUpgrade.limiter
                let limiterAttribute = this.slot.allowProtection?.limiter ?? 0;
                let limiterCost = protectionLevel;
                this.baseAffixes.forEach( a => {
                    const upgrade = a.armorUpgrade;
                    limiterCost += upgrade?.cost ?? 0;
                    if( !limiterAttribute && upgrade.limiter ) {
                        limiterAttribute = upgrade.limiter;
                    }
                });
                if( limiterCost != 0 ) {
                    this._limiter = { 
                        apply: true,
                        attribute: limiterAttribute,
                        step: limiterCost - 1 // Light protection need 0 on attribute
                    };
                }
            }

            // Still not defined
            if( !this._limiter ) {
                this._limiter = { apply: false };
            }
        }
        return this._limiter;
    }

    get givesProtection() {
        const sum = this.protections.reduce( (calculus, current) => {
            return calculus + current.value;
        }, 0);
        return sum != 0;
    }

    get protections() {

        if( !this._protections ) {

            if( ! this.slot.allowProtection ) {
                this._protections = [];

            } else {
                const against = this.slot.allowProtection?.against ?? [];
                this._protections = against.map( element => {
                    return {
                        element: element, 
                        initialValue: this.basedOn.protectionLevel.value,
                        value: this.basedOn.protectionLevel.value
                    };
                });
        
                // Affix managment: armorUpgrade.elementalArmor
                // Each element get a +2 armor bonus.
                // In exchange, it will make the equipment as heavy as on one of a upper one size
                this.affixes.filter( a => {
                    return a.armorUpgrade?.elementalArmor;
                 }).forEach( a => {
                    const elementalArmor = a.armorUpgrade.elementalArmor;
                    elementalArmor.forEach( e => {
                        const protect = this._protections.find(p => p.element === e );
                        if( protect ) { protect.value += 2; }
                    });
                });
            }
        }
        return this._protections;
    }

    get givesElementalResistances() {
        const sum = this.elementResistances.reduce( (calculus, current) => {
            return calculus + current.value;
        }, 0);
        return sum != 0;
    }

    get elementResistances() {

        if( !this._elementResistances ) {

            this._elementResistances = [];
        
            // Affix managment: armorUpgrade.elementalResist
            // Each element get a +1 resist bonus.
            // In exchange, it will make the equipment as heavy as on one of a upper one size
            this.affixes.filter( a => {
                return a.armorUpgrade?.elementalResist;
            }).forEach( a => {
                const elementalResist = a.armorUpgrade.elementalResist;
                elementalResist.forEach( e => {
                    const current = this._elementResistances.find( r => r.element === e );
                    if( current ) { current.value++; }
                    else {
                        this._elementResistances.push({
                            element: e,
                            value: 1
                        });
                    }
                });
            });
        }
        return this._elementResistances;
    }

    get givesCombatModifiers() {
        return this.combatModifiers.length > 0;
    }

    get combatModifiers() {

        if( !this._combatModifiers ) {

            // Affix managment: armorUpgrade.combat
            // Gives advantage and mastery modifiers during combat
            const modifiers = this.affixes.filter( a => {
                return a.armorUpgrade?.combat;
            }).map( a => {
                return a.armorUpgrade.combat;
            }).reduce( (myArray, current) => {
                current.types.forEach(type => {
                    const existing = myArray.find( m => m.on === current.on && m.type === type );
                    if( existing ) {
                        existing.advantage += current.advantage;
                        existing.mastery += current.mastery;
                    } else {
                        myArray.push({
                            on: current.on, 
                            type: type,
                            advantage: current.advantage, 
                            mastery: current.mastery
                        });
                    }
                });
                return myArray;
            }, []);

            this._combatModifiers = modifiers.filter( m => {
                return m.advantage || m.mastery;
            });
        }
        return this._combatModifiers;
    }

    /** @override */
    get rarity() {
        if(!this._rarity) {
            const count = this.baseAffixes.reduce( (result, affix) => {
                return Math.min( result + (affix.armorUpgrade?.cost ?? 0), armorMaxRarity() );
            }, 0);
            const cost = Math.max( count, 0 );
            this._rarity = armorRarityList().find( r => r.affixCost == cost );
        }
        return this._rarity;
    }

    /** @override */
    get cost() {
        const basePrice = this.protectionLevel?.basePrice ?? armorProtectionLevels().find( s => s.key === 'none').basePrice;
        return basePrice + this.rarity.additionalCost;
    }

    /** @override */
    clearCache() {
        super.clearCache();
        this._basedOn = null;
        this._limiter = null;
        this._protections = null;
        this._elementResistances = null;
        this._combatModifiers = null;
        this._rarity = null;
    }

    /** Which other affixes the armor can acquire */
    get availableAffixes() {

        const armorAffixNames = this.baseAffixes.map(a => a.key);
        return armorAffixList().filter(a => {
            if( armorAffixNames.includes(a.key) ) {
                return false;
            }
            // Filter on slot
            const slots = a.armorUpgrade.slots;
            if( slots && !slots.includes(this.slot.key) ) {
                return false;
            }
            // If armor alreay has a limiter, only allow affixes with same attribute
            const affixAttr = a.armorUpgrade.limiter;
            if( affixAttr && this.limiter.apply ) {
                return affixAttr === this.limiter.attribute;
            }
            return true;
        });
    }

    
    /**
     * Change current armor base and update cached data
     * @param {String} baseArmorRef : If null, will unset base armor
     */
     async setBaseArmor(baseArmorRef) {

        this.clearCache();
        const armor = commonArmorList().find(a => a.key === baseArmorRef) ?? null;

        const updateData = {};
        updateData['system.basedOn'] = baseArmorRef;
        updateData['system.affixes'] = armor?.affixes?.map( a => a.key ) ?? [];
        
        updateData['img'] = armor?.icon ?? this.item.img;

        await this.item.update(updateData);
    }
}