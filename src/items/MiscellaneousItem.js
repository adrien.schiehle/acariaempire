import { commonMiscList, miscAffixList, miscMaxRarity, miscRarityList, miscSlotList } from '../tools/WaIntegration.js';
import { AEEquipmentItem } from './EquipmentItem.js';
import { ALL_ITEMS_TYPES } from '../tools/constants.js';

const checkIntegrity = (item) => {

    let isOk = true;
    if(item.type != ALL_ITEMS_TYPES.misc) {
        isOk = false;
        console.error(item.name + ' is not a miscellaneous item. (' + item.type + ');');
    }
    
    if(! isOk ) {
        throw 'Invalid miscellaneous item';
    }
}

export class AEMiscellaneousItem extends AEEquipmentItem{

    constructor(item) {
        super(item);
        checkIntegrity(this._item);
    }

    get basedOn() {
        
        if( !this._basedOn && this.itemData.basedOn ) {
            this._basedOn = commonMiscList().find(m => m.key === this.itemData.basedOn) ?? null;
            if(!this._basedOn) { console.error(`Can't find common misc: ${this.itemData.basedOn}`); }
        }
        
        return this._basedOn;
    }

    /** @override */
    get rarity() {
        if(!this._rarity) {
            const count = this.baseAffixes.reduce( (result, affix) => {
                return Math.min( result + (affix.miscUpgrade?.cost ?? 0), miscMaxRarity() );
            }, 0);
            this._rarity = miscRarityList().find( r => r.affixCost == count );
        }
        return this._rarity;
    }

    /** @override */
    get cost() {
        const basePrice = this.basedOn?.basePrice ?? 0;
        return basePrice + this.rarity.additionalCost;
    }

    /** @override */
    get slot() {
        if( !this.basedOn ) {
            return miscSlotList().find( s => s.key === 'unset');
        }
        return this.basedOn.slot;        
    }

    /** @override */
    get canBeEquipped() {
        return false;
    }

    /** @override */
    clearCache() {
        super.clearCache();
        this._basedOn = null;
        this._rarity = null;
    }

    /** Which other affixes the item can acquire */
    get availableAffixes() {

        const miscAffixNames = this.baseAffixes.map(a => a.key);
        return miscAffixList().filter(a => {
            if( miscAffixNames.includes(a.key) ) { return false; }

            // Filter on slot
            const slots = a.miscUpgrade?.slots ?? [];
            if( !slots.includes(this.slot.key) ) {
                return false;
            }
            return true;
        });
    }    

    /**
     * Change current misc base and update cached data
     * @param {String} baseMiscRef : If null, will unset base misc
     */
     async setBaseMisc(baseMiscRef) {

        this.clearCache();
        const base = commonMiscList().find(m => m.key === baseMiscRef) ?? null;

        const updateData = {};
        updateData['system.basedOn'] = baseMiscRef;
        updateData['system.affixes'] = base?.affixes?.map( a => a.key ) ?? [];
        
        updateData['img'] = base?.icon ?? this.item.img;

        await this.item.update(updateData);
    }

}