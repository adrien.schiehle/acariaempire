/**
 * Base implementation wich will be extended by all ItemSheetImplem
 */
 export class AEItemSheetBaseImplem {

    /** @param {AEItemSheet} itemSheet : sheet delegating methods */
    constructor(itemSheet) {

        this._sheet = itemSheet;
    }

    get sheet() {
        return this._sheet;
    }

    get item() {
        return this._sheet.item;
    }

    /** May be overriden */
    get customTitle() { return null; }

    /** Complete data before rendering. Called by sheet.getData(). */
    fillData(data) {}

    /**
     * Complete header buttons.
     * buttons are an array of {label:, class:, icon:, onclick:}
     * Called by sheet._getHeaderButtons
     */
    fillHeaderButtons(buttons) {}

    /** 
     * Complete listeners.
     * Only generateWALinks() is done by th sheet prior to the delegation.
     * */
    activateListeners(html) {}

}
