import { onClickDisplayTargetsPanel } from './TargetsPopup.js';
import { onClickDisplaySpellShape } from './SpellShapePopup.js';

const modifyHtmlForAERollResult = (html) => {

    // ToggleDiv management
    const oldMessages = ! game.aesystem;
    toggleDivForAERollResult(html, {toggleConsequences: !oldMessages});

    const onClickToggleRolls = async (event) => { 
        event.preventDefault();
        toggleDivForAERollResult(html, {toggleRolls: true}); 
    };
    const onClickToggleConsequences = async (event) => { 
        event.preventDefault();
        toggleDivForAERollResult(html, {toggleConsequences: true}); 
    };
    
    const onClickToggleContextDisplay = async (event) => { 
        event.preventDefault();
        toggleDivForAERollResult(html, {toggleContext: true}); 
    };

    html.find('.roll-summary').click(event => onClickToggleRolls(event));
    html.find('.ae-formula').click(event => onClickToggleConsequences(event));
    html.find('.roll-message-description').click(event => onClickToggleContextDisplay(event));


    // Clickable actions
    const onClickDisplayWaLink = async (event) => { 
        event.preventDefault();
        const waHelpers = game.modules.get("world-anvil").helpers;
        waHelpers.displayWALink(event);
    };

    html.find('.consequence.wa-frame').click(event => onClickDisplayWaLink(event));
    html.find('.consequence.shape-display').click(event => onClickDisplaySpellShape(event));
}

const modifyHtmlForAERollResultGMOnly = (message, html) => {
    
    if( !game.user.isGM ) { return; }

    const flags = message.flags;
    const rollFlags = flags.acariaempire.aeroll;

    // Damage management
    const dmgLines = html.find(".consequence.gm-replace");
    for( let index = 0; index < dmgLines.length; index++ ) {
		const element = dmgLines[index];
		element.outerHTML = element.outerHTML.replace('gm-replace', 'clickable target-on-click');
	}

    html.find('.consequence.target-on-click').click(event => onClickDisplayTargetsPanel(event, rollFlags));
}

/**
 * Roll messages can have a flag 'hideDefenseScore'
 * Players won't see defenseScore for those messages
 * @param {*} message The whole message, with flags
 * @param {*} html The related html which can be modified
 */
const modifyHtmlForAERollHideDefenseScore = (message, html) => {
    
    // GM always see the whole details
    if( game.user.isGM ) { return; }

    const flags = message.flags;
    const hideDefenseScore = flags?.acariaempire.aeroll.hideDefenseScore ?? false;
    if( !hideDefenseScore ) { return; }

    const elements = html.find('.canBeHidden');
	for( let index = 0; index < elements.length; index++ ) {
		const elem = elements[index];
        elem.innerHTML = '?';
	}
}


const toggleDivForAERollResult = (html, {toggleRolls=false, toggleConsequences=false, toggleContext=false} = {}) => {
    
    const rollDiv = html.find('.roll-detail')[0];
    const displayRolls = toggleRolls ? rollDiv.style.display == 'none' : false;
    rollDiv.style.display = displayRolls ? ''  : 'none';

    const consequencesDiv = html.find('.roll-consequences')[0];
    const displayConsequences = toggleConsequences ? consequencesDiv.style.display == 'none' : false;
    consequencesDiv.style.display = displayConsequences ? ''  : 'none';

    const contextDiv = html.find('.roll-context')[0];
    const displayContext = toggleContext ? contextDiv.style.display == 'none' : false;
    contextDiv.style.display = displayContext ? ''  : 'none';
}

const modifyHtmlForAEItemDisplay = (message, html) => {
    // Allow clic on item
    const flags = message.flags;
    const eqtId = flags.acariaempire.item.eqtId;
    const tokenId = flags.acariaempire.item.tokenId;
    const actorId = flags.acariaempire.item.actorId;

    const onClickShowItem = async (event) => {
        const tools = game.aesystem.actorTools;
        const actor = tools.getActorOrTokenActor({
            tokenId: tokenId, 
            actorId: actorId, 
            allowCurrentSelection: false, 
            allowActorWithoutToken: true
        });

        const eqt = actor?.equipment.all.find(e => e.item.id === eqtId);
        if( eqt ) {

            event.preventDefault();
            eqt.item.sheet.render(true);
        };
    }

    html.find("#ae-item-on-chat .show-item").click(event => onClickShowItem(event));
}

/**
 * For managing message display
 * Skip classic roll events
 */
 export class AEChatMessage extends ChatMessage {

    constructor(...args) {
        super(...args);
    }

    get isInitiativeRoll() {
        const flags = this.flags;
        return flags.core && flags.core.initiativeRoll;
    }

    get isAERollResult() {
        const flags = this.flags;
        return flags.acariaempire?.aeroll;
    }

    get isAEItemDisplay() {
        const flags = this.flags;
        return flags.acariaempire?.item && flags.acariaempire.item.eqtId;
    }

    /**
     * Skip some messages that should not be rendered
     * @override
     */
    async getHTML() {

        if( this.isInitiativeRoll ) { 
            return; // There is no roll for initiative. => Hide those fake rolls
        }

        const html = await super.getHTML();

        if( this.isAERollResult ) { 
            modifyHtmlForAERollResult(html);
            modifyHtmlForAERollResultGMOnly(this, html);
            modifyHtmlForAERollHideDefenseScore(this, html);
        }

        if( this.isAEItemDisplay ) {
            modifyHtmlForAEItemDisplay(this, html);
        }

        // Add wa integration
        // - Can't use substitute here since old message will be displayed before shortcut initialization
        const waHelpers = game.modules.get("world-anvil").helpers;
        ['.wa-link', '.wa-tooltip'].forEach(cssPath => {
            html.find(cssPath).click(event => waHelpers.displayWALink(event));
        });

        return html;
    }


 }