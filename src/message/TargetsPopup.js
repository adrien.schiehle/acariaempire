import { SPELLEFFECT_STACK_TYPE } from '../tools/constants.js';
import {translate} from '../tools/Translator.js';
import { generateWALinks } from '../tools/WaIntegration.js';

const CURRENT_TARGET_ICON = 'systems/acariaempire/resources/message/current-targets.webp'

const POPUP_TYPES = [
    { type: 'damage', 
      flagKey: 'damageDealt', 
      createImpl: (parent) => new AETargetsDamageImpl(parent) },

    { type: 'heal', 
      flagKey: 'healDone', 
      createImpl: (parent) => new AETargetsHealImpl(parent) },

    { type: 'activeEffect', 
      flagKey: 'activeEffects', 
      createImpl: (parent) => new AETargetsActiveEffectsImpl(parent) }
];

export const onClickDisplayTargetsPanel = async (event, flags) => {
    event.preventDefault();
    const a = event.currentTarget;
    const tokenId = a.dataset.targetToken != '' ? a.dataset.targetToken : null;
    const type = a.dataset.type;

    const popupType = POPUP_TYPES.find( p => p.type === type );
    const details = flags[popupType.flagKey] ?? [];

    const popup = new AETargetsPopup();
    popup.init( popupType, details, tokenId );
    popup.render(true);
}

class AETargetsPopup extends Application {

    /** @override */
	static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
              classes: ["acariaempire", "targets-panel"],
              template: "systems/acariaempire/resources/message/targets-panel.hbs",
              width: 'auto',
              height: 'auto'
        });
    }

    constructor(options = {}) {
        super(options);
        this.doneTargets = [];
    }

    init( popupType, msgDetails, currentTokenId ) {

        const type = popupType.type;
        this.labels = {
            title : translate('AESYSTEM.targets.sheet.' + type + '.title'),
            done : translate('AESYSTEM.targets.sheet.' + type + '.done'),
            base : translate('AESYSTEM.targets.sheet.' + type + '.base'),
            detail : translate('AESYSTEM.targets.sheet.' + type + '.detail'),
            noTarget : translate('AESYSTEM.targets.sheet.all.noTarget'),
            currentTarget : translate('AESYSTEM.targets.sheet.all.currentTarget')
        };

        this.impl = popupType.createImpl(this);

        // All lines
        this.details = msgDetails.map( (detail) => {
            const data = {
                displayedName: detail.target.tokenId ? detail.target.name : this.labels.currentTarget,
                displayedPortrait: detail.target.tokenId ? detail.target.icon : CURRENT_TARGET_ICON
            };
            return mergeObject( data, detail );
        });
        this.details.sort( (a,b) => {
            const aLabel = a.target.tokenId ? a.displayedName : '';
            const bLabel = b.target.tokenId ? b.displayedName : '';
            return aLabel.localeCompare(bLabel);
        });

        // Default data
        this.defaultData = this.details.find( d => d => ! d.target.tokenId );
        this.defaultData.initialValue = this.defaultData.amount;

        // Sometimes, 'current target' should not be available in GUI 
        // (Like for invocation spells which are only targeting the caster)
        if( this.defaultData.restrictTargetSelection ) {
            this.details = this.details.filter( d => d.target.tokenId );
        }

        this.refreshDisplayedTexts();
        this.selectToken(currentTokenId);
    }

    refreshDisplayedTexts() {
        this.details.forEach( detail => {
            detail.displayedText = this.impl.displayedAmountForToken(detail.target.tokenId);
        });
    }

    /**
     * Refest .selected and .css subdata
     * @param {string} tokenId The tokenId we want to select. May be null 
     */
    selectToken(tokenId) {

        // Refresh details.selected
        const currentIndex = this.details.findIndex( d => {
            if( !tokenId ) { return !d.target.tokenId; }
            return d.target.tokenId === tokenId; 
        });

        this.details.forEach( (detail, index) => {
            const selected = index == currentIndex;
            const tokenId = detail.target.tokenId;
            const alreadyDone = tokenId ? this.doneTargets.includes(tokenId) : false;

            detail.selected = selected;
            detail.css = selected ? 'selected' : '';
            detail.buttonCss = alreadyDone ? 'done' : '';
        });

        // Refresh currentTargets
        const initialTargetSelected = tokenId && currentIndex != -1;
        this.currentTargets= this.impl.deduceCurrentTargets(initialTargetSelected);
    }

    async centerCanavasOnFirstTarget() {
        if( this.currentTargets.length == 0 ) {
            return ui.notifications.info(this.labels.noTarget); // Can't recenter canvas since there is no target
        }

        const target = this.currentTargets[0].actor;
        const tokens = target.token ? [target.token] : target.getActiveTokens();
        if( tokens.length == 0 ) {
            return ui.notifications.info(this.labels.noTarget); // This player actor has no active tokens. Can't recenter
        }
        return canvas.animatePan({x: tokens[0].data.x, y: tokens[0].data.y});
    }


    /** @override */
    get title() {
        return this.labels.title;
    }

    /** @override */
    getData() {

        const baseDiff = this.defaultData.amount - this.defaultData.initialValue;
        const guiBase = {
            amountCss: baseDiff ? 'red' : '',
            displayedTitle : this.labels.base,
            diff: baseDiff,
            displayBtn: this.impl.canChangeBaseValue
        };
        mergeObject(guiBase, this.defaultData);


        return {
            base: guiBase,
            details: this.details
        };
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);
        generateWALinks(html);

        html.find('.base .line-action.add').click(event => this._onClickModifyBaseAmount(event, 1));
        html.find('.base .line-action.remove').click(event => this._onClickModifyBaseAmount(event, -1));
        html.find('.target .line-icon').click(event => this._onClickSelectTarget(event));
        html.find('.target .line-action').click(event => this._onClickInflictDamage(event));
    }

    /**
     * When closing window, make sure to remove a stack from the attacker item buff if it is present
     * @param {*} options 
     */
    async close(options={}) {
        super.close(options);
        await this._whenClosingRemoveAttackStack();
    }

    async _whenClosingRemoveAttackStack() {
        if( !this.defaultData.weaponAttack ) { return; }

        const tools = game.aesystem.actorTools;
        const attacker = tools.getActorOrTokenActor({
            tokenId: this.defaultData.weaponAttack.tokenId, 
            allowCurrentSelection: false
        });
        if( !attacker ) { 
            console.log('AE-TargetsPopup | Weapon attacker not found. No need to remove attack stack from its weapon.');
            return; 
        }

        // Only trigger dialog if the attack has a weapon buff
        const spellEffectWithStacks = attacker.spellEffects.filter( se => se.stacks.type === SPELLEFFECT_STACK_TYPE.attack);
        if( spellEffectWithStacks.length == 0 ) {
            return;
        }

        // Build dialog content
        console.log('AE-TargetsPopup | Dialog asking if removing weapon stack should be done');
        const title = translate('AESYSTEM.targets.sheet.closing.weaponStack.title').replace('ACTOR_NAME', attacker.name)
        const contentStart = translate('AESYSTEM.targets.sheet.closing.weaponStack.firstLine') + '\n<ul>';
        const content = spellEffectWithStacks.reduce( (html, se) => {

            const spellDesc = translate('AESYSTEM.targets.sheet.closing.weaponStack.spellDesc')
                                .replace('SPELL_NAME', se.spell.name)
                                .replace('AMOUNT', '' + se.stacks.amount);
            return html + '\n\t<li>' + spellDesc + '</li>';
        }, contentStart) + '</ul>';

        await Dialog.confirm({
            title: title,
            content: content,
            yes: async () => {
                await attacker.consumeSpellEffectStack(SPELLEFFECT_STACK_TYPE.attack);
            }
        });
    }

    async _onClickSelectTarget(event) {
        event.preventDefault();
        const dataset = event.currentTarget.parentElement.dataset;
        const tokenId = dataset.tokenId != '' ? dataset.tokenId : null;

        this.selectToken(tokenId);
        await this.centerCanavasOnFirstTarget();
        this.render();
    }
    
    async _onClickInflictDamage(event) {
        event.preventDefault();
        const dataset = event.currentTarget.parentElement.parentElement.dataset;
        const tokenId = dataset.tokenId != '' ? dataset.tokenId : null;

        this.selectToken(tokenId);

        if( this.currentTargets.length == 0 ) {
            return ui.notifications.info(this.labels.noTarget);
        }

        for( const target of this.currentTargets ) {
            const amount = await this.impl.applyOnTarget(target);
            if( amount ) {
                const msg = this.labels.done.replace( 'ACTOR', target.actor.name ).replace( 'AMOUNT', amount );
                ui.notifications.info(msg);
            }
            
            // Store this target inside the ones who have been dealt
            const targetTokenId = target.actor.tokenId;
            if( ! this.doneTargets.includes(targetTokenId) ) {
                this.doneTargets.push( targetTokenId );
            }
        }

        // Refresh css values
        this.selectToken(tokenId);
        this.render();
    }

    async _onClickModifyBaseAmount(event, offset) {
        event.preventDefault();
        this.impl.modifiyBaseAmount(offset);
        this.refreshDisplayedTexts();
        this.render();
    }
    
}

class AETargetsBaseImpl {

    /**
     * Base implem which will be used 
     * @param {AETargetsPopup} parent main instance
     */
    constructor(parent) {
        this._parent = parent;
    }

    /**
     * If +1, -1 buttons are displayed
     */
    get canChangeBaseValue() {
        return true;
    }

    /**
     * Builds current target.
     * @param {boolean} initialTargetSelected Will depends if an initial targets is selected or not
     * @returns {object[]} array for this.currentTargets
     */
    deduceCurrentTargets(initialTargetSelected) {

        if( initialTargetSelected ) {
            return this._fromSelectedElement()
        }
        return this._fromPlayerInputs();

    }

    /**
     * The current selection in the GUI is a token.
     * Damage have already been calculated for this token during the roll.
     * Simply build currentTarget with filling data from this token
     * @returns {object[]} What will be stored in this.currentTargets. Contains info on actor and damage to deal
     */
    _fromSelectedElement() {
        const details = this._parent.details;
        const selection = details.find( d => d.selected );

        // One of the initial targets have been selected.
        const tools = game.aesystem.actorTools;
        const target = tools.getActorOrTokenActor({
            tokenId: selection.target.tokenId, 
            allowCurrentSelection: false
        });

        if( !target ) { return []; }

        const data = {
            actor: target,
            detail: this._calculateTargetDetails(target)
        };
        return [data];
    }

    /**
     * Build currentTargets from player selected targets.
     * Base impl does nothing. Should be overriden
     * @returns {object[]} array for this.currentTargets
     */
    _fromPlayerInputs() { 
        const targetTokens = Array.from(game.user.targets) ?? [];
        const allTargets =  targetTokens.map( t => t.actor ).filter( t => t ); // Remove null elements
        return allTargets.map( target => {
            return {
                actor: target,
                detail: this._calculateTargetDetails(target)
            };
        });
    }

     /**
     * Calculate the target.detail from actor
     * @param {AEActor} actor The targeted actor 
     * @returns The target.detail
     */
    _calculateTargetDetails(actor) { return {};}

    /**
     * Actually apply the action on target.
     * Base impl does nothing. Should be overriden
     * @param {object} target Contains {actor:xxx, detail:xxx }
     * @returns The resulted amount that was dealt/heal/given
     */
    async applyOnTarget ( target ) {
        return 1;
    }

    /**
     * Set how base amount can be modified
     * @param {int} offset 
     */
    modifiyBaseAmount(offset) {
        this._parent.defaultData.amount = Math.max( 0, this._parent.defaultData.amount + offset );
    }

    /**
     * What to dispay on GUI for each detail element. Should be overriden
     * @param {string} tokenId tokenId of the targeted actor. May be null
     * @returns {string} Will be displ
     */
    displayedAmountForToken(tokenId) { 
        return this._parent.labels.detail.replace( 'AMOUNT', '?' );
    }

}

class AETargetsDamageImpl extends AETargetsBaseImpl {

    /**
     * Can't change base value. damageLine has already been retrieved
     * @override
     */
     get canChangeBaseValue() {
        return false;
    } 

     /**
     * @override
     * @returns In the form of {amount: int, element: string}
     */
    _calculateTargetDetails(target) {
        
        const damageBase = this._parent.defaultData;
        const element = damageBase.element;
        const damageLine = damageBase.damageLine;

        // Calculate damage
		const protectionLevel = target.equipment.currentArmorProtection.find( p => p.element === element )?.value ?? 0;
        const inflictedDamage = damageLine.find( d => d.armorLevel === protectionLevel )?.value ?? 0;

        return {
            amount: inflictedDamage,
            element: element
        };
    }

    /**
     * Actually apply the action on target.
     * This one inflicts damage on target
     * @override
     * @param {object} target Contains {actor:xxx, detail:xxx }
     * @returns The resulted amount that was dealt
     */
    async applyOnTarget ( target ) {
        return target.actor.inflictDamage(target.detail.amount, {element: target.detail.element});
    }

    /**
     * Display inflicted damage (before damage reduction)
     * @override
     * @param {string} tokenId tokenId of the targeted actor. May be null
     * @returns {string} What will be be displayed
     */
     displayedAmountForToken(tokenId) { 

        if( tokenId ) {
            const tools = game.aesystem.actorTools;
            const target = tools.getActorOrTokenActor({
                tokenId: tokenId, 
                allowCurrentSelection: false
            });

            if( target ) {
                const detail = this._calculateTargetDetails(target);
                return this._parent.labels.detail.replace( 'AMOUNT', '' + detail.amount );
            }
        }
        return super.displayedAmountForToken(tokenId);
    }

}

class AETargetsHealImpl extends AETargetsBaseImpl {

     /**
     * The same for every targets
     * @override
     * @returns In the form of {amount: int, element: string}
     */
    _calculateTargetDetails(target) {

        const healBase = this._parent.defaultData;
        const element = healBase.element;
        const healAmount = healBase.amount;
        return {
            amount: healAmount,
            element: element
        };
    }

    /**
     * Actually apply the action on target.
     * This one heals the target
     * @override
     * @param {object} target Contains {actor:xxx, detail:xxx }
     * @returns The resulted amount that was heal
     */
     async applyOnTarget ( target ) {
        return target.actor.healDamage(target.detail.amount, {element: target.detail.element} );
    }

    /**
     * Display heal amount (the same for everyone)
     * @override
     * @param {string} tokenId tokenId of the targeted actor. May be null
     * @returns {string} What will be be displayed
     */
     displayedAmountForToken(tokenId) { 
        const healBase = this._parent.defaultData;
        const healAmount = healBase.amount;
        return this._parent.labels.detail.replace( 'AMOUNT', '' + healAmount );
    }
}

class AETargetsActiveEffectsImpl extends AETargetsBaseImpl {

    /**
     * The same for every targets
     * @override
     * @returns In the form of an activeEffect data
     */
    _calculateTargetDetails(actor) {
        const base = this._parent.defaultData.activeEffectData;
        const result = mergeObject({},base);

        // Duplicate base date and subtitute level
        const baseFlag = base.flags;
        result.flags = {
            core : mergeObject( {}, baseFlag.core ),
            acariaempire: {
                spell: mergeObject( {}, baseFlag.acariaempire.spell )
            }
        };

        const level = this._parent.defaultData.amount;
        result.flags.acariaempire.spell.level = level;
        return result;
    }

    /**
     * Actually apply the action on target.
     * This one apply a buff/debuff to the target
     * @override
     * @param {object} target Contains {actor:xxx, detail:xxx }
     * @returns The resulted amount that was heal
     */
    async applyOnTarget ( target ) {
        const activeEffectData = target.detail;
        const newEffect = await target.actor.addSpellEffect(activeEffectData);
        return newEffect ? 1 : 0;
    }

    /**
     * On this impl, baseAmount can't go above 6.
     * @override
     * @param {int} offset 
     */
    modifiyBaseAmount(offset) {
        this._parent.defaultData.amount = Math.clamped( 0, this._parent.defaultData.amount + offset, 6 );
    }

    /**
     * Display heal amount (the same for everyone)
     * @override
     * @param {string} tokenId tokenId of the targeted actor. May be null
     * @returns {string} What will be be displayed
     */
    displayedAmountForToken(tokenId) { 
        const effectBase = this._parent.defaultData;
        const effectLevel = effectBase.amount;
        return this._parent.labels.detail.replace( 'AMOUNT', '' + effectLevel );
    }

}
