import { allSpellsDefinition, spellShapes, generateWALinks, arcaneSpellModifiers, elementList } from '../tools/WaIntegration.js';

export const onClickDisplaySpellShape = async (event) => {
    event.preventDefault();
    const dataset = event.currentTarget.dataset;

    const initData = {};
    if( dataset.shape ) { initData.shapeKey = dataset.shape; }
    if( dataset.spell ) { initData.spellKey = dataset.spell; }
    if( dataset.spellLevel ) { initData.spellLevel = parseInt(dataset.spellLevel); }
    if( dataset.element ) { initData.mainElementKey = dataset.element; }
    if( dataset.shape ) { initData.shapeKey = dataset.shape; }

    initData.caracs = arcaneSpellModifiers().reduce( (result, modifier) => {
        const caracValue = dataset[modifier.key.toLowerCase()];
        if( caracValue != '' ) {
            result[modifier.key] = parseInt(caracValue);
        }
        return result;
    }, {} );
    

    const popup = new AESpellShapePopup();
    popup.init( initData );
    popup.render(true);
}

const DEFAULT_SHAPE_KEY = 'pulse';
const DEFAULT_SPELL_KEY = 'devouringChains';
const DEFAULT_ELEMENT_KEY = 'primary';
const DEFAULT_SPELL_LEVEL = 0;

class AESpellShapePopup extends Application {

    /** @override */
	static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
              classes: ["acariaempire", "spell-shape"],
              template: "systems/acariaempire/resources/message/spell-shape.hbs",
              width: 460,
              height: 'auto'
        });
    }

    constructor(options = {}) {
        super(options);
    }

    init( { shapeKey=DEFAULT_SHAPE_KEY, 
            spellKey=DEFAULT_SPELL_KEY, 
            spellLevel=DEFAULT_SPELL_LEVEL, 
            mainElementKey = DEFAULT_ELEMENT_KEY,
            caracs={} } = {} ) {
        
        console.log('AE-AESpellShapePopup | Initializing...');

        // Shape data
        const shapeDef = spellShapes().find( s => s.key === shapeKey );
        const shapeDesc = shapeDef.generateSpellDesc( 
            caracs['clone'], 
            caracs['range'], 
            caracs['areaOfEffect'], 
            caracs['duration'] 
        );
        this.shape = {
            fullDesc: [shapeDesc.title].concat(shapeDesc.details)
        };
        mergeObject(this.shape, shapeDef);

        // Spell data 
        const spellDef = allSpellsDefinition().spells.find( s => s.key === spellKey );
        const levelIcon = allSpellsDefinition().levelIcons[spellLevel];

        this.spell = {
            levelIcon: levelIcon
        };
        mergeObject(this.spell, spellDef);

        // Main Element
        const elem = elementList().find( e => e.key === mainElementKey );
        this.mainElement = mergeObject( {}, elem );

        // Spell caracteristics
        this.spellCaracteristics = Object.entries(caracs).map( ([caracName, caracLevel]) => {
            const modifier = arcaneSpellModifiers().find( m => m.key === caracName );
            const data = {
                level: caracLevel,
                levelIcon: allSpellsDefinition().levelIcons[caracLevel]
            };
            return mergeObject(data, modifier);
        });
    }

    /** @override */
    get title() {
        return this.spell?.name ?? '';
    }

    /** @override */
    getData() {
        return {
            shape: this.shape ?? {},
            spell: this.spell ?? {},
            mainElement: this.mainElement ?? {},
            spellCaracteristics: this.spellCaracteristics ?? []
        };
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);
        generateWALinks(html);
    }
}