import { modifyDamageWithAffixes } from '../tools/affixes.js';
import {translate} from '../tools/Translator.js';
import { commonWeaponList } from '../tools/WaIntegration.js';
import { InteractionMainActorBattleInterface } from './InteractionMainActorInterface.js';

const NOT_SELECTED = 'none';

export class InteractionMainActorImprovisedImpl extends InteractionMainActorBattleInterface {

    constructor(mainActor) {
        super('improvised', '01combat', mainActor);
        this.firstListboxInit();
        this.thirdListboxInit();
    }

    /** @override */
    get neededPowerToDoAction() {
        let amount = 0;
        if( this.maneuverChoice != NOT_SELECTED ) { amount++; };
        if( this.spiritManeuverChoice != NOT_SELECTED ) { amount++; };
        return amount;
    }

    /** @override */
    get maneuverBase() { return this.mainActor.improvised; }

    /** @override */
    get battleScore() { return this.mainActor.battle.improvised; }

    /** @override */
    get actionName() { 

        if( this.maneuverChoice != NOT_SELECTED ) {
            return translate('AESYSTEM.interaction.sheet.mainActor.improvised.' + this.attackStyle + '.maneuver'); 

        } else {
            return translate('AESYSTEM.interaction.sheet.mainActor.improvised.' + this.attackStyle + '.attack');
        }
    }

    /** @override */
    get actionFlavor() { 

        if( this.maneuverChoice != NOT_SELECTED ) {
            const maneuver =  this.mainActor.improvised.getManeuver(this.maneuverChoice);
            return maneuver.name;
        }
        
        return '';
    }

    /** @override */
    get actionDetails() {
        const result = super.actionDetails;

        result.push( this._textLineDetail(
            translate('AESYSTEM.interaction.sheet.details.attackStyle'), 
            translate('AESYSTEM.interaction.sheet.mainActor.improvised.attackStyle.' + this.attackStyle)
        ));

        // Spirit maneuver choice
        if( this.spiritManeuverChoice != NOT_SELECTED ) {

            result.push( this._textLineDetail(
                translate('AESYSTEM.interaction.sheet.labels.spiritManeuver'), 
               this.spiritAvailableChoices.find( c => c.key === this.spiritManeuverChoice ).label
            ));
        }
        return result;
    }    


    /** @override */
    get weaponId() {

        const bareHandedMode = this.attackStyle == 'barehanded';

        if( !bareHandedMode ) {
            // Check if it has an improvised weapon in hand
            const mainWeapon = this.mainActor.equipment.mainWeapon;
            if( mainWeapon?.isImprovisedWeapon ) {
                return mainWeapon.item.id;
            }
        }

        return null;
    }

    /** @override */
    get isBarehanded() {
        const bareHandedMode = this.attackStyle == 'barehanded';
        return bareHandedMode;
    }

    /* ---------------------------
          First lisbox: Improvised weapon  / barehanded
    ------------------------------ */

    firstListboxInit() {
        this.attackStyle = 'improvised';

        // Title
        this.styleTitle = translate('AESYSTEM.interaction.sheet.labels.attackStyle');

        // Available choices
        this.availableStyles = ['improvised', 'barehanded'].map( s => {
            return {
                key: s,
                label: translate('AESYSTEM.interaction.sheet.mainActor.improvised.attackStyle.' + s)
            };
        });
    }

    /** @override */
    get firstListboxDisplayed() { return true; }

    /** @override */
    get firstListboxTitle() { return this.styleTitle; }

    /** @override */
    get firstListboxAvailabeChoices() { return this.availableStyles; }

    /** @override */
    get firstListboxCurrentChoice() { return this.attackStyle; }

    /** @override */
    get firstListboxCurrentTokens() { return this.mainActor.battle.improvised.mastery; }

    /** @override */
    get firstListboxCurrentScore() { return this.mainActor.battle.improvised.advantage; }

    /** @override */
    firstListboxChangeChoice(newChoice) {

        if( this.attackStyle != newChoice ) {
            this.attackStyle = newChoice;
            this._reloadAvailableManeuvers();
        }
        
    }

    /* ---------------------------
          Second lisbox: Maneuver
    ------------------------------ */

    /** @override */
    filteringManeuvers( key, maneuver ) {
        const filterOnMartialArts =  this.attackStyle == 'barehanded';
        return maneuver.martialArt == filterOnMartialArts;
    }

    /** @override */
    get secondListboxDisplayed() { return this.availableManeuvers.length > 1; }

    /** @override */
    get secondListboxTitle() { return this.maneuverTitle; }

    /** @override */
    get secondListboxAvailabeChoices() { return this.availableManeuvers; }

    /** @override */
    get secondListboxCurrentChoice() { return this.maneuverChoice; }

    /** @override */
    get secondListboxCurrentTokens() { return this.maneuverModifiers.tokens; }

    /** @override */
    get secondListboxCurrentScore() { return this.maneuverModifiers.score; }

    /** @override */
    secondListboxChangeChoice(newChoice) { this.changeManeuver(newChoice); }

    /* ---------------------------
          Second lisbox: Spirit maneuver
    ------------------------------ */

    thirdListboxInit() {
        this.spiritManeuverChoice = NOT_SELECTED;

        // Title
        this.spiritTitle = translate('AESYSTEM.interaction.sheet.labels.spiritManeuver');

        // Available choices
        this.spiritAvailableChoices = this.mainActor.guardianSpirit.getAvailableManeuvers('combat').map( s => {
            return {
                key: s.maneuver,
                label: s.name
            };
        });

        this.spiritAvailableChoices.unshift({key: NOT_SELECTED, label: translate('AESYSTEM.interaction.sheet.mainActor.spiritManeuver.select') });
    }


    /** @override */
    get thirdListboxDisplayed() { return this.spiritAvailableChoices.length > 1; }

    /** @override */
    get thirdListboxTitle() { return this.spiritTitle; }

    /** @override */
    get thirdListboxAvailabeChoices() { return this.spiritAvailableChoices; }

    /** @override */
    get thirdListboxCurrentChoice() { return this.spiritManeuverChoice; }

    /** @override */
    thirdListboxChangeChoice(newChoice) {

        if( this.spiritManeuverChoice != newChoice ) {
            this.spiritManeuverChoice = newChoice;
        }
    }

}

