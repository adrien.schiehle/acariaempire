import {translate} from '../tools/Translator.js';
import { InteractionDefenseInterface } from './InteractionDefenseInterface.js';

const COMMUNION_IMG = 'systems/acariaempire/resources/interaction/spirit.png';

export class InteractionDefenseSpiritReleaseImpl extends InteractionDefenseInterface {

    constructor(mainActor, targets) {
        super('release', mainActor, targets);
    }

    /** @override */
    get img() { return COMMUNION_IMG; }

    /** @override */
    get putImgOnRollContext() { return false; }
    
    get releaseSpiritDisplayed() { return true; }
    
    /** 
     * User can release its contracted spirit alone
     * @override
     */
    get needGMInputs() {
        return false;
    }


    /* ---------------------------
        Reminder : No situation bonus sice there will be no roll
    ------------------------------ */

    /** @override */
    get reminders() {
        return [];
    }

    /** @override */
    get simpleMessagesToDisplay() {
        return [
            translate('AESYSTEM.interaction.sheet.communion.releaseDetail'),
            translate('AESYSTEM.interaction.sheet.communion.releaseFastAction')
        ];
    }

    /** @override */
    get actuallyRollDice() {
        return false;
    }

    /** @override */
    doThisInteadOfRollingDices() {
        this._mainActor.cards.discardAllSpirits();
        this._mainActor.sendMessage({
            content: translate('AESYSTEM.interaction.sheet.communion.releaseDone')
        });
    }
}

