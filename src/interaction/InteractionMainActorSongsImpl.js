import {translate} from '../tools/Translator.js';
import { InteractionMainActorBattleInterface } from './InteractionMainActorInterface.js';

const NOT_SELECTED = 'none';

export class InteractionMainActorSongsImpl extends InteractionMainActorBattleInterface {

    constructor(mainActor) {
        super('songs', '02magic', mainActor);
    }

    /** @override */
    get maneuverBase() { return this.mainActor.songs; }

    /** @override */
    get battleScore() { return this.mainActor.battle.songs; }

    /** @override */
    get isMentalAction() { return true; }

    /** @override */
    get actionName() { 

        if( this.maneuverChoice != NOT_SELECTED ) {
            return translate('AESYSTEM.interaction.sheet.mainActor.songs.maneuver'); 
        } 
        return translate('AESYSTEM.interaction.sheet.mainActor.songs.play');
    }

    /** @override */
    get actionFlavor() { 

        if( this.maneuverChoice != NOT_SELECTED ) {
            const maneuver =  this.mainActor.songs.getManeuver(this.maneuverChoice);
            return maneuver.name;
        }
        return '';
    }

    get currentPerformanceName() {
        const performance = this.mainActor.songs.currentPerformance;
        if( performance && performance.type != '' ) {
            return this.mainActor.songs.getMelody(performance.type).name;
        }
        return null;
    }

    /** @override */
    get targetNeedToDefendItself() {
        return false; 
    }

    /** @override */
    get availableDefenseMethods() {
        return ['melody'];
    }

    /* ---------------------------
        Reminder : Songs capacity
    ------------------------------ */

    /** @override */
    get reminderDisplayed() { return true; }

    /** @override */
    get reminderTitle() { return translate('AESYSTEM.interaction.sheet.mainActor.songs.reminder'); }

    /** @override */
    get reminderTokens() { 
        const base = this.mainActor.battle.songs;
        return base.mastery;
    }

    /** @override */
    get reminderScore() { 
        const base = this.mainActor.battle.songs;
        return base.advantage;
    }

    /* ---------------------------
          First lisbox: Maneuver
    ------------------------------ */

    /** @override */
    get firstListboxDisplayed() { return this.availableManeuvers.length > 1; }

    /** @override */
    get firstListboxTitle() { return this.maneuverTitle; }

    /** @override */
    get firstListboxAvailabeChoices() { return this.availableManeuvers; }

    /** @override */
    get firstListboxCurrentChoice() { return this.maneuverChoice; }

    /** @override */
    get firstListboxCurrentTokens() { return this.maneuverModifiers.tokens; }

    /** @override */
    get firstListboxCurrentScore() { return this.maneuverModifiers.score; }

    /** @override */
    firstListboxChangeChoice(newChoice) { this.changeManeuver(newChoice); }

}

