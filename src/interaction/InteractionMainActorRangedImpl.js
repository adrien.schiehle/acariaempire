import {translate} from '../tools/Translator.js';
import { InteractionMainActorBattleInterface } from './InteractionMainActorInterface.js';

const NOT_SELECTED = 'none';

export class InteractionMainActorRangedImpl extends InteractionMainActorBattleInterface {

    constructor(mainActor) {
        super('ranged', '01combat', mainActor);
        this.secondListboxInit();
    }

    /** @override */
    get neededPowerToDoAction() {
        let amount = 0;
        if( this.maneuverChoice != NOT_SELECTED ) { amount++; };
        if( this.spiritManeuverChoice != NOT_SELECTED ) { amount++; };
        return amount;
    }

    /** @override */
    get maneuverBase() { return this.mainActor.ranged; }

    /** @override */
    get battleScore() { return this.mainActor.battle.ranged; }

    /** @override */
    get actionName() { 

        if( this.maneuverChoice != NOT_SELECTED ) {
            return translate('AESYSTEM.interaction.sheet.mainActor.ranged.actionName.maneuver'); 

        } else {
            return translate('AESYSTEM.interaction.sheet.mainActor.ranged.actionName.attack');
        }
    }

    /** @override */
    get actionFlavor() { 

        if( this.maneuverChoice != NOT_SELECTED ) {
            const maneuver =  this.mainActor.ranged.getManeuver(this.maneuverChoice);
            return maneuver.name;
        }
        
        return '';
    }

    /** @override */
    get actionDetails() {
        const result = super.actionDetails;

        // Spirit maneuver choice
        if( this.spiritManeuverChoice != NOT_SELECTED ) {

            result.push( this._textLineDetail(
                translate('AESYSTEM.interaction.sheet.labels.spiritManeuver'), 
               this.spiritAvailableChoices.find( c => c.key === this.spiritManeuverChoice ).label
            ));
        }
        return result;
    }    


    /** @override */
    get weaponId() {

        const mainWeapon = this.mainActor.equipment.mainWeapon;
        if( mainWeapon?.isRangedWeapon ) {
            return mainWeapon.item.id;
        }

        return null;
    }

    /* ---------------------------
        Reminder : Battle capacity
    ------------------------------ */

    /** @override */
    get reminderDisplayed() { return true; }

    /** @override */
    get reminderTitle() { return translate('AESYSTEM.interaction.sheet.mainActor.ranged.reminder'); }

    /** @override */
    get reminderTokens() { 
        const base = this.mainActor.battle.ranged;
        return base.mastery;
    }

    /** @override */
    get reminderScore() { 
        const base = this.mainActor.battle.ranged;
        return base.advantage;
    }

    /* ---------------------------
          First lisbox: Maneuver
    ------------------------------ */

    /** @override */
    get firstListboxDisplayed() { return this.availableManeuvers.length > 1; }

    /** @override */
    get firstListboxTitle() { return this.maneuverTitle; }

    /** @override */
    get firstListboxAvailabeChoices() { return this.availableManeuvers; }

    /** @override */
    get firstListboxCurrentChoice() { return this.maneuverChoice; }

    /** @override */
    get firstListboxCurrentTokens() { return this.maneuverModifiers.tokens; }

    /** @override */
    get firstListboxCurrentScore() { return this.maneuverModifiers.score; }

    /** @override */
    firstListboxChangeChoice(newChoice) { this.changeManeuver(newChoice); }
    
    /* ---------------------------
          Second lisbox: Spirit maneuver
    ------------------------------ */

    secondListboxInit() {
        this.spiritManeuverChoice = NOT_SELECTED;

        // Title
        this.spiritTitle = translate('AESYSTEM.interaction.sheet.labels.spiritManeuver');

        // Available choices
        this.spiritAvailableChoices = this.mainActor.guardianSpirit.getAvailableManeuvers('combat').map( s => {
            return {
                key: s.maneuver,
                label: s.name
            };
        });

        this.spiritAvailableChoices.unshift({key: NOT_SELECTED, label: translate('AESYSTEM.interaction.sheet.mainActor.spiritManeuver.select') });
    }


    /** @override */
    get secondListboxDisplayed() { return this.spiritAvailableChoices.length > 1; }

    /** @override */
    get secondListboxTitle() { return this.spiritTitle; }

    /** @override */
    get secondListboxAvailabeChoices() { return this.spiritAvailableChoices; }

    /** @override */
    get secondListboxCurrentChoice() { return this.spiritManeuverChoice; }

    /** @override */
    secondListboxChangeChoice(newChoice) {

        if( this.spiritManeuverChoice != newChoice ) {
            this.spiritManeuverChoice = newChoice;
        }
    }

}

