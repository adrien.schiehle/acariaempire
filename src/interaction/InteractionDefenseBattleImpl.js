import {translate} from '../tools/Translator.js';
import {BATTLE_METHOD, DEFENSE_METHOD} from '../actor/ActorBattle.js';
import { InteractionDefenseInterface } from './InteractionDefenseInterface.js';

export class InteractionDefenseBattleImpl extends InteractionDefenseInterface {

    constructor(mainActor, targets) {
        super('battle', mainActor, targets);
        this.firstListboxInit();
        this.setDefenseMode('classic');
    }

    /** @override */
    get availableDefenseOptions() {
        return this.allDefenseOptions;
    }

    /** @override */
    get defenseName() {
        return this.battleChoiceName;
    }

    /** @override */
    get defenseDetails() {
        const result = super.defenseDetails;
        result.push( this._tokenLineDetail(
            translate('AESYSTEM.interaction.sheet.details.battleProficiency'), 
            {defenseScore: this.battleScore.advantage}
        ));

        return result;
    }

    /** @override */
    get needsTarget() {
        return true;
    }
    

    /* ---------------------------
      First lisbox: BattleMethods
    ------------------------------ */

    firstListboxInit() {
        // Title
        this.batttleTitle = translate('AESYSTEM.interaction.sheet.details.defenseType');

        // Available choices
        this.batttleAvailableChoices = Object.values(DEFENSE_METHOD).map( b => { 
            return {
                key: b,
                label: translate('AESYSTEM.interaction.sheet.defense.battle.type.' + b )
            };
        });
        this.firstListboxChangeChoice(DEFENSE_METHOD.melee);
    }

    /** @override */
    get firstListboxDisplayed() { return true; }

    /** @override */
    get firstListboxTitle() { return this.batttleTitle; }

    /** @override */
    get firstListboxAvailabeChoices() { return this.batttleAvailableChoices; }

    /** @override */
    get firstListboxCurrentChoice() { return this.battleChoice; }

    /** @override */
    get firstListboxCurrentScore() { return this.battleScore.advantage; }

    /** @override */
    firstListboxChangeChoice(newChoice) {
        this.battleChoice = newChoice;
        this.battleChoiceName = this.batttleAvailableChoices.find( c => c.key === newChoice).label;

        this.battleScore =  this.mainDefender?.battle.forDefense(this.battleChoice) ?? {advantage: 0, mastery: 0};
    }

    /**
     * Will choose the appropriate defense method for the given attack skill
     * @param {string} battleType See BATTLE_METHOD
     */
    selectCorrectDefenseModeForBattleType( battleType ) {
        let choice = DEFENSE_METHOD.magic; // Default choice
        if( [BATTLE_METHOD.melee, BATTLE_METHOD.improvised].includes(battleType) ) {
            choice = DEFENSE_METHOD.melee;
        } else if( BATTLE_METHOD.ranged === battleType ) {
            choice = DEFENSE_METHOD.ranged;
        }

        this.firstListboxChangeChoice(choice);
    }

}
