import { AEInteractionGUI, INTERACTION_MODE } from './InteractionGUI.js';
import {generateWALinks} from '../tools/WaIntegration.js';
import { InteractionDefenseData, InteractionMainActorData } from './InteractionBaseData.js';

export class AEInteractionGUIForDefense extends AEInteractionGUI {

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            id : "ae-interaction-defense",
            title : game.i18n.localize('AESYSTEM.interaction.sheet.title'),
            classes : ["acariaempire", "sheet", "interaction"],
            template : 'systems/acariaempire/resources/interaction/interaction.hbs',
            popOut : true,
            resizable : false,
            width : 'auto',
            height : 'auto'
        });
    }
    
    constructor(user, mainActor, targets, options = {}) {
        super(options);
        this.options.title = "Asked by " + user.name;
        this.mainActorData = new InteractionMainActorData(mainActor);
        this.defenseData = new InteractionDefenseData(mainActor, targets);

        this._riskLevel = 0;
        this.state.waitingAnswer = true;
        this.state.hasBeenAnswered = false;
        this._init = true;
        this._answered = false;
    }

    /** @override */
    get interactionMode() { return INTERACTION_MODE.DEFENSE; }

    get answered() { return this._answered; }

/*------------------------------------------
    Listeners
--------------------------------------------*/

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);
        generateWALinks(html);

        this.listenersForDefenseChoices(html);

        // Middle column: Versus button
        //------------------------------
        html.find(".answer-button.active").click( (event) => this._onClickAnswerPlayer(event) );
    }

/*------------------------------------------
    Listeners
--------------------------------------------*/

    async _onClickAnswerPlayer(event) {
        this._answered = true;
        this.close();
    }

}