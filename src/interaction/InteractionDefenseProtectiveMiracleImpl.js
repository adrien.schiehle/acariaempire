import {translate} from '../tools/Translator.js';

import { InteractionDefenseInterface } from './InteractionDefenseInterface.js';

const COMMUNION_IMG = 'systems/acariaempire/resources/interaction/spirit.png';

export class InteractionDefenseProtectiveMiracleImpl extends InteractionDefenseInterface {

    constructor(mainActor, targets) {
        super('protectiveMiracle', mainActor, targets);
        this.init();
    }


    /** @override */
    get img() { return this.mainDefender?.img ?? COMMUNION_IMG; }

    /** @override */
    get putImgOnRollContext() { return false; }

    /* ---------------------------
        Reminder : Protective miracle
    ------------------------------ */

    /** @override */
    get reminders() {
        const result = [{
            title: translate('AESYSTEM.interaction.sheet.protectiveMiracle.difficulty'), 
            score: {value: this.spiritPower }
        }];

        result.push(...super.reminders);
        return result;
    }

    /** @override */
    get defenseDetails() {
        const result = super.defenseDetails;
        
        result.push( this._tokenLineDetail(
            translate('AESYSTEM.interaction.sheet.protectiveMiracle.difficulty'), 
            {defenseScore: this.spiritPower}
        ));

        return result;
    }

    init() {
        const caster = this._mainActor;
        this.spiritPower = caster.divine.communion.spiritPower ?? 0;
    }

}

