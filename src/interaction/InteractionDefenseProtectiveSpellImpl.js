import {translate} from '../tools/Translator.js';

import { InteractionDefenseInterface } from './InteractionDefenseInterface.js';

const CIRCLE_IMG = 'systems/acariaempire/resources/interaction/choices/circle.png';

export class InteractionDefenseProtectiveSpellImpl extends InteractionDefenseInterface {

    constructor(mainActor, targets) {
        super('protectiveSpell', mainActor, targets);
        this.init();
    }


    /** @override */
    get img() { return this.mainDefender?.img ?? CIRCLE_IMG; }

    /** @override */
    get putImgOnRollContext() { return false; }

    /* ---------------------------
        Reminder : Melody refresh
    ------------------------------ */

    /** @override */
    get reminders() {
        const result = [{
            title: translate('AESYSTEM.interaction.sheet.protectiveSpell.difficulty'), 
            score: {value: this.maxLevel }
        }];

        result.push(...super.reminders);
        return result;
    }

    /** @override */
    get defenseDetails() {
        const result = super.defenseDetails;
        
        result.push( this._tokenLineDetail(
            translate('AESYSTEM.interaction.sheet.protectiveSpell.difficulty'), 
            {defenseScore: this.maxLevel}
        ));

        return result;
    }

    init() {
        const caster = this._mainActor;
        const caracs = caster.arcane.circleCaracteristics ?? [];
        if( caracs.length == 0 ) {
            this.maxLevel = 0;
        } else {
            this.maxLevel = caracs.map( c => c.level ).reduce( (result, current) => {
                return result > current ? result : current;
            });
        }
    }

}

