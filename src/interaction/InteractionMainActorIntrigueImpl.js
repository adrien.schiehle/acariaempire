import {translate} from '../tools/Translator.js';
import { InteractionMainActorInterface } from './InteractionMainActorInterface.js';
import { attributeList } from '../tools/WaIntegration.js';

const NOT_SELECTED = 'none';

export class InteractionMainActorIntrigueImpl extends InteractionMainActorInterface {

    constructor(mainActor) {
        super('intrigue', '03skill', mainActor);

        this.intrigueAbility = this.mainActor.abilities.intrigueAbility;

        this.firstListboxInit();
        this.secondListboxInit();
        this.thirdListboxInit();
        this.actionCategory = 'simple';
    }

    /** @override */
    get isMentalAction() { 
        return ['focus', 'perspicacity','socialEase'].includes(this.attributeChoice);
    }

    /** @override */
    get neededPowerToDoAction() {
        return this.spiritManeuverChoice != NOT_SELECTED ? 1 : 0;
    }

    /** @override */
    get actionFlavor() { 
        return this.allCompatibilityChoices.find(c => c.key === this.compatibilityChoice).label;
    }

    
    /** @override */
    get actionDetails() {
        const result = [];

        // Attribute
        if( this.attributeChoice != NOT_SELECTED ) {
            result.push( this._textLineDetail(
                translate('AESYSTEM.interaction.sheet.labels.attribute'), 
                attributeList().find(a => a.key === this.attributeChoice)?.name
            ));            
            result.push( this._tokenLineDetail(
                translate('AESYSTEM.interaction.sheet.labels.attribute'), 
                {baseScore: this.attributeLevel}
            ));
        }

        // Intrigue ability
        result.push( this._textLineDetail(
            translate('AESYSTEM.interaction.sheet.labels.intrigue'), 
            this.allCompatibilityChoices.find(c => c.key === this.compatibilityChoice).label
        ));            
        result.push( this._tokenLineDetail(
            translate('AESYSTEM.interaction.sheet.labels.intrigue'), 
            {cardBalance: this.compatibilityModifier.masteryModifier, baseScore: this.compatibilityModifier.advantageModifier}
        ));            

        // Spirit maneuver choice
        if( this.spiritManeuverChoice != NOT_SELECTED ) {

            result.push( this._textLineDetail(
                translate('AESYSTEM.interaction.sheet.labels.spiritManeuver'), 
               this.spiritAvailableChoices.find( c => c.key === this.spiritManeuverChoice ).label
            ));
        }
        
        result.push(...super.actionDetails);
        return result;
    }    
    
    /** @override */
    get simpleMessagesToDisplay() {
        return this.intrigueAbility.unlocked.map( s => s.name );
    }


    /* ---------------------------
      First lisbox: Attribute
    ------------------------------ */

    firstListboxInit() {
        this.attributeChoice = NOT_SELECTED;
        this.attributeLevel = 0;

        // Title
        this.attributeTitle = translate('AESYSTEM.interaction.sheet.labels.attribute');

        // Available choices
        this.attributeAvailableChoices = attributeList().map( a => { 
            return {
                key: a.key,
                label: a.name
            };
        });
        this.attributeAvailableChoices.unshift({key: NOT_SELECTED, label: translate('AESYSTEM.interaction.sheet.list.attribute.none') });
    }

    /** @override */
    get firstListboxDisplayed() { return true; }

    /** @override */
    get firstListboxTitle() { return this.attributeTitle; }

    /** @override */
    get firstListboxAvailabeChoices() { return this.attributeAvailableChoices; }

    /** @override */
    get firstListboxCurrentChoice() { return this.attributeChoice; }

    /** @override */
    get firstListboxCurrentScore() { return this.attributeLevel; }

    /** @override */
    firstListboxChangeChoice(newChoice) {
        this.attributeChoice = newChoice;

        const checkLevel = this.attributeChoice != NOT_SELECTED;
        this.attributeLevel = checkLevel ? this.mainActor.getAttributeLevel(this.attributeChoice) : 0;
    }

    /* ---------------------------
       Second lisbox: Explore Skill compatibility
    ------------------------------ */

    secondListboxInit() {
        this.allCompatibilityChoices = [
            {
                // Unknown skill methods have a -1 advantange, set to 0 mastery
                key: 'noCompatibility',
                label: translate('AESYSTEM.interaction.sheet.mainActor.compatibility.no'),
                modifier: {
                    advantageModifier: Math.max(0, this.intrigueAbility.level - 1),
                    masteryModifier: -2
                }
            },
            {
                // Far fetched skill methods have a -1 advantange, -1 mastery
                key: 'smallCompatibility',
                label: translate('AESYSTEM.interaction.sheet.mainActor.compatibility.small'),
                modifier: {
                    advantageModifier: this.intrigueAbility.level,
                    masteryModifier: -1
                }
            },
            {
                key: 'compatible',
                label: translate('AESYSTEM.interaction.sheet.mainActor.compatibility.ok'),
                modifier: {
                    advantageModifier: this.intrigueAbility.level,
                    masteryModifier: 0
                }
            }
        ];

        // Title
        this.compatibilityTitle = translate('AESYSTEM.interaction.sheet.labels.compatibility');
        this.secondListboxChangeChoice('compatible');
    }

    /** @override */
    get secondListboxDisplayed() { return true; }

    /** @override */
    get secondListboxTitle() { return this.compatibilityTitle; }

    /** @override */
    get secondListboxAvailabeChoices() { return this.allCompatibilityChoices; }

    /** @override */
    get secondListboxCurrentChoice() { return this.compatibilityChoice; }

    /** @override */
    get secondListboxCurrentTokens() { return this.compatibilityModifier.masteryModifier; }

    /** @override */
    get secondListboxCurrentScore() { return this.compatibilityModifier.advantageModifier; }

    /** @override */
    secondListboxChangeChoice(newChoice) {
        this.compatibilityChoice = newChoice;
        this.compatibilityModifier = this.allCompatibilityChoices.find(c => c.key === this.compatibilityChoice).modifier;
    }

    /* ---------------------------
          Fourth lisbox: Spirit maneuver
    ------------------------------ */

    thirdListboxInit() {
        this.spiritManeuverChoice = NOT_SELECTED;

        // Title
        this.spiritTitle = translate('AESYSTEM.interaction.sheet.labels.spiritManeuver');

        // Available choices
        this.spiritAvailableChoices = this.mainActor.guardianSpirit.getAvailableManeuvers('intrigue').map( s => {
            return {
                key: s.maneuver,
                label: s.name
            };
        });

        this.spiritAvailableChoices.unshift({key: NOT_SELECTED, label: translate('AESYSTEM.interaction.sheet.mainActor.spiritManeuver.select') });
    }


    /** @override */
    get thirdListboxDisplayed() { return this.spiritAvailableChoices.length > 1; }

    /** @override */
    get thirdListboxTitle() { return this.spiritTitle; }

    /** @override */
    get thirdListboxAvailabeChoices() { return this.spiritAvailableChoices; }

    /** @override */
    get thirdListboxCurrentChoice() { return this.spiritManeuverChoice; }

    /** @override */
    thirdListboxChangeChoice(newChoice) {

        if( this.spiritManeuverChoice != newChoice ) {
            this.spiritManeuverChoice = newChoice;
        }
    }

}

