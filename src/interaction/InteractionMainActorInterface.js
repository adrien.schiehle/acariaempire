import {translate} from '../tools/Translator.js';

const NOT_SELECTED = 'none';

/** Methods that should be implemented by every InteractionMainActorXXXXXImpl */
export class InteractionMainActorInterface {

    constructor(methodName, methodGroup, mainActor) {
        this._method = methodName;
        this._methodGroup = methodGroup;
        this._mainActor = mainActor;
    }

    get mainActor() {
        return this._mainActor;
    }

    get method() {
        return this._method;
    }

    get methodName() {
        return translate('AESYSTEM.interaction.sheet.mainActor.method.' + this.method + '.choice'); 
    }

    get methodGroup() {
        return this._methodGroup;
    }

    get methodGroupName() {
        return translate('AESYSTEM.interaction.sheet.mainActor.methodGroup.' + this.methodGroup);
    }

    /** May be overriden */
    get actionName() { 
        return translate('AESYSTEM.interaction.sheet.mainActor.method.' + this.method + '.title'); 
    }

    /** Should be overriden */
    get actionFlavor() {
        return '';
    }

    /** May be overriden */
    get actionDetails() {
        const result = [];
        result.push( this._tokenLineDetail(
            translate('AESYSTEM.interaction.sheet.details.startedWith'), 
            { cardBalance: this.mainActor.mastery, 
              baseScore: this.mainActor.advantage }
        ));

        return result;
    }

    get img() {
        return this._mainActor.img;
    }

    /** Should be overriden */
    get targetNeedToDefendItself() {
        return false; 
    }

    /** May be overriden */
    get neededPowerToDoAction() {
        return 0;
    }

    get allActionCategories() {

        if( !this._allActionCategories ) {
            const icons = ['far fa-hourglass', 'fas fa-hourglass-half', 'fas fa-hourglass'];
            this._allActionCategories = ['instant', 'fast', 'simple'].map( (action, index) => {
                return {
                    key: action,
                    icon: icons[index],
                    label: translate('AESYSTEM.interaction.sheet.action.' + action)
                };
            });
        }
        return this._allActionCategories;
    }

    get actionCategory() {
        
        if( !this._actionCategory ) {
            const options = this.availableActionsCategories;
            if( options.length > 0 ) { 
                const classicMode = options.find( o => o.key == 'simple' );
                this._actionCategory = classicMode?.key ?? options[0].key; 
            }
        }
        return this._actionCategory;
    }

    set actionCategory(value) {
        if( this.availableActionsCategories.find( o => o.key === value ) ) {
            this._actionCategory = value;
        } else {
            const label = translate('AESYSTEM.interaction.sheet.action.' + value);
            ui.notifications.info(translate("AESYSTEM.interaction.sheet.err.invalidActionCategory").replace('CATEGORY', label));
        }
    }    

    /** May be overriden */
    get availableActionsCategories() {
        return this.allActionCategories;
    }

    /** May be overriden. */
    get isMentalAction() { return false; }

    /** @returns The UT amount that will be consumed by this action */
    get neededTimeForAction() {

        // Instant, normal or fast action ?
        if( this.actionCategory == 'instant' ) { return 0; }
        const base = this.actionCategory == 'simple' ? this.mainActor.neededTime.simpleAction : this.mainActor.neededTime.fastAction;

        // Takes weapon speed into account if needed
        const weaponId = this.weaponId;
        if( weaponId ) {
            const baseSpeed = this.mainActor.equipment.weapons.find( w => w.item.id === weaponId ).speed.value;
            return base.fromBaseSpeed(baseSpeed);

        } else if( this.isBarehanded ) {
            const baseSpeed = this.mainActor.equipment.barehandedAttack.size.baseSpeed;
            return base.fromBaseSpeed(baseSpeed);
        }

        return this.isMentalAction ? base.mental : base.physical;
    }

    /** 
     * Allow the main GUI to know if the user can go further or if the current choices are not valid
     * May be overriden 
     */
    get isValid() {
        return true;
    }

    /**
     * Should be overriden if the interaction intent to do weapon damage to the target
     */
    get weaponId() {
        return null;
    }

    /**
     * Should be overriden if the interaction intent to do barehanded damage to the target
     */
    get isBarehanded() {
        return false;
    }

    /**
     * May be overriden
     */
    get availableDefenseMethods() {
        const result =  ['unknown', 'difficulty', 'nobattle'];
        if( this.targetNeedToDefendItself ) {
            result.push('battle');
        }
        return result;
    }


    get listBoxes() {
        const result = [];
        if( this.firstListboxDisplayed ) {
            const data = {
                bind: 'firstListbox',
                title: this.firstListboxTitle,
                choices: this.firstListboxAvailabeChoices,
                choice: this.firstListboxCurrentChoice
            };
            let val = this.firstListboxCurrentTokens;
            if( val != null ) { data.tokens = {value: val}; }

            val = this.firstListboxCurrentScore;
            if( val != null ) { data.score = {value: val}; }

            result.push(data);
        }

        if( this.secondListboxDisplayed ) {
            const data = {
                bind: 'secondListbox',
                title: this.secondListboxTitle,
                choices: this.secondListboxAvailabeChoices,
                choice: this.secondListboxCurrentChoice
            };
            let val = this.secondListboxCurrentTokens;
            if( val != null ) { data.tokens = {value: val}; }

            val = this.secondListboxCurrentScore;
            if( val != null ) { data.score = {value: val}; }

            result.push(data);
        }

        if( this.thirdListboxDisplayed ) {
            const data = {
                bind: 'thirdListbox',
                title: this.thirdListboxTitle,
                choices: this.thirdListboxAvailabeChoices,
                choice: this.thirdListboxCurrentChoice
            };
            let val = this.thirdListboxCurrentTokens;
            if( val != null ) { data.tokens = {value: val}; }

            val = this.thirdListboxCurrentScore;
            if( val != null ) { data.score = {value: val}; }

            result.push(data);
        }

        if( this.fourthListboxDisplayed ) {
            const data = {
                bind: 'fourthListbox',
                title: this.fourthListboxTitle,
                choices: this.fourthListboxAvailabeChoices,
                choice: this.fourthListboxCurrentChoice
            };
            let val = this.fourthListboxCurrentTokens;
            if( val != null ) { data.tokens = {value: val}; }

            val = this.fourthListboxCurrentScore;
            if( val != null ) { data.score = {value: val}; }

            result.push(data);
        }
        return result;        
    }

    /** Should be overriden in classes that needs it  */
    prepareAdditionalData() {}

    /** Should be overriden in classes that needs it  */
    activateSpecificListeners(sheet, html) {}

    get needToConsumeSpirits() { return false; } // Whether there is a need to retrieve miracleDataForRollContext
    get miracleDataForRollContext() { return null; } // Gives necessay data to put inside rollCotext so that actor will be updated in case the roll is a success

    get needToTriggerArcaneCircle() { return false; }
    get triggeredArcaneCircle() { return null; }

    /** 
     * Array of lines that will be displayed on message if the action is a success 
     * @returns { icon, name, (optional)overlay, (optional)shortcut }
    */
    get toDisplayOnSuccess() { return []; } 
    
    get hasSimpleMessagesToDisplay() { return this.simpleMessagesToDisplay.length > 0; }
    get simpleMessagesToDisplay() { return []; }

    get reminderDisplayed() { return false; }
    get reminderTitle() {}
    get reminderTips() {return null;}  // string ; Optional
    get reminderTokens() {return null;} // int
    get reminderScore() {return null;} // int
    get reminderRef() {return null;} // string : Only useful if you want the reminder to be clickable
    async reminderClicked(ref, type, leftClick) {} // To intercept clicks on reminder

    get reminder() {
        const data = {
            displayed: this.reminderDisplayed,
            title: this.reminderTitle,
        };

        let val = this.reminderTips;
        if(val != null) { data.tips = val; }

        val = this.reminderTokens;
        if( val != null) {data.tokens = {value: val}; }
        
        val = this.reminderScore;
        if( val != null) {data.score = {value: val}; }

        const ref = this.reminderRef;
        data.css = ref != null ? 'clickable' : '';
        data.ref = ref;

        return data;
    };

    get firstListboxDisplayed() { return false; }
    get firstListboxTitle() {}
    get firstListboxAvailabeChoices() {}  //  [{key: x , label: x }, ...{}]
    get firstListboxCurrentChoice() {}  // choiceKey
    get firstListboxCurrentTokens() {return null;}  // int
    get firstListboxCurrentScore() {return null;}  // int
    firstListboxChangeChoice(newChoice) {}

    get secondListboxDisplayed() { return false; }
    get secondListboxTitle() {}
    get secondListboxAvailabeChoices() {}  // [{key: x , label: x }, ...{}]
    get secondListboxCurrentChoice() {}  // choiceKey
    get secondListboxCurrentTokens() {return null;}  // int
    get secondListboxCurrentScore() {return null;}  // int
    secondListboxChangeChoice(newChoice) {}

    get thirdListboxDisplayed() { return false; }
    get thirdListboxTitle() {}
    get thirdListboxAvailabeChoices() {}  // [{key: x , label: x }, ...{}]
    get thirdListboxCurrentChoice() {}  // choiceKey
    get thirdListboxCurrentTokens() {return null;}  // int
    get thirdListboxCurrentScore() {return null;}  // int
    thirdListboxChangeChoice(newChoice) {}

    get fourthListboxDisplayed() { return false; }
    get fourthListboxTitle() {}
    get fourthListboxAvailabeChoices() {}  // [{key: x , label: x }, ...{}]
    get fourthListboxCurrentChoice() {}  // choiceKey
    get fourthListboxCurrentTokens() {return null;}  // int
    get fourthListboxCurrentScore() {return null;}  // int
    fourthListboxChangeChoice(newChoice) {}

    get startedWith() {
        return {
            score: {value: this.mainActor.advantage},
            tokens: {value: this.mainActor.mastery}
        };
    }


    /*-----------------------
       Score and tokens
    --------------------------*/
    
    /** 
     * Shouldn't be overriden. Use reminders, and boxes if you want to change tokens 
     * @returns {int} card balance token amount
     * */    
     get tokens() {
        let result = 0;

        // adjustment
        result += this.startedWith.tokens.value;

        // boxes
        for( const box of this.listBoxes ) {
            if( box.tokens ) {
                result += box.tokens.value;
            }
        }

        // reminder
        const reminderTokens = this.reminder.tokens;
        if( reminderTokens ) {
            result += reminderTokens.value;
        }

        return result;
    }

    /** 
     * Shouldn't be overriden. Use reminders, and boxes if you want to change tokens 
     * @returns {int} the defense score for this roll
     * */    
     get score() {
        let result = 0;

        // adjustment
        result += this.startedWith.score.value;

        // boxes
        for( const box of this.listBoxes ) {
            if( box.score ) {
                result += box.score.value;
            }
        }

        // reminder
        const reminderScore = this.reminder.score;
        if( reminderScore ) {
            result += reminderScore.value;
        }

        return result;
    }

    /*-----------------------
       Necessay data for socket
    --------------------------*/

    forSocket() {
        return {
            method: this.method,
            methodGroup: this.methodGroup,
            choices: this.listBoxes.map( b => b.choice )
        }
    }

    fromSocket(data) {
        let lastUpdatedIndex = -1;
        data.choices.forEach( c => {
            lastUpdatedIndex = this.loadChoice(c, lastUpdatedIndex);
        });
    }


    /**
     * @private
     * Used to load choices one by one.
     * 
     * @param {string} choice choice key, as retrieved from socket
     * @param {int} lastUpdatedIndex which choice was updated last
     * @returns last updated choice, once done
     */
    loadChoice( choice, lastUpdatedIndex ) {

        if( lastUpdatedIndex < 0 && this.firstListboxDisplayed ) {
            this.firstListboxChangeChoice(choice);
            return 0;
        }

        if( lastUpdatedIndex < 1 && this.secondListboxDisplayed ) {
            this.secondListboxChangeChoice(choice);
            return 1;
        }
        
        if( lastUpdatedIndex < 2 && this.thirdListboxDisplayed ) {
            this.thirdListboxChangeChoice(choice);
            return 2;
        }

        if( lastUpdatedIndex < 3 && this.fourthListboxDisplayed ) {
            this.fourthListboxChangeChoice(choice);
            return 3;
        }

        return 4;
    }

    /*-----------------------
       Convenience methods
    --------------------------*/
   
    /**
     * Format a detail so that it can be later displayed inside roll chat message.
     * This method allow token images
     * @param {string} title detail header
     * @param {int} [baseScore] will be added with its related icon
     * @param {int} [defenseScore] idem
     * @param {int} [cardBalance] Related icon will differ if the amount is positive or negative
     */
     _tokenLineDetail(title, {baseScore=null, defenseScore=null, cardBalance=null}) {
        
        const result = {
            title: title, 
            tokens: []
        };

        if( baseScore != null ) {
            const icon = 'systems/acariaempire/resources/interaction/tokens/action_score.png';
            result.tokens.push({text: '' + baseScore, icon: icon});
        }

        if( defenseScore != null ) {
            const icon = 'systems/acariaempire/resources/interaction/tokens/defense_score.png';
            result.tokens.push({text: '' + defenseScore, icon: icon});
        }

        if( cardBalance != null ) {
            const icon = 'systems/acariaempire/resources/interaction/tokens/card_balance_' + (cardBalance < 0 ? 'negative.png' : 'positive.png');
            result.tokens.push({text: '' + Math.abs(cardBalance), icon: icon});
        }

        return result;
    }

    _textLineDetail(title, text) {
        return {
            title: title, 
            text: text
        };
    }
}

export class InteractionMainActorBattleInterface extends InteractionMainActorInterface {

    constructor(methodName, methodGroup, mainActor) {
        super(methodName, methodGroup, mainActor);
        this.initManeuvers();
        this.actionCategory = 'simple';
    }

    /** @override */
    get targetNeedToDefendItself() {
        return true; 
    }

    /** @override */
    get neededPowerToDoAction() {
        return this.maneuverChoice != NOT_SELECTED ? 1 : 0;
    }

    get maneuverBase() { throw 'Should be overriden'; }

    initManeuvers() {
        this.maneuverChoice = NOT_SELECTED;
        this.maneuverModifier = {advantageModifier: 0, masteryModifier: 0};

        // Title
        this.maneuverTitle = translate('AESYSTEM.interaction.sheet.labels.maneuver');

        this._reloadAvailableManeuvers();
    }

    // May be overriden
    filteringManeuvers( key, maneuver ) {
        return true;
    }

    _reloadAvailableManeuvers() {
        // Available choices
        this.availableManeuvers = this.maneuverBase.maneuvers.filter( ([key, maneuver]) => {
            return maneuver.level.level > 0;

        }).filter( ([key, maneuver]) => {
            return this.filteringManeuvers(key, maneuver);

        }).map( ([key, maneuver]) => {
            return {
                key: key,
                label: maneuver.name
            };
        });

        this.availableManeuvers.unshift({key: NOT_SELECTED, label: translate('AESYSTEM.interaction.sheet.list.maneuver.none') });

        const maneuverOk = this.availableManeuvers.find( m => m.key === this.maneuverChoice );
        if( !maneuverOk ) {
            this.changeManeuver(NOT_SELECTED);
        }
    }


    get maneuverModifiers() { 
        return {
            score: this.maneuverModifier.advantageModifier,
            tokens: this.maneuverModifier.masteryModifier
        };
    }

    changeManeuver(newManeuver) {
        this.maneuverChoice = newManeuver;

        // Update maneuverModifier if possible (otherwise, put 0 as modifier)
        this.maneuverModifier = {advantageModifier: 0, masteryModifier: 0};
        if(this.maneuverChoice != NOT_SELECTED) {
            try {
                const maneuver =  this.maneuverBase.getManeuver(this.maneuverChoice);
                this.maneuverModifier = {advantageModifier: maneuver.advantageModifier, masteryModifier: maneuver.masteryModifier};
            } catch(e) {
                ui.notifications.error(translate("AESYSTEM.interaction.sheet.err.invalidManeuver").replace('MANEUVER', this.maneuverChoice));
            }
        }
    }

    get battleScore() { throw 'Should be overriden'; }


    /** @override */
    get actionDetails() {
        const result = super.actionDetails;

        result.push( this._tokenLineDetail(
            translate('AESYSTEM.interaction.sheet.details.battleProficiency'), 
            {cardBalance: this.battleScore.mastery, baseScore: this.battleScore.advantage}
        ));

        if( this.maneuverChoice != NOT_SELECTED ) {
            result.push( this._tokenLineDetail(
                translate('AESYSTEM.interaction.sheet.details.fromManeuver'), 
                { cardBalance: this.maneuverModifier.masteryModifier, 
                  baseScore: this.maneuverModifier.advantageModifier }
            ));
        }

        return result;
    }    

    /** @override */
    get toDisplayOnSuccess() {
        if( this.maneuverChoice == NOT_SELECTED ) { return []; }

        const maneuver =  this.maneuverBase.getManeuver(this.maneuverChoice);
        const line = mergeObject( {overlay: maneuver.level.icon}, maneuver);
        return [line];
    }
}

