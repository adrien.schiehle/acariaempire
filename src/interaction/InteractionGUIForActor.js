import {translate} from '../tools/Translator.js';
import {generateWALinks} from '../tools/WaIntegration.js';
import { AEInteractionGUI, INTERACTION_MODE } from './InteractionGUI.js';
import { InteractionDefenseData, InteractionMainActorData } from './InteractionBaseData.js';
import { askGMForInteractionDifficulty } from '../sockets/AskingDifficultyGUI.js';

const consumePowerIfNeeded = async (mainImpl) => {

    const amount = mainImpl.neededPowerToDoAction;
    if(amount == 0) { return; }
    if( mainImpl.mainActor.power < amount) {
        throw translate('AESYSTEM.interaction.sheet.err.powerTokenNeeded');
    }
    await mainImpl.mainActor.setPower( mainImpl.mainActor.power - amount);
    await ui.notifications.info(translate('AESYSTEM.interaction.sheet.info.powerTokenSpent'));
}


export class AEInteractionGUIForActor extends AEInteractionGUI {

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            id : "ae-interaction-actor",
            title : game.i18n.localize('AESYSTEM.interaction.sheet.title'),
            classes : ["acariaempire", "sheet", "interaction"],
            template : 'systems/acariaempire/resources/interaction/interaction.hbs',
            popOut : true,
            resizable : false,
            width : 'auto',
            height : 'auto'
        });
    }
    
    constructor(options = {}) {
        super(options);
    }

    /** @override */
    get interactionMode() { return INTERACTION_MODE.ACTION; }

    newInteraction(mainActor, targets) {
        this.mainActorData = new InteractionMainActorData(mainActor);
        this.defenseData = new InteractionDefenseData(mainActor, targets);
        this._cardForRoll = undefined;

        this._riskLevel = 0;
        this.state.waitingAnswer = false;
        this.state.hasBeenAnswered = false;

        this._init = true;
    }

    /**
     * Choose the card to use inside roll
     * @param {AEEventCard} card 
     */
    chooseCardForRoll(card) {
        this._cardForRoll = card;
        this.render();
    }

/*------------------------------------------
    Listeners
--------------------------------------------*/

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);
        generateWALinks(html);

        // First choice: Actor action
        //------------------------------
        html.find(".first-choice .roll-category").click( (event) =>this._onClickChangeFirstChoice(event));
        html.find(".first-choice select").change( (event) => this._onChangeSelectionForMainActor(event));
        // - clicks on reminder (only custom bonus for now)
        html.find(".first-choice .reminder.clickable .token-value img").click( (event) => this._onClickChangeReminderBonus(event, this.mainActorData.impl, true));
        html.find(".first-choice .reminder.clickable .token-value img").contextmenu( (event) => this._onClickChangeReminderBonus(event, this.mainActorData.impl, false));
        html.find(".first-choice .reminder.clickable-tips .tips").click( (event) => this._onClickTriggerReminderTipsAction(event, this.mainActorData.impl));
        // - Specific listeners
        this.mainActorData.impl.activateSpecificListeners(this, html);

        // Middle column: Versus button
        //------------------------------
        html.find(".versus-button.active").click( (event) => this._onClickVersusButton(event) );

        // Final step: Finishing touch
        //------------------------------
        html.find(".risk-section .change-risk").click( (event) => this._onClickChangeRisk(event));
        html.find(".risk-section .show-stats").click( (event) => this._onClickWhisperRollChances(event));
        html.find(".action-section .change-action").click( (event) => this._onClickChangeActionCategory(event));
        html.find(".result-section .roll-button.active").click( (event) => this._onClickDoAction(event));

        html.find(".bottom-side-panel.card-choice.available").click( (event) => this._onClickChooseEventCard(event));
        html.find(".bottom-side-panel.card-choice.chosen .roll-bonus").click( (event) => this._onClickChooseEventCard(event));
        html.find(".bottom-side-panel.card-choice.chosen .roll-bonus").contextmenu( (event) => this._onClickRemoveEventCard(event));

        html.find(".bottom-side-panel.trouble.active .trouble-img").click( (event) => this._onClickDisplayTrouble(event));

        // Activate right panel listeners if player is autonomous
        if( !this.defenseData.impl.needGMInputs ) {
            this.listenersForDefenseChoices(html);
        }
    }

    /** Ask GM to set defense */
    async _onClickVersusButton(event) {
        event.preventDefault();
        this.state.waitingAnswer = true;
        this.render();

        const result = await askGMForInteractionDifficulty(this.mainActorData, this.defenseData);

        this.state.waitingAnswer = false;
        this.state.hasBeenAnswered = !!result;
        if( this.state.hasBeenAnswered ) {
            this.defenseData.changeMethod(result.method);
            this.defenseData.impl.fromSocket(result);
        }
        this.render();
    }

    /** Inside first-choice: Change mainActorData implementation */
    async _onClickChangeFirstChoice(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const choiceKey = a.dataset.key;

        let panelChanged = false;
        if( a.dataset.choiceType == 'parent' ) {
            panelChanged = this.mainActorData.changeParentMethod(choiceKey);
        } else {
            panelChanged = this.mainActorData.changeMethod(choiceKey);
        }

        if( panelChanged ) {
            this.state.hasBeenAnswered = false;
            this.state.waitingAnswer = false;
        }

        this.render();
    }

    /** Inside first-choice: Alter mainActorData choices */
    async _onChangeSelectionForMainActor(event) {

        event.preventDefault();
        const a = event.currentTarget;
        const select = a.dataset.selection;
        const choice = a.value;

        if( select == 'firstListbox' ) {
            this.mainActorData.impl.firstListboxChangeChoice(choice);

        } else if( select == 'secondListbox' ) {
            this.mainActorData.impl.secondListboxChangeChoice(choice);

        } else if( select == 'thirdListbox' ) {
            this.mainActorData.impl.thirdListboxChangeChoice(choice);

        } else if( select == 'fourthListbox' ) {
            this.mainActorData.impl.fourthListboxChangeChoice(choice);
            
        } else {
            console.err('Select type not found : ' + select)
        }

        this.render(true);
    }

    /** Inside risk-section: Change taken risk [0-3] dices */
    async _onClickChangeRisk(event) {

        event.preventDefault();
        const a = event.currentTarget;
        const level = a.dataset.level;

        this._riskLevel = parseInt(level);
        this.render(true);
    }

    /** Inside action-section: Change action duration {instant, fast, classic} */
    async _onClickChangeActionCategory(event) {

        event.preventDefault();
        const a = event.currentTarget;
        const action = a.dataset.action;

        this.mainActorData.impl.actionCategory = action;
        this.render(true);
    }

    /** Inside interaction-section: Display current chances on chat */
    async _onClickWhisperRollChances(event) {
        event.preventDefault();
        game.aedice.whisperRollChances({
            score: this.rollValue.scoreBase, 
            cardBalance: this.rollValue.cardBalance, 
            risk: this._riskLevel
        });
    }

    /** Open your hand to choose an event card */
    async _onClickChooseEventCard(event) {
        const cardStacks = game.modules.get('ready-to-use-cards').cardStacks;
        cardStacks.myHand.stack.sheet.render(true);
    }

    /** Open your hand to choose an event card */
    async _onClickRemoveEventCard(event) {
        this.chooseCardForRoll(undefined);
    }

    /** Open your revelead cards to display you guardian spirit troubles */
    async _onClickDisplayTrouble(event) {
        const trouble = this.mainActorData.impl.mainActor.cards.troubles[0];
        trouble?.card.sheet.render(true);
    }

    /**
     * Apply modification due to the chosen card before rolling dice.
     * Most of them will directly impact the actor and won't interfere with the roll
     * But some reduce the amount of roll dice and add the remaining ones with fixed values.
     * This is what this methods return so that it can be taken into account when rolling them
     * @returns 
     */
    async applyModifFromEventCard() {

        let replacedDiceFace = [];

        if(this._cardForRoll) { 
            const actor = this.mainActorData.impl.mainActor;
            const onroll = this._cardForRoll.eventDefinition.onroll;
    
            if( onroll.key === "token_success" ) {
                await actor.setAdvantage(actor.advantage + 1);
    
            } else if( onroll.key === "token_cardbalance" ) {
                await actor.setMastery(actor.mastery + 2);
    
            } else if( onroll.key === "health" ) {
                await actor.healDamage(2);
    
            } else if( onroll.key === "initiative" ) {
                await actor.spendTime(-3);
    
            } else if( onroll.key === "die_success") {
                replacedDiceFace = [5];
    
            } else if( onroll.key === "die_power") {
                replacedDiceFace = [2];
            }

            // Card has been used => discarding it
            const cardStacks = game.modules.get('ready-to-use-cards').cardStacks;
            await cardStacks.myHand.discardCards([this._cardForRoll.card.id]);
            this._cardForRoll = undefined;
        }


        return replacedDiceFace;
    }

/*------------------------------------------
    Do the action !
--------------------------------------------*/

    /** Inside roll-section: Roll the dices! */
    async _onClickDoAction(event) {

        event.preventDefault();
        const a = event.currentTarget;
        const automaticSuccess = a.dataset.type == 'automatic';

        const main = this.mainActorData.impl;
        const def = this.defenseData.impl;

        try {
            this.doAction_assertVality();
            await consumePowerIfNeeded(main);

            // Update timetracker
            this.doAction_updateTimetracker();

            // Roll dice or alternative
            if( def.actuallyRollDice ) {
                let replacedDiceFaceFromCard = await this.applyModifFromEventCard();
                const replacedDiceFace = automaticSuccess ? [5, 1, 1] : replacedDiceFaceFromCard;
                this.doAction_rollDice(replacedDiceFace);
            } else {
                def.doThisInteadOfRollingDices();
            }

            // Close the window
            this.close();

        } catch( e ) {
            ui.notifications.warn(e);
        }
    }

    doAction_assertVality() {
        if( !this.mainActorData.impl.isValid ) {
            throw translate('AESYSTEM.interaction.sheet.err.invalidMainActorChoice');
        }

        if( !this.defenseData.impl.isValid ) {
            throw translate('AESYSTEM.interaction.sheet.err.invalidDefenseChoice');
        }
    }

    
    /**
     * Resolve the interaction
     * @param {int[]} replacedDiceFace Dice which will not really be rolled
     */
    doAction_rollDice(replacedDiceFace=[]) {

        const roll = this.rollValue;

        const main = this.mainActorData.impl;
        const mainActor = main.mainActor;
        const actorScore = main.score;

        const def = this.defenseData.impl;
        const defender = def.mainDefender;
        const defenseScore = def.score;
        const defDetails = [].concat(def.defenseDetails);
        if( replacedDiceFace.length == 3 ) {
            // Add a details if automatic success
            defDetails.push( def._textLineDetail(
                translate('AESYSTEM.interaction.sheet.details.automaticSuccess'), 
                ''
            ));
        }

        let context = {
            roll: roll,
            actor : {
                tokenId: mainActor?.tokenId ?? null,
                id: mainActor.id,
                portrait: main.img,
                advantage: '' + actorScore,
                actionName: main.actionName,
                actionFlavor: main.actionFlavor,
                weaponId: main.weaponId,
                isBarehanded: main.isBarehanded,
                details: main.actionDetails
            },
            defense : {
                tokenId: defender?.tokenId ?? null,
                id: defender?.id ?? null,
                portrait: def.putImgOnRollContext ? def.img : null,
                advantage: '' + defenseScore,
                hideDefense: def.hideDefense,
                details: defDetails,
                allTargets: def.allTargets
            }, 
            arcane: {
                triggerSpell: main.needToTriggerArcaneCircle ? main.triggeredArcaneCircle : null,
                buildCircle : def.circleDataForRollContext
            },
            communion: main.needToConsumeSpirits ? main.miracleDataForRollContext : def.communionDataForRollContext,
            melody: def.melodyDataForRollContext,
            onSuccess: main.toDisplayOnSuccess.concat(def.toDisplayOnSuccess)
        }

        // Launch the dices
        game.aedice.rollDice({
            tokenId : mainActor.tokenId,
            scoreBase : roll.scoreBase,
            risk : this._riskLevel,
            context: context,
            replacedDiceFace: replacedDiceFace
        });
    }

    doAction_updateTimetracker() {

        const main = this.mainActorData.impl;
        const mainActor = main.mainActor;

        const def = this.defenseData.impl;
        const defender = def.mainDefender;

        // - Main Actor
        mainActor.spendTime( main.neededTimeForAction );

        // - Defense
        if( main.targetNeedToDefendItself ) {
            defender?.spendTime( def.defenseMode.neededTime );
        }

    }


}