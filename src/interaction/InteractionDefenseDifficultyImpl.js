import {translate} from '../tools/Translator.js';
import { InteractionDefenseInterface } from './InteractionDefenseInterface.js';

const NO_ACTOR_IMG = 'systems/acariaempire/resources/interaction/choices/difficulty.png';

export class InteractionDefenseDifficultyImpl extends InteractionDefenseInterface {

    constructor(mainActor, targets) {
        super('difficulty', mainActor, targets);
        this.firstListboxInit();
    }

    /** @override */
    get img() { return NO_ACTOR_IMG; }

    /** @override */
    get putImgOnRollContext() { return false; }

    /** @override */
    get defenseDetails() {
        const result = super.defenseDetails;
        
        result.push( this._tokenLineDetail(
            translate('AESYSTEM.interaction.sheet.difficulty.' + this.difficulty), 
            {defenseScore: this.difficulty}
        ));
        return result;
    }

    /* ---------------------------
      First lisbox: Difficulty
    ------------------------------ */

    firstListboxInit() {
        this.difficulty = 0;

        // Title
        this.difficultyTitle = translate('AESYSTEM.interaction.sheet.details.difficulty');

        // Available choices
        this.difficultyAvailableChoices = [-1, 0, 1, 2, 3, 4, 5, 6].map(d => {
            return {
                key: '' + d,
                label: translate('AESYSTEM.interaction.sheet.difficulty.' + d )
            };
        });
    }

    /** @override */
    get firstListboxDisplayed() { return true; }

    /** @override */
    get firstListboxTitle() { return this.difficultyTitle; }

    /** @override */
    get firstListboxAvailabeChoices() { return this.difficultyAvailableChoices; }

    /** @override */
    get firstListboxCurrentChoice() { return '' + this.difficulty; }

    /** @override */
    get firstListboxCurrentScore() { return this.difficulty; }

    /** @override */
    firstListboxChangeChoice(newDifficulty) {
        this.difficulty = parseInt(newDifficulty);
    }
}

