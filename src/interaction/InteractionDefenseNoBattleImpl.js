import {translate} from '../tools/Translator.js';
import { InteractionDefenseInterface } from './InteractionDefenseInterface.js';
import { attributeList } from '../tools/WaIntegration.js';

const NOT_SELECTED = 'none';

export class InteractionDefenseNoBattleImpl extends InteractionDefenseInterface {

    constructor(mainActor, targets) {
        super('nobattle', mainActor, targets);

        if( !this.mainDefender ) { return; } // Needs a mainDefender to work properly
        this.exploreAbility = this.mainDefender.abilities.exploreAbility;
        this.intrigueAbility = this.mainDefender.abilities.intrigueAbility;

        this.firstListboxInit();
        this.secondListboxInit();
        this.thirdListboxInit();
    }

    /** @override */
    get defenseDetails() {
        const result = super.defenseDetails;

        // Explore ability
        const ability = this.nonCombatElement.ability;
        if( ability ) {
            result.push( this._tokenLineDetail(
                this.nonCombatElement.label, 
                {defenseScore: ability.level} 
            ));            
        }

        // Compatibility
        if( ability && this.compatibilityChoice != 'compatible' ) {

            const modifier = this.calculateCompatibilityModifier();
            result.push( this._tokenLineDetail(
                translate('AESYSTEM.interaction.sheet.labels.compatibility'), 
                {cardBalance: modifier.masteryModifier, defenseScore: modifier.advantageModifier}
            ));
        }

        // Attribute
        if( this.attributeChoice != NOT_SELECTED ) {
            result.push( this._textLineDetail(
                translate('AESYSTEM.interaction.sheet.labels.attribute'), 
                attributeList().find( a => a.key === this.attributeChoice ).name
            ));            
            result.push( this._tokenLineDetail(
                translate('AESYSTEM.interaction.sheet.labels.attribute'), 
                {defenseScore: ability.level} 
            ));
        }

        return result;
    }    

    /** @override */
    get needsTarget() {
        return true;
    }
    
    /* ---------------------------
      First lisbox: Attribute
    ------------------------------ */

    firstListboxInit() {

        this.allNonCombatChoices = [
            {
                key: 'explore',
                label: translate('AESYSTEM.interaction.sheet.mainActor.method.explore.title'),
                ability: this.exploreAbility,
                modifier: {
                    displayed: true,
                    advantageModifier: this.exploreAbility.level,
                    masteryModifier: this.exploreAbility.level
                }
            },
            {
                key: 'intrigue',
                label: translate('AESYSTEM.interaction.sheet.mainActor.method.intrigue.title'),
                ability: this.intrigueAbility,
                modifier: {
                    displayed: true,
                    advantageModifier: this.intrigueAbility.level,
                    masteryModifier: this.intrigueAbility.level
                }
            }
        ];

        // Title
        this.nonCombatTitle = translate('AESYSTEM.interaction.sheet.labels.compatibility');

        // Available choices
        this.firstListboxChangeChoice('explore');
    }

    /** @override */
    get firstListboxDisplayed() { return true; }

    /** @override */
    get firstListboxTitle() { return this.nonCombatTitle; }

    /** @override */
    get firstListboxAvailabeChoices() { return this.allNonCombatChoices; }

    /** @override */
    get firstListboxCurrentChoice() { return this.nonCombatChoice; }

    /** @override */
    get firstListboxCurrentScore() { 
        
        const modifier = this.nonCombatElement.modifier;
        if( ! modifier.displayed ) {
            return null; // <= Not displayed
        }
        
        return modifier.advantageModifier;
    }

    /** @override */
    firstListboxChangeChoice(newChoice) {
        this.nonCombatChoice = newChoice;
        this.nonCombatElement = this.allNonCombatChoices.find(c => c.key === this.nonCombatChoice);
    }

    /* ---------------------------
          Second lisbox: Skill
    ------------------------------ */

    secondListboxInit() {

        this.attributeChoice = NOT_SELECTED;
        this.attributeLevel = 0;

        // Title
        this.attributeTitle = translate('AESYSTEM.interaction.sheet.labels.attribute');

        // Available choices
        this.attributeAvailableChoices = attributeList().map( a => { 
            return {
                key: a.key,
                label: a.name
            };
        });
        this.attributeAvailableChoices.unshift({key: NOT_SELECTED, label: translate('AESYSTEM.interaction.sheet.list.attribute.none') });
    }


    /** @override */
    get secondListboxDisplayed() { return true; }

    /** @override */
    get secondListboxTitle() { return this.attributeTitle; }

    /** @override */
    get secondListboxAvailabeChoices() { return this.attributeAvailableChoices; }

    /** @override */
    get secondListboxCurrentChoice() { return this.attributeChoice; }

    /** @override */
    get secondListboxCurrentScore() { return this.attributeLevel; }

    /** @override */
    secondListboxChangeChoice(newChoice) {
        this.attributeChoice = newChoice;

        const checkLevel = this.attributeChoice != NOT_SELECTED && this.mainDefender != null;
        this.attributeLevel = checkLevel ? this.mainDefender.getAttributeLevel(this.attributeChoice) : 0;
    }

    /* ---------------------------
          Third lisbox: Compatibility
    ------------------------------ */

    thirdListboxInit() {


        const noCompatibilityCalculator = (ability) => {
            return {
                advantageModifier: 0 - Math.min(1, ability.level),
                masteryModifier: 2
            };
        };
        const smallCompatibilityCalculator = (ability) => {
            return {
                advantageModifier: 0,
                masteryModifier: 1
            };
        };
        const okCompatibilityCalculator = (ability) => {
            return {
                advantageModifier: 0,
                masteryModifier: 0
            };
        };



        this.allCompatibilityChoices = [
            {
                key: 'noCompatibility',
                label: translate('AESYSTEM.interaction.sheet.mainActor.compatibility.no'),
                calculator: noCompatibilityCalculator
            },
            {
                key: 'smallCompatibility',
                label: translate('AESYSTEM.interaction.sheet.mainActor.compatibility.small'),
                calculator: smallCompatibilityCalculator
            },
            {
                key: 'compatible',
                label: translate('AESYSTEM.interaction.sheet.mainActor.compatibility.ok'),
                calculator: okCompatibilityCalculator
            }
        ];

        // Title
        this.compatibilityTitle = translate('AESYSTEM.interaction.sheet.labels.compatibility');

        // Available choices
        this.thirdListboxChangeChoice('compatible');
    }

    /** @override */
    get thirdListboxDisplayed() { return true; }

    /** @override */
    get thirdListboxTitle() { return this.compatibilityTitle; }

    /** @override */
    get thirdListboxAvailabeChoices() { return this.allCompatibilityChoices; }

    /** @override */
    get thirdListboxCurrentChoice() { return this.compatibilityChoice; }

    /** @override */
    get thirdListboxCurrentScore() { return this.calculateCompatibilityModifier().advantageModifier; }

    get thirdListboxCurrentTokens() {return this.calculateCompatibilityModifier().masteryModifier;}

    /** @override */
    thirdListboxChangeChoice(newChoice) {
        this.compatibilityChoice = newChoice;
        this.compatibilityElement = this.allCompatibilityChoices.find(c => c.key === this.compatibilityChoice);
    }

    calculateCompatibilityModifier() {
        const ability = this.nonCombatElement.ability;
        return ability ? this.compatibilityElement.calculator(ability) : {advantageModifier: 0, masteryModifier: 0};
    }

}
