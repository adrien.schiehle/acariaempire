import {translate} from '../tools/Translator.js';

/** Methods that should be implemented by every InteractionDefenseXXXXXImpl */
export class InteractionDefenseInterface {

    constructor(methodName, mainActor, targets) {
        this._method = methodName;
        this._mainActor = mainActor;
        this._targets = targets;
        this.customDefense = 0;
        this.customCardBalance = 0;
        this._answered = false;
    }

    get targetsTitle() {
        if( this._targets.length == 1 ) {
            return this._targets[0].name;
        }
        if( this._targets.length > 1 ) {
            return translate('AESYSTEM.interaction.sheet.defense.targets.multiple');
        }
        return translate('AESYSTEM.interaction.sheet.defense.targets.none');
    }

    get mainDefender() {
        return this._targets.length > 0 ? this._targets[0] : null;
    }

    get method() {
        return this._method;
    }

    get methodName() {
        return translate('AESYSTEM.interaction.sheet.defense.method.' + this.method + '.choice');
    }

    get img() {
        return this.mainDefender?.img ?? null;
    }

    // May be overriden
    get putImgOnRollContext() { return this.img != null; }

    // May be overriden
    get defenseName() {
        return translate('AESYSTEM.interaction.sheet.defense.method.' + this.method + '.title');
    }

    // May be overriden
    get defenseDetails() {
        const result = [];

        result.push( this._textLineDetail (
            translate('AESYSTEM.interaction.sheet.labels.defense'), 
            this.defenseName
        ));

        for( let line of this.reminders ) {
            const options = {}
            if( line.score ) {options.defenseScore = line.score.value; }
            if( line.tokens ) {options.cardBalance = line.tokens.value; }
            result.push( this._tokenLineDetail( line.title, options) );
        }

        // From defenseMode (only added for "ignored" since it's the only one giving a malus)
        if( this.defenseMode.score != 0 || this.defenseMode.token != 0 )  {
            const options = {}
            if( this.defenseMode.score ) { options.defenseScore = this.defenseMode.score; }
            if( this.defenseMode.tokens ) { options.cardBalance = this.defenseMode.tokens; }
            result.push( this._tokenLineDetail ( this.defenseMode.label, options ) );
        }

        return result;
    }

    // Will be given to interaction roll. May be used once roll is resolved (See DiceInterpretor)
    get allTargets() {
        return this._targets.map( t => t.tokenId );
    }

    get allTargetsUuid() {
        return this._targets.map( t => t.uuid );
    }

    // May be overriden
    get needsTarget() {
        return false;
    }

    /**
     * If we need to ask GM for inputs or if the user can do everything
     */
    get needGMInputs() {
        return true;
    }
    
    /** 
     * Allow the main GUI to know if the user can go further or if the current choices are not valid
     * May be overriden 
     */
    get isValid() {
        return true;
    }

    /**
     * If set to true, defense values will be hidden in the GUI
     */
    get hideDefense() {
        return false;
    }

    
    get allDefenseOptions() {
        if( ! this._allDefenseOptions ) {
            this._allDefenseOptions =  ['focused', 'classic', 'ignore'].map( option => {
                return {
                    key : option,
                    label : translate('AESYSTEM.interaction.sheet.defenseMode.' + option),
                    neededTime: option == 'ignore' ? 0 : (option == "classic" ? 2 : 4),
                    score: option == 'ignore' ? -1 : 0,
                    tokens: option == 'ignore' ? 2 : 0
                };
            });
        }
        return this._allDefenseOptions;
    }

    get defenseMode() {
        
        if( !this._defenseMode ) {
            const options = this.availableDefenseOptions;
            if( options.length > 0 ) { this._defenseMode = options[0]; }
        }
        return this._defenseMode;
    }

    setDefenseMode(defKey) {
        const newDefMode = this.availableDefenseOptions.find( o => o.key === defKey );
        if( newDefMode ) {
            this._defenseMode = newDefMode;
        }
    }

    get availableDefenseOptions() {
        return this.allDefenseOptions.filter(o => o.key == 'focused');
    }

    get listBoxes() {
        const result = [];
        if( this.firstListboxDisplayed ) {
            const data = {
                bind: 'firstListbox',
                title: this.firstListboxTitle,
                choices: this.firstListboxAvailabeChoices,
                choice: this.firstListboxCurrentChoice,
                hideScore: this.hideDefense
            };
            let val = this.firstListboxCurrentTokens;
            if( val != null ) { data.tokens = {value: val}; }

            val = this.firstListboxCurrentScore;
            if( val != null ) { data.score = {value: val}; }

            result.push(data);
        }

        if( this.secondListboxDisplayed ) {
            const data = {
                bind: 'secondListbox',
                title: this.secondListboxTitle,
                choices: this.secondListboxAvailabeChoices,
                choice: this.secondListboxCurrentChoice,
                hideScore: this.hideDefense
            };
            let val = this.secondListboxCurrentTokens;
            if( val != null ) { data.tokens = {value: val}; }

            val = this.secondListboxCurrentScore;
            if( val != null ) { data.score = {value: val}; }

            result.push(data);
        }

        if( this.thirdListboxDisplayed ) {
            const data = {
                bind: 'thirdListbox',
                title: this.thirdListboxTitle,
                choices: this.thirdListboxAvailabeChoices,
                choice: this.thirdListboxCurrentChoice,
                hideScore: this.hideDefense
            };
            let val = this.thirdListboxCurrentTokens;
            if( val != null ) { data.tokens = {value: val}; }

            val = this.thirdListboxCurrentScore;
            if( val != null ) { data.score = {value: val}; }

            result.push(data);
        }
        return result;        
    }

    /** Should be overriden in classes that needs it  */
    activateSpecificListeners(sheet, html) {}

    get hasSimpleMessagesToDisplay() { return this.simpleMessagesToDisplay.length > 0; }
    get simpleMessagesToDisplay() { return []; }
    get actuallyRollDice() { return true; }
    doThisInteadOfRollingDices() {}

    /**
     * May be completed by subclasses
     * By default only add the situation adjustment
     */
    get reminders() { 
        const adjustment = {
            title : translate('AESYSTEM.interaction.sheet.labels.context'),
            tips : translate('AESYSTEM.interaction.sheet.labels.contextTips'),
            score: {value: this.customDefense},
            tokens: {value: this.customCardBalance },
            css: 'clickable',
            ref: 'custom',
            hideScore: this.hideDefense
        };
        return [adjustment]; 
    }

    /** @override */
    async reminderClicked(ref, type, leftClick) { 
        if( ref == 'custom' ) {
            if( type == 'score' ) {
                this.customDefense += leftClick ? 1 : -1;
            } else {
                this.customCardBalance += leftClick ? 1 : -1;
            }
        }
    }


    get firstListboxDisplayed() { return false; }
    get firstListboxTitle() {}
    get firstListboxAvailabeChoices() {}  //  [{key: x , label: x }, ...{}]
    get firstListboxCurrentChoice() {}  // choiceKey
    get firstListboxCurrentTokens() {return null;}  // int
    get firstListboxCurrentScore() {return null;}  // int
    firstListboxChangeChoice(newChoice) {}

    get secondListboxDisplayed() { return false; }
    get secondListboxTitle() {}
    get secondListboxAvailabeChoices() {}  // [{key: x , label: x }, ...{}]
    get secondListboxCurrentChoice() {}  // choiceKey
    get secondListboxCurrentTokens() {return null;}  // int
    get secondListboxCurrentScore() {return null;}  // int
    secondListboxChangeChoice(newChoice) {}

    get thirdListboxDisplayed() { return false; }
    get thirdListboxTitle() {}
    get thirdListboxAvailabeChoices() {}  // [{key: x , label: x }, ...{}]
    get thirdListboxCurrentChoice() {}  // choiceKey
    get thirdListboxCurrentTokens() {return null;}  // int
    get thirdListboxCurrentScore() {return null;}  // int
    thirdListboxChangeChoice(newChoice) {}

    get circleDisplayed() { return false; }     // Method for modifying it are specific to only class, will be handled directly in subclass
    get circleDataForRollContext() {return null;}

    get communionDisplayed() { return false; }  // Method for modifying it are specific to only class, will be handled directly in subclass
    get communionDataForRollContext() {return null;}

    get melodyDisplayed() { return false; }     // Method for modifying it are specific to only class, will be handled directly in subclass
    get melodyDataForRollContext() {return null;}

    get toDisplayOnSuccess() { return []; } // Array of { icon, name, (optional)shortcut } that will be displayed on message if the action is a success
    
    /*-----------------------
       Score and tokens
    --------------------------*/

    /** 
     * Shouldn't be overriden. Use reminders, and boxes if you want to change tokens 
     * @returns {int} card balance token amount
     * */    
     get tokens() {
        let result = 0;

        // From defenseMode (actually, only ignored gives an impact)
        result += this.defenseMode.tokens;

        // boxes
        for( const box of this.listBoxes ) {
            if( box.tokens ) {
                result += box.tokens.value;
            }
        }

        // reminders
        for( const reminder of this.reminders ) {
            if( reminder.tokens ) {
                result += reminder.tokens.value;
            }
        }

        return result;
    }

    /** 
     * Shouldn't be overriden. Use reminders, and boxes if you want to change tokens 
     * @returns {int} base score the character will have for this interaction
     * */    
     get score() {
        let result = 0;

        // From defenseMode (actually, only ignored gives an impact)
        result += this.defenseMode.score;
        
        // boxes
        for( const box of this.listBoxes ) {
            if( box.score ) {
                result += box.score.value;
            }
        }

        // reminders
        for( const reminder of this.reminders ) {
            if( reminder.score ) {
                result += reminder.score.value;
            }
        }

        return result;
    }

    /*-----------------------
       Necessay data for socket
    --------------------------*/

    forSocket() {
        return {
            method: this.method,
            custom: {
                cardBalance: this.customCardBalance,
                defense: this.customDefense
            },
            defenseMode: this.defenseMode.key,
            choices: this.listBoxes.map( b => b.choice )
        }
    }

    fromSocket(data) {
        this.customCardBalance = data.custom.cardBalance;
        this.customDefense = data.custom.defense;

        this.setDefenseMode(data.defenseMode)

        let lastUpdatedIndex = -1;
        data.choices.forEach( c => {
            lastUpdatedIndex = this.loadChoice(c, lastUpdatedIndex);
        });
    }


    /**
     * @private
     * Used to load choices one by one.
     * 
     * @param {string} choice choice key, as retrieved from socket
     * @param {int} lastUpdatedIndex which choice was updated last
     * @returns last updated choice, once done
     */
    loadChoice( choice, lastUpdatedIndex ) {

        if( lastUpdatedIndex < 0 && this.firstListboxDisplayed ) {
            this.firstListboxChangeChoice(choice);
            return 0;
        }

        if( lastUpdatedIndex < 1 && this.secondListboxDisplayed ) {
            this.secondListboxChangeChoice(choice);
            return 1;
        }
        
        if( lastUpdatedIndex < 2 && this.thirdListboxDisplayed ) {
            this.thirdListboxChangeChoice(choice);
            return 2;
        }

        return 3;
    }

    /*-----------------------
       Convenience methods
    --------------------------*/

    /**
     * Format a detail so that it can be later displayed inside roll chat message.
     * This method allow token images
     * @param {string} title detail header
     * @param {int} [baseScore] will be added with its related icon
     * @param {int} [defenseScore] idem
     * @param {int} [cardBalance] Related icon will differ if the amount is positive or negative
     */
    _tokenLineDetail(title, {baseScore=null, defenseScore=null, cardBalance=null}) {
        
        const result = {
            title: title, 
            tokens: []
        };

        if( baseScore != null ) {
            const icon = 'systems/acariaempire/resources/interaction/tokens/action_score.png';
            result.tokens.push({text: '' + baseScore, icon: icon});
        }

        if( defenseScore != null ) {
            const icon = 'systems/acariaempire/resources/interaction/tokens/defense_score.png';
            result.tokens.push({text: '' + defenseScore, icon: icon, canBeHidden: true});
        }

        if( cardBalance != null ) {
            const icon = 'systems/acariaempire/resources/interaction/tokens/card_balance_' + (cardBalance < 0 ? 'negative.png' : 'positive.png');
            result.tokens.push({text: '' + Math.abs(cardBalance), icon: icon});
        }

        return result;
    }

    /**
     * Format a detail so that it can be later displayed inside roll chat message.
     * This method only allow a text detail
     * @param {string} title detail header
     * @param {string} text detail text
     */
     _textLineDetail(title, text) {
        return {
            title: title, 
            text: text
        };
    }
}


