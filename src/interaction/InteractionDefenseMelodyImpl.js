import { finalizeSpellData } from '../tools/spells.js';
import { translate } from '../tools/Translator.js';
import { allSpellsDefinition, songsMelodies } from '../tools/WaIntegration.js';

import { InteractionDefenseInterface } from './InteractionDefenseInterface.js';

const MELODY_IMG = 'systems/acariaempire/resources/interaction/songs.png';

const melodyToken = (type) => {
    return 'systems/acariaempire/resources/interaction/melody_' + type + '.png';
}

export class InteractionDefenseMelodyImpl extends InteractionDefenseInterface {

    constructor(mainActor, targets) {
        super('melody', mainActor, targets);
        this.resetMelodyInfo();
    }

    /** 
     * Allow the main GUI to know if the user can go further or if the current choices are not valid
     * @override
     */
     get isValid() {
        return !this.melody.choosing;
    }
    
    /** 
     * User can upgrade his songs alone
     * @override
     */
     get needGMInputs() {
        return false;
    }

    /* ---------------------------
        Reminder : Melody upgrade
    ------------------------------ */

    /** @override */
    get reminders() { 

        const result = [{
            title: translate('AESYSTEM.interaction.sheet.melody.diffMelodyLevel'), 
            score: { value: this.melody.newLevel }
        }];
        if( this.melody.enhancementSuppl > 0 ) {
            result.push({
                title: translate('AESYSTEM.interaction.sheet.melody.diffEnhancementSuppl'), 
                score: {value: 2*this.melody.enhancementSuppl}
            });
        }
        if( this.melody.enhancementCount > 0 && this.melody.dissonance > 0 ) {
            result.push({
                title: translate('AESYSTEM.interaction.sheet.melody.diffDissonance'), 
                score: {value: this.melody.dissonance},
                tokens: {value: 0-this.melody.dissonance}
            });
        }

        result.push(...super.reminders);
        return result;
    }

    /** @override */
    get simpleMessagesToDisplay() {

        if( !this.isValid ) { return []; }
        
        const result = [];
        if( this.melody.enhancementCount == 0 ) {
            result.push(
                translate('AESYSTEM.interaction.sheet.defense.songs.refresh.only'),
                translate('AESYSTEM.interaction.sheet.defense.songs.refresh.fastaction'),
                translate('AESYSTEM.interaction.sheet.defense.songs.refresh.benefit')
            );
        }

        result.push(
            translate('AESYSTEM.interaction.sheet.defense.songs.refresh.duration')
        );

        return result;
    }

    /* ---------------------------
        Melody calculus
    ------------------------------ */

    get availableMelodyList() {

        if( ! this._availableMelodyList ) {

            const caster = this._mainActor;
            this._availableMelodyList = caster.songs.melodies.filter( ([key, value]) => {
                return value.level > 0;
             }).map( ([key, value]) => {
                 return {
                     key: key,
                     label: value.name,
                     icon: melodyToken(key)
                 };
             });
         }
         return this._availableMelodyList ?? [];
    }
    

    resetMelodyInfo() {
        const caster = this._mainActor;
        const performance = caster.songs.currentPerformance;
        const currentLevel = performance?.level ?? 0;

        this.melody = {
            newOne: false,
            choosing: ( performance == null ),
            gmAlter: false,
            performance: performance,
            newLevel: currentLevel
        };

        this.updateMelodyModifiers();
    }

    startNewMelody(melodyKey) {

        const caster = this._mainActor;
        const melodyDef = caster.songs.getMelody(melodyKey);
        this.melody = {
            newOne: true,
            choosing: false,
            gmAlter: false,
            performance: {
                melodyKey: melodyDef.melody,
                melodyName: melodyDef.name,
                level: 0,
                shortcut: melodyDef.shortcut,
                shortcutChild: melodyDef.shortcutChild,
                dissonance: 0
            },
            newLevel: 1
        };

        this.updateMelodyModifiers();
    }

    updateMelodyModifiers() {

        // Current melody icon and displayedLevel
        this.melody.icon = melodyToken(this.melody.performance?.melodyKey ?? '');

        const currentLevel = this.melody.performance?.level ?? 0;
        const enhancementCount = this.melody.newLevel - currentLevel; // newOne already taken into accound with performance.level = 0
        if( this.melody.gmAlter ) {
            this.melody.displayedLevel = currentLevel;
        } else {
            this.melody.displayedLevel = this.melody.newLevel + ' (+'  + enhancementCount + ')';
        }

        // Right text description
        const label = this.melody.gmAlter ? 'gmAlterMelody' : (this.melody.choosing ? 'choosingMelody' : ( this.melody.newOne ? 'newMelody' : 'upgradeMelody' ));
        this.melody.subtitle = translate('AESYSTEM.interaction.sheet.labels.' + label);

        //Right buttons
        const btns = [];
        if(game.user.isGM ) {
            // GM have additional options
            if(this.melody.gmAlter)        { btns.push( { action: 'stopAlter', icon: 'far fa-window-close' } ); } 
            else if(!this.melody.choosing) { btns.push( { action: 'startAlter', icon: 'fas fa-wrench' } ); }
        }

        if(! this.melody.gmAlter) {
            // When altering, no other options are available
            if(this.melody.choosing) { 
                if( this.melody.performance != null) { btns.push( { action: 'forget', icon: 'fas fa-undo-alt' } ); }
            } else { 
                btns.push( { action: 'new', icon: 'fas fa-trash' } );  
            }
        }
        this.melody.rightBtns = btns;

        // Dissonance gives +1 <+1> for each stack
        const dissonance = this.melody.newOne ? 0 : (this.melody.performance?.dissonance ?? 0);
        this.melody.dissonance = dissonance;
        this.melody.dissonanceDisplayed = this.melody.gmAlter || dissonance > 0;
        this.melody.dissonanceIcon = melodyToken('dissonance');

        // For each enhancement after the first one : +1 <+1>
        const enhancementSuppl = Math.max(0, enhancementCount - 1);

        this.melody.enhancementCount = enhancementCount;
        this.melody.enhancementSuppl = enhancementSuppl;
    }

    /** @override */
    get img() { return MELODY_IMG; }

    /** @override */
    get putImgOnRollContext() { return false; }

    /** @override */
    get defenseDetails() {
        const result = super.defenseDetails;
        
        if( this.melody.performance ) {
            const title = translate('AESYSTEM.interaction.sheet.details.' + ( this.melody.newOne ? 'newMelody' : 'intensifyMelody') );
            result.push( this._textLineDetail(
                title, 
                this.melody.performance.melodyName
            ));
        }

        if( !this.melody.newOne ) {
            result.push( this._textLineDetail(
                translate('AESYSTEM.interaction.sheet.details.previousMelodyLevel'), 
                'Lvl ' + this._mainActor.songs.performance?.level ?? 0
            ));
        }

        return result;
    }

    /* ---------------------------
            Melody data
    ------------------------------ */

    /** @override */
    get melodyDisplayed() { return true; }

    /** @override */
    get melodyDataForRollContext() {
        
        if( ! this.melody.performance ) { 
            throw translate("AESYSTEM.interaction.sheet.err.noPerformanceSelected");
        }

        return {
            melodyKey: this.melody.performance.melodyKey,
            melodyLevel: this.melody.newLevel,
            dissonance: this.melody.dissonance,
            melodyAsSpell: this._createSpellLikeMelody()
        };
    }

    /**
     * Goal : Display it the same way as the divine or arcane magic. Even if it doesn't work the same way
     * @returns info with the same structure as a spellData from arcane or divine magic
     */
    _createSpellLikeMelody() {

        // Only until info & spell
        const data = {};
        
        // 5 - Spell (Not a real one)
        const levelIcons = allSpellsDefinition().levelIcons;
        const spell = songsMelodies().find( m => this.melody.performance.melodyKey );
        if( spell ) {
            data.spell = {
                level: this.melody.newLevel,
                levelIcon : levelIcons[this.melody.newLevel]
            };
            mergeObject( data.spell, spell ); 
        }

        // 6 - Until
        const baseMelodyDuration = 40;
        const timetracker = this._mainActor.timetracker;
        if( timetracker != null ) {
            data.until = timetracker + baseMelodyDuration;
        }

        finalizeSpellData(this._mainActor, data);
        return data;
    }


    /* ---------------------------
          Right panel handling
    ------------------------------ */

    /** @override */
    activateSpecificListeners(sheet, html) {
        this._sheet = sheet;

        html.find(".melody-actions").on("click", ".melody-button", this._onClickDisplayMelodyList.bind(this));
        html.find(".melody-choice").on("click", ".create-melody", this._onClickStartNewMelody.bind(this));
        html.find(".melody-construction").on("click", ".theme-level", this._onClickUpgradeMelodyTheme.bind(this));
        html.find(".melody-construction").on("click", ".dissonance-level", this._onClickUpgradeMelodyDissonance.bind(this));
        html.find(".melody-construction").on("contextmenu", ".theme-level", this._onClickDowngradeMelodyTheme.bind(this));
        html.find(".melody-construction").on("contextmenu", ".dissonance-level", this._onClickDowngradeMelodyDissonance.bind(this));
    }

    async _onClickDisplayMelodyList(event) {

        event.preventDefault();
        const a = event.currentTarget;
        const choice = a.dataset.action;
        this.melody.choosing = (choice == 'new');
        this.melody.gmAlter = (choice == 'startAlter');

        this.updateMelodyModifiers();

        this._sheet.render(true);
    }

    async _onClickStartNewMelody(event) {

        event.preventDefault();
        const a = event.currentTarget;
        const key = a.dataset.key;
        this.startNewMelody(key);

        this._sheet.render(true);
    }

    async _onClickUpgradeMelodyTheme(event) {

        event.preventDefault();

        if(this.melody.gmAlter && this.melody.performance) {
            const currentLevel = this.melody.performance?.level ?? 0;
            const newLevel = currentLevel + 1;

            await this._mainActor.songs.alterCurrentPerformance(this.melody.performance.melodyKey, newLevel, this.melody.dissonance);
            this.resetMelodyInfo();
            this.melody.gmAlter= true;

        } else {
            this.melody.newLevel = this.melody.newLevel + 1;
        }

        this.updateMelodyModifiers();
        this._sheet.render(true);
    }

    async _onClickDowngradeMelodyTheme(event) {

        event.preventDefault();

        if(this.melody.gmAlter && this.melody.performance) {
            const currentLevel = this.melody.performance?.level ?? 0;
            const newLevel = Math.max( currentLevel - 1, 1 );

            await this._mainActor.songs.alterCurrentPerformance(this.melody.performance.melodyKey, newLevel, this.melody.dissonance);
            this.resetMelodyInfo();
            this.melody.gmAlter= true;

        } else {
            const minLevel = this.melody.newOne ? 1 : (this.melody.performance.level);
            this.melody.newLevel = Math.max( this.melody.newLevel - 1, minLevel );
        }

        this.updateMelodyModifiers();
        this._sheet.render(true);
    }

    async _onClickUpgradeMelodyDissonance(event) {

        event.preventDefault();

        if(! this.melody.gmAlter || !this.melody.performance ) { return; } // Can only be used as GM and when a performance is currently active

        const dissonance = this.melody.performance.dissonance ?? 0;
        await this._mainActor.songs.alterCurrentPerformance(this.melody.performance.melodyKey, this.melody.performance.level, dissonance + 1);
        this.resetMelodyInfo();
        this.melody.gmAlter= true;

        this.updateMelodyModifiers();
        this._sheet.render(true);
    }

    async _onClickDowngradeMelodyDissonance(event) {

        event.preventDefault();

        if(! this.melody.gmAlter || !this.melody.performance ) { return; } // Can only be used as GM and when a performance is currently active

        const dissonance = this.melody.performance.dissonance ?? 0;
        await this._mainActor.songs.alterCurrentPerformance(this.melody.performance.melodyKey, this.melody.performance.level, Math.max( dissonance - 1, 0 ) );
        this.resetMelodyInfo();
        this.melody.gmAlter= true;

        this.updateMelodyModifiers();
        this._sheet.render(true);
    }

}

