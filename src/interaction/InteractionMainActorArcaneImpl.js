import { finalizeSpellData } from '../tools/spells.js';
import { translate } from '../tools/Translator.js';
import { elementList, spellCategories, spellShapes } from '../tools/WaIntegration.js';
import { InteractionMainActorBattleInterface } from './InteractionMainActorInterface.js';

const NOT_SELECTED = 'none';


const retrieveInputChoices = (spellData) => {
    
    const result = new Map();
    const inputs = spellData.inputs ?? [];
    inputs.forEach( input => {
        result.set(input.key, input.currentChoice);
    });
    return result;
}

/**
 * Add an element to spellData.inputs
 * @param {string} inputKey element reference
 * @param {string} title input tite
 * @param {object} spellData parent object
 * @param {object[]} availableChoices available input choices. Shoudl at least have .key, .name
 * @param {Map} currentChoices all inputs current choices
 */
const prepareSpellSubdata = (inputKey, title, spellData, availableChoices, currentChoices) => {

    const newInput = {
        key: inputKey,
        title: title,
        availableChoices: availableChoices
    };

    // Current selection
    let newChoice = currentChoices.get(inputKey);
    if( ! newChoice || !newInput.availableChoices.find( s => s.key === newChoice.key ) ) {
        newChoice = newInput.availableChoices[0] ?? null;
    }
    newInput.currentChoice = newChoice;
    
    // .selected and css init
    newInput.availableChoices.forEach( s => {
        s.selected = s.key === newChoice?.key;
        s.css = s.selected ? 'selected' : '';
    });

    spellData.inputs.push( newInput );
}

const prepareSpellShapes = (spellData, currentChoices) => {

    const inputKey = 'spellShape';
    const title = translate('AESYSTEM.interaction.sheet.labels.spellShape');
    
    const availableChoices = spellData.harmonizations.map( h => {
        const shape = spellShapes().find( s => s.key === h.spellShape );
        return mergeObject( {}, shape );
    });

    prepareSpellSubdata(inputKey, title, spellData, availableChoices, currentChoices);
}

const prepareMainElement = (spellData, currentChoices) => {

    const inputKey = 'mainElement';
    const title = translate('AESYSTEM.interaction.sheet.labels.mainElement');
    
    const availableChoices = spellData.harmonizations.map( h => {
        return mergeObject( {}, h );
    });

    prepareSpellSubdata(inputKey, title, spellData, availableChoices, currentChoices);
}

const prepareArcanePenaly = (actor, spellData, currentChoices) => {

    const inputKey = 'penalty';
    const title = translate('AESYSTEM.interaction.sheet.mainActor.arcane.attunement.title');

    const effectCarac = spellData.caracs.find( c => c.name.startsWith('effect'));
    const effectLevel = effectCarac?.level ?? 0;

    const currentMainElement = spellData.inputs.find( i => i.key === 'mainElement')?.currentChoice?.key ?? 'primary';

    // Attunement penalty management
    const attunementPenalty = actor.arcane.getAttunementPenalty(currentMainElement);
    const effectiveLevel = effectLevel - attunementPenalty.value;
    
    // Details will change if there is no penalty
    let details = '';
    if( attunementPenalty.value > 0 ) {
        const elementName = elementList().find( e => e.key === attunementPenalty.from ).name;
        details = translate('AESYSTEM.interaction.sheet.mainActor.arcane.attunement.details')
            .replace( 'ELEMENT', elementName )
            .replace( 'AMOUNT', attunementPenalty.value );
    } else {
        details = translate('AESYSTEM.interaction.sheet.mainActor.arcane.attunement.none');
    }

    const onlyChoice = {
        key: 'penalty_' + attunementPenalty.value,
        name: details,
        penalty: attunementPenalty.value,
        effectiveLevel: effectiveLevel
    }

    prepareSpellSubdata(inputKey, title, spellData, [onlyChoice], currentChoices);
}

const prepareSpellEffect = (spellData, currentChoices) => {

    const effectName = spellData.caracs.find( c => c.name.startsWith('effect') ).name;
    const spellEffect = spellCategories().find( se => se.effect === effectName );

    const availableSpells = spellData.harmonizations.reduce( (list, element) => {
        element.spells.forEach( spellKey => {

            // Do not insert duplicates
            if( list.findIndex( s => s.key === spellKey ) != -1 ) { return list; }

            const spell = spellEffect.spells.find( s => s.key === spellKey );
            if( spell ) { list.push( mergeObject({}, spell)); }
        });

        return list;
    }, []);
    availableSpells.sort( (a, b) => a.name.localeCompare(b.name) );

    prepareSpellSubdata(effectName, spellEffect.name, spellData, availableSpells, currentChoices);
}

export class InteractionMainActorArcaneImpl extends InteractionMainActorBattleInterface {

    constructor(mainActor) {
        super('arcane', '02magic', mainActor);

        this.spellData = {};
        this.firstListboxInit();
        this.prepareAdditionalData();
    }

    /** @override */
    get maneuverBase() { return this.mainActor.arcane; }

    /** @override */
    get battleScore() { return this.mainActor.battle.arcane; }

    /** @override */
    get isMentalAction() { return true; }

    /** @override */
    get actionName() { 

        return translate('AESYSTEM.interaction.sheet.mainActor.arcane.' + this.attackStyle + '.attack');
    }

    /** @override */
    get actionFlavor() { 

        if( this.spellData.displayed ) {
            return this.spellData.effect?.name ?? '';
        }
        return '';
    }

    /** @override */
    get actionDetails() {
        const result = super.actionDetails;

        result.push( this._textLineDetail(
            translate('AESYSTEM.interaction.sheet.details.arcane'), 
            translate('AESYSTEM.interaction.sheet.mainActor.arcane.attackStyle.' + this.attackStyle)
        ));

        return result;
    }    

    /** @override */
    get targetNeedToDefendItself() { 
        if( this.attackStyle == 'circle' || !this.spellData.circleIsReady ) { 
            return false; 
        }
        return this.spellData.effect.offensive;
    }

    /** 
     * When casting the spell, there is a need for the circle to be built
     * @override 
     */
    get isValid() {
        if( this.attackStyle == 'circle' ) {
            return true;
        } 
        return this.spellData.circleIsReady;
    }


    /** @override */
    get availableDefenseMethods() {
        if( this.attackStyle == 'circle' ) {
            return ['circle'];
        } 
        
        if( this.spellData.circleIsReady && !this.spellData.effect.offensive ) {
            return ['protectiveSpell'];
        }
        return super.availableDefenseMethods;
    }

    /* ---------------------------
          First lisbox: Cast a spell / Build a circle
    ------------------------------ */

    firstListboxInit() {
        this.attackStyle = 'spell';

        // Title
        this.styleTitle = translate('AESYSTEM.interaction.sheet.labels.arcane');

        // Available choices
        this.availableStyles = ['spell', 'circle'].map( s => {
            return {
                key: s,
                label: translate('AESYSTEM.interaction.sheet.mainActor.arcane.attackStyle.' + s)
            };
        });
    }

    /** @override */
    get firstListboxDisplayed() { return true; }

    /** @override */
    get firstListboxTitle() { return this.styleTitle; }

    /** @override */
    get firstListboxAvailabeChoices() { return this.availableStyles; }

    /** @override */
    get firstListboxCurrentChoice() { return this.attackStyle; }

    /** @override */
    get firstListboxCurrentTokens() { return this.mainActor.battle.arcane.mastery; };

    /** @override */
    get firstListboxCurrentScore() { return this.mainActor.battle.arcane.advantage; };

    /** @override */
    firstListboxChangeChoice(newChoice) {

        if( this.attackStyle != newChoice ) {
            this.attackStyle = newChoice;
            this._reloadAvailableManeuvers();
        }
        
    }

    /* ---------------------------
          Second lisbox: Maneuver
    ------------------------------ */

    /** @override */
    filteringManeuvers( key, maneuver ) {
        // Maneuvers are only when castering the spell.
        return this.attackStyle != 'circle';
    }

    /** @override */
    get secondListboxDisplayed() { return this.availableManeuvers.length > 1; }

    /** @override */
    get secondListboxTitle() { return this.maneuverTitle; }

    /** @override */
    get secondListboxAvailabeChoices() { return this.availableManeuvers; }

    /** @override */
    get secondListboxCurrentChoice() { return this.maneuverChoice; }

    /** @override */
    get secondListboxCurrentTokens() { return this.maneuverModifiers.tokens; }

    /** @override */
    get secondListboxCurrentScore() { return this.maneuverModifiers.score; }

    /** @override */
    secondListboxChangeChoice(newChoice) { this.changeManeuver(newChoice); }


    /* ---------------------------
          Right panel handling
    ------------------------------ */

    /** @override */
    prepareAdditionalData() {

        this.spellData.displayed = this.needToTriggerArcaneCircle;

        if( this.spellData.displayed ) {

            const arcane = this.mainActor.arcane;

            // Circle harmonizations
            const uniqueHarmonizations = arcane.circleHarmonizations.reduce( (list,current) => {
                if( !list.find( h => h.key === current.key ) ) { list.push(current); }
                return list;
            }, []);
            this.spellData.harmonizations = uniqueHarmonizations;

            // Circle caracteristics
            this.spellData.caracs = arcane.circleCaracteristics;
            const effectCarac = this.spellData.caracs.find( c => c.name.startsWith('effect'));

            // Inputs
            const currentChoices = retrieveInputChoices(this.spellData);
            this.spellData.inputs = [];
            prepareSpellShapes(this.spellData, currentChoices);
            prepareMainElement(this.spellData, currentChoices);
            prepareArcanePenaly(this.mainActor, this.spellData, currentChoices);

            // Title
            const effectiveLevel = this.spellData.inputs.find( i => i.key === 'penalty').currentChoice.effectiveLevel;
            if( this.spellData.caracs.length == 0 ) {
                this.spellData.title = translate('AESYSTEM.interaction.sheet.mainActor.arcane.title.noCaracs');
                this.spellData.circleIsReady = false;
            } else if( effectiveLevel < 0 ) {
                this.spellData.title = translate('AESYSTEM.interaction.sheet.mainActor.arcane.title.penaltyTooBig');
                this.spellData.circleIsReady = false;
            } else {
                this.spellData.title = translate('AESYSTEM.interaction.sheet.mainActor.arcane.title.ready');
                this.spellData.circleIsReady = true;
            }
            
            // Last input
            if( this.spellData.circleIsReady ) {
                this.spellData.effect = mergeObject( {level: effectiveLevel}, spellCategories().find( e => e.key == effectCarac.name ));
                prepareSpellEffect(this.spellData, currentChoices);
            }
        }

    }


    /** @override */
    get needToTriggerArcaneCircle() { return this.attackStyle == 'spell'; }

    /** @override */
    get triggeredArcaneCircle() {

        if( !this.spellData.circleIsReady ) {
            throw translate("AESYSTEM.interaction.sheet.err.noCircleBuild");
        }

        const caracLevels = this.spellData.caracs.reduce( (levels, carac) => {
            levels[carac.name] = carac.level;
            return levels;
        }, {});

        const inputs = this.spellData.inputs.reduce( (all, current) => {
            all[current.key] = current.currentChoice;
            return all;
        }, {});


        // Shape 
        const data = {};
        const shapeDef = spellShapes().find( s => s.key === inputs['spellShape'].key );
        data.shape = mergeObject( {}, caracLevels );
        data.shape.fullDesc = shapeDef.generateSpellDesc( caracLevels.clone, caracLevels.range, caracLevels.areaOfEffect, caracLevels.duration );
        mergeObject( data.shape, shapeDef );

        // Main element
        const mainElementChoice = inputs['mainElement'];
        data.mainElement = mergeObject( {}, elementList().find( e => e.key === mainElementChoice.key ));
        
        // Magic type
        const effect = this.spellData.effect;
        data.effect= mergeObject( {}, effect );
        data.effect.spells = undefined; // Unlink all related spells

        // Spell
        const effectInput = inputs[effect.key];
        const spell = effect.spells.find( s => s.key === effectInput?.key );
        if( spell ) { 
            data.spell = {
                level: effect.level,
                levelIcon : effect.levelIcons[effect.level]
            };
            mergeObject( data.spell, spell ); 
        }

        // Finalization
        finalizeSpellData(this.mainActor, data);

        return data;
    }

    /** @override */
    activateSpecificListeners(sheet, html) {
        this._sheet = sheet;

        html.find(".arcane-result-section").on("click", ".change-choice", this._onClickChangeInputChoice.bind(this));
    }

    async _onClickChangeInputChoice(event) {
        const a = event.currentTarget;
        const newChoiceId = a.dataset.key;
        const inputKey = a.parentElement.dataset.input;

        const inputs = this.spellData.inputs ?? [];
        const relatedInput = inputs.find( input => {
            return input.key === inputKey;
        });
        if( relatedInput ) { 
            relatedInput.currentChoice = relatedInput.availableChoices.find( c => c.key === newChoiceId ) ?? null; 
        }

        this._sheet.render();
    }
}

