import {translate} from '../tools/Translator.js';

import { InteractionDefenseInterface } from './InteractionDefenseInterface.js';
import { arcaneSpellCategories, elementList } from '../tools/WaIntegration.js';

const CIRCLE_IMG = 'systems/acariaempire/resources/interaction/choices/circle.png';

const CIRCLE_FRAGILITY_IMG = 'systems/acariaempire/resources/interaction/circle/fragility_token.png';

const fragilityForGUI = (fragility) => {
    return {
        icon: CIRCLE_FRAGILITY_IMG,
        level: fragility
    };
}

const caracsForGUI = (caracs, caster) => {
    const maxLevel = caster?.battle.arcane.maneuverMaxLevel ?? 0;
    return caracs.map(carac => {
        return  {
            name: carac.name,
            level: carac.level,
            newLevel: carac.level,
            icon: 'systems/acariaempire/resources/interaction/circle/' + carac.name + '_token.png',
            maxLevel: Math.min(caster?.arcane.getKnowledge(carac.name), maxLevel)
        };
    });
}

export class InteractionDefenseCircleImpl extends InteractionDefenseInterface {

    constructor(mainActor, targets) {
        super('circle', mainActor, targets);
        this.resetArcaneCircle();
    }

    /** 
     * User can define its circle alone
     * @override
     */
     get needGMInputs() {
        return false;
    }

    /** 
     * Allow the main GUI to know if the user can go further or if the current choices are not valid
     * @override
     */
     get isValid() {
        return this.circle.enhancementCount != 0 || this.circle.harmonization.modifier != 0;
    }

    /* ---------------------------
        Reminder : Circle upgrade
    ------------------------------ */

    /** @override */
    get reminders() { 

        const result = [{
            title: translate('AESYSTEM.interaction.sheet.circle.diffFromCircleLvl'), 
            score: {value: this.circle.maxLvl }
        }];
        if( this.circle.harmonization.modifier > 0 ) {
            result.push({
                title: translate('AESYSTEM.interaction.sheet.circle.newHarmonization'), 
                score: {value: this.circle.harmonization.modifier }
            });
        }
        if( this.circle.enhancementSuppl > 0 ) {
            result.push({
                title: translate('AESYSTEM.interaction.sheet.circle.diffFromParallelCast'), 
                score: {value: 2*this.circle.enhancementSuppl }
            });
        }

        if( this.circle.fragility.level > 0 ) {
            result.push({
                title: translate('AESYSTEM.interaction.sheet.circle.diffFromFragility'), 
                score: {value: this.circle.fragility.level}, 
                tokens: {value: 0-this.circle.fragility.level}
            });
        }

        result.push(...super.reminders);
        return result;
    }


    /* ---------------------------
        Circle managment
    ------------------------------ */

    get circleEffectList() {
        // Can't be loaded on initialisation since WA part not sync
         if( ! this._circleEffectList && arcaneSpellCategories() ) {
             this._circleEffectList = arcaneSpellCategories().map( e => {
                 return {
                     effect: e.effect,
                     icon: 'systems/acariaempire/resources/interaction/circle/' + e.effect + '_symbol.png',
                     label: e.name
                 };
             });
         }
         return this._circleEffectList ?? [];
    }
    

    resetArcaneCircle() {
        const caster = this._mainActor;
        const caracs = caster.arcane.circleCaracteristics ?? [];
        const harmonizations =  caster.arcane.circleHarmonizations.map( elementDef => {
            return mergeObject( {modified: false, oldValue: elementDef.key}, elementDef );
        });


        this.circle = {
            newOne: false,
            choosing: caracs.length == 0,
            gmAlter: false,
            harmonization : { list: harmonizations },
            caracs : caracsForGUI(caracs, caster),
            fragility: fragilityForGUI( caster?.arcane.circleFragility )
        }

        this.updateArcaneCircleModifiers();
    }

    buildNewCircle(effectName) {

        const caster = this._mainActor;
        const base = caster.arcane.baseCircleStructure(effectName);
        const harmonizations = base.harmonizations.map( elementKey => {
            return mergeObject( {modified: false, oldValue: elementKey},
                                elementList().find( e => e.key === elementKey ) );
        });

        this.circle = {
            newOne: true,
            choosing: false,
            gmAlter: false,
            harmonization : { list: harmonizations },
            caracs : caracsForGUI(base.caracs, caster),
            fragility: fragilityForGUI( 0 )
        }

        this.updateArcaneCircleModifiers();
    }

    updateArcaneCircleModifiers() {

        let enhancementCount = this.circle.newOne ? 1 : 0;
        let maxLvl = 0;
        this.circle.caracs.forEach(carac => {
            // For GUI
            carac.label = translate('AESYSTEM.interaction.sheet.circle.' + carac.name);

            // Displayed label will differ in gmAlter
            carac.displayedLevel = this.circle.gmAlter ? '' + carac.level : '' + carac.newLevel;
            const added = carac.newLevel - carac.level;
            if( ! this.circle.gmAlter && added > 0 ) {
                carac.displayedLevel += ' (+' + added + ')';
            }

            // For defense calcul
            enhancementCount += (carac.newLevel - carac.level);
            maxLvl = Math.max(maxLvl, carac.newLevel);
        });

        // New harmonization ?
        const harmonizations = this.circle.harmonization.list;
        this.circle.harmonization.list.forEach( h => { h.css = h.modified ? 'modified' : '' });

        const newHarmonization = harmonizations.filter(h => h.modified).length > 0;
        this.circle.harmonization.modifier = newHarmonization ? 1 : 0;

        this.circle.harmonization.choosePanel = { displayed: false };

        // Fragility display
        this.circle.fragility.displayed = this.circle.fragility.level > 0 || this.circle.gmAlter;

        // Right text description
        const label = this.circle.gmAlter ? 'gmAlter' : (this.circle.choosing ? 'chooseCircle' : ( this.circle.newOne ? 'creatingCircle' : 'upgradeCircle' ));
        this.circle.subtitle = translate('AESYSTEM.interaction.sheet.labels.' + label);

        //Right buttons
        const btns = [];
        if(game.user.isGM) {
            // GM have additional options
            if(this.circle.gmAlter)        { btns.push( { action: 'stopAlter', icon: 'far fa-window-close' } ); } 
            else if(!this.circle.choosing) { btns.push( { action: 'startAlter', icon: 'fas fa-wrench' } ); }
        }

        if(! this.circle.gmAlter) {
            // When altering, no other options are available
            if(this.circle.choosing) { 
                if( this.circle.caracs.length > 0) { btns.push( { action: 'forget', icon: 'fas fa-undo-alt' } ); }
            } else { 
                btns.push( { action: 'new', icon: 'fas fa-trash' } );  
            }
        }
        this.circle.rightBtns = btns;

        // For each enhancement after the first one : +1 <+1>
        const enhancementSuppl = Math.max(0, enhancementCount - 1);
        this.circle.maxLvl = maxLvl;
        this.circle.enhancementCount = enhancementCount;
        this.circle.enhancementSuppl = enhancementSuppl;
    }

    toggleChooseHarmonizationPanel(elementKey) {
        const index = this.circle.harmonization.list.findIndex( h => h.oldValue === elementKey );
        if( index == -1 ) { throw 'Can\'t find harmonized element ' + elementKey; }

        const oldKeyValue = this.circle.harmonization.list[index].oldValue;
        this.circle.harmonization.choosePanel = { 
            displayed: true,
            changingIndex: index,
            availableElements: elementList().map( e => {
                const data = { css: e.key === oldKeyValue ? 'selected' : '' };
                return mergeObject( data, e );
            })
        };
    }

    selectNewHarmonization(elementKey) {
        const modifiedIndex = this.circle.harmonization.choosePanel.changingIndex;
        if( !modifiedIndex && modifiedIndex != 0 ) { throw 'Can\'t change harmonized element. Changed one not found'; }
        if( modifiedIndex >= this.circle.harmonization.list.length ) { throw 'Can\'t change harmonized element. Invalid index : ' + modifiedIndex; }

        // Updating this.circle.harmonization.list, restoring oldValues if necessary and flagging as modified the selected one.
        for( let index = 0; index < this.circle.harmonization.list.length; index++ ) {
            const currentElem = this.circle.harmonization.list[index];

            const modified = (index == modifiedIndex) && (currentElem.oldValue != elementKey);
            const newElementKey = modified ? elementKey : currentElem.oldValue;
            const elementDef = elementList().find( e => e.key === newElementKey );

            const data = {modified: modified, oldValue: currentElem.oldValue};
            this.circle.harmonization.list[index] = mergeObject(data, elementDef);
        }

        // Closing choose Panel and reset modifiers
        this.updateArcaneCircleModifiers();
    }

    /** @override */
    get img() { return CIRCLE_IMG; }

    /** @override */
    get putImgOnRollContext() { return false; }

    /** @override */
    get circleDisplayed() { return true; }

    /** @override */
    get defenseDetails() {
        const result = super.defenseDetails;
        
        if( this.circle.caracs.length > 0 ) {
            const title = translate('AESYSTEM.interaction.sheet.details.' + ( this.circle.newOne ? 'newCircle' : 'circleUpgrade') );
            result.push( this._textLineDetail(
                title, 
                this.circle.caracs[0].name
            ));
        }

        return result;
    }

    /** @override */
    get circleDataForRollContext() {

        return {
            newOne: this.circle.newOne,
            harmonizations : this.circle.harmonization.list.map( h => h.key ),
            caracs : this.circle.caracs.map(carac => {
                return {
                    name: carac.name,
                    level: carac.newLevel
                };
            }),
            fragility: this.circle.fragility.level
        };
    }


    /* ---------------------------
          Right panel handling
    ------------------------------ */

    /** @override */
    activateSpecificListeners(sheet, html) {
        this._sheet = sheet;

        html.find(".config-panel .circle-actions").on("click", ".circle-button", this._onClickDisplayCircleList.bind(this));
        html.find(".config-panel .circle-choice").on("click", ".create-circle", this._onClickBuildNewCircle.bind(this));
        html.find(".config-panel .circle-construction").on("click", ".carac-level", this._onClickUpgradeCircle.bind(this));
        html.find(".config-panel .circle-construction").on("click", ".fragility-level", this._onClickUpgradeCircleFragility.bind(this));
        html.find(".config-panel .circle-construction").on("contextmenu", ".carac-level", this._onClickDowngradeCircle.bind(this));
        html.find(".config-panel .circle-construction").on("contextmenu", ".fragility-level", this._onClickDowngradeCircleFragility.bind(this));

        html.find(".config-panel .harmonization-panel").on("click", ".change-harmonization", this._onClickChangeHarmonization.bind(this));
        html.find(".config-panel .harmonization-panel").on("click", ".select-harmonization", this._onClickSelectNewHarmonization.bind(this));
    }

    async _onClickDisplayCircleList(event) {

        event.preventDefault();
        const a = event.currentTarget;
        const choice = a.dataset.action;
        this.circle.choosing = (choice == 'new');
        this.circle.gmAlter = (choice == 'startAlter');

        this.updateArcaneCircleModifiers();

        this._sheet.render(true);
    }

    async _onClickBuildNewCircle(event) {

        event.preventDefault();
        const a = event.currentTarget;
        const caracName = a.dataset.effect;
        this.buildNewCircle(caracName);

        this._sheet.render(true);
    }

    async _onClickUpgradeCircle(event) {

        event.preventDefault();
        const a = event.currentTarget;
        const caracName = a.parentElement.dataset.carac;

        const carac = this.circle.caracs.find(elem => elem.name === caracName);

        if(!carac) { throw 'Carac not found : ' + caracName; }

        const baseLevel = this.circle.gmAlter ? carac.level : carac.newLevel;
        if( baseLevel >= carac.maxLevel ) {
            const text = translate('AESYSTEM.interaction.sheet.circle.maxCaracReached').replace( 'DOMAIN', carac.label );
            return ui.notifications.info(text);
        }
        carac.newLevel = baseLevel + 1;

        if(this.circle.gmAlter) {
            await this._mainActor.arcane.alterArcaneCircle(this.circleDataForRollContext);
            this.resetArcaneCircle();
            this.circle.gmAlter= true;
        }

        this.updateArcaneCircleModifiers();
        this._sheet.render(true);
    }

    async _onClickDowngradeCircle(event) {

        event.preventDefault();
        const a = event.currentTarget;
        const caracName = a.parentElement.dataset.carac;

        const carac = this.circle.caracs.find(elem => elem.name === caracName);

        if(!carac) { throw 'Carac not found : ' + caracName; }

        const baseLevel = this.circle.gmAlter ? carac.level : carac.newLevel;
        if( this.circle.gmAlter && baseLevel == 0) {
            return ui.notifications.info(translate('AESYSTEM.interaction.sheet.circle.caracAlreadyLvl0') );

        } else if( ! this.circle.gmAlter && baseLevel <= carac.level ) {
            return ui.notifications.info(translate('AESYSTEM.interaction.sheet.circle.noUpgradeToRemove') );
        }

        carac.newLevel = baseLevel - 1;

        if(this.circle.gmAlter) {
            await this._mainActor.arcane.alterArcaneCircle(this.circleDataForRollContext);
            this.resetArcaneCircle();
            this.circle.gmAlter= true;
        }

        this.updateArcaneCircleModifiers();
        this._sheet.render(true);
    }

    async _onClickUpgradeCircleFragility(event) {

        event.preventDefault();

        if(this.circle.gmAlter) { // only useful on GM actions
            const newCircle = this.circleDataForRollContext;
            newCircle.fragility = Math.min( 6, newCircle.fragility + 1 );
            await this._mainActor.arcane.alterArcaneCircle(newCircle);

            this.resetArcaneCircle();
            this.circle.gmAlter= true;
        }

        this.updateArcaneCircleModifiers();
        this._sheet.render(true);
    }

    async _onClickDowngradeCircleFragility(event) {

        event.preventDefault();

        if(this.circle.gmAlter) { // only useful on GM actions
            const newCircle = this.circleDataForRollContext;
            newCircle.fragility = Math.max( 0, newCircle.fragility - 1 );
            await this._mainActor.arcane.alterArcaneCircle(newCircle);

            this.resetArcaneCircle();
            this.circle.gmAlter= true;
        }

        this.updateArcaneCircleModifiers();
        this._sheet.render(true);
    }

    async _onClickChangeHarmonization(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const element = a.dataset.element;
        this.toggleChooseHarmonizationPanel(element);
        
        this._sheet.render(true);
    }

    async _onClickSelectNewHarmonization(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const element = a.dataset.element;
        this.selectNewHarmonization(element);
        
        this._sheet.render(true);
    }



}

