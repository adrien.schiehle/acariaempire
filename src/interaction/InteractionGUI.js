import { AEEventCard } from '../card/EventCard.js';
import {translate} from '../tools/Translator.js';

export const INTERACTION_MODE = {
    ACTION: "ACTION",
    DEFENSE: "DEFENSE"
};

export class AEInteractionGUI extends Application {

    constructor(options = {}) {
        super(options);
        this.mainActorData = undefined;
        this.defenseData = undefined;
        this._cardForRoll = undefined;
        this.cache = {};
        this.state = {
            waitingAnswer: false,
            hasBeenAnswered: false
        }
    }

    /** Will be overriden by extended class, if user can choose the action  */
    get interactionMode() { return undefined; }

    get rollValue() {
        const scoreActor = this.mainActorData.impl.score;
        const scoreDefense = this.defenseData.impl.score;

        const tokenActor = this.mainActorData.impl.tokens;
        const tokenDefense = this.defenseData.impl.tokens;

        // Some event card can alter the card balance
        let scoreFromEventCard = 0;
        let tokenFromEventCard = 0;
        if( this._cardForRoll ) {
            const onRoll = this._cardForRoll.eventDefinition.onroll;
            if( onRoll.key === "token_success" ) {
                scoreFromEventCard = 1;
            } else if( onRoll.key === "token_cardbalance" ) {
                tokenFromEventCard = 2;
            }
        }

        const scoreBase = scoreActor + scoreFromEventCard - scoreDefense;
        const cardBalance = Math.clamped( tokenActor + tokenFromEventCard + tokenDefense, -3, 3 );

        return {
            actorScore: scoreActor,
            defenseScore : scoreDefense,
            scoreBase: scoreBase,
            cardBalance: cardBalance
        };
    }


/*------------------------------------------
    getData() management
--------------------------------------------*/

    /** @override */
    async getData() {

        if( !this._init ) { 
            return {}; // Not ready (should not happen, outside manual macros)
        }

        this.mainActorData.impl.prepareAdditionalData();

        // Each time, change defenseData if needed
        const availableMethods = this.mainActorData.impl.availableDefenseMethods;
        this.defenseData.computeAvailableMethods(availableMethods);

        let data = {
            mainActorData: this.mainActorData,
            defenseData: this.defenseData,
            roll: this.rollValue,
            calc : this._computeCalcData(),
            state: this._computeState()
        };

        return data;
    }

    _computeCalcData() {

        let result = {
            scores : this._computeScoreIcons(),
            tokens : this._computeTokensIcons(),
            eventCard: this._computeEventCard(),
            trouble: this._computeTrouble(),
            risk : this._computeRisk(),
            timed : this._computeTimed(),
            miracle : this._computeMiracle(),
            arcane: this._computeArcaneTrigger(),
            defenseModeDisplayed: this.mainActorData.impl.targetNeedToDefendItself && this.defenseData.impl.mainDefender != null,
            doActionBtnLabel: this._computeDoActionBtnLabel(),
            automaticSuccess: this._computeAutomaticSuccessBtn()
        };

        return result;
    }

    _computeScoreIcons() {

        const roll = this.rollValue;

        const result = {
            actor: {
                label: translate('AESYSTEM.interaction.sheet.labels.actionScore'),
                value: roll.actorScore,
                icon: "systems/acariaempire/resources/interaction/tokens/action_score.png"
            },
            defense : {
                label: translate('AESYSTEM.interaction.sheet.labels.defenseScore'),
                value: this.defenseData.impl.hideDefense ? '?' : roll.defenseScore,
                icon: "systems/acariaempire/resources/interaction/tokens/defense_score.png"
            }, 
            all : {
                label: translate('AESYSTEM.interaction.sheet.labels.baseScore'),
                value: this.defenseData.impl.hideDefense ? '?' :roll.scoreBase,
                icon: "systems/acariaempire/resources/interaction/tokens/action_score.png"
            }
        };
        return result;
    }

    _computeTokensIcons() {
        const createIconList = (cardBalance) => {
            const list = [];
            const iconSuffix = cardBalance > 0 ? 'positive.png' : 'negative.png';
            const icon = 'systems/acariaempire/resources/interaction/tokens/card_balance_' + iconSuffix;
            for( let i = 0; i < Math.abs(cardBalance); i++ ) {
                list.push(icon);
            }

            return list;
        }

        const result = {
            actor: {
                label: translate('AESYSTEM.interaction.sheet.cardBalance.actor'),
                list: createIconList(this.mainActorData.impl.tokens)
            },
            defense : {
                label: translate('AESYSTEM.interaction.sheet.cardBalance.defense'),
                list: createIconList(this.defenseData.impl.tokens)
            }, 
            all : {
                label: translate('AESYSTEM.interaction.sheet.cardBalance.all'),
                list: createIconList(this.rollValue.cardBalance)
            }
        };
        result.actor.displayed = result.actor.list.length > 0;
        result.defense.displayed = result.defense.list.length > 0;
        result.all.displayed = result.all.list.length > 0;
        return result;
    }

    _computeEventCard() {
        const cardStacks = game.modules.get('ready-to-use-cards').cardStacks;
        const eventCardsAvailable = cardStacks.myHand.sortedCardList.some( c => c.type === "event" );
        const cssState = this._cardForRoll ? "chosen" : ( eventCardsAvailable ? "available" : "none" );
        return {
            cssState: cssState,
            chosen: cssState === "chosen",
            available: cssState === "available",
            rollBonus: this._cardForRoll?.eventDefinition.onroll
        };
    }

    _computeTrouble() {
        const trouble = this.mainActorData.impl.mainActor.cards.troubles[0];
        
        const result = {
            active: !!trouble
        };
        
        if( !trouble ) {
            result.label = translate('AESYSTEM.interaction.sheet.trouble.none');
            result.img = {
                displayed: false
            };

        } else if( trouble.isPositive ) {
            result.label = translate('AESYSTEM.interaction.sheet.trouble.positive');
            result.img = {
                displayed: true,
                src: trouble.effect.positiveIcon
            };


        } else if( trouble.isNegative ) {
            result.label = translate('AESYSTEM.interaction.sheet.trouble.negative');
            result.img = {
                displayed: true,
                src: trouble.effect.negativeIcon
            };


        } else {
            result.label = translate('AESYSTEM.interaction.sheet.trouble.calm');
            result.img = {
                displayed: false
            };
        }
        
        return result;
    }

    _computeRisk() {
        const risk = {
            level : this._riskLevel,
            label : translate('AESYSTEM.interaction.sheet.risk.' + this._riskLevel),
            selection : {
                risk1 : this._riskLevel >= 1 ? ' selected' : '',
                risk2 : this._riskLevel >= 2 ? ' selected' : '',
                risk3 : this._riskLevel >= 3 ? ' selected' : ''
            }
        };
        return risk;
    }

    _computeTimed() {

        // Convenience methods
        //-----------------------
        const retrieveAvailableCategories = (mainImpl, defImpl) => {

            const base = mainImpl.availableActionsCategories;
            // Special case : Refreshing melody cost only a fast-action. We know if it's a refresh only after melody inputs
            if( defImpl.method == 'melody' && defImpl.melody.enhancementCount == 0) {
                return base.filter( c => c.key != 'simple');
            }
            return base;
        }

        const retrieveSelectedIndex = (previousCategories, currentCategories, mainImpl) => {

            // When available choices change => Reset choice to the latest one
            if( previousCategories.length != currentCategories.length ) {
                mainImpl.actionCategory = currentCategories[currentCategories.length - 1].key;
            }
            return currentCategories.findIndex(action => action.key === impl.actionCategory);
        }

        const impl = this.mainActorData.impl;

        const previousCategories = this.cache.previousCategories ?? [];
        const categories = retrieveAvailableCategories(impl, this.defenseData.impl);
        const selectedIndex = retrieveSelectedIndex(previousCategories, categories, impl);


        const duration = impl.neededTimeForAction;
        const durationSuffix = translate('AESYSTEM.interaction.sheet.action.suffix').replace('TIME', duration);
        const currentLabel = selectedIndex != -1 ? categories[selectedIndex].label + durationSuffix
                                                 : 'Not selected';

        const choices = categories.map( (action, index) => {
            return {
                key: action.key,
                icon: action.icon, 
                active: index <= selectedIndex ? ' selected' : ''
            };
        });

        const timed = {
            tracked: impl.mainActor.isInBattle,
            label: currentLabel,
            choices: choices
        }
        this.cache.previousCategories = categories;
        return timed;
    }

    _computeMiracle() {

        const impl = this.mainActorData.impl;
        if( !impl.needToConsumeSpirits ) {
            return { displayed: false };
        }
        return impl.miracle;
    }

    _computeArcaneTrigger() {

        const impl = this.mainActorData.impl;
        if( !impl.needToTriggerArcaneCircle ) {
            return { displayed: false };
        }
        return impl.spellData;
    }

    _computeDoActionBtnLabel() {

        if( !this.defenseData.impl.actuallyRollDice ) {
            return translate('AESYSTEM.interaction.sheet.labels.doAction');
        }
        return translate('AESYSTEM.interaction.sheet.labels.roll');
    }

    _computeAutomaticSuccessBtn() {

        const roll = this.rollValue;
        const active = roll.scoreBase >= 2 && roll.cardBalance >= 0;
        return {
            css: active ? 'active' : '',
            label: translate('AESYSTEM.interaction.sheet.labels.automaticSuccess')
        };
    }

    _computeState() {

        const actionMode = this.interactionMode === INTERACTION_MODE.ACTION;
        const defenseMode = this.interactionMode === INTERACTION_MODE.DEFENSE;

        const defenseDataRetrieved = this.state.hasBeenAnswered || !this.defenseData.impl.needGMInputs;

        const result = {};
        result.actionPartDisplayed = actionMode;
        result.defensePartDisplayed = defenseDataRetrieved || defenseMode;
        result.riskPartDisplayed = actionMode && defenseDataRetrieved;

        result.selects = {
            disabledOnActor: !actionMode,
            disabledOnDefense: this.defenseData.impl.needGMInputs && !defenseMode
        };

        result.circleButton = {
            displayed: this.defenseData.impl.circleDisplayed
        };
        result.melodyButton = {
            displayed: this.defenseData.impl.melodyDisplayed
        };
        result.spiritButton = {
            displayed: this.defenseData.impl.communionDisplayed || this.defenseData.impl.releaseSpiritDisplayed
        };
        result.answerButton = {
            displayed: defenseMode,
            active: this.defenseData.impl.isValid
        };
        result.waitButton = {
            displayed: actionMode && this.state.waitingAnswer
        };
        result.versusButton = {
            displayed: actionMode && this.defenseData.impl.needGMInputs && !this.state.waitingAnswer,
            active: this.mainActorData.impl.isValid
        };
        result.rollButton = {
            displayed: actionMode,
            active: this.mainActorData.impl.isValid && this.defenseData.impl.isValid && defenseDataRetrieved
        };

        return result;
    }

/*------------------------------------------
    Listeners and actions used by both implementation
--------------------------------------------*/

    /**
     * Stored in main class since it can be used by the two implementations (depending on it needs GM inputs or not)
     * @param {HTMLElement} html see activateListeners
     */
    listenersForDefenseChoices(html) {

        // Second choice: Target defense
        //------------------------------
        html.find(".second-choice .roll-category").click( (event) =>this._onClickChangeSecondChoice(event));
        html.find(".second-choice select").change( (event) => this._onChangeSelectionForDefense(event));
        // - clicks on reminder (only custom bonus for now)
        html.find(".second-choice .reminder.clickable .token-value img").click( (event) => this._onClickChangeReminderBonus(event, this.defenseData.impl, true));
        html.find(".second-choice .reminder.clickable .token-value img").contextmenu( (event) => this._onClickChangeReminderBonus(event, this.defenseData.impl, false));
        html.find(".second-choice .reminder.clickable-tips .tips").click( (event) => this._onClickTriggerReminderTipsAction(event, this.defenseData.impl));
        // - Specific listeners
        this.defenseData.impl.activateSpecificListeners(this, html);
    }

    /** Inside second-choice: Change defenseData implementation */
    async _onClickChangeSecondChoice(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const choiceKey = a.dataset.key;
        this.defenseData.changeMethod(choiceKey);
        this.render();
    }

    /** Inside second-choice: Alter defenseData choices */
    async _onChangeSelectionForDefense(event) {

        event.preventDefault();
        const a = event.currentTarget;
        const select = a.dataset.selection;
        const choice = a.value;

        if( select == 'defenseOption' ) {
            this.defenseData.impl.setDefenseMode( choice );
            
        } else if( select == 'firstListbox' ) {
            this.defenseData.impl.firstListboxChangeChoice(choice);

        } else if( select == 'secondListbox' ) {
            this.defenseData.impl.secondListboxChangeChoice(choice);

        } else if( select == 'thirdListbox' ) {
            this.defenseData.impl.thirdListboxChangeChoice(choice);

        } else {
            console.err('Select type not found : ' + select)
        }
        this.render(true);
    }

    /**
     * Inside first-choice: Modify reminder score or card balance.  
     * Render the sheet after change is done
     * @param {*} event Mouse click (be it left or right click)
     * @param {object} source  this.mainActorData.impl, or this.defenseData.impl
     * @param {boolean} leftClick 
     */
    async _onClickChangeReminderBonus(event, source, leftClick) {

        event.preventDefault();
        const a = event.currentTarget.parentElement;
        const type = a.dataset.type;
        const ref = a.parentElement.dataset.ref;
        
        await source.reminderClicked(ref, type, leftClick);
        this.render(true);
    }

    /**
     * Same as previous one. Except that the tips was clicked instead of a token
     * @param {*} event Mouse click (be it left or right click)
     * @param {object} source  this.mainActorData.impl, or this.defenseData.impl
     */
     async _onClickTriggerReminderTipsAction(event, source) {

        event.preventDefault();
        const a = event.currentTarget.parentElement;
        const ref = a.dataset.ref;
        
        await source.reminderClicked(ref, '', true);
        this.render(true);
    }

}
