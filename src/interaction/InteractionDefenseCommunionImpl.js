import { MAX_SPIRIT_LEVEL } from '../actor/ActorDivineData.js';
import {translate} from '../tools/Translator.js';
import { InteractionDefenseInterface } from './InteractionDefenseInterface.js';

const COMMUNION_IMG = 'systems/acariaempire/resources/interaction/spirit.png';

const SPIRIT_LVL_IMG = 'systems/acariaempire/resources/interaction/spirit_level_token.png';
const SPIRIT_AMOUNT_IMG = 'systems/acariaempire/resources/interaction/spirit_quantity_token.png';
const SPIRIT_DISATISFACTION_IMG = 'systems/acariaempire/resources/interaction/spirit_angry_token.png';


export class InteractionDefenseCommunionImpl extends InteractionDefenseInterface {

    constructor(mainActor, targets) {
        super('communion', mainActor, targets);
        this.resetCommunion();
    }

    /** 
     * User can define its communion alone
     * @override
     */
    get needGMInputs() {
        return false;
    }

    get title() {
        return translate('AESYSTEM.interaction.sheet.communion.title');
    }

    /* ---------------------------
        Reminder : Communion difficulty
    ------------------------------ */

    /** @override */
    get reminders() { 

        const level = this.communion.newSpiritLevel;
        const parallelCalls = this.communion.callAmount - 1;

        const result = [{
            title: translate('AESYSTEM.interaction.sheet.communion.diffFromSpiritLvl'), 
            score: {value: level }
        }];
        if( parallelCalls > 0 ) {
            result.push({
                title: translate('AESYSTEM.interaction.sheet.communion.diffFromParallelCalls'), 
                score: {value: 2*parallelCalls}
            });
        }
        const dissatisfaction = this.communion.dissatisfaction.level;
        if( dissatisfaction > 0 ) {
            result.push({
                title: translate('AESYSTEM.interaction.sheet.communion.diffFromDissatisfaction'), 
                score: {value: dissatisfaction},
                tokens: {value: 0-dissatisfaction}
            });
        }

        result.push(...super.reminders);
        return result;
    }

    resetCommunion() {
        const caster = this._mainActor;
        const currentCommunion = caster.divine.communion;
        const casterLevel = caster?.battle.prayer.maneuverMaxLevel ?? 0;

        const isNewCommunion = currentCommunion.calledSpirits == 0;
        const maxPower = isNewCommunion ? MAX_SPIRIT_LEVEL : currentCommunion.spiritPower; // Spirits can be at 

        this.communion = {
            casterLevel: casterLevel,
            maxPower: maxPower,
            currentSpiritLevel: currentCommunion.spiritPower,
            newSpiritLevel: currentCommunion.spiritPower,
            alreadyHereAmount: currentCommunion.calledSpirits,
            dissatisfaction: { level: currentCommunion.dissatisfaction },
            alreadyHereTitle: translate('AESYSTEM.interaction.sheet.communion.alreadyHereTitle').replace('AMOUNT', currentCommunion.calledSpirits),
            callAmount: 1,
            newCommunion: isNewCommunion
        };

        this.updateCommunionModifiers();
    }

    updateCommunionModifiers() {

        const c = this.communion;

        // Right text
        c.subtitle = translate('AESYSTEM.interaction.sheet.communion.title')

        // Available level data
        let levelText = c.newSpiritLevel;
        if( !c.newCommunion &&  c.newSpiritLevel != c.currentSpiritLevel ) {
            const diff = c.newSpiritLevel - c.currentSpiritLevel;
            levelText += ' (' + diff + ')';
        }
        const aboveMastery = c.newSpiritLevel > this._mainActor.battle.prayer.maneuverMaxLevel;
        c.availableLevel = {
            level: levelText,
            icon: SPIRIT_LVL_IMG,
            label: translate('AESYSTEM.interaction.sheet.communion.powerLevel') ,
            css: aboveMastery ? 'above' : ''
        };

        // Quantity of spirits
        const total = c.alreadyHereAmount + c.callAmount;
        c.callData = {
            quantity: '' + total + ' (+' + c.callAmount +')',
            icon: SPIRIT_AMOUNT_IMG,
            label: translate('AESYSTEM.interaction.sheet.communion.spiritAmount').replace('NBSPIRIT', '' + c.callAmount)
        }

        // Dissatisfaction
        c.dissatisfaction.displayed = c.dissatisfaction.level != 0;
        c.dissatisfaction.icon = SPIRIT_DISATISFACTION_IMG;
        c.dissatisfaction.label = translate('AESYSTEM.interaction.sheet.communion.spiritDissatisfaction');
    }

    /** @override */
    get img() { return COMMUNION_IMG; }

    /** @override */
    get putImgOnRollContext() { return false; }

    /** @override */
    get defenseDetails() {
        const result = super.defenseDetails;
        
        if( this.communion.alreadyHereAmount > 0 ) {
            result.push( this._textLineDetail(
                translate('AESYSTEM.interaction.sheet.details.alreadyCalled'), 
                translate('AESYSTEM.interaction.sheet.details.nbSpirits').replace( 'NB', '' + this.communion.alreadyHereAmount)
            ));
        }

        result.push( this._textLineDetail(
            translate('AESYSTEM.interaction.sheet.details.callingNew'), 
            translate('AESYSTEM.interaction.sheet.details.nbSpirits').replace( 'NB', '' + this.communion.callAmount)
        ));

        result.push( this._textLineDetail(
            translate('AESYSTEM.interaction.sheet.details.spiritPower'), 
            'Lvl ' + this.communion.newSpiritLevel
        ));

        return result;
    }


    /* ---------------------------
            Communion data
    ------------------------------ */

    /** @override */
    get communionDisplayed() { return true; }

    /** @override */
    get communionDataForRollContext() {
        
        return {
            callAmount : this.communion.callAmount,
            currentSpiritMaxPower: this.communion.newSpiritLevel,
            newCommunion: this.communion.newCommunion,
            dissatisfaction: this.communion.dissatisfaction.level
        };
    }

    /* ---------------------------
          Right panel handling
    ------------------------------ */

    /** @override */
    activateSpecificListeners(sheet, html) {
        this._sheet = sheet;

        html.find(".communion-part").on("click", ".spirit-level", this._onClickChangeCommunionSpiritLevel.bind(this));
        html.find(".communion-part").on("contextmenu", ".spirit-level", this._onRightClickChangeCommunionSpiritLevel.bind(this));
        html.find(".communion-part").on("click", ".spirit-amount", this._onClickChangeCommunionSpiritAmount.bind(this));
        html.find(".communion-part").on("contextmenu", ".spirit-amount", this._onRightClickChangeCommunionSpiritAmount.bind(this));
    }

    async _onClickChangeCommunionSpiritLevel(event) {
        event.preventDefault();

        this.communion.newSpiritLevel = Math.min( this.communion.maxPower, this.communion.newSpiritLevel + 1);
        this.updateCommunionModifiers();
        this._sheet.render(true);
    }

    async _onRightClickChangeCommunionSpiritLevel(event) {
        event.preventDefault();

        this.communion.newSpiritLevel = Math.max( 0, this.communion.newSpiritLevel - 1);
        this.updateCommunionModifiers();
        this._sheet.render(true);
    }

    async _onClickChangeCommunionSpiritAmount(event) {
        event.preventDefault();

        this.communion.callAmount = this.communion.callAmount + 1;
        this.updateCommunionModifiers();
        this._sheet.render(true);
    }

    async _onRightClickChangeCommunionSpiritAmount(event) {
        event.preventDefault();

        this.communion.callAmount = Math.max( 1,this.communion.callAmount - 1);
        this.updateCommunionModifiers();
        this._sheet.render(true);
    }

}
