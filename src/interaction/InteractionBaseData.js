import {translate} from '../tools/Translator.js';
import { InteractionDefenseDifficultyImpl } from './InteractionDefenseDifficultyImpl.js';
import { InteractionDefenseCircleImpl } from './InteractionDefenseCircleImpl.js';
import { InteractionDefenseCommunionImpl } from './InteractionDefenseCommunionImpl.js';
import { InteractionDefenseBattleImpl } from './InteractionDefenseBattleImpl.js';
import { InteractionMainActorRangedImpl } from './InteractionMainActorRangedImpl.js';
import { InteractionMainActorMeleeImpl } from './InteractionMainActorMeleeImpl.js';
import { InteractionMainActorImprovisedImpl } from './InteractionMainActorImprovisedImpl.js';
import { InteractionMainActorArcaneImpl } from './InteractionMainActorArcaneImpl.js';
import { InteractionMainActorPrayerImpl } from './InteractionMainActorPrayerImpl.js';
import { InteractionMainActorSongsImpl } from './InteractionMainActorSongsImpl.js';
import { InteractionDefenseSpiritReleaseImpl } from './InteractionDefenseSpiritReleaseImpl.js';
import { InteractionDefenseMelodyImpl } from './InteractionDefenseMelodyImpl.js';
import { InteractionMainActorExploreImpl } from './InteractionMainActorExploreImpl.js';
import { InteractionMainActorIntrigueImpl } from './InteractionMainActorIntrigueImpl.js';
import { InteractionDefenseNoBattleImpl } from './InteractionDefenseNoBattleImpl.js';
import { InteractionDefenseProtectiveSpellImpl } from './InteractionDefenseProtectiveSpellImpl.js';
import { InteractionDefenseProtectiveMiracleImpl } from './InteractionDefenseProtectiveMiracleImpl.js';

export class InteractionMainActorData {

    constructor(mainActor) {
        this._mainActor = mainActor;

        this._allMethods = [
            new InteractionMainActorExploreImpl(mainActor),
            new InteractionMainActorIntrigueImpl(mainActor),
            new InteractionMainActorMeleeImpl(mainActor),
            new InteractionMainActorRangedImpl(mainActor),
            new InteractionMainActorImprovisedImpl(mainActor),
            new InteractionMainActorArcaneImpl(mainActor),
            new InteractionMainActorPrayerImpl(mainActor),
            new InteractionMainActorSongsImpl(mainActor),
        ];
        this._impl = this._allMethods[0];
    }

    get impl() {
        return this._impl;
    }

    /**
     * Retrieve all choices.
     * The selected firstLevel will display it's subchoices.
     */
    get allChoices() {
        const currentParent = this.impl.methodGroup;
        const firstLevel = this._allMethods.reduce( (array, current) => {
            const selected = current.methodGroup === currentParent;
            const found = array.find( el => el.key === current.methodGroup );
            if( !found ) {
                array.push({
                    key: current.methodGroup,
                    label: current.methodGroupName, 
                    selected: selected,
                    css: selected ? 'selected' : ''
                });
            }
            return array;
        }, []);
        firstLevel.sort( (a,b) => a.label.localeCompare(b.label) );

        // Add subcategories
        firstLevel.forEach( el => {
            if( !el.selected ) {
                el.subCategories = [];
                return; 
            }
            el.subCategories = this._allMethods.filter(m => {
                return m.methodGroup === currentParent;
            }).map( m => {
                const selected = (this.impl == m);
                return {
                    key: m.method,
                    label: m.methodName, 
                    css: selected ? 'selected' : ''
                };
            } );
        });
        return firstLevel;

    }

    /**
     * Try to change mainActor method
     * @param {String} methodType The wanted implem name for defense data
     * @returns TRUE if the change is a success
     */
     changeMethod(methodType) {

        if( this.impl.method === methodType ) { return false; }

        const method = this._allMethods.find( m => m.method === methodType );
        if( ! method ) {
            ui.notifications.warn(translate("AESYSTEM.interaction.sheet.err.invalidMainActorMethod").replace('METHOD', methodType));
            return false;
        } else {
            this._impl = method;
            return true;
        }
    }

    changeParentMethod(parentMethod) {

        if( this.impl.methodGroup === parentMethod ) { 
            return false; 
        }
        const newImpl = this._allMethods.find( m => m.methodGroup === parentMethod ) ?? null;
        if( newImpl ) {
            this._impl = newImpl;
        }
        return newImpl != null
    }

    initialize(attribute, abilityCheck, skillChoice, battle, maneuver, battleSubchoice) {

        if( ['explore', 'intrigue'].includes(abilityCheck) ) {
            this.changeMethod(abilityCheck);

            if( attribute ) { this.impl.firstListboxChangeChoice(attribute); }
            if( skillChoice && !this.impl.mainActor.abilities.skillIsKnown(abilityCheck, skillChoice)  ) { 
                this.impl.secondListboxChangeChoice("noCompatibility"); 
            }

        } else if( ['melee', 'ranged', 'improvised', 'arcane', 'prayer', 'songs'].includes(battle) ) { 
            
            this.changeMethod(battle); 

            if( ['improvised', 'arcane', 'prayer'].includes(battle) ) {
                if( this.impl.availableStyles.find( s  => s.key === battleSubchoice ) ) {
                    this.impl.firstListboxChangeChoice(battleSubchoice); 
                }
            }

            if( maneuver ) { 
                this.impl.changeManeuver(maneuver); 
            }
        }
    }
}

export class InteractionDefenseData {

    constructor(mainActor, targets) {
        this._allMethods = [
            new InteractionDefenseCircleImpl(mainActor, targets),
            new InteractionDefenseCommunionImpl(mainActor, targets),
            new InteractionDefenseSpiritReleaseImpl(mainActor, targets),
            new InteractionDefenseMelodyImpl(mainActor, targets),
            new InteractionDefenseProtectiveMiracleImpl(mainActor, targets),
            new InteractionDefenseProtectiveSpellImpl(mainActor, targets),
            new InteractionDefenseNoBattleImpl(mainActor, targets),
            new InteractionDefenseBattleImpl(mainActor, targets),
            new InteractionDefenseDifficultyImpl(mainActor, targets),
        ];
        this._impl = this._allMethods[0];
        this._mainActor = mainActor;
        this._targets = targets ?? [];
        this._validMethods = [];
    }

    get impl() {
        return this._impl;
    }

    computeAvailableMethods(availableDefenseMethods) {

        this._validMethods = this._allMethods.filter( m => {
            if( !availableDefenseMethods.includes(m.method) ) { return false; }
            return !m.needsTarget || this._targets.length > 0;
        });
        this._refreshMethod();
    }

    get allChoices() {
        return this._validMethods.map( m => {
            const selected = m.method === this.impl.method;
            return {
                key: m.method,
                label: m.methodName, 
                css: selected ? 'selected' : ''
            };
        } );
    }

    /**
     * Try to change defense method
     * @param {String} methodType The wanted implem name for defense data
     * @returns TRUE if the change is a success
     */
    changeMethod(methodType) {

        if( this.impl.method === methodType ) { return; }

        const method = this._validMethods.find( m => m.method === methodType );
        if( ! method ) {
            console.info( 'AESystem | Defense method ' + methodType + ' not found. Skipped.' );
            return false;
        } else {
            this._impl = method;
            return true;
        }
    }


    _refreshMethod() {

        if( this._validMethods.length == 0 ) { // Can't change method
            return; 
        }

        // Switch to available method if needed
        const currentMethod = this._validMethods.find( m => m.method === this.impl.method );
        if( currentMethod ) {// All is good
            return;
        }

        const battleMethod = this._validMethods.find( m => m.method === 'battle' );
        if( battleMethod ) { // Battle method present ?
            this._impl = battleMethod;
            if( this.battleChoice ) {
                this.impl.firstListboxChangeChoice(this.battleChoice);
            }
            return;
        }

        // By default, take the fist one
        this._impl = this._validMethods[0];
    }


    initialize(difficulty, battle, battleSubchoice) {

        const noTarget = ! this.impl.mainDefender;
        const assertTargetPresent = () => {
            if( noTarget ) {
                ui.notifications.info(translate("AESYSTEM.interaction.sheet.err.noTargetThenDifficulty"));
            }
            return !noTarget;
        }

        if( ['circle', 'communion', 'release'].includes(battleSubchoice) ) {
            this.changeMethod(battleSubchoice); // Display the defense method with the same name (only choice available for those ones)

        } else if( battle == 'songs' ) {
            if( battleSubchoice == 'refresh' ) {
                this.changeMethod('melodyRefresh');
            } else {
                this.changeMethod('melody');
            }

        } else if( battle && assertTargetPresent() ) {
            
            this.battleChoice = battle; // Remember battle choice is case
            if( this.changeMethod('battle') ) {
                this.impl.selectCorrectDefenseModeForBattleType(battle);
            }

        } else if( difficulty ) {

            if( this.changeMethod('difficulty') ) {
                this.impl.firstListboxChangeChoice(difficulty);
            }
        }
    }

}
