import { finalizeSpellData } from '../tools/spells.js';
import { translate } from '../tools/Translator.js';
import { elementList, spellCategories, spellShapes, spiritBonusList, spiritEffectList } from '../tools/WaIntegration.js';
import { InteractionMainActorBattleInterface } from './InteractionMainActorInterface.js';

const NOT_SELECTED = 'none';

const EFFECT_MAPPING = {
    offense: ['effectAttack', 'effectDebuff'],
    defense: ['effectProtect', 'effectHeal'],
    alteration: ['effectBuff', 'effectCreation'],
    other: ['effectAnalyze', 'effectInvocation'],
    any: ['effectAttack', 'effectDebuff', 'effectProtect', 'effectHeal', 'effectBuff', 'effectCreation', 'effectAnalyze', 'effectInvocation']
};

/**
 * Retrieve current choice for each input and store it inside a map.
 * @param {object} miracle where all relative data are stored
 * @returns The map
 */
const retrieveInputChoices = (miracle) => {
    
    const result = new Map();
    const inputs = miracle.inputs ?? [];
    inputs.forEach( input => {
        result.set(input.key, input.currentChoice);
    });
    return result;
}

/**
 * Add a new element to miracle.input
 * @param {string} inputKey key used later for retrieving data
 * @param {string} title What will be displayed on top of the input
 * @param {object} miracle where all relative data are stored
 * @param {*} availableChoices List of available choices
 * @param {*} currentChoices A map of the previous current choices. Useful for adding sectected attribute
 */
const prepareMiracleSubdata = (inputKey, title, miracle, availableChoices, currentChoices) => {

    const newInput = {
        key: inputKey,
        title: title,
        availableChoices: availableChoices
    };

    // Current selection
    let newChoice = currentChoices.get(inputKey);
    if( ! newChoice || !newInput.availableChoices.find( s => s.key === newChoice.key ) ) {
        newChoice = newInput.availableChoices[0] ?? null;
    }
    newInput.currentChoice = newChoice;
    
    // .selected and css init
    newInput.availableChoices.forEach( s => {
        s.selected = s.key === newChoice?.key;
        s.css = s.selected ? 'selected' : '';
    });

    miracle.inputs.push( newInput );
}


/**
 * Add miracle.inputs[ {key: 'mainElement', ...} ]
 * Store the chosen spirit element. Or give a choice if prismatic spirit is selected.
 * @param {object} miracle where all relative data are stored
 * @param {Map} currentChoices A map of the previous current choices. Useful for adding sectected attribute
 */
 const prepareAvailableMainElements = (miracle, currentChoices) => {

    if( !miracle.element ) { return; } // data skipped

    const spiritElementKey = miracle.choices.find( c => c.element.selected ).element.key;
    const title = translate('AESYSTEM.interaction.sheet.labels.selectedSpirit.element');

    const availableChoices = elementList().filter( el => {
        return spiritElementKey == 'any' || el.key === spiritElementKey;
    }).map(el => {
        return mergeObject({}, el);
    });

    prepareMiracleSubdata('mainElement', title, miracle, availableChoices, currentChoices);
}

/**
 * Add miracle.inputs[ {key: 'divineProtection', ...} ]
 * Seek among actor pacts if one matches the current element
 * @param {AEActor} actor The priest
 * @param {object} miracle where all relative data are stored
 * @param {Map} currentChoices A map of the previous current choices. Useful for adding sectected attribute
 */
 const prepareAvailableDivineProtections = (actor, miracle, currentChoices) => {

    if( !miracle.element ) { return; } // data skipped
    
    const mainElementKey = miracle.inputs.find( i => i.key === 'mainElement' ).currentChoice.key;
    const allPacts = actor.divine.pacts;
    
    const availableChoices = allPacts.filter( pact => {
        return pact.elements.findIndex( el => el.name === mainElementKey ) != -1;
    });
    availableChoices.sort( (a, b) => a.name.localeCompare(b.name) );

    const titleKey = 'AESYSTEM.interaction.sheet.labels.divineProtection.' + ( availableChoices.length > 0 ? 'choose' : 'none' );
    const title = translate(titleKey);

    prepareMiracleSubdata('divineProtection', title, miracle, availableChoices, currentChoices);
}

/**
 * Add miracle.inputs[ {key: 'spiritCapacity', ...} ]
 * Store the chosen spirit capacity. Or give a choice if prismatic spirit is selected.
 * @param {object} miracle where all relative data are stored
 * @param {Map} currentChoices A map of the previous current choices. Useful for adding sectected attribute
 */
 const prepareAvailableSpiritCapacity = (miracle, currentChoices) => {

    if( !miracle.effect ) { return; } // data skipped

    const spiritChoice = miracle.choices.find( c => c.effect.selected );
    const spiritEffectKey = spiritChoice.effect.key;
    const title = translate('AESYSTEM.interaction.sheet.labels.selectedSpirit.capacity');

    const availableChoices = spiritEffectList().filter( el => {
        if( el.key == 'any') { return false; } // 'any' cannot be chosen as input. (Whole goal of this function)
        return spiritEffectKey == 'any' || el.key === spiritEffectKey;
    }).map(el => {
        return mergeObject({}, el);
    });

    prepareMiracleSubdata('spiritCapacity', title, miracle, availableChoices, currentChoices);
}

/**
 * Add miracle.inputs[ {key: 'spellChoice', ...} ]
 * Seek for every available spells matching one of the element spirit, and the selected spirit capacity
 * @param {object} miracle where all relative data are stored
 * @param {Map} currentChoices A map of the previous current choices. Useful for adding sectected attribute
 */
const prepareAvailableMiracleEffects = (miracle, currentChoices) => {

    if( !miracle.element || !miracle.effect ) { return; } // data skipped

    const title = translate('AESYSTEM.interaction.sheet.labels.miracleEffect');
    const spiritCapacity = miracle.inputs.find( i => i.key === 'spiritCapacity' ).currentChoice.key;
    
    // Retrieve all available spells corresponding to the selected spiritCapacity
    const relatedEffects = spellCategories().filter( se => {
        return EFFECT_MAPPING[spiritCapacity].includes(se.effect); 
    });
    const allAvailableSpells = relatedEffects.map( se => {
        return se.spells;
    }).reduce( (list, effectSpells) => {

        const newSpells = effectSpells.filter( spell => {
            return list.findIndex( s => s.key === spell.key ) == -1;
        });
        return list.concat(newSpells);
    }, []);

    // Retrieve all elements in the selected spirits so that we can build the real spell list
    const mainElementKey = miracle.inputs.find( i => i.key === 'mainElement' ).currentChoice.key;

    const selectedElementSpiritId = miracle.element;
    const otherSelectedSpirits = miracle.choices.filter( c => {
        if( c.id === selectedElementSpiritId ) { return false; }
        return c.element.selected || c.effect.selected || c.bonus.selected;
    });
    const prismaticPresentOnOthers = otherSelectedSpirits.reduce( (isPresent, current) => {
        return isPresent || current.element.key == 'any';
    }, false);

    let allElementKeys = []; // Not directly taking it from miracle.choices because of prismatic spirits.
    if( prismaticPresentOnOthers ) {
        allElementKeys = elementList().map( e => e.key );
    } else {
        allElementKeys = otherSelectedSpirits.reduce( (list, current) => {
            const newKey = current.element.key;
            if( !list.includes(newKey) ) { list.push(newKey); }
            return list;
        }, [mainElementKey] );
    }
    const allElements = allElementKeys.map( key => {
        return elementList().find( e => e.key === key );
    });


    // Select available choices among all available spells
    const availableChoices = allElements.reduce( (list, element) => {
        element.spells.forEach( spellKey => {
            // Do not insert duplicates
            if( list.findIndex( s => s.key === spellKey ) == -1 ) { 
                const spell = allAvailableSpells.find( s => s.key === spellKey );
                if( spell ) { list.push( mergeObject({}, spell)); }
            }
        });

        return list;
    }, []);

    prepareMiracleSubdata('spellChoice', title, miracle, availableChoices, currentChoices);
}

/**
 * Add miracle.inputs[ {key: 'spiritBonus', ...} ]
 * Store the chosen spirit bonus. Or give a choice if prismatic spirit is selected.
 * @param {object} miracle where all relative data are stored
 * @param {Map} currentChoices A map of the previous current choices. Useful for adding sectected attribute
 */
 const prepareAvailableSpiriBonus= (miracle, currentChoices) => {

    if( !miracle.bonus ) { return; } // data skipped

    const spiritChoice = miracle.choices.find( c => c.bonus.selected );
    const spiritBonusKey = spiritChoice.bonus.key;
    const title = translate('AESYSTEM.interaction.sheet.labels.selectedSpirit.bonus');

    const availableChoices = spiritBonusList().filter( el => {
        if( el.key == 'any') { return false; } // 'any' cannot be chosen as input. (Whole goal of this function)
        return spiritBonusKey == 'any' || el.key === spiritBonusKey;
    }).map(el => {
        return mergeObject({}, el);
    });

    prepareMiracleSubdata('spiritBonus', title, miracle, availableChoices, currentChoices);
}



/** Build all miracles available choices. */
export const buildMiracleChoices = (actor, miracle) => {

    const cssWhenSelected = ' for-one-icon selected-bg';
    const cssNoSelection = ' for-one-icon simple-bg';

    return actor.cards.spirits.map( s => {

        const id = s.card.id;
        const def = s.spiritDefinition;

        const result = {
            id: id,
            element: mergeObject({}, def.element),
            effect: mergeObject({}, def.effect),
            bonus: mergeObject({}, def.bonus)
        };
        
        // Selection
        result.element.selected = miracle.element === id;
        result.effect.selected = miracle.effect === id;
        result.bonus.selected = miracle.bonus === id;

        // Css
        result.element.css = result.element.selected ? cssWhenSelected : cssNoSelection;
        result.effect.css = result.effect.selected ? cssWhenSelected : cssNoSelection;
        result.bonus.css = result.bonus.selected ? cssWhenSelected : cssNoSelection;

        return result;
    });
}

export class InteractionMainActorPrayerImpl extends InteractionMainActorBattleInterface {

    constructor(mainActor) {
        super('prayer', '02magic', mainActor);

        this.miracle = {};
        this.firstListboxInit();
        this.prepareAdditionalData();
    }

    /** @override */
    get maneuverBase() { return this.mainActor.divine; }

    /** @override */
    get battleScore() { return this.mainActor.battle.prayer; }

    /** @override */
    get isMentalAction() { return true; }

    /** @override */
    get actionName() { 

        return translate('AESYSTEM.interaction.sheet.mainActor.prayer.' + this.attackStyle + '.simple');
    }

    /** @override */
    get actionFlavor() { 
        return this.miracle.choices.find(c => c.effect.selected)?.effect.name ?? '';
    }

    /** @override */
    get actionDetails() {
        const result = super.actionDetails;

        result.push( this._textLineDetail(
            translate('AESYSTEM.interaction.sheet.details.divine'), 
            translate('AESYSTEM.interaction.sheet.mainActor.prayer.attackStyle.' + this.attackStyle)
        ));

        if( this.attackStyle == 'miracle' ) {
            result.push( this._textLineDetail(
                translate('AESYSTEM.interaction.sheet.details.spiritPower'), 
                'Lvl ' + this.mainActor.divine.communion.spiritPower
            ));
        }


        return result;
    }    

    /** @override */
    get targetNeedToDefendItself() { 
        if( this.attackStyle != 'miracle') {
            return false;
        }
        if( this.miracle.spiritsHaveBeenChosen && !this.miracle.offensive ) {
            return false;
        }
        return true;
    }

    /** 
     * When invoking a miracle, there is a need for spirit to be called
     * @override 
     */
     get isValid() {
        if( this.attackStyle != 'miracle' ) {
            return true;
        } 
        return this.miracle.spiritsHaveBeenChosen;
    }


    /** @override */
    get availableDefenseMethods() {
        if( this.attackStyle == 'communion' ) {
            return ['communion'];
        }
        if( this.attackStyle == 'release' ) {
            return ['release'];
        }

        if( this.miracle.spiritsHaveBeenChosen && !this.miracle.offensive ) {
            return ['protectiveMiracle'];
        }

        return super.availableDefenseMethods;
    }


    /** @override */
    get availableActionsCategories() {

        if( this.attackStyle != 'release' ) {
            return this.allActionCategories;
        }
        return this.allActionCategories.filter(c => c.key != 'simple');
    }

    /* ---------------------------
          First lisbox: Cast a spell / Build a circle
    ------------------------------ */

    firstListboxInit() {
        this.attackStyle = 'miracle';

        // Title
        this.styleTitle = translate('AESYSTEM.interaction.sheet.labels.prayer');

        // Available choices
        this.availableStyles = ['miracle', 'communion', 'release'].map( s => {
            return {
                key: s,
                label: translate('AESYSTEM.interaction.sheet.mainActor.prayer.attackStyle.' + s)
            };
        });
    }

    /** @override */
    get firstListboxDisplayed() { return true; }

    /** @override */
    get firstListboxTitle() { return this.styleTitle; }

    /** @override */
    get firstListboxAvailabeChoices() { return this.availableStyles; }

    /** @override */
    get firstListboxCurrentChoice() { return this.attackStyle; }

    /** @override */
    get firstListboxCurrentTokens() { return this.mainActor.battle.prayer.mastery; }

    /** @override */
    get firstListboxCurrentScore() { return this.mainActor.battle.prayer.advantage; }

    /** @override */
    firstListboxChangeChoice(newChoice) {

        if( this.attackStyle != newChoice ) {
            this.attackStyle = newChoice;
            this._actionCategory = this.attackStyle == 'release' ? 'fast': 'simple';

            this._reloadAvailableManeuvers();
        }
        
    }

    /* ---------------------------
          Second lisbox: Maneuver
    ------------------------------ */

    /** @override */
    filteringManeuvers( key, maneuver ) {
        // Maneuvers are only when triggering the miracle.
        return  this.attackStyle == 'miracle';
    }

    /** @override */
    get secondListboxDisplayed() { return this.availableManeuvers.length > 1; }

    /** @override */
    get secondListboxTitle() { return this.maneuverTitle; }

    /** @override */
    get secondListboxAvailabeChoices() { return this.availableManeuvers; }

    /** @override */
    get secondListboxCurrentChoice() { return this.maneuverChoice; }

    /** @override */
    get secondListboxCurrentTokens() { return this.maneuverModifiers.tokens; }

    /** @override */
    get secondListboxCurrentScore() { return this.maneuverModifiers.score; }

    /** @override */
    secondListboxChangeChoice(newChoice) { this.changeManeuver(newChoice); }


    /* ---------------------------
          Right panel handling
    ------------------------------ */

    /** @override */
    prepareAdditionalData() {

        this.miracle.displayed = this.needToConsumeSpirits;

        // Choices are only available if miracle is displayed. It's reset if player change type
        this.miracle.choices = this.miracle.displayed ? buildMiracleChoices(this.mainActor, this.miracle) : [];
        this.miracle.element = this.miracle.choices.find(c => c.element.selected)?.id ?? null;
        this.miracle.effect = this.miracle.choices.find(c => c.effect.selected)?.id ?? null;
        this.miracle.bonus = this.miracle.choices.find(c => c.bonus.selected)?.id ?? null;

        this.miracle.spiritsHaveBeenChosen = this.miracle.element != null && this.miracle.effect != null;
        this.miracle.offensive = this.miracle.choices.find(c => c.effect.selected)?.effect.offensive ?? false;

        // Title
        if( this.miracle.choices.length > 0 ) {
            this.miracle.title = translate('AESYSTEM.interaction.sheet.labels.selectSpirits');
        } else {
            this.miracle.title = translate('AESYSTEM.interaction.sheet.labels.spiritsAreNeeded');
        }

        // Other inputs
        const currentChoices = retrieveInputChoices(this.miracle);
        this.miracle.inputs = [];

        prepareAvailableMainElements(this.miracle, currentChoices);
        prepareAvailableSpiritCapacity(this.miracle, currentChoices);
        prepareAvailableSpiriBonus(this.miracle, currentChoices);

        // Spell choice
        prepareAvailableMiracleEffects(this.miracle, currentChoices);

        // Divine protection
        prepareAvailableDivineProtections(this.mainActor, this.miracle, currentChoices);
    }

    /** @override */
    get needToConsumeSpirits() { return this.attackStyle == 'miracle'; }

    /** @override */
    get miracleDataForRollContext() {

        // Verify if all has been correctly selected
        if(!this.miracle.spiritsHaveBeenChosen) {
            throw translate("AESYSTEM.interaction.sheet.err.noSpiritSelected");
        }

        const result = {};
        result.usedSpirits = this._consumedSpiritData();
        result.miracle = this._createMiracleData();
        result.drain = this._calculateDrain(result.miracle);
    
        return result;
    }
    
    _consumedSpiritData() {

        const miracle = this.miracle;
    
        // Store usedSpirits
        const spiritData = [miracle.element];
        if( !spiritData.includes(miracle.effect) ) { 
            spiritData.push(miracle.effect); 
        }
        if( miracle.bonus && ! spiritData.includes(miracle.bonus) ) { 
            spiritData.push(miracle.bonus); // Third one is optional
        }
        return spiritData;
    }

    _createMiracleData() {

        // Deduce spell from inputs
        const inputs = this.miracle.inputs.reduce( (all, current) => {
            all[current.key] = current.currentChoice;
            return all;
        }, {});

        const miracleData = {};
        const spiritPower = this.mainActor.divine.communion.spiritPower;
        const effectKey = inputs.spellChoice?.relatedEffect ?? 'effectAttack'; // FIXME Need to remove default value when all effect list will have at least a spell
        const bonusKey = inputs.spiritBonus?.key ?? null;

        // 1 - Spell caracteristics
        const caracLevels = {};
        const usedCaracs = spellCategories().find( se => se.key === effectKey ).caracteristics;

        const effectLevel = (bonusKey == 'effect') ? Math.min( 6, spiritPower+1 ) : spiritPower;
        caracLevels[effectKey] = effectLevel;
        if( usedCaracs.includes( 'range' ) ) {
            // Maximum range for spirits
            caracLevels['range'] = 5; 
        }
        if( usedCaracs.includes( 'duration' ) ) {
            caracLevels['duration'] = (bonusKey == 'duration') ? 2 : 1; 
        }
        if( usedCaracs.includes( 'areaOfEffect' ) ) {
            caracLevels['areaOfEffect'] = (bonusKey == 'area') ? 2 : 0; 
        }
        if( usedCaracs.includes( 'clone' ) ) {
            caracLevels['clone'] = (bonusKey == 'duplication') ? 2 : 0; 
        }

        // 2 - Spell shape
        const shapeDef = spellShapes().find( s => s.key === 'rift' );
        miracleData.shape = mergeObject( {}, caracLevels );
        miracleData.shape.fullDesc = shapeDef.generateSpellDesc( caracLevels.clone, caracLevels.range, caracLevels.areaOfEffect, caracLevels.duration );
        mergeObject( miracleData.shape, shapeDef );

        // 3 - Main element
        const mainElementChoice = inputs.mainElement;
        miracleData.mainElement = mergeObject( {}, elementList().find( e => e.key === mainElementChoice.key ));
        
        // 4 - Magic type
        const effect = spellCategories().find( se => se.key === effectKey );
        miracleData.effect= mergeObject( {level: effectLevel}, effect );
        miracleData.effect.spells = undefined; // Unlink all related spells

        // 5 - Spell
        const spell = effect.spells.find( s => s.key === inputs.spellChoice?.key );
        if( spell ) {
            miracleData.spell = {
                level: effectLevel,
                levelIcon : effect.levelIcons[effectLevel]
            };
            mergeObject( miracleData.spell, spell ); 
        }

        // Finalization
        finalizeSpellData(this.mainActor, miracleData);

        return miracleData;
    }

    _calculateDrain(spell) {

        // Deduce spell from inputs
        const bonusInput = this.miracle.inputs.find( i => i.key == 'spiritBonus' )?.currentChoice ?? null;
        const bonusKey = bonusInput?.key ?? null;

        const pactChoice = this.miracle.inputs.find( i => i.key == 'divineProtection' )?.currentChoice ?? null;
        const pactName = pactChoice?.key;

        const element = spell.mainElement.key;
        const effect = spell.effect.key;

        const spiritPower = this.mainActor.divine.communion.spiritPower;


        return this.mainActor.divine.calculateDrain(element, effect, bonusKey, spiritPower, pactName );
    }

    /** @override */
    activateSpecificListeners(sheet, html) {
        this._sheet = sheet;

        html.find(".miracle-section .card-choice").on("click", ".card-show", this._onClickShowSpiritCard.bind(this));
        html.find(".miracle-section .card-choice").on("click", ".card-element", this._onClickSelectElementForMiracle.bind(this));
        html.find(".miracle-section .card-choice").on("click", ".card-effect", this._onClickSelectEffectForMiracle.bind(this));
        html.find(".miracle-section .card-choice").on("click", ".card-bonus", this._onClickSelectBonusForMiracle.bind(this));
        html.find(".miracle-section").on("click", ".change-choice", this._onClickChangeInputChoice.bind(this));
    }

    async _onClickShowSpiritCard(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const cardId = a.parentElement.dataset.card;

        this.mainActor.items.get(cardId)?.sheet.render(true);
    }

    async _onClickSelectElementForMiracle(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const cardId = a.parentElement.dataset.card;

        if( this.miracle.element === cardId ) {
            this.miracle.element = null; 
        } else { 
            this.miracle.element = cardId; 
        }
        this._sheet.render(true);
    }
    
    async _onClickSelectEffectForMiracle(event){
        event.preventDefault();
        const a = event.currentTarget;
        const cardId = a.parentElement.dataset.card;

        if( this.miracle.effect === cardId ) {
            this.miracle.effect = null; 
        } else { 
            this.miracle.effect = cardId; 
        }
        this._sheet.render(true);
    }
    
    async _onClickSelectBonusForMiracle(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const cardId = a.parentElement.dataset.card;

        if( this.miracle.bonus === cardId ) {
            this.miracle.bonus = null; 
        } else { 
            this.miracle.bonus = cardId; 
        }
        this._sheet.render(true);
    }

    async _onClickChangeInputChoice(event) {
        const a = event.currentTarget;
        const newChoiceId = a.dataset.key;
        const inputKey = a.parentElement.dataset.input;

        const inputs = this.miracle.inputs ?? [];
        const relatedInput = inputs.find( input => {
            return input.key === inputKey;
        });
        if( relatedInput ) { 
            relatedInput.currentChoice = relatedInput.availableChoices.find( c => c.key === newChoiceId ) ?? null; 
        }

        this._sheet.render();
    }
}

