const cleanTimetracker = async (gmActor, timetracker) => {

    await gmActor.setFlag('acariaempire', 'timetracker', null);
    await gmActor.setFlag('acariaempire', 'timetracker', timetracker);
}

/**
 * Updates flags in gmActor proxy, so that the current spend time takes the actual initiative of the character
 * @param {*} combatant The actor who had an intiative change 
 * @param {*} wasDeleted or was delete (optional, default: false)
 */
const synchronizeTimeTrackerFlag =  async ( combatant, {wasDeleted=false} = {} )  => {
    
    // Spell trackers can't be automatically updated after push, so there is no need to synchronize them back after creation in the timetracker
    if( isSpellTrack(combatant) ) { return; }

    const actor = combatant?.actor;
    if(!actor) { return; }

    const initiative = wasDeleted ? null : combatant.initiative;
    if( !initiative && initiative != 0 ) { 
        console.log('AE-BattleTracker | Reseting timetracker after combatant reset. ' + combatant.name + ': ' + actor.timetracker + ' => null');
        await actor.setTimetracker(null);

    } else if( actor.timetracker == null || initiative != actor.timetracker ) {
        console.log('AE-BattleTracker | Updating timetracker after combatant init change. ' + combatant.name + ': ' + actor.timetracker + ' => ' + initiative);
        await actor.setTimetracker(initiative);
    }
}

/**
 * Once a battle has ended, remove all trace of timetrack of this battle inside gmActor flags
 * @param {} combatId 
 */
const cleanTimeTrackerProxy = async (combatId) => {
    //Delete stored data for this battle
    const gmActor = game.aesystem.actorTools.gmActor;
    const timetrackerFlags = gmActor.flags?.acariaempire?.timetracker ?? {};
    if(timetrackerFlags[combatId]) {
        const newTimetracker = duplicate(timetrackerFlags);
        delete newTimetracker[combatId];
        cleanTimetracker(gmActor, newTimetracker);
    };
}

/**
 * Once a spellTrack as been removed from combatTracker, remove it from gmActor so that is doesn't come back
 * @param {Combatant} spellTrack A spellTrack combatant which has been removed from tracker
 */
const removeSpellTrack = async (spellTrack) => {

    // Only handle spell tracks
    if( !isSpellTrack(spellTrack) ) { return; }

    const actor = spellTrack?.actor;
    if(!actor) { return; }

    await  actor.stopTrackingSpell( getSpellId(spellTrack) );
}

/**
 * When a spell is removed from the combat tracker. Delete all related effects on tokens
 * @param {Combatant} spellTrack  A spellTrack that have been removed from tracker
 */
const removeActiveEffectFromTokens = async (spellTrack) => {

    // Only handle spell tracks
    if( !isSpellTrack(spellTrack) ) { return; }

    const id = getSpellId(spellTrack);

    for( const token of canvas.tokens.placeables ) {
        await token.actor?.removeSpellEffects([id]);
    }
}


/**
 * When a combatant is removed from the combat tracker, remove allthe spell effects he was under.
 * @param {Combatant} combatant  A real combatant
 */
 const removeAllSpellEffectsFromCombatant = async (combatant) => {

    // Only handle combatant
    if( isSpellTrack(combatant) ) { return; }

    const spellIds = combatant.actor?.spellEffects.map( se => se.spellId ) ?? [];
    if( spellIds.length > 0 ) {
        await combatant.actor.removeSpellEffects(spellIds);
    }
}


/** 
 * Some combatant are not really linked to the actor token.
 * But are only here for keeping tracks of spell durations
 */
 const isSpellTrack = (combatant) => {
    return getSpellId(combatant) ? true :false;
}

/** 
 * Fictive combatant all have a spellId which is a guid generated when inserted on track
 */
 const getSpellId = (combatant) => {
    return combatant.getFlag('acariaempire', 'trackedSpell');
}

/** 
 * No turn management. Always first on list will be playing.
 */
const disableTurAndRoundManagement = async (parent) => {
    const round = parent.combatants.size > 0 ? 1 : 0;
    return parent.update({round: round, turn: 0});    
}

/**
 * Will set the current combatant as the first in the turn order
 * Available as long as there is at least one initiative set.
 * @returns A ContextMenu option
 */
const contextOptionInterrupt = () => {
    return  {
        name: "AESYSTEM.combat.contextmenu.enters.now",
        icon: '<i class="fas fa-angle-double-up"></i>',
        condition: li => {
            const combat = ui.combat.viewed;
            if( !combat.started ) { return false;} 
            if( combat.combatants.filter( c => c.initiative ).length == 0 ) { return false; }
            
            const combatant = combat.combatants.get(li.data("combatant-id"));
            return !isSpellTrack(combatant);
        },
        callback: li => {
            const combat = ui.combat.viewed;
            const combatant = combat.combatants.get(li.data("combatant-id"));
            if ( combatant?.actor && !isSpellTrack(combatant) ) {
                return combat.combatantInterruptsBattle(combatant);
            }
        }
    };
}

/**
 * The related actor will start this battle as surprised.
 * Available as long as there is an actor related to this combatant
 * @returns A ContextMenu option
 */
 const contextOptionEntersBattle = () => {
    return  {
        name: "AESYSTEM.combat.contextmenu.enters.classic",
        icon: '<i class="fas fa-angle-double-right"></i>',
        condition: li => {
            const combatant = ui.combat.viewed.combatants.get(li.data("combatant-id"));
            return combatant?.actor && !isSpellTrack(combatant);
        },
        callback: li => {
            const combat = ui.combat.viewed;
            const combatant = combat.combatants.get(li.data("combatant-id"));
            if ( combatant?.actor && !isSpellTrack(combatant) ) {
                return combat.combatantEntersBattle(combatant);
            }
        }
    };
}

/**
 * The related actor will start this battle as surprised.
 * Available as long as there is an actor related to this combatant
 * @returns A ContextMenu option
 */
 const contextOptionSurprised = () => {
    return  {
        name: "AESYSTEM.combat.contextmenu.enters.surprised",
        icon: '<i class="fas fa-angle-right"></i>',
        condition: li => {
            const combatant = ui.combat.viewed.combatants.get(li.data("combatant-id"));
            return !isSpellTrack(combatant);
        },
        callback: li => {
            const combat = ui.combat.viewed;
            const combatant = combat.combatants.get(li.data("combatant-id"));
            if ( combatant?.actor && !isSpellTrack(combatant) ) {
                return combat.combatantStartsSurprised(combatant);
            }
        }
    };
}

/**
 * The related actor is doing a fast mental action during the battle.
 * Available once the battle has start
 * @returns A ContextMenu option
 */
 const contextOptionDoSomeAction = (actionSpeed, actionType, icon) => {
    return  {
        name: 'AESYSTEM.combat.contextmenu.' + actionSpeed + '.' + actionType,
        icon: '<i class="' + icon + '"></i>',
        condition: li => {
            const combat = ui.combat.viewed;
            const combatant = combat.combatants.get(li.data("combatant-id"));
            const init = combatant?.initiative;
            return combat.started && !isSpellTrack(combatant) && (init || init == 0);
        },
        callback: li => {
            const combat = ui.combat.viewed;
            const combatant = combat.combatants.get(li.data("combatant-id"));
            if ( combatant?.actor ) {
                const neededTime = combatant.actor.neededTime[actionSpeed];
                return combat.combatantSpendTime(combatant, neededTime[actionType]);
            }
        }
    };
}

const contextOptionRemoveActor = () => {
    return  {
        name: "AESYSTEM.combat.contextmenu.remove.actor",
        icon: '<i class="fas fa-trash"></i>',
        condition: li => {
            const combatant = ui.combat.viewed.combatants.get(li.data("combatant-id"));
            return !isSpellTrack(combatant);
        },
        callback: li => {
            const combat = ui.combat.viewed;
            const combatant = combat.combatants.get(li.data("combatant-id"));
          if ( combatant ) return combatant.delete();
        }
    };
}

const contextOptionRemoveMagicEffet = () => {
    return  {
        name: "AESYSTEM.combat.contextmenu.remove.magicEffect",
        icon: '<i class="fas fa-trash"></i>',
        condition: li => {
            const combatant = ui.combat.viewed.combatants.get(li.data("combatant-id"));
            return isSpellTrack(combatant);
        },
        callback: li => {
            const combat = ui.combat.viewed;
            const combatant = combat.combatants.get(li.data("combatant-id"));
          if ( combatant ) return combatant.delete();
        }
    };
}

/**
 * Alter context menu for combatants
 * @param {object[]} entryOptions 
 */
export const alterCombatantContextMenu = ( entryOptions ) => {

	// Remove unsued options
    let index = entryOptions.findIndex( eo => eo.name == 'COMBAT.CombatantClear' );
	if( index != -1 ) { entryOptions.splice(index, 1); }

	entryOptions.findIndex( eo => eo.name == 'COMBAT.CombatantReroll' );
	if( index != -1 ) { entryOptions.splice(index, 1); }

	entryOptions.findIndex( eo => eo.name == 'COMBAT.CombatantRemove' );
	if( index != -1 ) { entryOptions.splice(index, 1); }

    // New options
    entryOptions.push( contextOptionDoSomeAction( 'simpleAction', 'mental', 'fas fa-hourglass') );
    entryOptions.push( contextOptionDoSomeAction( 'simpleAction', 'physical', 'fas fa-hourglass') );
    entryOptions.push( contextOptionDoSomeAction( 'fastAction', 'mental', 'far fa-hourglass') );
    entryOptions.push( contextOptionDoSomeAction( 'fastAction', 'physical', 'far fa-hourglass') );
    entryOptions.push( contextOptionInterrupt() );
    entryOptions.push( contextOptionEntersBattle() );
    entryOptions.push( contextOptionSurprised() );

    // Remove if put at the end of the list and has a different label depending on the fact it's an actor or a spell
    entryOptions.push( contextOptionRemoveActor() );
    entryOptions.push( contextOptionRemoveMagicEffet() );
}


/**
 * For managing combat tracker data
 */
 export class AECombat extends Combat {


    get timePassedSinceBeginning() {

        if(!this.started) { return 0;}

        const combatantsWithInit = this.combatants.filter( c => {
            return  ( c.initiative || c.initiative == 0 );
        });
        if( combatantsWithInit.length == 0 ) { return 0; } // Now time have passed since nobody has played.
    
        const lowestInit = combatantsWithInit.map( c => {
            return c.initiative;
        }).reduce( (init, current) => {
            return current < init ? current : init;
        });
        return lowestInit - 1;
    }

    findSpellTrack( spellId ) {
        return this.combatants.find( c => getSpellId(c) === spellId );
    }

    async combatantInterruptsBattle(combatant) {
        const currentTime = this.timePassedSinceBeginning;
        if( !combatant.initiative  || combatant.initiative != currentTime + 1 ) {
            const newInit = Math.max( 0, currentTime );
            await combatant.update( {initiative: newInit} );
        }
    }

    async combatantStartsSurprised(combatant) {
        const init = combatant.actor.neededTime.battle.surprised + this.timePassedSinceBeginning;
        return combatant.update( {initiative: init} );
    }

    async combatantEntersBattle(combatant) {
        const init = combatant.actor.neededTime.battle.enters + this.timePassedSinceBeginning;
        return combatant.update( {initiative: init} );
    }

    async combatantSpendTime(combatant, timeSpent) {
        const init = combatant.initiative + timeSpent;
        return combatant.update( {initiative: init} );
    }

    /**
     * Backward sort. Combatant with no initiative set are put at the end.
     * @override
     */
    _sortCombatants(a, b) {
        const ia = Number.isNumeric(a.initiative) ? a.initiative : 9999;
        const ib = Number.isNumeric(b.initiative) ? b.initiative : 9999;
        let ci = ia - ib;
        if ( ci !== 0 ) return ci;
        let [an, bn] = [a.token?.name || "", b.token?.name || ""];
        let cn = an.localeCompare(bn);
        if ( cn !== 0 ) return cn;
        return a.tokenId - b.tokenId;
    }

    /**
     * Dangerous rework. Skip the message
     * @override
     */
    async rollInitiative(ids, {formula=null, updateTurn=true, messageOptions={}}={}) {
        
        // Structure input data
        ids = typeof ids === "string" ? [ids] : ids;
        const currentId = this.combatant.id;

        // Iterate over Combatants, performing an initiative roll for each
        const updates = [];
        for( const id of ids ) {
            // Get Combatant data
            const c = this.combatants.get(id);
            if ( !c || !c.isOwner || !c.actor || isSpellTrack(c) ) {
                continue;
            }

            if( !c.initiative && c.initiative != 0 ) {
                // Rolling initiative doesn't take timePassedSinceBeginning into account.
                // Need to use combatantEntersBattle if you want to set it while comparing it to other combatants
                const init = c.actor.neededTime.battle.enters;
                updates.push({_id: id, initiative: init});
            }
        }

        // Need to update combatant data?
        if ( updates.length > 0 ) {
            await this.updateEmbeddedDocuments("Combatant", updates);

            // Ensure the turn order remains with the same combatant
            if ( updateTurn ) {
              await this.update({turn: this.turns.findIndex(t => t.id === currentId)});
            }
        }
        return this;
    }

    /** @override */
    _onDelete(...args) {
        super._onDelete(args);
        
        cleanTimeTrackerProxy(this.id);
        // Clean all spell effects on combatants
        for( const combatant of this.combatants ) {
            removeAllSpellEffectsFromCombatant(combatant);
        }
    }


    /** @override */
    _onCreateEmbeddedDocuments(embeddedName, child, options, userId) {
        super._onCreateEmbeddedDocuments(embeddedName, child, options, userId);
    }

    /** @override */
    _onUpdateEmbeddedDocuments(embeddedName, documents, result, options, userId) {
        super._onUpdateEmbeddedDocuments(embeddedName, documents, result, options, userId);

        if( embeddedName == 'Combatant' && game.user.id === userId ) {
            const updatedCombatants = documents instanceof Array ? documents : [documents];
            const realCombatants = updatedCombatants.filter(c => !isSpellTrack(c));
            realCombatants.forEach( c => {
                synchronizeTimeTrackerFlag(c);
            });
        }
    }

    /** @override */
    _onDeleteEmbeddedDocuments(embeddedName, documents, result, options, userId) {
        super._onDeleteEmbeddedDocuments(embeddedName, documents, result, options, userId);

        if( embeddedName == 'Combatant' && game.user.id === userId ) {
            const updatedCombatants = documents instanceof Array ? documents : [documents];
            
            const realCombatants = updatedCombatants.filter(c => !isSpellTrack(c));
            realCombatants.forEach( c => {
                synchronizeTimeTrackerFlag(c, {wasDeleted: true} );
                removeAllSpellEffectsFromCombatant(c);
            });

            const deleteSpells = updatedCombatants.filter(c => isSpellTrack(c));
            deleteSpells.forEach( c => {
                removeSpellTrack(c);
                removeActiveEffectFromTokens(c);
            });
        }
    }

    /** 
     * @override 
     */
    async previousRound() {
        return disableTurAndRoundManagement(this);
    }

    /** 
     * @override 
     */
     async nextRound() {
        return disableTurAndRoundManagement(this);
    }

    /** 
     * @override 
     */
     async previousTurn() {
        return disableTurAndRoundManagement(this);
    }


    /** 
     * @override 
     */
     async nextTurn() {
        return disableTurAndRoundManagement(this);
    }


    /**
     * When battle starts, every present combatant automatically roll their dice.
     * Reseting initiative means : interrupting battle.
     * @override
     */
     async resetAll() {
         // Interrupting battle
        await this.update({round: 0, turn: 0}); 

        // Reset all intiative. (Rewrote parent method since it didn't trigger the _onUpdateEmbeddedDocuments )
        const updates = [];
        this.combatants.filter( combatant => {
            return combatant.initiative || combatant.initiative == 0;
        }).forEach( combatant => {
            updates.push({_id: combatant.id, initiative: null});
        });
        if( updates.length > 0 ) {
            await this.updateEmbeddedDocuments("Combatant", updates);
        }
    }

    /**
     * When battle starts, every present combatant automatically roll their dice.
     * @override
     */
    async startCombat() {
        await super.startCombat();
        await this.rollAll();
    }


    async gmTimetrackersMayHaveChanged(modifiedTokenIds) {

        // Only the GM need to update the battle tracker
        if( ! game.user.isGM ) return;

        const gmActor = game.aesystem.actorTools.gmActor;
        const combatTrackerFlags = gmActor.flags?.acariaempire?.combattracker ?? {};

        const combatFlags = combatTrackerFlags[this.id] ?? {};
        const relatedEntries = Object.entries(combatFlags).filter( ([key, value]) => {
            return modifiedTokenIds.includes(key);
        });
        
        const newInitiatives = relatedEntries.filter( ([key, value]) => {
            return value.initiative || value.initiative == 0;
        }).map(([key, value]) => {
            return {
                tokenId: key,
                newInit: value.initiative
            };
        });

        const newSpells = [];
        relatedEntries.filter( ([key, value]) => {
            return value.spells;
        }).forEach( ([tokenId, value]) => {
            const spells = Object.entries( value.spells );
            spells.forEach( ([spellId, spellData]) => {
                if( !spellData.removed ) {
                    newSpells.push( {tokenId: tokenId, spellId: spellId, spellData: spellData} );
                }
            });
        });

        await this._updateCombatantInitiativeAfterGMTimetrackerChanged(newInitiatives);
        await this._updateSpellTracksAfterGMTimetrackerChanged(newSpells);
    }


    /**
     * Find which combatant have its initiative changed and update combat tracker
     * @param {object[]} newInitiatives Eached modified token on gmActor stored as {tokenId: tokenId, newInit: initiative}
     */
    async _updateCombatantInitiativeAfterGMTimetrackerChanged(newInitiatives) {

        const updates = [];
        for( const initData of newInitiatives ) {

            // Find related combatant
            const combatant = this.combatants.find( c => {
                if( isSpellTrack(c) ) { return false; }
                const tokenId = c.actor?.tokenId ?? null;
                return tokenId === initData.tokenId;
            });
            if( !combatant ) { continue; }

            // Do not set initiative of some token that should be reset
            if( ! combatant.initiative && combatant.initiative != 0 ) { continue; }

            if( combatant.initiative != initData.newInit ) {
                console.log('AE-BattleTracker | Updating combatant init after token change. ' + combatant.name + ': ' + combatant.initiative + ' => ' + initData.newInit );
			    updates.push({_id: combatant.id, initiative: initData.newInit });
            }
        }
        if( updates.length > 0 ) {
            await this.updateEmbeddedDocuments("Combatant", updates);
        }
    }

    /**
     * Find which new spells have ben cast and push it on combat tracker
     * @param {object[]} newSpells Eached modified spell entries on gmActor stored as {tokenId: tokenId, spellId: spellId, spellData: spellData}
     */
    async _updateSpellTracksAfterGMTimetrackerChanged(newSpells) {

        const currentSpellIdsOnTracker = this.combatants.filter( c => {
            return isSpellTrack(c); 
        }).map( c => {
            return {
                tokenId: c.actor?.tokenId ?? null,
                spellId: getSpellId(c)
            };
        });

        const creations = [];
        for( const spellLine of newSpells ) {

            const existingTrack = currentSpellIdsOnTracker.find( c => {
                return c.tokenId === spellLine.tokenId && c.spellId === spellLine.spellId;
            });
            if( existingTrack ) { continue; }

            const spell = spellLine.spellData.spell;
            const initiative = spellLine.spellData.until ?? 0;

            creations.push({
                tokenId: spellLine.tokenId, 
                hidden: false, 
                initiative: initiative, 
                name: spell.name, 
                img: spell.icon,
                "flags.acariaempire.trackedSpell": spellLine.spellId
            });
        };
        if( creations.length > 0 ) {
            await this.createEmbeddedDocuments("Combatant", creations);
        }
    }
}

