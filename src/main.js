import { translate } from './tools/Translator.js';
import { assertGMOnly } from './tools/gmactions.js';
import { initShortcuts } from './tools/WaIntegration.js';
import { manageCustomEffects } from './tools/activeEffects.js';
import { overrideUserConfigPopup } from './user/playerconfig.js';

// Import Modules
import { handleActorDisplayInSidebar, handleActorsDefaultData } from "./actor/actor.js";
import { alterCombatantContextMenu } from './combat/combat.js';
import { alterCardStacksDefinition, loadCardTemplates, overrideGlobalRTUCardsSettings } from './card/CustomStackDefinition.js';
import { AEActorTools } from './actor/ActorTool.js';
import * as config from './tools/config.js';
import { registerAskingDifficultySocket } from './sockets/AskingDifficultyGUI.js';
import { AEInteractionGUIForActor } from './interaction/InteractionGUIForActor.js';
import { registerMerchantTransactionSockets } from './sockets/MerchantTransactions.js';

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

Hooks.once("init", async function() {

    console.log('AE-System | Initializing Acaria Empire System');

	config.setDefaultSheets();
	config.registerSettings();

	await loadCardTemplates();
});

Hooks.once('ready', async () => {

    game.aesystem = new AESystem();
	initShortcuts();
	await overrideGlobalRTUCardsSettings();

	// GM Actor should not be displayed when selecting player character
	if( !game.user.isGM ) { overrideUserConfigPopup(); }
	
	Hooks.call("acariaEmpireReady");
});

Hooks.on('applyActiveEffect', (actor, change) => {
	manageCustomEffects(actor,change);
});

/**
 * Triggered when the create is soon to be created
 */
 Hooks.on('preCreateActor', (actorData) => {

	handleActorsDefaultData(actorData);
});

/**
 * For Keeping track of player XP
 */
Hooks.on("renderActorDirectory", (app, html, data) => {

	const actorDisplays = html.find(".directory-item.document.actor");
	for( let index = 0; index < actorDisplays.length; index++ ) {
		const element = actorDisplays[index];
		const actorId = element.dataset.documentId;
		const actor = app.documents.find( a => a.id === actorId );
		handleActorDisplayInSidebar(element, actor);
	}
});
  
Hooks.on("getCombatTrackerEntryContext", (app, entryOptions) => {

	alterCombatantContextMenu(entryOptions);
});

/**
 * For loading the different card stacks
 */
Hooks.on("loadCardStacksDefinition", (cardStacksDefinition) => {

	alterCardStacksDefinition(cardStacksDefinition);
});

/**
 * For communicating between players and GMs
 */
 Hooks.once("socketlib.ready", () => {
	socketlib.registerSystem("acariaempire");
	registerAskingDifficultySocket();
	registerMerchantTransactionSockets();
});

  

export class AESystem {

	constructor() {

		this.actorTools = new AEActorTools();
		this.interactionGUIForActor = new AEInteractionGUIForActor();
	}

	attackWithMainWeapon({tokenId=null, actorId=null}={}) {

		// Protagonists
		let actor= game.aesystem.actorTools.getActorOrTokenActor({tokenId: tokenId, actorId: actorId}); // If not specified, will take the current selection
		if( ! actor ) {	return ui.notifications.warn(game.i18n.localize("AESYSTEM.roll.noActorSelected")); }

		const weapon = actor.equipment.mainWeapon;
		const battle = weapon?.category ?? 'improvised';
		this.triggerRollPanel({tokenId: tokenId, actorId: actorId, battle:battle });
	}

	/**
	 * Display the GUI for doing rolls
	 * @returns {boolean} True if the GUI succeded in being displayed
	 */
	triggerRollPanel({tokenId=null, actorId=null, 
					  attribute=null, abilityCheck=null, skillChoice=null, 
					  battle=null, maneuver=null, battleSubchoice=null,
					  difficulty=null}={}) {

	    const gui = this.interactionGUIForActor;

		// Protagonists
		let actor= game.aesystem.actorTools.getActorOrTokenActor({tokenId: tokenId, actorId: actorId}); // If not specified, will take the current selection
		if( ! actor ) {	
			ui.notifications.warn(game.i18n.localize("AESYSTEM.roll.noActorSelected")); 
			return false;
		}
		
		const targetTokens = Array.from(game.user.targets) ?? [];
		const targets = targetTokens.map( t => t.actor );
		gui.newInteraction(actor, targets);

		gui.mainActorData.initialize(
			attribute, 
			abilityCheck, 
			skillChoice, 
			battle, 
			maneuver, 
			battleSubchoice
		);

		const availableMethods = gui.mainActorData.impl.availableDefenseMethods;
		gui.defenseData.computeAvailableMethods(availableMethods);
		gui.defenseData.initialize(
			difficulty,
			battle, 
			battleSubchoice
		);

		gui.render(true);
		return true;
	}

	async equipItemAndUseIt({tokenId=null, actorId=null, itemId=null, itemName=null}={}) {
        const actor = game.aesystem.actorTools.getActorOrTokenActor({tokenId: tokenId, actorId: actorId, allowCurrentSelection: true, allowActorWithoutToken: true});
		let eqtItem = actor?.equipment.all.find(e => e.item.id === itemId);
		if( !eqtItem ) { 
			// Try to retrieve it by its name (useful when duplicating token and wanting only one hotbar action)
			eqtItem = actor?.equipment.all.find(e => e.item.name === itemName);
		}
		if( ! eqtItem ) { 
			// If still not here : Equipment is not on this character
			return ui.notifications.warn(translate('AESYSTEM.actor.itemNotInEquipment'));
		}

		// Equip item if possible
		if( !eqtItem.isEquipped && eqtItem.canBeEquipped ) {
			await actor.equipment.equipItem(eqtItem);
		}

		const currentEqt = actor?.equipment.currentlyEquipped;
		if( eqtItem.item.id === currentEqt.weapon.item.id ) {
			// Weapon use : Attack
			await this.attackWithMainWeapon( {tokenId: actor.tokenId, actorId: actor.id} );
		} else {
			// For ohers : Show on chat
			await eqtItem.showOnChat();
		}
	}

	/**
	 * Allow the GM to reset all tokens and cards for all actors
	 */
	async gmChangeScene() {

        // GM action
		if( assertGMOnly() ) return;

		const cardStacks = game.modules.get('ready-to-use-cards').cardStacks;
		await cardStacks.decks.event.resetDeck();
		for( let actor of game.aesystem.actorTools.allTokens ) {
            await actor.resetAllTokens();
        }

		ui.notifications.info(translate('AESYSTEM.changescene.done'));
	}

	/**
	 * Change the scene and also reset guardian spirit behavior
	 */
	 async gmChangeDay() {

        // GM action
		if( assertGMOnly() ) return;

		await this.gmChangeScene();

		// Give a new trouble to each character
		const cardStacks = game.modules.get('ready-to-use-cards').cardStacks;
		await cardStacks.decks.trouble.resetDeck();
        for( const actor of game.actors.filter( a => a.guardianSpirit.canContractTroubles ) ) {
			await actor.guardianSpirit.updateTrougleGaugeWithDayPassing();
            await actor.cards.drawTroubles(1);
        }

		ui.notifications.info(translate('AESYSTEM.changeday.done'));
	}

    /** Allow initiative update for a token from outside */
    async gmSpendTime({tokenId=null, amount=1}={}) {

        // GM action
		if( assertGMOnly() ) return;

		const actorToken = game.aesystem.actorTools.getActorOrTokenActor({tokenId: tokenId})
        if( ! actorToken )  return ui.notifications.warn(translate('AESYSTEM.actor.noneSelected'));

		// Update time track if needed
        return actorToken.spendTime( amount );
    }

	async gmGiveToken({actorId=null, advantage=0, mastery=0}={}) {

        // GM action
		if( assertGMOnly() ) return;

		const actor = this.actorTools.getCharacter(actorId);
		if( ! actor ) {
			return ui.notifications.warn(translate("AESYSTEM.actor.noneSelected"));
		}

		if( advantage != 0 ) { await actor.setAdvantage(actor.advantage + advantage); }
		if( mastery != 0 ) { await actor.setMastery(actor.mastery + mastery); }
	}

	async gmResetTokens({actorId=null}={}) {

        // GM action
		if( assertGMOnly() ) return;

		const actor = this.actorTools.getCharacter(actorId);
		if( ! actor ) {
			return ui.notifications.warn(translate("AESYSTEM.actor.noneSelected"));
		}

		await actor.resetAllTokens();

		return ui.notifications.info(translate("AESYSTEM.resettoken.done"));
	}
}