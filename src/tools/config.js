import { AEActor } from '../actor/actor.js';
import { AEActorSheet } from '../actor/ActorSheet.js';
import { AECard } from '../card/AECard.js';
import { AECombat } from '../combat/combat.js';
import { AEItem } from '../items/item.js';
import { AEItemSheet } from '../items/ItemSheet.js';
import { AEChatMessage } from '../message/message.js';

export const setDefaultSheets = () => {
    // Define custom Entity classes
	CONFIG.ChatMessage.documentClass = AEChatMessage;
	CONFIG.Combat.documentClass = AECombat;
    CONFIG.Actor.documentClass = AEActor;
	CONFIG.Item.documentClass = AEItem;
	CONFIG.Card.documentClass = AECard;

    // Register sheet application classes
    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet("acariaEmpire", AEActorSheet, { makeDefault: true });
    Items.unregisterSheet("core", ItemSheet);
    Items.registerSheet("acariaEmpire", AEItemSheet, { makeDefault: true });
}

export const registerSettings = () => {

    // Register GM settings
    game.settings.register("acariaempire", "removingXPAllowed", {
		name: "AESYSTEM.config.removingXP.name",
		hint: "AESYSTEM.config.removingXP.details",
		scope: "world",
		type: Boolean,
		default: false,
		config: true
	});
    game.settings.register("acariaempire", "displayActorSpirit", {
		name: "AESYSTEM.config.displayActorSpirit.name",
		hint: "AESYSTEM.config.displayActorSpirit.details",
		scope: "world",
		type: Boolean,
		default: true,
		config: true
	});
}

export const isConfiguredWithRemovingXPAllowed = () => {
    return game.settings.get("acariaempire", "removingXPAllowed");
}


export const isConfiguredWithActorSpiritDisplayed = () => {
    return game.settings.get("acariaempire", "displayActorSpirit");
}
