/**
 * Among the weapon affixes, take the last one with damage element modifier and return it as the current element key
 * If there is no affix with this modifier, return the item base element
 * @param {string} baseElement item base element
 * @param {object[]} affixes List of item affixes
 * @returns current element key for this item
 */
export const modifyElementWithAffixes = (baseElement, affixes) => {
    const filteredAffixes = affixes.filter(a => a.weaponUpgrade).map(a => a.weaponUpgrade);
    const damageModifiers = filteredAffixes.reduce( (newArray, current) => {
        return newArray.concat(current.damageModifiers);
    }, []);

    let result = baseElement;
    damageModifiers.forEach( dm => {
        if( dm.element ) { result = dm.element }
    });
    return result;
}

/**
 * Alter base damage by taking into account affixes
 * @param {*} baseDamage Should be a simple value
 * @param {*} affixes Your items affixes
 * @returns In the form  [ [partialDamage], [completeDamage], [incredibleDamage] ]. Each one with [{initialValue, value}, {..}, {..}, {..}, {..}
 */
export const modifyDamageWithAffixes = (baseDamage, affixes) => {

    // Only handle weaponUpgrade affixes for now
    const filteredAffixes = affixes.filter(a => a.weaponUpgrade).map(a => a.weaponUpgrade);

    // 0 : Prepare useful methods
    //--------------------------
    const computeDamage = ( modifier ) => {
        let dmg = baseDamage + modifier.flat;
        dmg = Math.ceil( dmg * modifier.multiplier.beforeArmor );

        const reduction = Math.max( modifier.armorLevel - modifier.penetration, 0 );
        dmg = Math.max( dmg - reduction, 0 );
        dmg = Math.ceil( dmg * modifier.multiplier.afterArmor );
        return dmg;
    };

    const validAffixForModifier = (affix, modifier) => {
        if( !affix.on ) { return true; }

        if( affix.on == 'no_armor') { return modifier.armorLevel == 0; }
        if( affix.on == 'big_armor') { return modifier.armorLevel >= 3; }
        if( affix.on == 'partial') { return modifier.successLevel == 0; }
        if( affix.on == 'incredible') { return modifier.successLevel == 2; }
        return false;
    };

    const damageModifierForEachSuccessLevel = [
        { successLevel:0, flat:0,  multiplier: { beforeArmor: 1, afterArmor: 0.5}, penetration:0 }, // Partial success
        { successLevel:1, flat:0,  multiplier: { beforeArmor: 1, afterArmor: 1}, penetration:0 }, // Complete success
        { successLevel:2, flat:0,  multiplier: { beforeArmor: 2, afterArmor: 1}, penetration:0 }  // Incredible success
    ];

    const allDamagesDefinition = damageModifierForEachSuccessLevel.map( modifier => {
        return [0, 1, 2, 3, 4, 5, 6].map( armorLevel => {
            return mergeObject( {armorLevel: armorLevel}, modifier);
        });
    });

    // 1 : Start calculating initialDamage for given baseDamage
    //-----------------------------------------------------------
    const allDamages = allDamagesDefinition.map( successLine => {
        return successLine.map( modifier => { // One modifier for each armor value
            const data = {
                initialValue: computeDamage(modifier)
            };
            return mergeObject(data, modifier); // Adding initialValue to each element
        });
    });


    // 2 : Loop through each damageModifier affix and edit modifier
    //--------------------------------------------------------------
    const damageModifiers = filteredAffixes.reduce( (newArray, current) => {
        return newArray.concat(current.damageModifiers);
    }, []);
    damageModifiers.forEach( affixModifier => {

        allDamages.forEach( successLine => {
            successLine.forEach( modifier => {
                if( validAffixForModifier(affixModifier, modifier) ) {

                    modifier.flat += affixModifier.flat;
                    modifier.penetration += affixModifier.penetration;
                    modifier.multiplier.beforeArmor *= affixModifier.multiplier.beforeArmor;
                    modifier.multiplier.afterArmor *= affixModifier.multiplier.afterArmor;
                }
            });
        });
    });

    // 2 : Calculate actual damages
    //-----------------------------
    const actualDamages = allDamages.map( successLine => {
        return successLine.map( modifier => { // One modifier for each armor value
            const data = {
                value: computeDamage(modifier)
            };
            return mergeObject(data, modifier); // Adding value to each element
        });
    });

    return actualDamages;
}

/**
 * Alter base damage by taking into account affixes
 * @param {int} baseHeal Should be a simple value
 * @param {string} element Element used for this heal
 * @param {object[]} affixes Your items affixes
 * @returns In the form  [partialHeal], [completeHeal], [incredibleHeal]. Each one with [{initialValue, value}, {..}, {..}, {..}, {..}
 */
 export const modifyHealWithAffixes = (baseHeal, element, affixes) => {

    // Only handle weaponUpgrade affixes for now
    const filteredAffixes = affixes.filter(a => a.weaponUpgrade).map(a => a.weaponUpgrade);

    // 0 : Prepare useful methods
    //--------------------------
    const computeHeal = ( modifier ) => {
        let heal = baseHeal + modifier.flat;
        heal = Math.ceil( heal * modifier.multiplier );
        return heal;
    };

    const validAffixForModifier = (affix) => {
        return !affix.withElement || affix.withElement === element;
    };

    const successLine = [
        { successLevel:0, flat:0,  multiplier: 0.5 }, // Partial success
        { successLevel:1, flat:0,  multiplier: 1 }, // Complete success
        { successLevel:2, flat:0,  multiplier: 2 }  // Incredible success
    ];

    // 1 : Start calculating initialHeal for given baseHeal
    //-----------------------------------------------------------
    const allHeals = successLine.map( modifier => { // One modifier for each armor value
        const data = {
            initialValue: computeHeal(modifier)
        };
        return mergeObject(data, modifier); // Adding initialValue to each element
    });


    // 2 : Loop through each damageModifier affix and edit modifier
    //--------------------------------------------------------------
    // FIXME : healModifier is not implemented yet.
    filteredAffixes.filter(a => a.healModifier).forEach( affix => {

        const affixModifier = affix.healModifier;

        allHeals.forEach( modifier => {
            if( validAffixForModifier(affixModifier) ) {
                modifier.flat += affixModifier.flat;
                modifier.multiplier *= affixModifier.multiplier;
            }
        });
    });

    // 2 : Calculate actual damages
    //-----------------------------
    const actualHeals = allHeals.map( modifier => {
        const data = {
            value: computeHeal(modifier)
        };
        return mergeObject(data, modifier); // Adding value to each element
    });

    return actualHeals;
}

export const modifySpeedWithAffixes = (baseSpeed, affixes) => {

    // Only handle weaponUpgrade affixes for now
    const filteredAffixes = affixes.filter(a => a.weaponUpgrade).map(a => a.weaponUpgrade);

    const speed = {
        initialValue:baseSpeed,
        value: baseSpeed
    };

    filteredAffixes.filter(affix => {
        return affix.speedModifier != null;
    }).forEach( affix => {
        speed.value += affix.speedModifier;
    });

    return speed;
}

/**
 * Merge all possible affix with onAttackSuccess part. And presen it in an array form, for each successLevel
 * @param {object[]} affixes Affixes object, as describe in WAIntegration.affixList
 * @returns In the form  [partialSucess], [completeSucess], [incredibleSuccess]. Each one with [{selfDamage: , powerGain: }]
 */
export const mergeOnAttackSucessAffixes = ( affixes ) => {

    const successLine = [
        { successLevel:0, selfDamage:0,  powerGain: 0 }, // Partial success
        { successLevel:1, selfDamage:0,  powerGain: 0 }, // Complete success
        { successLevel:2, selfDamage:0,  powerGain: 0 }  // Incredible success
    ];

    const validAffixForModifier = (affix, modifier) => {
        if( !affix.on ) { return true; }
        if( affix.on == 'partial') { return modifier.successLevel == 0; }
        if( affix.on == 'incredible') { return modifier.successLevel == 2; }
        return false;
    };

    const onSuccessAffixes = affixes.filter( (affix) => {
        return affix.weaponUpgrade;
    }).reduce( (newArray, affix) => {
        return newArray.concat(affix.weaponUpgrade.onAttackSuccess);
    }, []);

    onSuccessAffixes.forEach( affix => {
        successLine.forEach( modifier => {
            if( validAffixForModifier(affix, modifier) ) {
                modifier.selfDamage += affix.selfDamage;
                modifier.powerGain += affix.powerGain;
            }
        });
    });
    return successLine;
}

