import { modifyDamageWithAffixes, modifyHealWithAffixes } from './affixes.js';
import { EFFECT_PRIORITY, INVOC_COMPENDIUM, INVOC_IMPROVISED_MANEUVER, INVOC_MELEE_MANEUVER, INVOC_RANGED_MANEUVER, INVOC_SONGS_MELODY, SPELLEFFECT_STACK_TYPE, WPN_ELEMENTAL_AFFIX } from './constants.js';
import { translate } from './Translator.js';
import { affixList } from './WaIntegration.js';

const BASEDAMAGE_FOR_LEVEL0_SPELLS = 2;
const BASEHEAL_FOR_LEVEL0_SPELLS = 2;

const BASEDURATION_FOR_SPELLS = [20, 50, 100, 200, 400, 800];
// const BASEDURATION_FOR_RITUALS = 0.5d, 1d, 3d, 10d, 1m, 1y

/**
 * Finalize the spellData construction which will be given inside roll context.
 * @param {AEActor} actor Main actor for the roll
 * @param {object} spellData spellData which have already been build in parts by the interaction class
 */
export const finalizeSpellData = (actor, spellData) => {

    const spellLevel = spellData.effect?.level ?? 0;
    const mainElement = spellData.mainElement ?? null;

    // Duration
    const shape = spellData.shape;
    if( shape?.duration || shape?.duration == 0 ) {
        const timetracker = actor.timetracker;
        const duration = BASEDURATION_FOR_SPELLS[spellLevel] ?? 0;
        if( timetracker != null ) {
            spellData.until = timetracker + duration;
        }
    }

    // Damages
    const effectKey = spellData.effect?.key;
    spellData.dealsDamage = effectKey == 'effectAttack';
    if( spellData.dealsDamage ) {
        const baseDamage = BASEDAMAGE_FOR_LEVEL0_SPELLS + 2*spellLevel;
        const spellAffixKeys = spellData.spell?.affixes ?? [];
        const affixes = affixList().filter( a => spellAffixKeys.includes(a.key) );
        spellData.damage = modifyDamageWithAffixes(baseDamage, affixes);
    } 

    // Heal
    spellData.healsDamage = effectKey == 'effectHeal';
    if( spellData.healsDamage ) {
        const baseHeal = BASEHEAL_FOR_LEVEL0_SPELLS + 2*spellLevel;
        spellData.heal = modifyHealWithAffixes(baseHeal, mainElement?.key, []);
    }

    // ActiveEffects
    const spell = spellData.spell;
    spellData.givesActiveEffect = spell && ['effectDebuff', 'effectProtect', 'effectBuff', 'effectCreation', 'effectInvocation', 'effectAnalyze'].includes(effectKey);
    spellData.targetsOnlyCaster = spell && ['effectInvocation', 'effectAnalyze'].includes(effectKey);
    if( spellData.givesActiveEffect ) {
        spellData.activeEffectData = {
            label: spell.name,
            icon: spell.icon,
            changes: [],
            flags: {
                core : {
                    statusId: spell.key
                }, 
                acariaempire : {
                    spell: {
                        definition: spell.key,
                        mainElement: mainElement?.key ?? ''
                    }
                }
            }
        };
    }
}

/**
 * When an effect is added to an actor, it gives a custom actoiveEffect change.
 * The changed will be applied on actor data here.
 */
export const manageCustomSpellEffects = (actor, change) => {
    if( change.key != namespace.CHANGE_KEY ) { return; }

    // Parsing value
    const splitted = change.value.split(':');
    const spellKey = splitted[0];
    const level = parseInt(splitted[1]);
    const mainElement = splitted[2];

    const effect = namespace[spellKey];
    if( !effect ) {
        console.warn('AE-SpellEffect | Can\'t retrieve spell effect named ' + spellKey );
        return;
    }
    effect.apply(actor, level, mainElement);
}

// Grouping all convenience methods
const utils = {

    // Commonly used by spell which don't have stack managment
    defaultStacks: (level) => { 
        return {
            type: SPELLEFFECT_STACK_TYPE.none,
            amount: 0
        };
    },

    // For protect effects
    shieldStacks: (level) => {

        let amount = 1;
        if( level >= 1 ) { amount++; }
        if( level >= 3 ) { amount++; }
        if( level >= 5 ) { amount++; }

        return {
            type: SPELLEFFECT_STACK_TYPE.shield,
            amount: amount
        };
    },

    // For protect effects
    attackStacks: (level) => {

        let amount = 1;
        if( level >= 1 ) { amount++; }
        if( level >= 3 ) { amount++; }
        if( level >= 5 ) { amount++; }

        return {
            type: SPELLEFFECT_STACK_TYPE.attack,
            amount: amount
        };
    },
    
    // Apply a shield on each element
    applyShieldProtection: (actor, level, {doubleForThisElement = null, offset = 0} = {}) => {
        
        const calculateShield = ( base, element ) => {
            let result = base + offset;
            if( element === doubleForThisElement ) { result *= 2; }
            return result;
        }

        let shielded = 2;
        if( level >= 2 ) { shielded+=2; }
        if( level >= 4 ) { shielded+=2; }
        if( level >= 6 ) { shielded+=2; }

        Object.entries(actor.system.elements).forEach( ([elementKey, node]) => {
            const currentAmount = node.shielded;
            const newAmount = calculateShield(shielded, elementKey);
            if( newAmount > currentAmount ) {
                node.shielded = newAmount;
            }
        });
    },

    // Convenience function used by all effectBuffSpell which are giving attributes.
    applyAttributeBuff: (actor, level, mainAttribute, nbLevelsForSecondaryAttributes=1) => {

        let bonus = 1;
        if( level >= 3 ) { bonus++; }
        if( level >= 5 ) { bonus++; }

        let secondaryAttrMaxLevel = -1;
        if( level >= 1 ) { secondaryAttrMaxLevel++; }
        if( level >= 2 ) { secondaryAttrMaxLevel++; }
        if( level >= 4 ) { secondaryAttrMaxLevel++; }
        if( level >= 6 ) { secondaryAttrMaxLevel++; }

        const mainAttrMaxLevel = secondaryAttrMaxLevel + 3;

        // Retrieve lowest attributes from actor.data
        const attributesByLevel = Object.entries(actor.system.attributes).map( ([key, value]) => {
            return {
                attr: key,
                level: value.level
            };
        }).reduce( (myArray, current) => {

            const forThisLevel = myArray.find( el => el.level === current.level );
            if( forThisLevel ) {
                forThisLevel.attributes.push(current.attr);
            } else {
                myArray.push( {level: current.level, attributes: [current.attr]} );
            }
            return myArray;
        }, []);

        attributesByLevel.sort( (a,b) => a.level - b.level );

        const lowestAttributes = attributesByLevel.filter( (el, index) => {
            return index < nbLevelsForSecondaryAttributes;
        }).reduce( (myArray, current) => {
            return myArray.concat(current.attributes);
        }, []);

        // Make the changes on actor
        // - mainAttribute
        if( mainAttribute ) {
            const attr = actor.system.attributes[mainAttribute];
            if( attr.level < mainAttrMaxLevel ) {
                attr.level = Math.min( mainAttrMaxLevel, attr.level + bonus );
            }
        }

        // - secondary ones
        lowestAttributes.filter(attributeName => {
            return attributeName != mainAttribute;

        }).forEach( attributeName => {
            const attr = actor.system.attributes[attributeName];
            if( attr.level < mainAttrMaxLevel ) {
                attr.level = Math.min( secondaryAttrMaxLevel, attr.level + bonus );
            }
        });
    },


    // Convenience function used by all effectBuffSpell which are enhancing weapons.
    applyElementalWeaponEnhancement: (actor, level, mainElement, level2Affix) => {
        const upgrades = {
            affixes: [WPN_ELEMENTAL_AFFIX[mainElement]],
            nbAttacks: 1
        };
        if( level >= 2 ) { upgrades.affixes.push(level2Affix); }
        if( level >= 4 ) { upgrades.affixes.push('elementalPower1'); }
        if( level >= 5 ) { upgrades.affixes.push('light'); }
        if( level >= 6 ) { upgrades.affixes.push('elementalPower2'); }

        // Store affix list inside actor data
        const spellsOnEquipment = actor.system.spellsOnEquipment;
        if( !spellsOnEquipment.weapon ) { spellsOnEquipment.weapon = []; }
        const spellsOnWeapons = spellsOnEquipment.weapon;

        upgrades.affixes.forEach( affix => {
            if( !spellsOnWeapons.includes(affix) ) {
                spellsOnWeapons.push( affix );
            }
        });
    },

    // Convenience function used by all invocation spells
    //---------------------------------------------------
    /**
     * Done when the caster get the Active Effect storing the fact he has invoked some creatures.
     * The creatures templates are loaded as Actors. They are modified to match current spell level and mainElement.
     * All which is left to do is drag them into the scene.
     * @param {AEActor} actor the caster
     * @param {string} spellId id of the casted spell
     * @param {int} level spell invocation level
     * @param {string} mainElement spell main element
     * @param {string} template spell template (see names inside invoc compendium)
     */
    createNewInvocations: async (actor, spellId, level, mainElement, template) => {

        const pack = game.packs.find(p => p.collection === INVOC_COMPENDIUM );
        const index = await pack.getIndex();
        const entry = index.find(elt => elt.name === template);

        const compendiumActor = await pack.getDocument(entry._id);
        const actorData = compendiumActor.data._source;
        delete actorData["_id"];
        delete actorData["flags.world-anvil"]; // Duplicate. No need to link this one to a potential WA Article?
        actorData['flags.acariaempire.invocation'] = spellId;

        actorData["name"] = translate('AESYSTEM.targets.sheet.invocation.invokeName').replace('ACTOR_NAME', actor.name);
        actorData["folder"] = actor.data._source.folder;
        actorData["permission"] = actor.data._source.permission;
        actorData["token.disposition"] = actor.data._source.token.disposition;

        // Monster augments : Will manage attribute, combat ability, resists and maxHealth modification
        actorData["data.monster.augments"] = [
            {augment: 'invocationBeing', level: level+1}, // invocationBeing level 1 is for a spell level 0
            {augment: mainElement + 'Resist', level: 3} // Elemental resist : 3 for the chosen element
        ];

        // Handle maneuvers or additional benefit du to mainElement
        const attackStyle = actorData.data.abilities.combat.unlocked[0];
        if( attackStyle == 'melee' ) { // Unlock a maneuver
            const maneuver = INVOC_MELEE_MANEUVER[mainElement]; 
            actorData.data.melee = { maneuvers:{} };
            actorData.data.melee.maneuvers[maneuver] = {xp: 0, level: 2};

        } else if( attackStyle == 'ranged' ) { // Unlock a maneuver
            const maneuver = INVOC_RANGED_MANEUVER[mainElement]; 
            actorData.data.ranged = { maneuvers:{} };
            actorData.data.ranged.maneuvers[maneuver] = {xp: 0, level: 2};

        } else if( attackStyle == 'improvised' ) { // Unlock a maneuver
            const maneuver = INVOC_IMPROVISED_MANEUVER[mainElement]; 
            actorData.data.improvised = { maneuvers:{} };
            actorData.data.improvised.maneuvers[maneuver] = {xp: 0, level: 2};

        } else if( attackStyle == 'arcane' ) { // Gives an advantage equivalent to guardianSpirit element Harmonization
            actorData["data.monster.augments"].push(
                {augment: mainElement + 'Attunement', level: 1}
            );

        } else if( attackStyle == 'songs' ) {
            const melody = INVOC_SONGS_MELODY[mainElement];
            actorData.data.songs = { melodies:{} };
            actorData.data.songs.melodies[melody] = 1;

        } // No invocations are using prayers

        // Also set current health to its maximum value (max will be udated via ActiveEffect from monsterAugments)
        actorData.data.health.value = 5 + level;

        // Swarms are really weaker compared to the other ones. (but they are twice the number)
        // - They start with a base threat reduced by 20
        if( template == 'swarmInvocation' ) {
            actorData.data.monster.threat = 30;
        }

        const invocationTemplate = await Actor.implementation.create(actorData);
        invocationTemplate.reloadAllEffects();

    },
     
    /**
     * Done when the caster loose the Active Effect storing the fact he has invoked some creatures.
     * Creature template and linked tokens are deleted
     * @param {string} spellId id of the spell related to this lost ActiveEffect
     */
    banishCurrentInvocations: async (spellId) => {

        const relatedActors = game.actors.filter( actor => {
            const invocSpellId = actor.data.flags?.acariaempire?.invocation;
            return invocSpellId === spellId;
        });
        const relatedActorIds = relatedActors.map( a => a.id );

        const relatedTokens = canvas.tokens.placeables.filter(t => {
            return relatedActorIds.includes( t.data.actorId );
        });

        for( const token of relatedTokens ) {
            await token.document.delete();
        }

        for( const actor of relatedActors ) {
            await actor.delete();
        }
    }
}

export const spellEffects = {};
//--------------------------
// By convention, namepace.xxxx should be the same as the spellKey 
// it contains those attributes:
// priority : when the spellEffect data should be triggered
// createStacks: {type:string, amount:int} For managinf effect stacks before removal
// apply : apply effect on actor. takes actor and level as parameter
//--------------------------

const namespace = spellEffects;
namespace.CHANGE_KEY = 'ae-spells';

// Protect effects
//---------------------------
namespace.elementalShield = {
    priority: EFFECT_PRIORITY.upgrade,
    createStacks: utils.shieldStacks,
    apply: (actor, level, mainElement) => {
        utils.applyShieldProtection(actor, level, {doubleForThisElement: mainElement});
    }
}

namespace.freeMovement = {
    priority: EFFECT_PRIORITY.upgrade,
    createStacks: utils.shieldStacks,
    apply: (actor, level, mainElement) => {
        utils.applyShieldProtection(actor, level);
    }
}

namespace.instinctiveDefense = {
    priority: EFFECT_PRIORITY.upgrade,
    createStacks: utils.shieldStacks,
    apply: (actor, level, mainElement) => {
        utils.applyShieldProtection(actor, level);
    }
}

namespace.sixthSense = {
    priority: EFFECT_PRIORITY.upgrade,
    createStacks: utils.shieldStacks,
    apply: (actor, level, mainElement) => {
        utils.applyShieldProtection(actor, level);
    }
}

namespace.mentalFortitude = {
    priority: EFFECT_PRIORITY.upgrade,
    createStacks: utils.shieldStacks,
    apply: (actor, level, mainElement) => {
        utils.applyShieldProtection(actor, level);
    }
}

namespace.sanctuary = {
    priority: EFFECT_PRIORITY.upgrade,
    createStacks: (level) => {
        const base = utils.shieldStacks(level);
        base.amount *= 2;
        return base;
    },
    apply: (actor, level, mainElement) => {
        utils.applyShieldProtection(actor, level, {doubleForThisElement: mainElement, offset: 3});
    }
}

namespace.purge = {
    priority: EFFECT_PRIORITY.upgrade,
    createStacks: utils.shieldStacks,
    apply: (actor, level, mainElement) => {
        utils.applyShieldProtection(actor, level);
    }
}


// Buff effects
//---------------------------

namespace.timekeeperAspect = {
    priority: EFFECT_PRIORITY.handlingMaxValues,
    createStacks: utils.defaultStacks,
    apply: (actor, level, mainElement) => {
        let nbLevelsForSecondaryAttributes = 1;
        if( level >= 2 ) { nbLevelsForSecondaryAttributes++; }
        if( level >= 4 ) { nbLevelsForSecondaryAttributes++; }
        if( level >= 6 ) { nbLevelsForSecondaryAttributes = 6; }

        utils.applyAttributeBuff(actor, level, null, nbLevelsForSecondaryAttributes);
    }
}

namespace.predatorAspect = {
    priority: EFFECT_PRIORITY.handlingMaxValues,
    createStacks: utils.defaultStacks,
    apply: (actor, level, mainElement) => {
        utils.applyAttributeBuff(actor, level, 'versatility');
    }
}

namespace.ghostAspect = {
    priority: EFFECT_PRIORITY.handlingMaxValues,
    createStacks: utils.defaultStacks,
    apply: (actor, level, mainElement) => {
        utils.applyAttributeBuff(actor, level, 'speed');
    }
}

namespace.golemAspect = {
    priority: EFFECT_PRIORITY.handlingMaxValues,
    createStacks: utils.defaultStacks,
    apply: (actor, level, mainElement) => {
        utils.applyAttributeBuff(actor, level, 'vigor');
    }
}

namespace.cyclopsAspect = {
    priority: EFFECT_PRIORITY.handlingMaxValues,
    createStacks: utils.defaultStacks,
    apply: (actor, level, mainElement) => {
        utils.applyAttributeBuff(actor, level, 'perspicacity');
    }
}

namespace.dragonAspect = {
    priority: EFFECT_PRIORITY.handlingMaxValues,
    createStacks: utils.defaultStacks,
    apply: (actor, level, mainElement) => {
        utils.applyAttributeBuff(actor, level, 'focus');
    }
}

namespace.alphaAspect = {
    priority: EFFECT_PRIORITY.handlingMaxValues,
    createStacks: utils.defaultStacks,
    apply: (actor, level, mainElement) => {
        utils.applyAttributeBuff(actor, level, 'socialEase');
    }
}

// Creation effects
//---------------------------

namespace.yearningAssassin = {
    priority: EFFECT_PRIORITY.classic,
    createStacks: utils.attackStacks,
    apply: (actor, level, mainElement) => {
        utils.applyElementalWeaponEnhancement(actor, level, mainElement, 'assassin');
    }
}

namespace.yearningCrafty = {
    priority: EFFECT_PRIORITY.classic,
    createStacks: utils.attackStacks,
    apply: (actor, level, mainElement) => {
        utils.applyElementalWeaponEnhancement(actor, level, mainElement, 'sharpened');
    }
}

namespace.yearningDestroyer = {
    priority: EFFECT_PRIORITY.classic,
    createStacks: utils.attackStacks,
    apply: (actor, level, mainElement) => {
        utils.applyElementalWeaponEnhancement(actor, level, mainElement, 'piercing');
    }
}

namespace.yearningGambler = {
    priority: EFFECT_PRIORITY.classic,
    createStacks: utils.attackStacks,
    apply: (actor, level, mainElement) => {
        utils.applyElementalWeaponEnhancement(actor, level, mainElement, 'lucky');
    }
}

namespace.yearningRavenous = {
    priority: EFFECT_PRIORITY.classic,
    createStacks: utils.attackStacks,
    apply: (actor, level, mainElement) => {
        utils.applyElementalWeaponEnhancement(actor, level, mainElement, 'thirsty');
    }
}

namespace.yearningResentful = {
    priority: EFFECT_PRIORITY.classic,
    createStacks: utils.attackStacks,
    apply: (actor, level, mainElement) => {
        utils.applyElementalWeaponEnhancement(actor, level, mainElement, 'vicious');
    }
}

namespace.yearningSteadiness = {
    priority: EFFECT_PRIORITY.classic,
    createStacks: utils.attackStacks,
    apply: (actor, level, mainElement) => {
        utils.applyElementalWeaponEnhancement(actor, level, mainElement, 'reliable');
    }
}

// Invocation effects
//---------------------------
// An Active Effect is put on the caster. It keeps a link to every invocation.
// Deleting the activeEffect will automatically destroy the invocations.

namespace.flyingInvocation = {
    priority: EFFECT_PRIORITY.classic,
    createStacks: utils.defaultStacks,
    onCreateSpell: async (actor, spellId, level, mainElement) => {
        await utils.createNewInvocations(actor, spellId, level, mainElement, 'flyingInvocation');
    },
    onDropSpell: async (actor, spellId, level, mainElement) => {
        await utils.banishCurrentInvocations(spellId);
    },
    apply: (actor, level, mainElement) => {} // Nothing to do here
}

namespace.puppetInvocation = {
    priority: EFFECT_PRIORITY.classic,
    createStacks: utils.defaultStacks,
    onCreateSpell: async (actor, spellId, level, mainElement) => {
        await utils.createNewInvocations(actor, spellId, level, mainElement, 'puppetInvocation');
    },
    onDropSpell: async (actor, spellId, level, mainElement) => {
        await utils.banishCurrentInvocations(spellId);
    },
    apply: (actor, level, mainElement) => {} // Nothing to do here
}

namespace.rangedInvocation = {
    priority: EFFECT_PRIORITY.classic,
    createStacks: utils.defaultStacks,
    onCreateSpell: async (actor, spellId, level, mainElement) => {
        await utils.createNewInvocations(actor, spellId, level, mainElement, 'rangedInvocation');
    },
    onDropSpell: async (actor, spellId, level, mainElement) => {
        await utils.banishCurrentInvocations(spellId);
    },
    apply: (actor, level, mainElement) => {} // Nothing to do here
}

namespace.soldierInvocation = {
    priority: EFFECT_PRIORITY.classic,
    createStacks: utils.defaultStacks,
    onCreateSpell: async (actor, spellId, level, mainElement) => {
        await utils.createNewInvocations(actor, spellId, level, mainElement, 'soldierInvocation');
    },
    onDropSpell: async (actor, spellId, level, mainElement) => {
        await utils.banishCurrentInvocations(spellId);
    },
    apply: (actor, level, mainElement) => {} // Nothing to do here
}

namespace.songsInvocation = {
    priority: EFFECT_PRIORITY.classic,
    createStacks: utils.defaultStacks,
    onCreateSpell: async (actor, spellId, level, mainElement) => {
        await utils.createNewInvocations(actor, spellId, level, mainElement, 'songsInvocation');
    },
    onDropSpell: async (actor, spellId, level, mainElement) => {
        await utils.banishCurrentInvocations(spellId);
    },
    apply: (actor, level, mainElement) => {} // Nothing to do here
}

namespace.swarmInvocation = {
    priority: EFFECT_PRIORITY.classic,
    createStacks: utils.defaultStacks,
    onCreateSpell: async (actor, spellId, level, mainElement) => {
        await utils.createNewInvocations(actor, spellId, level, mainElement, 'swarmInvocation');
    },
    onDropSpell: async (actor, spellId, level, mainElement) => {
        await utils.banishCurrentInvocations(spellId);
    },
    apply: (actor, level, mainElement) => {} // Nothing to do here
}

namespace.tankInvocation = {
    priority: EFFECT_PRIORITY.classic,
    createStacks: utils.defaultStacks,
    onCreateSpell: async (actor, spellId, level, mainElement) => {
        await utils.createNewInvocations(actor, spellId, level, mainElement, 'tankInvocation');
    },
    onDropSpell: async (actor, spellId, level, mainElement) => {
        await utils.banishCurrentInvocations(spellId);
    },
    apply: (actor, level, mainElement) => {} // Nothing to do here
}
