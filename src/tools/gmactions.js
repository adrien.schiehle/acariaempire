import { isConfiguredWithRemovingXPAllowed, isConfiguredWithActorSpiritDisplayed } from './config.js';

export const assertGMOnly = () => {
    const gm = game.user.isGM;
    if(! gm) {
        ui.notifications.warn(game.i18n.localize('AESYSTEM.gm.restricted'));
    }
    return ! gm;
}

export const removingXPAllowed = () => {
    return game.user.isGM || isConfiguredWithRemovingXPAllowed();
}

export const spritDisplayedOnActorSheet = () => {
    return game.user.isGM || isConfiguredWithActorSpiritDisplayed();
}


