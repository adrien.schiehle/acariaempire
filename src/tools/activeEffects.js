import { EFFECT_PRIORITY } from './constants.js';
import { manageCustomMonsterEffects } from './monsters.js';
import { manageCustomSpellEffects } from './spells.js'

/**
 * Called when updating active effects.
 * For custom changes
 * @param {AEActor} actor 
 * @param {object} change 
 */
export const manageCustomEffects = (actor, change) => {
    manageCustomSpellEffects(actor, change);
    manageCustomMonsterEffects(actor, change);

    if( FATIGUE_EFFECT.key === change.key ) {
        applyFatigueEffect(actor);

    } else if( OVERLOAD_EFFECT.key === change.key ) {
        const splitted = change.value.split(':');
        const attributeName = splitted[0];
        const step = parseInt( splitted[1] );
        applyOverloadEffect(actor, attributeName, step);

    } else if( MANEUVER_EFFECT.key === change.key ) {
        const splitted = change.value.split(':');
        const maneuverPath = splitted[0];
        const advantage = parseInt( splitted[1] );
        const mastery = parseInt( splitted[2] );
        applyManeuverEffect(actor, maneuverPath, advantage, mastery);

    } else if( COMBAT_CAPACITY_EFFECT.key === change.key ) {
        const splitted = change.value.split(':');
        const style = splitted[0]; // 'offense' or 'defense'
        const type = splitted[1]; // skillKey or defenseType
        applyCombatCapacityEffect(actor, style, type);
    } 
}

// FATIGUE

export const FATIGUE_EFFECT ={
    key: 'ae-fatigue',
    priority: EFFECT_PRIORITY.calculatedFromPeers
};

const applyFatigueEffect = (actor) => {
    const fatigue = actor.currentFatigue;
    actor.system.health.max -= fatigue;
}

// OVERLOAD

export const OVERLOAD_EFFECT ={
    key: 'ae-overload',
    priority: EFFECT_PRIORITY.calculatedFromPeers
};

const applyOverloadEffect = (actor, attributeName, step) => {
    const checkedAttribute = actor.system.attributes[attributeName];
    const diff = checkedAttribute.level - step;
    if( diff < 0 ) {
        ['speed', 'focus'].forEach( key => {
            const attr = actor.system.attributes[key];
            attr.level = Math.max( attr.min, attr.level + diff );
        });
        actor.system.status.overloaded = true;
    }
}


// MANEUVER
export const MANEUVER_EFFECT ={
    key: 'ae-maneuver',
    priority: EFFECT_PRIORITY.classic
};

const applyManeuverEffect = (actor, maneuverPath, advantage, mastery) => {
    const maneuverData = getProperty(actor.system, maneuverPath);
    if( maneuverData ) {
        if( ! maneuverData.activeEffects ) { 
            maneuverData.activeEffects = { advantage: 0, mastery: 0 };
        }
        if( advantage ) {
            maneuverData.activeEffects.advantage += advantage;
        }
        if( mastery ) {
            maneuverData.activeEffects.mastery += mastery;
        }
    }
}


// COMBAT_CAPACITY
export const COMBAT_CAPACITY_EFFECT ={
    key: 'ae-combat',
    priority: EFFECT_PRIORITY.afterAllIsSet
};

const applyCombatCapacityEffect = (actor, style, type) => {

    const baseStyle = actor.system.battle[style];
    const baseType = baseStyle[type];

    const currentCapacities = style == 'offense' ? actor.battle.forOffense(type) : actor.battle.forDefense(type);
    baseType.advantage += currentCapacities.initialAdvantage;
    baseType.mastery += currentCapacities.initialMastery;
}

