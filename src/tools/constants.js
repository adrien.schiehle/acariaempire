export const ACTOR_GM_NAME = 'GM';

export const EFFECT_PRIORITY = {
    classic: 20,
    upgrade: 40,
    handlingMaxValues: 49,
    calculatedFromPeers: 59,
    afterAllIsSet: 60
}

export const SPELLEFFECT_STACK_TYPE = {
    none: 'none',
    shield: 'shield',
    attack: 'attack'
}

export const ALL_ITEMS_TYPES = {
    weapon: 'weapon',
    armor: 'armor',
    misc: 'misc',
    trouble: 'trouble'
};

export const ALL_CARDS_TYPES = {
    base: 'base',
    event: 'event',
    trouble: 'trouble'
};

export const TROUBLE_STATUS = {
    positive: 'positive', 
    negative: 'negative', 
    ignored: 'ignored'
};

export const WPN_ELEMENTAL_AFFIX = {
    primary: 'primaryImbue',
    fire: 'fireImbue',
    electricity: 'electricityImbue',
    wind: 'windImbue',
    moon: 'moonImbue',
    animal: 'animalImbue',
    mind: 'mindImbue',
    shadow: 'shadowImbue',
    ice: 'iceImbue',
    water: 'waterImbue',
    plant: 'plantImbue',
    earth: 'earthImbue',
    metal: 'metalImbue'
};

export const  INVOC_COMPENDIUM = 'acariaempire.ae-invocations';

export const INVOC_MELEE_MANEUVER = {
    primary: 'hit_and_run',
    fire: 'swipe',
    electricity: 'finesse',
    wind: 'disarm',
    moon: 'harassing',
    animal: 'bucker_smash',
    mind: 'disarm',
    shadow: 'harassing',
    ice: 'overthrow',
    water: 'swipe',
    plant: 'overthrow',
    earth: 'bucker_smash',
    metal: 'finesse'
};

export const INVOC_RANGED_MANEUVER = {
    primary: 'interruptingshot',
    fire: 'multishot',
    electricity: 'finesse',
    wind: 'harassing',
    moon: 'sniper',
    animal: 'ricochet',
    mind: 'harassing',
    shadow: 'sniper',
    ice: 'crippleshot',
    water: 'multishot',
    plant: 'crippleshot',
    earth: 'ricochet',
    metal: 'finesse'
};


export const INVOC_IMPROVISED_MANEUVER = { //FIXME: Not enough maneuvers for now
    primary: 'versatility',
    fire: 'versatility',
    electricity: 'versatility',
    wind: 'versatility',
    moon: 'throw_down',
    animal: 'throw_down',
    mind: 'throw_down',
    shadow: 'splash_damage',
    ice: 'splash_damage',
    water: 'splash_damage',
    plant: 'perfect_tool',
    earth: 'perfect_tool',
    metal: 'perfect_tool'
};

export const INVOC_SONGS_MELODY = { 
    primary: 'initiative',
    fire: 'advantage',
    electricity: 'mastery',
    wind: 'initiative',
    moon: 'advantage',
    animal: 'mastery',
    mind: 'initiative',
    shadow: 'advantage',
    ice: 'mastery',
    water: 'initiative',
    plant: 'advantage',
    earth: 'mastery',
    metal: 'initiative'
};