import { EFFECT_PRIORITY } from './constants.js';

export const monsterEffects = {};
const namespace = monsterEffects;

namespace.CHANGE_KEY = 'ae-monsters';

/**
 * When an effect is added to an actor, it gives a custom actoiveEffect change.
 * The changed will be applied on actor data here.
 */
 export const manageCustomMonsterEffects = (actor, change) => {
    if( change.key != namespace.CHANGE_KEY ) { return; }

    // Parsing value
    const splitted = change.value.split(':');
    const augment = splitted[0];
    const level = parseInt(splitted[1]);

    const effect = namespace[augment];
    if( !effect ) {
        console.warn('AE-MonsterEffect | Can\'t retrieve monster augment named ' + augment );
        return;
    }
    effect.apply(actor, level);
}

// Grouping all convenience methods
const utils = {

    elementalResist(actor, level, mainElement) {

        const base = actor.system.elements[mainElement];
        base.resist += level;

        actor.system.monster.threat += level * (level+1);  //  Sum: n.(n+1)/2 
    },

    elementalAttunement(actor, mainElement) {

        const base = actor.system.elements[mainElement];
        base.attuned = 1;

        actor.system.monster.threat += 2;
    }
}

//--------------------------
// By convention, namepace.xxxx should be the same as the augment 
// it contains those attributes:
// priority : when the spellEffect data should be triggered
// apply : apply effect on actor. takes actor and level as parameter
//--------------------------

// Classic augments
//---------------------------
namespace.healthAugment = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        actor.system.health.max += 3 * level;
        actor.system.monster.threat += 10 * level;  //  Sum: n.(n+1)/2 
    }
}

// Invocation augments
//---------------------------
namespace.invocationBeing = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {

        // actual level is level-1 : Augment level 1 is for Invoc level 0
        const invocLevel = level - 1;

        let threatLevel = -20; // Calculated taking into account attribute et HP reduce
        let attributeUpgrade = 0; // Level 0 begins with 0 as its max values, and -1 for the other ones
        let combatUpgrade = 0;
        if( invocLevel >= 1 ) { attributeUpgrade++; threatLevel += 10; }
        if( invocLevel >= 2 ) { combatUpgrade++; threatLevel += 10; }
        if( invocLevel >= 3 ) { attributeUpgrade++; threatLevel += 15; }
        if( invocLevel >= 4 ) { combatUpgrade++; threatLevel += 15; }
        if( invocLevel >= 5 ) { attributeUpgrade++; threatLevel += 20; }
        if( invocLevel >= 6 ) { combatUpgrade++; threatLevel += 20; }

        // Upgrade health
        actor.system.health.max += invocLevel - 5; // Level 5 invoc have 10 HP

        // Upgrade invocation attributes
        const attributes = actor.system.attributes;
        Object.keys(attributes).forEach( attrKey => {
            attributes[attrKey].level += attributeUpgrade;
            attributes[attrKey].max += 3; // Max level for attribute : 6
        });
        
        // Upgrade combat abilities
        actor.system.abilities.combat.level += combatUpgrade;
        actor.system.abilities.combat.max += 3; // Max level for combat ability : 6

        // Also upgrade arcane knowledge and melodies if the invocation is using it
        const mainAttackStyle = actor.system.abilities.combat.unlocked[0];
        if( mainAttackStyle == 'arcane' ) { // Also upgrade arcane knowledges for puppets
            const knowledges = actor.system.arcane.knowledge;
            Object.keys(knowledges).forEach( knowledge => {
                if( knowledges[knowledge] != 0 ) {
                    knowledges[knowledge] += combatUpgrade;
                }
            });
        } else if( mainAttackStyle == 'songs' ) { // Also upgrade arcane knowledges for puppets
            const melodies = actor.system.songs.melodies;
            Object.keys(melodies).forEach( melody => {
                if( melodies[melody] != 0 ) {
                    melodies[melody] += combatUpgrade;
                }
            });
        } 

        // Threat level is adjusted
        actor.system.monster.threat += threatLevel;
    }
}

// Elemental resists
// ----------------------
namespace.primaryResist = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalResist(actor, level, 'primary')
    }
}

namespace.fireResist = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalResist(actor, level, 'fire')
    }
}

namespace.electricityResist = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalResist(actor, level, 'electricity')
    }
}

namespace.windResist = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalResist(actor, level, 'wind')
    }
}

namespace.moonResist = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalResist(actor, level, 'moon')
    }
}

namespace.animalResist = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalResist(actor, level, 'animal')
    }
}

namespace.mindResist = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalResist(actor, level, 'mind')
    }
}

namespace.shadowResist = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalResist(actor, level, 'shadow')
    }
}

namespace.iceResist = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalResist(actor, level, 'ice')
    }
}

namespace.waterResist = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalResist(actor, level, 'water')
    }
}

namespace.plantResist = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalResist(actor, level, 'plant')
    }
}

namespace.earthResist = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalResist(actor, level, 'earth')
    }
}

namespace.metalResist = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalResist(actor, level, 'metal')
    }
}


// Elemental attunement
// ----------------------
namespace.primaryAttunement = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalAttunement(actor, 'primary')
    }
}

namespace.fireAttunement = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalAttunement(actor, 'fire')
    }
}

namespace.electricityAttunement = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalAttunement(actor, 'electricity')
    }
}

namespace.windAttunement = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalAttunement(actor, 'wind')
    }
}

namespace.moonAttunement = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalAttunement(actor, 'moon')
    }
}

namespace.animalAttunement = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalAttunement(actor, 'animal')
    }
}

namespace.mindAttunement = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalAttunement(actor, 'mind')
    }
}

namespace.shadowAttunement = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalAttunement(actor, 'shadow')
    }
}

namespace.iceAttunement = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalAttunement(actor, 'ice')
    }
}

namespace.waterAttunement = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalAttunement(actor, 'water')
    }
}

namespace.plantAttunement = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalAttunement(actor, 'plant')
    }
}

namespace.earthAttunement = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalAttunement(actor, 'earth')
    }
}

namespace.metalAttunement = {
    priority: EFFECT_PRIORITY.classic,
    apply: (actor, level) => {
        utils.elementalAttunement(actor, 'metal')
    }
}
