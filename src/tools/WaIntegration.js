const JOURNAL_ENTRIES = {
    magic: {
        effects: {
            effectAttack: [
                //FIXME : Kept to give the structure example. For now only fr is done  : {lang: 'en', label: 'Attack spells', articleId: 'c3e321ee-efac-48c3-bd88-31a8475d003b'},    
                {lang: 'fr', label: 'Magie d\'attaque', articleId: 'c3e321ee-efac-48c3-bd88-31a8475d003b'},
                {additionalData:true, offensive: true },
                {additionalData:true, caracteristics: ['effectAttack', 'range', 'areaOfEffect', 'clone'] },   
                {additionalData:true, translated:true, spells: {
                    devouringChains : [
                        {lang: 'fr', label: 'Chaines dévorantes', anchor: 'devouringChains'},
                        {additionalData:true, affixes: ['reliable'] }
                    ],
                    energyExplosion : [
                        {lang: 'fr', label: 'Explosion d\'énergie', anchor: 'energyExplosion'},
                        {additionalData:true, affixes: ['sharpened'] }
                    ],
                    piercingShards : [
                        {lang: 'fr', label: 'Eclats perforants', anchor: 'piercingShards'},
                        {additionalData:true, affixes: ['piercing'] }
                    ],
                    suffocatingBubble : [
                        {lang: 'fr', label: 'Bulle suffocante', anchor: 'suffocatingBubble'},
                        {additionalData:true, affixes: ['thirsty'] }
                    ],
                    revengeOfNature : [
                        {lang: 'fr', label: 'Revanche de la nature', anchor: 'revengeOfNature'},
                        {additionalData:true, affixes: ['vicious'] }
                    ],
                    deathWish : [
                        {lang: 'fr', label: 'Désir de mort', anchor: 'deathWish'},
                        {additionalData:true, affixes: ['assassin'] }
                    ],
                    decapitation : [
                        {lang: 'fr', label: 'Décapitation', anchor: 'decapitation'},
                        {additionalData:true, affixes: ['lucky'] }
                    ]
                }}
            ],
            effectDebuff: [
                {lang: 'fr', label: 'Magie d\'affaiblissement', articleId: 'c8e0c026-6762-4d11-a62e-ad35e4385450'},
                {additionalData:true, offensive: true },
                {additionalData:true, caracteristics: ['effectDebuff', 'range', 'duration', 'areaOfEffect'] }
            ],
            effectProtect: [
                {lang: 'fr', label: 'Magie de protection', articleId: 'ab560a96-9d11-44ab-84a5-4a45a6bf752c'},
                {additionalData:true, caracteristics: ['effectProtect', 'range', 'duration', 'areaOfEffect'] },
                {additionalData:true, translated:true, spells: {
                    elementalShield : [
                        {lang: 'fr', label: 'Bouclier élémentaire', anchor: 'elementalShield'}
                    ],
                    freeMovement : [
                        {lang: 'fr', label: 'Liberté de mouvement', anchor: 'freeMovement'}
                    ],
                    instinctiveDefense : [
                        {lang: 'fr', label: 'Défense instinctive', anchor: 'instinctiveDefense'}
                    ],
                    sixthSense : [
                        {lang: 'fr', label: 'Sixième sens', anchor: 'sixthSense'}
                    ],
                    mentalFortitude : [
                        {lang: 'fr', label: 'Clarté d\'esprit', anchor: 'mentalFortitude'}
                    ],
                    sanctuary : [
                        {lang: 'fr', label: 'Sanctuaire', anchor: 'sanctuary'}
                    ],
                    purge : [
                        {lang: 'fr', label: 'Purge des afflictions', anchor: 'purge'}
                    ],
                }}
            ],
            effectBuff: [
                {lang: 'fr', label: 'Magie de renforcement', articleId: '6f7a5142-f7cf-4f07-ac90-135534a5cb5c'},
                {additionalData:true, offensive: false },
                {additionalData:true, caracteristics: ['effectBuff', 'range', 'duration', 'areaOfEffect'] },   
                {additionalData:true, translated:true, spells: {
                    timekeeperAspect : [
                        {lang: 'fr', label: 'Gardien du temps', anchor: 'timekeeperAspect'}
                    ],
                    predatorAspect : [
                        {lang: 'fr', label: 'Aspect du prédateur', anchor: 'predatorAspect'}
                    ],
                    ghostAspect : [
                        {lang: 'fr', label: 'Aspect fantômatiqu', anchor: 'ghostAspect'}
                    ],
                    golemAspect : [
                        {lang: 'fr', label: 'Aspect du golem', anchor: 'golemAspect'}
                    ],
                    cyclopsAspect : [
                        {lang: 'fr', label: 'Aspect du cyclope', anchor: 'cyclopsAspect'}
                    ],
                    dragonAspect : [
                        {lang: 'fr', label: 'Aspect du dragon', anchor: 'dragonAspect'}
                    ],
                    alphaAspect : [
                        {lang: 'fr', label: 'Aspect de l\'Alpha', anchor: 'alphaAspect'}
                    ],
                }}
            ],
            effectInvocation: [
                {lang: 'fr', label: 'Magie d\'invocation', articleId: '99f57f11-da41-4afa-9ea9-c89bce491f29'},
                {additionalData:true, offensive: false },
                {additionalData:true, caracteristics: ['effectInvocation', 'range', 'duration', 'clone'] },
                {additionalData:true, translated:true, spells: {
                    flyingInvocation : [
                        {lang: 'fr', label: 'Invocation: Soldat hyménoptère', anchor: 'flyingInvocation'}
                    ],
                    puppetInvocation : [
                        {lang: 'fr', label: 'Invocation: Poupée maudite', anchor: 'puppetInvocation'}
                    ],
                    rangedInvocation : [
                        {lang: 'fr', label: 'Invocation: Cracheur d\'épines', anchor: 'rangedInvocation'}
                    ],
                    soldierInvocation : [
                        {lang: 'fr', label: 'Invocation: Armure animée', anchor: 'soldierInvocation'}
                    ],
                    songsInvocation : [
                        {lang: 'fr', label: 'Invocation: Citrouille hurleuse', anchor: 'songsInvocation'}
                    ],
                    swarmInvocation : [
                        {lang: 'fr', label: 'Invocation: Nuée de vermines', anchor: 'swarmInvocation'}
                    ],
                    tankInvocation : [
                        {lang: 'fr', label: 'Invocation: Golem', anchor: 'tankInvocation'}
                    ],
                }}
            ],
            effectCreation: [
                {lang: 'fr', label: 'Magie de matérialisation', articleId: 'e30bc059-98ff-444d-b738-b1e7055a2cdb'},
                {additionalData:true, offensive: false },
                {additionalData:true, caracteristics: ['effectCreation', 'range', 'duration', 'areaOfEffect'] },
                {additionalData:true, translated:true, spells: {
                    yearningAssassin : [
                        {lang: 'fr', label: 'Aspirations assassines', anchor: 'yearningAssassin'}
                    ],
                    yearningCrafty : [
                        {lang: 'fr', label: 'Aspirations rusées', anchor: 'yearningCrafty'}
                    ],
                    yearningDestroyer : [
                        {lang: 'fr', label: 'Aspirations destructrices', anchor: 'yearningDestroyer'}
                    ],
                    yearningGambler : [
                        {lang: 'fr', label: 'Aspirations incertaines', anchor: 'yearningGambler'}
                    ],
                    yearningRavenous : [
                        {lang: 'fr', label: 'Aspirations dévorantes', anchor: 'yearningRavenous'}
                    ],
                    yearningResentful : [
                        {lang: 'fr', label: 'Aspirations de revanche', anchor: 'yearningResentful'}
                    ],
                    yearningSteadiness : [
                        {lang: 'fr', label: 'Aspirations de stabilité', anchor: 'yearningSteadiness'}
                    ]
                }}
            ],
            effectAnalyze: [
                {lang: 'fr', label: 'Magie de divination', articleId: 'f829138b-4aca-47f6-9e4e-e45b4d4e0234'},
                {additionalData:true, offensive: false },
                {additionalData:true, caracteristics: ['effectAnalyze', 'range', 'duration', 'areaOfEffect'] }
            ],
            effectHeal: [
                {lang: 'fr', label: 'Magie de guérison', articleId: '6c3d9054-7b90-490d-819e-0f6157098bcc'},
                {additionalData:true, caracteristics: ['effectHeal', 'range', 'areaOfEffect', 'clone'] },
                {additionalData:true, offensive: false },
                {additionalData:true, translated:true, spells: {
                    elementalHeal : [
                        {lang: 'fr', label: 'Soin élémentaire', anchor: 'elementalHeal'}
                    ],
                    freedom : [
                        {lang: 'fr', label: 'Libération', anchor: 'freedom'}
                    ],
                    bless : [
                        {lang: 'fr', label: 'Bénédiction', anchor: 'bless'}
                    ],
                    sonar : [
                        {lang: 'fr', label: 'Sonar', anchor: 'sonar'}
                    ],
                    destiny : [
                        {lang: 'fr', label: 'Destinée', anchor: 'destiny'}
                    ],
                    panacea : [
                        {lang: 'fr', label: 'Panacé', anchor: 'panacea'}
                    ],
                    recovery : [
                        {lang: 'fr', label: 'Repos réparateur', anchor: 'recovery'}
                    ],
                }}
            ]
        },
        modifiers: {
            duration: [
                {lang: 'fr', label: 'Durée d\'un sort', articleId: '8281b826-23a2-4d01-9c24-90193ec419cd'}
            ],
            range: [
                {lang: 'fr', label: 'Portée d\'un sort', articleId: '733c0f4b-c87b-4cac-8bd2-628bd25bf3c6'}
            ],
            areaOfEffect: [
                {lang: 'fr', label: 'Zone d\'effet d\'un sort', articleId: '078ccc3d-1d17-4ec0-804d-fb0405996d4c'}
            ],
            clone: [
                {lang: 'fr', label: 'Clonage d\'un sort', articleId: '7d3e1dbf-8718-4092-892d-c4b39aff9b7c'}
            ]
        },
        elements: {
            primary: [
                {lang: 'fr', label: 'Elément de la lumière', articleId: 'ae92a00c-0367-4485-822d-0f04cd1ad6f5',
                 description: ['Repousse les ténébres. Synonyme d\'espoir et attribut principal de la race humaine']},
                {additionalData:true, arcanePenalty: 0},
                {additionalData:true, arcaneNeighbor: []},
                {additionalData:true, spellShape: 'pulse'},
                {additionalData:true, spells: {
                    attack: ['devouringChains'],
                    heal: ['elementalHeal'],
                    protect: ['elementalShield'],
                    buff: ['timekeeperAspect'],
                    creation: ['yearningSteadiness'],
                    invocation: ['soldierInvocation']
                }},
                {additionalData:true, weaponAffix: 'primaryImbue'}
                
            ],
            fire: [
                {lang: 'fr', label: 'Elément du feu', articleId: '7f5596ed-2987-4d89-b2be-75252d40b12c',
                description: ['Brule tout ce qu\'il rencontre. Synonyme de destruction, il peut aussi insuffler une indomptable volonté.']},
                {additionalData:true, arcanePenalty: 2},
                {additionalData:true, arcaneNeighbor: ['electricity', 'wind', 'metal', 'earth']},
                {additionalData:true, spellShape: 'projectile'},
                {additionalData:true, spells: {
                    attack: ['devouringChains', 'energyExplosion'],
                    heal: ['elementalHeal', 'freedom'],
                    protect: ['elementalShield', 'freeMovement'],
                    buff: ['golemAspect'],
                    creation: ['yearningCrafty'],
                    invocation: ['tankInvocation']
                }},
                {additionalData:true, weaponAffix: 'fireImbue'}
            ],
            electricity: [
                {lang: 'fr', label: 'Elément de l\'électricité', articleId: '0da74ec0-1216-4fba-8562-25f0f0839fa0',
                 description: ['Associé à l\'orage, les décharges électriques sont souvent accompagnés d\'un bruit retentissant.', 'Il est synonyme de vitesse']},
                {additionalData:true, arcanePenalty: 3},
                {additionalData:true, arcaneNeighbor: ['fire', 'wind']},
                {additionalData:true, spellShape: 'ray'},
                {additionalData:true, spells: {
                    attack: ['devouringChains', 'piercingShards'],
                    heal: ['elementalHeal', 'bless'],
                    protect: ['elementalShield', 'instinctiveDefense'],
                    buff: ['ghostAspect'],
                    creation: ['yearningDestroyer'],
                    invocation: ['flyingInvocation']
                }},
                {additionalData:true, weaponAffix: 'electricityImbue'}
            ],
            wind: [
                {lang: 'fr', label: 'Elément du vent', articleId: 'ee9988c7-a66b-4bfe-a9b1-99263b31b363',
                 description: ['Se concrétise en tempêtes ou simples brises, il est synonyme d\'insaisissable.']},
                {additionalData:true, arcanePenalty: 2},
                {additionalData:true, arcaneNeighbor: ['fire', 'electricity', 'animal', 'moon']},
                {additionalData:true, spellShape: 'flood'},
                {additionalData:true, spells: {
                    attack: ['devouringChains', 'suffocatingBubble'],
                    heal: ['elementalHeal', 'freedom'],
                    protect: ['elementalShield', 'freeMovement'],
                    buff: ['ghostAspect'],
                    creation: ['yearningRavenous'],
                    invocation: ['flyingInvocation']
                }},
                {additionalData:true, weaponAffix: 'windImbue'}
            ],
            moon: [
                {lang: 'fr', label: 'Elément de la lune', articleId: '3d468e5f-d3d3-4534-bba4-427ec052d9f4',
                 description: ['La lune et les étoiles reignent en maître durant la nuit. là où l\'animal voit surtout un point de repère, certains voyants arrivent à y voir l\'avenir']},
                {additionalData:true, arcanePenalty: 3},
                {additionalData:true, arcaneNeighbor: ['wind', 'animal']},
                {additionalData:true, spellShape: 'ray'},
                {additionalData:true, spells: {
                    attack: ['devouringChains', 'piercingShards'],
                    heal: ['elementalHeal', 'sonar'],
                    protect: ['elementalShield', 'sixthSense'],
                    buff: ['alphaAspect'],
                    creation: ['yearningDestroyer'],
                    invocation: ['puppetInvocation']
                }},
                {additionalData:true, weaponAffix: 'moonImbue'}
            ],
            animal: [
                {lang: 'fr', label: 'Elément des animaux', articleId: '80face25-442a-4d9e-8371-557c830f29fe',
                 description: ['La nature abrite une grande variété de créatures. Toutes ayant une place bien définie dans la chaîne alimentaire. La sauvagerie reste l\'aspect de cet élément.']},
                {additionalData:true, arcanePenalty: 2},
                {additionalData:true, arcaneNeighbor: ['wind', 'moon', 'shadow', 'mind']},
                {additionalData:true, spellShape: 'rift'},
                {additionalData:true, spells: {
                    attack: ['devouringChains', 'revengeOfNature'],
                    heal: ['elementalHeal', 'bless'],
                    protect: ['elementalShield', 'instinctiveDefense'],
                    buff: ['predatorAspect'],
                    creation: ['yearningResentful'],
                    invocation: ['swarmInvocation']
                }}
            ],
            mind: [
                {lang: 'fr', label: 'Elément de l\'esprit', articleId: '39542d27-a497-407a-ba24-69cbb51ca10b',
                description: ['Totalement intangible, cet élément agit directement sur le psyché des êtres vivants. Un peu à l\'instar d\'un barde, mais de manière beaucoup plus direct.']},
                {additionalData:true, arcanePenalty: 3},
                {additionalData:true, arcaneNeighbor: ['animal', 'shadow']},
                {additionalData:true, spellShape: 'fog'},
                {additionalData:true, spells: {
                    attack: ['devouringChains', 'deathWish'],
                    heal: ['elementalHeal', 'destiny'],
                    protect: ['elementalShield', 'mentalFortitude'],
                    buff: ['dragonAspect'],
                    creation: ['yearningAssassin'],
                    invocation: ['puppetInvocation']
                }}
            ],
            shadow: [
                {lang: 'fr', label: 'Elément de l\'ombre', articleId: '77c477e6-6e8e-42f4-bc78-a5d416e71724',
                description: ['L\'opposé de la lumière. Cet élément est souvent associé à la mort et tout ce qui lui est associé.']},
                {additionalData:true, arcanePenalty: 2},
                {additionalData:true, arcaneNeighbor: ['animal', 'mind', 'water', 'ice']},
                {additionalData:true, spellShape: 'fog'},
                {additionalData:true, spells: {
                    attack: ['devouringChains', 'deathWish'],
                    heal: ['elementalHeal', 'sonar'],
                    protect: ['elementalShield', 'sixthSense'],
                    buff: ['cyclopsAspect'],
                    creation: ['yearningAssassin'],
                    invocation: ['swarmInvocation']
                }}
            ],
            ice: [
                {lang: 'fr', label: 'Elément de la glace', articleId: '3cdd43ef-6733-4c33-93c9-d205e554b7c1',
                 description: ['Une plaque de glace peut s\'avérer être aussi solide et tranchant qu\'un morceau de métal. L\'élément de la glace est souvent associé à l\'immobilisme et la préservation.']},
                {additionalData:true, arcanePenalty: 3},
                {additionalData:true, arcaneNeighbor: ['shadow', 'water']},
                {additionalData:true, spellShape: 'projectile'},
                {additionalData:true, spells: {
                    attack: ['devouringChains', 'energyExplosion'],
                    heal: ['elementalHeal', 'recovery'],
                    protect: ['elementalShield', 'sanctuary'],
                    buff: ['dragonAspect'],
                    creation: ['yearningCrafty'],
                    invocation: ['rangedInvocation']
                }}
            ],
            water: [
                {lang: 'fr', label: 'Elément de l\'eau', articleId: 'd4f8b7eb-21bf-4c73-933e-543950de0aa3',
                description: ['L\'eau est la source de toute vie. La magie en découlant est souvent associé à la guérison.']},
                {additionalData:true, arcanePenalty: 2},
                {additionalData:true, arcaneNeighbor: ['shadow', 'ice', 'earth', 'plant']},
                {additionalData:true, spellShape: 'flood'},
                {additionalData:true, spells: {
                    attack: ['devouringChains', 'suffocatingBubble'],
                    heal: ['elementalHeal', 'panacea'],
                    protect: ['elementalShield', 'purge'],
                    buff: ['alphaAspect'],
                    creation: ['yearningRavenous'],
                    invocation: ['rangedInvocation']
                }}
            ],
            plant: [
                {lang: 'fr', label: 'Elément des plantes', articleId: 'd1f0dcdc-0373-4440-a6a6-1445ae2989c2',
                 description: ['Il existe une multitude de plantes différentes. De l\'arbre centenaire au lychen jonchant les rochers, elles ont chacune une fonction différentes.',
                               'L\'homme découvre peu à peu ses secrets et en utilise certaines pour guérir, d\'autres pour tuer.']},
                {additionalData:true, arcanePenalty: 3},
                {additionalData:true, arcaneNeighbor: ['water', 'earth']},
                {additionalData:true, spellShape: 'rift'},
                {additionalData:true, spells: {
                    attack: ['devouringChains', 'revengeOfNature'],
                    heal: ['elementalHeal', 'panacea'],
                    protect: ['elementalShield', 'purge'],
                    buff: ['predatorAspect'],
                    creation: ['yearningResentful'],
                    invocation: ['songsInvocation']
                }}
            ],
            earth: [
                {lang: 'fr', label: 'Elément de la terre', articleId: '64037b2a-e5f2-41d5-87fe-aeb11b03e09a',
                description: ['Les montagnes étaient là bien avant l\'ère des hommes. Elle sont les gardiennes d\'un savoir passé.']},
                {additionalData:true, arcanePenalty: 2},
                {additionalData:true, arcaneNeighbor: ['water', 'plant', 'fire', 'metal']},
                {additionalData:true, spellShape: 'pillar'},
                {additionalData:true, spells: {
                    attack: ['devouringChains', 'decapitation'],
                    heal: ['elementalHeal', 'destiny'],
                    protect: ['elementalShield', 'mentalFortitude'],
                    buff: ['cyclopsAspect'],
                    creation: ['yearningGambler'],
                    invocation: ['tankInvocation']
                }}
            ],
            metal: [
                {lang: 'fr', label: 'Elément du métal', articleId: 'a55a6941-2b7f-46e4-a31e-a8209c7cf46d',
                description: ['Forgé par l\'homme, le métal est devenu un composant essentiel dans la fabrication d\'outils. Qu\'ils aient un dessein martial ou non.']},
                {additionalData:true, arcanePenalty: 3},
                {additionalData:true, arcaneNeighbor: ['earth', 'fire']},
                {additionalData:true, spellShape: 'pillar'},
                {additionalData:true, spells: {
                    attack: ['devouringChains', 'decapitation'],
                    heal: ['elementalHeal', 'recovery'],
                    protect: ['elementalShield', 'sanctuary'],
                    buff: ['golemAspect'],
                    creation: ['yearningGambler'],
                    invocation: ['soldierInvocation']
                }}
            ],
        },
        divinities: {
            acaria:  [
                {lang: 'fr', label: 'Acaria, Protectrice de l\'empire', articleId: '02870f9d-cb85-4428-8678-8ddf7a2836ce'},
                {additionalData:true, elements:['primary', 'primary']}
            ],
            agnis:  [
                {lang: 'fr', label: 'Agnis, Incarnation de la force', articleId: '498049c2-98c0-417c-a7de-cbbe69a7a896'},
                {additionalData:true, elements:['fire', 'water']}
            ],
            kraken:  [
                {lang: 'fr', label: 'Kraken, Terreur des profondeurs', articleId: '97ea5806-6f53-4c6c-a4da-2f08ed297aba'},
                {additionalData:true, elements:['water', 'animal']}
            ],
            jaumard:  [
                {lang: 'fr', label: 'Jaumard, Déesse de la chasse', articleId: '8ad77d90-9f07-454c-b6a0-8608c7ff0da8'},
                {additionalData:true, elements:['animal', 'plant']}
            ],
            rarmonan:  [
                {lang: 'fr', label: 'Rarmonan, Dame de l\'hiver', articleId: '45560ee1-fa94-4a9f-8f20-6b72edd7fe05'},
                {additionalData:true, elements:['wind', 'ice']}
            ],
            ghekumi:  [
                {lang: 'fr', label: 'Ghekumi, l\'éternel', articleId: '34065895-feb7-42fd-b0cb-5cd2840190ac'},
                {additionalData:true, elements:['earth', 'metal']}
            ],
            mukeshuj:  [
                {lang: 'fr', label: 'Mukeshuj, l\'Enigmatique', articleId: '999f9998-ec81-4ca0-baad-d8f9de12704f'},
                {additionalData:true, elements:['shadow', 'mind']}
            ]
        },
        shapes: {
            pulse: [
                {lang: 'fr', label: 'Pulsation', articleId: '5fd75e15-90a6-4402-bf2b-726b68ea36d0', anchor:'pulse'},
                {additionalData:true, translated:true, spellDescription: {
                    cloneDesc : [
                        {lang: 'fr', label: '', description: [
                                                    'Une pulsation apparait', 
                                                    'Une pulsation apparait', 
                                                    'Deux pulsations apparaissent', 
                                                    'Trois pulsations apparaissent', 
                                                    'Cinq pulsations apparaissent', 
                                                    'Dix pulsations apparaissent' ]}
                    ],
                    rangeDesc : [
                        {lang: 'fr', label: '', description: [
                                                    ' à côté du lanceur.', 
                                                    ' à 5m du lanceur.', 
                                                    ' à 20m du lanceur.', 
                                                    ' à 100m du lanceur.', 
                                                    ' à portée de voix du lanceur.', 
                                                    ' à un endroit visible du lanceur.']}
                    ],
                    areaDesc : [
                        {lang: 'fr', label: '', description: [
                                                    '', 
                                                    'Chaque pulsation s\'étale sur une zone de 3m de rayon.', 
                                                    'Chaque pulsation s\'étale sur une zone de 10m de rayon.', 
                                                    'Chaque pulsation s\'étale sur une zone de 25m de rayon', 
                                                    'Chaque pulsation s\'étale sur une zone de 100m de rayon', 
                                                    'Chaque pulsation est gigantesque et engloble facilement la taille d\'une petite ville'] }
                    ],
                    durationDesc : [
                        {lang: 'fr', label: '', description: [
                                                    'Chaque pulsation persiste pendant 1min (20UT).', 
                                                    'Chaque pulsation persiste pendant 3min (60UT).', 
                                                    'Chaque pulsation persiste pendant 10min (200UT).', 
                                                    'Chaque pulsation persiste pendant 1h (Tout la scène de combat).', 
                                                    'Chaque pulsation persiste pendant 1 journée.', 
                                                    'Chaque pulsation persiste pendant 1 semaine.'] }
                    ], 
                    additionalInfo : [
                        {lang: 'fr', label: '', description: [
                                                    'Les pulsations ne sont réellement efficaces qu\'à leur centre. L\'effet est réduit d\'un niveau pour les cibles éloignées', 
                                                    'Toutes les cibles, alliées ou ennemies prises dans une pulsation subissent les effets.'] }
                    ]
                }}

            ],
            ray: [
                {lang: 'fr', label: 'Rayon', articleId: '5fd75e15-90a6-4402-bf2b-726b68ea36d0', anchor:'ray'},
                {additionalData:true, translated:true, spellDescription: {
                    cloneDesc : [
                        {lang: 'fr', label: '', description: [
                                                    'Le lanceur émet de ses mains un rayon', 
                                                    'Le lanceur émet de ses mains un rayon', 
                                                    'Le lanceur émet de ses mains deux rayons ', 
                                                    'Le lanceur émet de ses mains trois rayons ', 
                                                    'Le lanceur émet de ses mains cinq rayons', 
                                                    'Le lanceur émet de ses mains dix rayons' ]}
                    ],
                    rangeDesc : [
                        {lang: 'fr', label: '', description: [
                                                    ' se prolongeant sur quelques centimètres.', 
                                                    ' se prolongeant sur 5m.', 
                                                    ' se prolongeant sur 20m.', 
                                                    ' se prolongeant sur 100m.', 
                                                    ' se prolongeant jusqu\'à portée de voix.', 
                                                    ' se prolongeant jusqu\'à portée de vision.']}
                    ],
                    areaDesc : [
                        {lang: 'fr', label: '', description: [
                                                    'Chaque rayon possède la largeur d\'un doigt.', 
                                                    'Chaque rayon possède la largeur d\'un doigt. Ils traversent la première cible rencontrée.', 
                                                    'Chaque rayon possède 20cm de largeur. Ils peuvent traverser jusqu\'à deux cibles.', 
                                                    'Chaque rayon possède 50cm de largeur. Ils peuvent traverser jusqu\'à trois cibles.', 
                                                    'Chaque rayon possède 50cm de largeur. Ils traversent toutes les cibles. Il devient possible de combiner plusieurs rayons en un seul, maximisant ainsi l\'effet.', 
                                                    'Chaque rayon possède 1m de largeur. Ils traversent toutes les cibles. Il devient possible de combiner tous rayons en un seul, maximisant ainsi l\'effet.'] }
                    ],
                    durationDesc : [
                        {lang: 'fr', label: '', description: [
                                                    'L\'effet du rayon sur les cibles touchées persiste pendant 1min (20UT).', 
                                                    'L\'effet du rayon sur les cibles touchées persiste pendant 3min (60UT).', 
                                                    'L\'effet du rayon sur les cibles touchées persiste pendant 10min (200UT).', 
                                                    'L\'effet du rayon sur les cibles touchées persiste pendant 1h (Tout la scène de combat).', 
                                                    'L\'effet du rayon sur les cibles touchées persiste pendant 1 journée.', 
                                                    'L\'effet du rayon sur les cibles touchées persiste pendant 1 semaine.'] }
                    ], 
                    additionalInfo : [
                        {lang: 'fr', label: '', description: [
                                                    'Les rayons ne durent qu\'un instant mais peuvent avoir des effets prolongés sur les cibles touchées', 
                                                    'Toutes les cibles, alliées ou ennemis touchées par un rayon subissent les effets.'] }
                    ]
                }}
            ],
            flood: [
                {lang: 'fr', label: 'Torrent', articleId: '5fd75e15-90a6-4402-bf2b-726b68ea36d0', anchor:'flood'},
                {additionalData:true, translated:true, spellDescription: {
                    cloneDesc : [
                        {lang: 'fr', label: '', description: [
                                                    'Le lanceur émet de ses mains un torrent d\'énergie ', 
                                                    'Le lanceur émet de ses mains un torrent d\'énergie ', 
                                                    'Le lanceur émet de ses mains deux torrents d\'énergie  ', 
                                                    'Le lanceur émet de ses mains trois torrents d\'énergie  ', 
                                                    'Le lanceur émet de ses mains cinq torrents d\'énergie ', 
                                                    'Le lanceur émet de ses mains dix torrents d\'énergie ' ]}
                    ],
                    rangeDesc : [
                        {lang: 'fr', label: '', description: [
                                                    ' se prolongeant sur quelques centimètres.', 
                                                    ' se prolongeant sur 5m.', 
                                                    ' se prolongeant sur 20m.', 
                                                    ' se prolongeant sur 100m.', 
                                                    ' se prolongeant jusqu\'à portée de voix.', 
                                                    ' se prolongeant jusqu\'à portée de vision.']}
                    ],
                    areaDesc : [
                        {lang: 'fr', label: '', description: [
                                                    'Chaque torrent d\'énergie se propage en ligne droite et s\'arrête à la première cible touchée.', 
                                                    'Chaque torrent d\'énergie se propage en ligne droite et s\'arrête à la première cible touchée. Il est possible de contouner des cibles pour atteindre d\'autres plus éloignées.', 
                                                    'Chaque torrent d\'énergie se propage dans un cone de 45° dans une direction donnée. Chaque torrent peut impacter jusqu\'à deux cibles au choix dans le cone de déferlement.', 
                                                    'Chaque torrent d\'énergie se propage dans un cone de 90° dans une direction donnée. Chaque torrent peut impacter jusqu\'à trois cibles au choix dans le cone de déferlement.', 
                                                    'Chaque torrent d\'énergie se propage dans un cone de 180° dans une direction donnée. Chaque torrent peut impacter un nombre illimité de cibles au choix dans le cone de déferlement.', 
                                                    'Chaque torrent d\'énergie se propage dans un cone de 360° dans une direction donnée. Chaque torrent peut impacter un nombre illimité de cibles au choix dans le cone de déferlement.']}
                    ],
                    durationDesc : [
                        {lang: 'fr', label: '', description: [
                                                    'L\'effet du torrent d\'énergie sur les cibles touchées persiste pendant 1min (20UT).', 
                                                    'L\'effet du torrent d\'énergie sur les cibles touchées persiste pendant 3min (60UT).', 
                                                    'L\'effet du torrent d\'énergie sur les cibles touchées persiste pendant 10min (200UT).', 
                                                    'L\'effet du torrent d\'énergie sur les cibles touchées persiste pendant 1h (Tout la scène de combat).', 
                                                    'L\'effet du torrent d\'énergie sur les cibles touchées persiste pendant 1 journée.', 
                                                    'L\'effet du torrent d\'énergie sur les cibles touchées persiste pendant 1 semaine.'] }
                    ], 
                    additionalInfo : [
                        {lang: 'fr', label: '', description: [
                                                    'Les torrents d\'énergie ne durent qu\'un instant mais peuvent avoir des effets prolongés sur les cibles touchées'] }
                    ]
                }}
            ],
            pillar: [
                {lang: 'fr', label: 'Pillier', articleId: '5fd75e15-90a6-4402-bf2b-726b68ea36d0', anchor:'pillar'},
                {additionalData:true, translated:true, spellDescription: {
                    cloneDesc : [
                        {lang: 'fr', label: '', description: [
                                                    'Un pillier d\'énergie jaillit', 
                                                    'Un pillier d\'énergie jaillit', 
                                                    'Deux pilliers d\'énergie jaillissent', 
                                                    'Trois pilliers d\'énergie jaillissent', 
                                                    'Cinq pilliers d\'énergie jaillissent', 
                                                    'Dix pilliers d\'énergie jaillissent' ]}
                    ],
                    rangeDesc : [
                        {lang: 'fr', label: '', description: [
                                                    ' à côté du lanceur.', 
                                                    ' à 5m du lanceur.', 
                                                    ' à 20m du lanceur.', 
                                                    ' à 100m du lanceur.', 
                                                    ' à portée de voix du lanceur.', 
                                                    ' à un endroit visible du lanceur.']}
                    ],
                    areaDesc : [
                        {lang: 'fr', label: '', description: [
                                                    '', 
                                                    'Chaque pillier possède une largeur de 3m de rayon.', 
                                                    'Chaque pillier possède une largeur de 10m de rayon.', 
                                                    'Chaque pillier possède une largeur de 25m de rayon', 
                                                    'Chaque pillier possède une largeur de 100m de rayon', 
                                                    'Chaque pillier est gigantesque et engloble facilement la taille d\'une petite ville'] }
                    ],
                    durationDesc : [
                        {lang: 'fr', label: '', description: [
                                                    'Chaque pillier persiste pendant 1min (20UT).', 
                                                    'Chaque pillier persiste pendant 3min (60UT).', 
                                                    'Chaque pillier persiste pendant 10min (200UT).', 
                                                    'Chaque pillier persiste pendant 1h (Tout la scène de combat).', 
                                                    'Chaque pillier persiste pendant 1 journée.', 
                                                    'Chaque pillier persiste pendant 1 semaine.'] }
                    ], 
                    additionalInfo : [
                        {lang: 'fr', label: '', description: [
                                                    'Les pilliers se doivent de jaillir d\'un des éléments harmonisés pour être réellement efficace. L\'élément du vent permet de faire jaillir les pilliers dans les airs dans un environnement venteux. Pour la lumière, l\'ombre ou la lune, le pillier sort du sol s\'il est correctement éclairé.', 
                                                    'Toutes les cibles, alliées ou ennemies prises dans un pillier subissent les effets.'] }
                    ]
                }}
            ],
            projectile: [
                {lang: 'fr', label: 'Projectile', articleId: '5fd75e15-90a6-4402-bf2b-726b68ea36d0', anchor:'projectile'},
                {additionalData:true, translated:true, spellDescription: {
                    cloneDesc : [
                        {lang: 'fr', label: '', description: [
                                                    'Le lanceur émet de ses mains un projectile ', 
                                                    'Le lanceur émet de ses mains un projectile ', 
                                                    'Le lanceur émet de ses mains deux projectiles  ', 
                                                    'Le lanceur émet de ses mains trois projectiles  ', 
                                                    'Le lanceur émet de ses mains cinq projectiles ', 
                                                    'Le lanceur émet de ses mains dix projectiles ' ]}
                    ],
                    rangeDesc : [
                        {lang: 'fr', label: '', description: [
                                                    ' pouvant parcourir quelques centimètres de distance.', 
                                                    ' pouvant parcourir 5m de distance.', 
                                                    ' pouvant parcourir 20m de distance.', 
                                                    ' pouvant parcourir 100m de distance.', 
                                                    ' pouvant atteindre une cible à portée de voix.', 
                                                    ' pouvant atteindre une cible à portée de vision.']}
                    ],
                    areaDesc : [
                        {lang: 'fr', label: '', description: [
                                                    'Chaque projectile se fige dans la première cible touchée.', 
                                                    'Chaque projectile se fige dans la première cible touchée et explose impactant toutes les personnes dans une zone de 3m de large', 
                                                    'Chaque projectile se fige dans la première cible touchée et explose impactant toutes les personnes dans une zone de 10m de large', 
                                                    'Chaque projectile se fige dans la première cible touchée et explose impactant toutes les personnes dans une zone de 25m de large', 
                                                    'Chaque projectile se fige dans la première cible touchée et explose impactant toutes les personnes dans une zone de 100m de large', 
                                                    'Chaque projectile se fige dans la première cible touchée et provoque une explosion se propageant sur la taille d\'une petite ville']}
                    ],
                    durationDesc : [
                        {lang: 'fr', label: '', description: [
                                                    'L\'effet du projectile sur les cibles touchées persiste pendant 1min (20UT).', 
                                                    'L\'effet du projectile sur les cibles touchées persiste pendant 3min (60UT).', 
                                                    'L\'effet du projectile sur les cibles touchées persiste pendant 10min (200UT).', 
                                                    'L\'effet du projectile sur les cibles touchées persiste pendant 1h (Tout la scène de combat).', 
                                                    'L\'effet du projectile sur les cibles touchées persiste pendant 1 journée.', 
                                                    'L\'effet du projectile sur les cibles touchées persiste pendant 1 semaine.'] }
                    ], 
                    additionalInfo : [
                        {lang: 'fr', label: '', description: [
                                                    'Les projectiles peuvent amorcer une trajectoire légèrement courber pour contourner les premiers obstacles.',
                                                    'Lorsqu\'ils explosent, les personnes impactés sont celles (alliées ou ennemis) se trouvant dans le souffre de l\'explosion. Un mur solide peut par exemple protéger de ce genre d\'effet.'] }
                    ]
                }}
            ],
            fog: [
                {lang: 'fr', label: 'Nuage', articleId: '5fd75e15-90a6-4402-bf2b-726b68ea36d0', anchor:'fog'},
                {additionalData:true, translated:true, spellDescription: {
                    cloneDesc : [
                        {lang: 'fr', label: '', description: [
                                                    'Un nuage d\'énergie se forme', 
                                                    'Un nuage d\'énergie se forme', 
                                                    'Deux nuages d\'énergie se forment', 
                                                    'Trois nuages d\'énergie se forment', 
                                                    'Cinq nuages d\'énergie se forment', 
                                                    'Dix nuages d\'énergie se forment' ]}
                    ],
                    rangeDesc : [
                        {lang: 'fr', label: '', description: [
                                                    ' à côté du lanceur.', 
                                                    ' à 5m du lanceur.', 
                                                    ' à 20m du lanceur.', 
                                                    ' à 100m du lanceur.', 
                                                    ' à portée de voix du lanceur.', 
                                                    ' à un endroit visible du lanceur.']}
                    ],
                    areaDesc : [
                        {lang: 'fr', label: '', description: [
                                                    '',
                                                    'Chaque nuage se répand sur une zone de 3m de rayon.', 
                                                    'Chaque nuage se répand sur une zone de 10m de rayon.', 
                                                    'Chaque nuage se répand sur une zone de 25m de rayon', 
                                                    'Chaque nuage se répand sur une zone de 100m de rayon', 
                                                    'Chaque nuage se répand et engloble facilement la taille d\'une petite ville'] }
                    ],
                    durationDesc : [
                        {lang: 'fr', label: '', description: [
                                                    'Chaque nuage persiste pendant 1min (20UT).', 
                                                    'Chaque nuage persiste pendant 3min (60UT).', 
                                                    'Chaque nuage persiste pendant 10min (200UT).', 
                                                    'Chaque nuage persiste pendant 1h (Tout la scène de combat).', 
                                                    'Chaque nuage persiste pendant 1 journée.', 
                                                    'Chaque nuage persiste pendant 1 semaine.'] }
                    ], 
                    additionalInfo : [
                        {lang: 'fr', label: '', description: [
                                                    'Les nuages sont formés de particules élémentaires qui provoquent un effet lorsqu\'elles sont inhalées. Une personne avertie peut tenter de retenir sa respiration pour mitiger les effets.', 
                                                    'Il est possible de controler un minimum le nuage pour n\'affecter qu\'une partie des cibles. Pour pouvoir exclure une personnes des effets du nuage, il faut qu\'elle soit visible du lanceur de sort.'] }
                    ]
                }}
            ],
            rift: [
                {lang: 'fr', label: 'Faille éthérée', articleId: '5fd75e15-90a6-4402-bf2b-726b68ea36d0', anchor:'rift'},
                {additionalData:true, translated:true, spellDescription: {
                    cloneDesc : [
                        {lang: 'fr', label: '', description: [
                                                    'Une faille dans le plan éthéré se crée', 
                                                    'Une faille dans le plan éthéré se crée', 
                                                    'Deux failles dans le plan éthéré se créent', 
                                                    'Trois failles dans le plan éthéré se créent', 
                                                    'Cinq failles dans le plan éthéré se créent', 
                                                    'Dix failles dans le plan éthéré se créent']}
                    ],
                    rangeDesc : [
                        {lang: 'fr', label: '', description: [
                                                    ' à côté du lanceur.', 
                                                    ' à 5m du lanceur.', 
                                                    ' à 20m du lanceur.', 
                                                    ' à 100m du lanceur.', 
                                                    ' à portée de voix du lanceur.', 
                                                    ' à un endroit visible du lanceur.']}
                    ],
                    areaDesc : [
                        {lang: 'fr', label: '', description: [
                                                    'De la faille surgit une entitée d\'une dizaine de centimètres.', 
                                                    'De la faille surgit une entitée pouvant impacter une zone de 3m de rayon.', 
                                                    'De la faille surgit une entitée pouvant impacter sur une zone de 10m de rayon.', 
                                                    'De la faille surgit une entitée pouvant impacter sur une zone de 25m de rayon.', 
                                                    'De la faille surgit une entitée pouvant impacter sur une zone de 100m de rayon.', 
                                                    'De la faille surgit une entitée titanesque.'] }
                    ],
                    durationDesc : [
                        {lang: 'fr', label: '', description: [
                                                    'Chaque entité persiste sur le plan matériel pendant 1min (20UT).', 
                                                    'Chaque entité persiste sur le plan matériel pendant 3min (60UT).', 
                                                    'Chaque entité persiste sur le plan matériel pendant 10min (200UT).', 
                                                    'Chaque entité persiste sur le plan matériel pendant 1h (Tout la scène de combat).', 
                                                    'Chaque entité persiste sur le plan matériel pendant 1 journée.', 
                                                    'Chaque entité persiste sur le plan matériel pendant 1 semaine.'] }
                    ], 
                    additionalInfo : [
                        {lang: 'fr', label: '', description: [
                                                    'La faille est la forme de magie la plus polyvalentes, ne s\'encombrant pas de notion de ligne de vue et pouvant filtrer à loisir les personnes affectées.', 
                                                    'Elle est par contre plus dangereuse que les autres, et il n\'est pas rare que quelque chose d\'inattendu en sorte. Pour modéliser cela, le camp adverse pioche automatiquement une carte évênement lorsque une faille est générée par un magicien.'] }
                    ]
                }}
            ],
        },
        system: {
            arcane: [
                {lang: 'fr', label: 'La magie profane', articleId: '8751cc34-69cd-477b-8fd8-c9f1116b6120'},
                {additionalData:true, translated:true, maneuvers: {
                    flexible_magic : [
                        {lang: 'fr', label: 'Magie flexible', anchor: 'flexible_magic'}
                    ],
                    maintain_circle : [
                        {lang: 'fr', label: 'Conservation du cercle', anchor: 'maintain_circle'}
                    ]
                }}
            ],
            divine: [
                {lang: 'fr', label: 'La magie divine', articleId: 'da866d1c-3cab-4311-bed0-9fa9922ce784'},
                {additionalData:true, translated:true, maneuvers: {
                    apostle : [
                        {lang: 'fr', label: 'Je sers mon dieu', anchor: 'apostle'}
                    ],
                    spirit_recall : [
                        {lang: 'fr', label: 'Rappel de l\'esprit', anchor: 'spirit_recall'}
                    ]
                }},
                {additionalData:true, translated:true, element: {
                    any : [
                        {lang: 'fr', label: 'Esprit prismatique', anchor: 'element-any'}
                    ],
                    primary : [
                        {lang: 'fr', label: 'Esprit lumineux'}
                    ],
                    fire : [
                        {lang: 'fr', label: 'Esprit du feu'}
                    ],
                    electricity : [
                        {lang: 'fr', label: 'Esprit électrique'}
                    ],
                    wind : [
                        {lang: 'fr', label: 'Esprit du vent'}
                    ],
                    moon : [
                        {lang: 'fr', label: 'Esprit de la lune'}
                    ],
                    animal : [
                        {lang: 'fr', label: 'Esprit animal'}
                    ],
                    mind : [
                        {lang: 'fr', label: 'Esprit des rêves'}
                    ],
                    shadow : [
                        {lang: 'fr', label: 'Esprit des ombres'}
                    ],
                    ice : [
                        {lang: 'fr', label: 'Esprit de glace'}
                    ],
                    water : [
                        {lang: 'fr', label: 'Esprit de l\'eau'}
                    ],
                    plant : [
                        {lang: 'fr', label: 'Esprit des plantes'}
                    ],
                    earth : [
                        {lang: 'fr', label: 'Esprit de la terre'}
                    ],
                    metal : [
                        {lang: 'fr', label: 'Esprit métalique'}
                    ]
                }},
                {additionalData:true, translated:true, effect: {
                    offense : [
                        {lang: 'fr', label: 'Esprit aggressif', anchor: 'effect',
                         description: ['L\'esprit adore en découdre avec les êtres vivants.','Il accepte d\'effectuer des miracles d\'attaque  ou d\'affaiblissement'] },
                        {additionalData:true, offensive: true }
                    ],
                    defense : [
                        {lang: 'fr', label: 'Esprit défensif', anchor: 'effect',
                         description: ['L\'esprit déteste la violence et vénère la vie sur toutes ses formes.','Il accepte d\'effectuer des miracles de protection ou de guérison ']},
                        {additionalData:true, offensive: false }
                    ],
                    alteration : [
                        {lang: 'fr', label: 'Esprit modeleur', anchor: 'effect',
                         description: ['L\'esprit adore altérer la réalité.', 'Il accepte d\'effectuer des miracles de renforcement ou de matérialisation.']},
                        {additionalData:true, offensive: false }
                    ],
                    other : [
                        {lang: 'fr', label: 'Esprit curieux', anchor: 'effect',
                         description: ['L\'esprit aime découvrir de nouvelles choses.','Il accepte d\'effectuer des miracles de divination ou d\'invocation.']},
                        {additionalData:true, offensive: false }
                    ],
                    any : [
                        {lang: 'fr', label: 'Esprit prismatique', anchor: 'effect',
                         description: ['L\'esprit primsatique est capable d\'effectuer n\'importe quel type de miracle.']}
                    ]
                }},
                {additionalData:true, translated:true, bonus: {
                    area : [
                        {lang: 'fr', label: 'Zone d\'effet accrue', anchor: 'bonus', 
                         description: ['L\'effet impacte toutes les cibles voulues dans une zone de 10m autour de la cible principale.']}
                    ],
                    duration : [
                        {lang: 'fr', label: 'Durée accrue', anchor: 'bonus', 
                        description: ['L\'effet persiste pendant 10min (20UT).', 'Incompatible avec les miracles d\'attaques ou de soins']}
                    ],
                    duplication : [
                        {lang: 'fr', label: 'Duplication', anchor: 'bonus', 
                        description: ['L\'esprit peut envoyer le miracle sur deux cibles distinctes.', 'Uniquement compatibles avec les miracles d\'attaques, soins, et invocations.']}
                    ],
                    effect : [
                        {lang: 'fr', label: 'Effet renforcé', anchor: 'bonus', 
                        description: ['L\'esprit est particulièrement doué.', 'L\'efficacité du miracle est amélioré d\'un rang.']}
                    ],
                    drain : [
                        {lang: 'fr', label: 'Drain diminué', anchor: 'bonus', 
                        description: ['L\'esprit est moins gourmant en énergie vitale que ses homologues.', 'Le drain est diminué d\'un rang.']}
                    ],
                    nothing : [
                        {lang: 'fr', label: 'Aucun bonus', anchor: 'bonus', 
                         description: ['L\'esprit n\'a aucun talent particulier.']}
                    ],
                    any : [
                        {lang: 'fr', label: 'Esprit prismatique', anchor: 'bonus', 
                         description: ['L\'esprit prismatique est capable de fournir n\'importe quel bonus au prêtre.']}
                    ]
                }}
            ],
            songs: [
                {lang: 'fr', label: 'La magie des émotions', articleId: '386af97f-460c-48e3-8971-b71b5c295555'},
                {additionalData:true, translated:true, melodies: {
                    initiative : [
                        {lang: 'fr', label: 'Laisse le rythme te guider', anchor: 'initiative'}
                    ],
                    advantage : [
                        {lang: 'fr', label: 'Succès pour les vertueux', anchor: 'advantage'}
                    ],
                    mastery : [
                        {lang: 'fr', label: 'Ouvre ton esprit au monde', anchor: 'mastery'}
                    ]
                }},
                {additionalData:true, translated:true, maneuvers: {
                    everlasting_melody : [
                        {lang: 'fr', label: 'Mélodie incessante', anchor: 'incessante'}
                    ],
                    solo : [
                        {lang: 'fr', label: 'Effectuer un solo', anchor: 'solo'}
                    ],
                    reversal : [
                        {lang: 'fr', label: 'Reversal', anchor: 'reversal'}
                    ],
                    apotheosis : [
                        {lang: 'fr', label: 'Apothéose', anchor: 'apotheose'}
                    ]
                }}
            ]
        }
    },
    interaction: {
        system: {
            model : [
                {lang: 'fr', label: 'Modéliser une interaction', articleId: '62cf49c1-dbf6-4b71-b49f-f9d48cc177f2'},
                {additionalData:true, translated:true, section: {
                    difficulty : [
                        {lang: 'fr', label: 'Calcul de la difficulté', anchor: 'difficulty'}
                    ],
                    environment : [
                        {lang: 'fr', label: 'Prendre en compte l\'environnement', anchor: 'modifiers'}
                    ]
                }}
            ],
            resolve : [
                {lang: 'fr', label: 'Résoudre une interaction', articleId: '77f8d29a-2706-4506-8db5-f0bc66625dac'},
                {additionalData:true, translated:true, section: {
                    time : [
                        {lang: 'fr', label: 'temps nécessaire pour effectuer une action', anchor: 'time'}
                    ],
                    risk : [
                        {lang: 'fr', label: 'La prise de risque délibérée', anchor: 'risk'}
                    ],
                    successScore : [
                        {lang: 'fr', label: 'Degrés de réussite', anchor: 'successScore'}
                    ],
                    cards : [
                        {lang: 'fr', label: 'Pioche des cartes évênements', anchor: 'cards'}
                    ],
                    tokens : [
                        {lang: 'fr', label: 'Mise à jour du pool de jetons', anchor: 'tokens'}
                    ],
                    summary: [
                        {lang: 'fr', label: 'Récapitulatif', anchor: 'synthese'}
                    ]
                }}
            ],
            events: [
                {lang: 'fr', label: 'Utilisation des cartes événements', articleId: '6c964d2b-0a6e-48bc-bc61-ce085a7ed3a9'},
                {additionalData:true, translated:true, types : {
                    melee01: [
                        {lang: 'fr', label: 'Une armure mal entretenue', anchor: 'effect',
                         description: ['L\'armure portée par votre adversaire n\'a pas été bien entretenue. Le MJ vous indique une manière de la rendre totalement inerte. Vous pouvez être source de proposition.'] },
                        {lang: 'en', label: 'Poorly maintened armor', anchor: 'effect',
                         description: ['The armor worn by your opponent has not been well maintained. The GM tells you a way to make it totally inert. You can give some suggestions.'] }
                    ],
                    melee02: [
                        {lang: 'fr', label: 'Mouvement improbable', anchor: 'effect',
                         description: ['L\'attaque en mélée échoue et l\'adversaire est désarmé. Il est de plus complètement désemparé par ce qu\'il vient de constater. Il récupère trois jetons poisses.'] },
                        {lang: 'en', label: 'Implausible movement', anchor: 'effect',
                         description: ['The melee attack fails and the opponent is disarmed. He is moreover completely distraught by what he has just witnessed. He takes three bad luck tokens.'] }
                    ],
                    ranged01: [
                        {lang: 'fr', label: 'Position surélevée', anchor: 'effect',
                         description: ['Grâce à une acrobatie bien placée, vous vous positionnez à un endroit difficilement accessible. De là, les ennemis ne peuvent pas s\'interposer pour protéger les leurs. De plus, il leur sera difficile de vous atteindre au corps à corps.'] },
                        {lang: 'en', label: 'Higher ground', anchor: 'effect',
                         description: ['Thanks to some acrobatics, you reach somewhere difficult to access. From here, ennemies can\'t protect their friends from your projectiles and they will have a hard time reaching you in melee.'] }
                    ],
                    ranged02: [
                        {lang: 'fr', label: 'Mauvaise cible', anchor: 'effect',
                         description: ['L\'adversaire vous a manifestement confondu avec un autre cible. Le tir aurait été parfait s\'il avait visé la bonne cible. Une autre cible, alliée ou ennemie, subit l\'attaque. Le niveau de réussite de l\'attaque augmente d\'un niveau.'] },
                        {lang: 'en', label: 'Wrong target', anchor: 'effect',
                         description: ['The opponent has obviously confused you with another target. The shot would have been perfect if he had hit the right target. Another target, friendly or foe, suffers the attack. The success level of the attack increases by one level.'] }
                    ],
                    improvised01: [
                        {lang: 'fr', label: 'Un gros boum!', anchor: 'effect',
                         description: ['Quelquechose explose/s\'effondre en plein milieu de la scène. Les armes improvisées sont à foison et la typologie du terrain se voit modifiée.'] },
                        {lang: 'en', label: 'A big crash!', anchor: 'effect',
                         description: ['Something explodes/collapses in the middle of the scene. Improvised weapons are in abundance and the typology of the terrain becomes modified.'] }
                    ],
                    improvised02: [
                        {lang: 'fr', label: 'Manque d\'entrainement', anchor: 'effect',
                         description: ['L\'attaque a main nue reste un success mais l\'adversaire se fait mal en frappant, se cassant un os. Il subit 5 dégâts de fatigue et ne peut plus utiliser le membre cassé.'] },
                        {lang: 'en', label: 'Lack of training', anchor: 'effect',
                         description: ['The barehanded attack remains a success but the opponent hurts himself by hitting you, breaking a bone. He suffers 5 fatigue damages and can no longer use the broken limb.'] }
                    ],
                    arcane01: [
                        {lang: 'fr', label: 'Terrain élémentaire', anchor: 'effect',
                         description: ['Le sol sur lequel vous êtes positionné agit comme un catalyseur élémentaire. Choisissez un élément. Tant que vous restez sur ce sol, les harmonisations à cet élément ne vous impose aucun malus.'] },
                        {lang: 'en', label: 'Elemental ground', anchor: 'effect',
                         description: ['The ground on which you are positioned acts as an elemental catalyst. Choose an element. As long as you stay on this ground, attunements to this element impose no penalty on you.'] }
                    ],
                    arcane02: [
                        {lang: 'fr', label: 'Explosion du cercle', anchor: 'effect',
                         description: ['Au moment du lancement du sort, quelque chose casse. Le cercle explose dans un grand fracas infligeant des dégats élémentaire égal au nombre d\'améliorations effectués. Toutes les personnes dans un rayon de 5m sont impactés.'] },
                        {lang: 'en', label: 'Circle explosion', anchor: 'effect',
                         description: ['As the spell is cast, something breaks. The circle explodes with a loud crash inflicting elemental damage equal to the number of upgrades made. Everyone within a 5m radius is affected.'] }
                    ],
                    prayer01: [
                        {lang: 'fr', label: 'Faveur divine', anchor: 'effect',
                         description: ['Une divininté avec laquelle vous avez contracté un pacte est satisfait de vos récentes actions. Choisissez un type de miracle. Cette divinité vous accordera se protection pour tous les miracles de ce type, quelque soit l\'élément utilisé.'] },
                        {lang: 'en', label: 'A deity favors you', anchor: 'effect',
                         description: ['A deity with whom you have made a pact is pleased with your recent actions. Choose a miracle type. This deity will grant you protection for all miracles of this type, regardless of the used element.'] }
                    ],
                    prayer02: [
                        {lang: 'fr', label: 'Colère divine', anchor: 'effect',
                         description: ['Le miracle demandé rend la divinité folle de rage. Non seulement elle ne protère pas son serviteur du drain mais elle invoque un miracle du même niveau avantageant le camp adversaire. Le MJ choisit le miracle, mais vous êtes source de proposition.'] },
                        {lang: 'en', label: 'Deity wrath', anchor: 'effect',
                         description: ['The requested miracle drives the deity mad with rage. Not only does she not protect her servitor from the drain, but she invokes a miracle of the same level benefiting the opposing camp. The GM chooses the miracle, but youcan give some suggestion.'] }
                    ],
                    melody01: [
                        {lang: 'fr', label: 'Mélodie improvisée', anchor: 'effect',
                         description: ['La mélodie actuelle vous permet d\'appliquer les effets de nimporte laquelle de vos mélodies connues pour chaque charge dépensée. Vous pouvez appliquer les effets de plusieurs mélodies à un même cible. Chaque effet s\'applique alors comme si c\'était la première charge dépensée.'] },
                        {lang: 'en', label: 'Improvised melody', anchor: 'effect',
                         description: ['Current Melody allows you to apply the effects of any of your known melodies for each charge spent. You can apply the effects of several melodies to the same target. Each effect then applies as if it were the first expended charge.'] }
                    ],
                    melody02: [
                        {lang: 'fr', label: 'Extinction de voix', anchor: 'effect',
                         description: ['Un barde adverse casse son matériel de chant/perd sa voix. Sa mélodie est directement interrompue et il se trouve dans l\'incapacité d\'en démarrer une nouvelle.'] },
                        {lang: 'en', label: 'Loss of voice', anchor: 'effect',
                         description: ['An opposing bard breaks her singing gear/loses her voice. His melody is immediately interrupted and he finds himself unable to start a new one.'] }
                    ],
                    explore01: [
                        {lang: 'fr', label: 'Un détail intéressant', anchor: 'effect',
                         description: ['Vous remarquez quelquechose d\'inhabituel. Vous pouvez ajouter un élément narratif lorsque le MJ détail une scène.'] },
                        {lang: 'en', label: 'Something is strange', anchor: 'effect',
                         description: ['You spot something uncommon. You can add a detail when the GM is describing a scene.'] }
                    ],
                    explore02: [
                        {lang: 'fr', label: 'Un endroit familier', anchor: 'effect',
                         description: ['Vous visitez un endroit déjà visitez par votre esprit gardien lors d\'une de ses précédentes possessions. Des bribes de souvenirs refont surface. L\'humeur de votre esprit gardien progresse favorablement.'] },
                        {lang: 'en', label: 'A familiar place', anchor: 'effect',
                         description: ['You visit a place already visited by your guardian spirit during one of its previous possessions. Fragments of memories resurface. The mood of your guardian spirit progresses favorably.'] }
                    ],
                    explore03: [
                        {lang: 'fr', label: 'Oubli bête', anchor: 'effect',
                         description: ['Un PJ a oublié quelque chose au dernier endroit où il a dormi. C\'est dommage, il en aurait grandement besoin à présent.'] },
                        {lang: 'en', label: 'Stupid loss', anchor: 'effect',
                         description: ['A PC forgot something at the last place he slept. It\'s a shame, he would need it badly now.'] }
                    ],
                    explore04: [
                        {lang: 'fr', label: 'Ignorer la fatigue', anchor: 'effect',
                         description: ['Vous faîtes fit de votre épuisement et forcez votre corps à fonctionner à plein régime. Pour la scène, réduisez votre fatigue à zéro et récupérez temporairement autant de PV. La fatigue reprend son dû à la fin de la scène.'] },
                        {lang: 'en', label: 'Overcome exhaustion', anchor: 'effect',
                         description: ['You overcome your exhaustion and force your body to work at full force. For the scene, reduce your fatigue to zero and temporarily recover as much HP. Fatigue regains its due at the end of the scene.'] }
                    ],
                    intrigue01: [
                        {lang: 'fr', label: 'Personnalité intéressante', anchor: 'effect',
                         description: ['Quelquechose vous dérange dans le comportement d\'une personne. En regardant de plus près, vous comprenenez ce que c\'est. Vous pouvez rajouter un trait de personnalité à un des PNJ de la scène.'] },
                        {lang: 'en', label: 'Interesting personnality', anchor: 'effect',
                         description: ['Something is bothering you. Looking closer, you understand what it is. You can add a personality trait to one of the NPCs in the scene.'] }
                    ],
                    intrigue02: [
                        {lang: 'fr', label: 'Sérieusement ?!', anchor: 'effect',
                         description: ['Les stigmates d\'un personnage prennent des proportions démesurées. Ils deviennent impossible à cacher pour le reste de la scène et peuvent entrainer des effets secondaires sur l\'entourage de la personne.'] },
                        {lang: 'en', label: 'What the hell!', anchor: 'effect',
                         description: ['The stigmata of a character take on disproportionate effects. They become impossible to hide for the rest of the scene and can cause side effects on the person\'s entourage.'] }
                    ],
                    intrigue03: [
                        {lang: 'fr', label: 'Ne me dévisage pas', anchor: 'effect',
                         description: ['Un PNJ a pris un PJ en grippe. Il tentera par tout les moyens de le rabaisser. Toute interaction pacifiste avec ce dernier devient plus difficile (2 niveaux)'] },
                        {lang: 'en', label: 'Don\'t stare at me', anchor: 'effect',
                         description: ['An NPC has taken a dislike to a PC. He will try any means to put him down. Any pacifist interaction with the latter becomes more difficult (2 levels)'] }
                    ],
                    intrigue04: [
                        {lang: 'fr', label: 'Nouvelle rencontre', anchor: 'effect',
                         description: ['Un nouveau PNJ fait son apparition. Donnez lui un nom et la raison de sa présence ici. Le MJ se charge du reste.'] },
                        {lang: 'en', label: 'New encounter', anchor: 'effect',
                         description: ['A new NPC appears. Give him a name and the reason for his presence here. The GM takes care of the rest.'] }
                    ],

                    sideeffect : [
                        {lang: 'fr', label: 'Effet secondaire', anchor: 'effect',
                         description: ['L\'action adverse reste un succès. Mais elle entraine un effet secondaire. Selon l\'effet, cela vous coutera entre 0 et 3 cartes événements.'] },
                        {lang: 'en', label: 'Side effect', anchor: 'effect',
                         description: ['The opponent interaction still succeed, but it also cause something else to happen. Depending on the desired effect, it will cost you between one and three additional cards.'] }
                    ],
                    discard : [
                        {lang: 'fr', label: 'Situation sous contrôle', anchor: 'effect',
                         description: ['Un adversaire défausse aléatoirement jusqu\'à deux cartes événements.'] },
                        {lang: 'en', label: 'Restoring order', anchor: 'effect',
                         description: ['An opponent discard up to two cards. Cards are discarded at random.'] }
                    ],
                    otheraction : [
                        {lang: 'fr', label: 'Changement de plan', anchor: 'effect',
                         description: ['L\'adversaire doit choisir une autre action. Expliquez en quoi l\'action initiale devient impossible. Ex: Un chandelier tombe en plein milieu de la scène, la séparant en deux.'] },
                        {lang: 'en', label: 'Change of plan', anchor: 'effect',
                         description: ['The opponent must choose another action. Narratively justify why. Example : A candlestick falls right in the middle of the stage, cutting the fight scene in two.'] }
                    ],
                    powersurge : [
                        {lang: 'fr', label: 'Perte de puissance', anchor: 'effect',
                         description: ['Un adversaire perds tout ses jetons pouvoirs. Il restera en mesure d\'en regagner par la suite.'] },
                        {lang: 'en', label: 'Surging power', anchor: 'effect',
                         description: ['An opponent loose all it\'s power tokens. He will still be able to regain some on its next action.'] }
                    ],
                    ornot : [
                        {lang: 'fr', label: 'Anticipation', anchor: 'effect',
                         description: ['Une carte événement adverse est annulée à moins qu\'il ne dépense deux cartes supplémentaires.'] },
                        {lang: 'en', label: 'You saw that coming', anchor: 'effect',
                         description: ['Opponent event is cancelled unless he spend two additional cards'] }
                    ],
                    swap : [
                        {lang: 'fr', label: 'C\'est mon tour!', anchor: 'effect',
                         description: ['Nécessite l\'approbation de l\'allié. Vous intervertissez vos tours de jeu.'] },
                        {lang: 'en', label: 'It\'s my turn!', anchor: 'effect',
                         description: ['Requires ally approval. You switch your turns.'] }
                    ],
                    rescue : [
                        {lang: 'fr', label: 'A la rescousse!', anchor: 'effect',
                         description: ['Vous vous interposez et devenez la nouvelle cible d\'une attaque. Si la carte est jouée avant que les dés ne soient lancés, l\'adverse défausse tout ses jetons avantage et maitrise.'] },
                        {lang: 'en', label: 'To the rescue!', anchor: 'effect',
                         description: ['You rush to an ally rescue. You become the new target of the ennemy attack. If played before the roll, you discard the advantage and chance tokens the ennemy may have. If played afterward, the result success is not altered, even if you had a better defense.'] }
                    ],
                }},
                {additionalData:true, translated:true, timings : {
                    yourturn : [
                        {lang: 'fr', label: 'A votre tour', anchor: 'when'},
                        {lang: 'en', label: 'On your turn', anchor: 'when'},
                    ],
                    enemyturn : [
                        {lang: 'fr', label: 'Avant une action adverse', anchor: 'when'},
                        {lang: 'en', label: 'Before an enemy turn', anchor: 'when'},
                    ],
                    allyturn : [
                        {lang: 'fr', label: 'Au tour d\'un allié', anchor: 'when'},
                        {lang: 'en', label: 'On ally turn', anchor: 'when'},
                    ],
                    successenemy : [
                        {lang: 'fr', label: 'Sur une réussite adverse', anchor: 'when'},
                        {lang: 'en', label: 'Enemy success roll', anchor: 'when'},
                    ],
                    cardplayed : [
                        {lang: 'fr', label: 'Un autre événement a été joué', anchor: 'when'},
                        {lang: 'en', label: 'Another card is played', anchor: 'when'},
                    ],
                    gmonly : [
                        {lang: 'fr', label: 'MJ uniquement', anchor: 'when'},
                        {lang: 'en', label: 'GM only', anchor: 'when'},
                    ],
                    actionattempt : [
                        {lang: 'fr', label: 'Avant un jet de dés adverse', anchor: 'when'},
                        {lang: 'en', label: 'Before enemy roll', anchor: 'when'},
                    ],
                }},
                {additionalData:true, translated:true, onrolls : {
                    die_success : [
                        {lang: 'fr', label: 'Action maîtrisée', anchor: 'onroll'},
                        {lang: 'en', label: 'Mastered action', anchor: 'onroll'},
                    ],
                    die_power : [
                        {lang: 'fr', label: 'Regain d\'énergie', anchor: 'onroll'},
                        {lang: 'en', label: 'Energy boost', anchor: 'onroll'},
                    ],
                    token_success : [
                        {lang: 'fr', label: 'Prise d\'avantage', anchor: 'onroll'},
                        {lang: 'en', label: 'Taking advantage', anchor: 'onroll'},
                    ],
                    token_cardbalance : [
                        {lang: 'fr', label: 'Coup de chance', anchor: 'onroll'},
                        {lang: 'en', label: 'Luck on your side', anchor: 'onroll'},
                    ],
                    initiative : [
                        {lang: 'fr', label: 'Action galvanisante', anchor: 'onroll'},
                        {lang: 'en', label: 'Galvanizing action', anchor: 'onroll'},
                    ],
                    health : [
                        {lang: 'fr', label: 'Récupération rapide', anchor: 'onroll'},
                        {lang: 'en', label: 'Fast recovering', anchor: 'onroll'},
                    ],
                }},
                {additionalData:true, translated:true, costs : {
                    '0' : [
                        {lang: 'fr', label: '0', anchor: 'cost'},
                        {lang: 'en', label: '0', anchor: 'cost'},
                        {additionalData:true, min: 0},
                        {additionalData:true, max: 0}
                    ],
                    '1' : [
                        {lang: 'fr', label: '1', anchor: 'cost'},
                        {lang: 'en', label: '1', anchor: 'cost'},
                        {additionalData:true, min: 1},
                        {additionalData:true, max: 1}
                    ],
                    '2' : [
                        {lang: 'fr', label: '2', anchor: 'cost'},
                        {lang: 'en', label: '2', anchor: 'cost'},
                        {additionalData:true, min: 2},
                        {additionalData:true, max: 2}
                    ],
                    '3' : [
                        {lang: 'fr', label: '3', anchor: 'cost'},
                        {lang: 'en', label: '3', anchor: 'cost'},
                        {additionalData:true, min: 3},
                        {additionalData:true, max: 3}
                    ],
                    V : [
                        {lang: 'fr', label: '0-3', anchor: 'cost'},
                        {lang: 'en', label: '0-3', anchor: 'cost'},
                        {additionalData:true, min: 0},
                        {additionalData:true, max: 3}
                    ]
                }}
            ]
        }
    },
    combat: {
        system: {
            base : [
                {lang: 'fr', label: 'Résoudre un combat', articleId: 'c2d56973-bbde-4b4c-a58c-954acc28e843'}
            ],
            melee : [
                {lang: 'fr', label: 'Se battre avec une arme de mélée', articleId: '7f46febb-5e52-4b40-9606-5e8a0688b9e7'},
                {additionalData:true, translated:true, maneuvers: {
                    bucker_smash : [
                        {lang: 'fr', label: 'Brise bouclier', anchor: 'bucker_smash'}
                    ],
                    disarm : [
                        {lang: 'fr', label: 'Désarmement', anchor: 'disarm'}
                    ],
                    finesse : [
                        {lang: 'fr', label: 'Attaque précise', anchor: 'finesse'}
                    ],
                    harassing : [
                        {lang: 'fr', label: 'Attaque incessante', anchor: 'harassing'}
                    ],
                    hit_and_run : [
                        {lang: 'fr', label: 'Aller-Retour', anchor: 'hit_and_run'}
                    ],
                    overthrow : [
                        {lang: 'fr', label: 'Renversement', anchor: 'overthrow'}
                    ],
                    powerAttack : [
                        {lang: 'fr', label: 'Attaque en puissance', anchor: 'powerAttack'}
                    ],
                    second_weapon : [
                        {lang: 'fr', label: 'Deuxième arme', anchor: 'second_weapon'}
                    ],
                    swipe : [
                        {lang: 'fr', label: 'Balayage', anchor: 'swipe'}
                    ]
                }}
            ],
            ranged : [
                {lang: 'fr', label: 'Se battre avec une arme à distance', articleId: 'd96faae1-f392-48e3-a1b0-0714ee945714'},
                {additionalData:true, translated:true, maneuvers: {
                    ricochet : [
                        {lang: 'fr', label: 'Ricochet', anchor: 'ricochet'}
                    ],
                    finesse : [
                        {lang: 'fr', label: 'Tir précis', anchor: 'finesse'}
                    ],
                    multishot : [
                        {lang: 'fr', label: 'Tir multiple', anchor: 'multishot'}
                    ],
                    sniper : [
                        {lang: 'fr', label: 'Tir préparé', anchor: 'sniper'}
                    ],
                    harassing : [
                        {lang: 'fr', label: 'Tir de barrage', anchor: 'harassing'}
                    ],
                    crippleshot : [
                        {lang: 'fr', label: 'Tir handicapant', anchor: 'crippleshot'}
                    ],
                    interruptingshot : [
                        {lang: 'fr', label: 'Tir d\'interruption', anchor: 'interruptingshot'}
                    ],
                }}
            ],
            improvised : [
                {lang: 'fr', label: 'Se battre avec une arme improvisée', articleId: '51988368-abc3-439d-ac35-a0952a9ac3ff'},
                {additionalData:true, translated:true, maneuvers: {
                    perfect_tool : [
                        {lang: 'fr', label: 'L\'outil parfait', anchor: 'perfect_tool'},
                        {additionalData:true, martialArt: false}
                    ],
                    splash_damage : [
                        {lang: 'fr', label: 'Débris dirigée', anchor: 'splash_damage'},
                        {additionalData:true, martialArt: false}
                    ],
                    throw_down : [
                        {lang: 'fr', label: 'Mise à terre', anchor: 'throw_down'},
                        {additionalData:true, martialArt: true}
                    ],
                    versatility : [
                        {lang: 'fr', label: 'Polyvalence', anchor: 'versatility'},
                        {additionalData:true, martialArt: true}
                    ]

                }}
            ]
        }
    },
    character: {
        attributes : {
            definition: [
                {lang: 'fr', label: 'Les attributs', articleId: 'd67f99be-ec6c-4bd3-a96f-9e4bf020187a'},
                {additionalData:true, translated:true, list: {
                    vigor : [
                        {lang: 'fr', label: 'Vigueur', anchor: 'vigor',
                         description:['Vos capacités physiques globales. Concerne autant l\'endurance que la force.','Est obligatoire pour tous ceux qui veulent se battre en mêlée']},
                        {additionalData:true, order: 0}
                    ],
                    speed : [
                        {lang: 'fr', label: 'Vitesse', anchor: 'speed',
                         description:['Votre capacité à réagir rapidement. Détermine aussi à quel point vous êtes agile.', 'La durée de toutes vos actions physiques dépendront de votre vitesse']},
                        {additionalData:true, order: 1}
                    ],
                    versatility : [
                        {lang: 'fr', label: 'Versatilité', anchor: 'versatility',
                         description:['Détermine à quel point vous êtes capable de vous adapter à des situations imprévus.', 'Aide aussi lorsque vous n\'avez pas en votre possession les outils adéquats.', 'Les artistes et les adeptes en arts martiaux sont grandements dépendants de cet attribut']},
                        {additionalData:true, order: 2}
                    ],
                    perspicacity : [
                        {lang: 'fr', label: 'Perspicacité', anchor: 'perspicacity',
                         description:['Votre capacité à comprendre ce qui se passe autour de vous. Votre manière d\'appréhender les choses peut prendre des formes multiples :', 'Une bonne intuition, une éruidition hors norme, ou encore une forte capacité d\'abstration', 'Les magiciens en ont besoin pour former leurs cercles d\'incantation']},
                        {additionalData:true, order: 3}
                    ],
                    focus : [
                        {lang: 'fr', label: 'Concentration', anchor: 'focus',
                         description:['La force de votre esprit. Votre capacité à rester concentrer et à vous rappeler d\'événements lointains.', 'La durée de toutes vos actions mentales dépendront de votre concentration']},
                        {additionalData:true, order: 4}
                    ],
                    socialEase : [
                        {lang: 'fr', label: 'Aisance sociale', anchor: 'socialEase',
                         description:['Que ce soit des êtres humains ou des esprits immatériels, l\'aisance sociale vous permet de définir à quel point vous arrivez à entretenir des relations.', 'Cet attribut est indispensable pout touy prêtre désireux d\'invoquer des miracles']},
                        {additionalData:true, order: 5}
                    ]
                }},
                {additionalData:true, translated:true, levels: {
                    handicap : [
                        {lang: 'fr', label: 'Handicap', anchor: 'levels'},
                        {additionalData:true, level: -2},
                        {additionalData:true, xpCost: 4 }
                    ],
                    weak : [
                        {lang: 'fr', label: 'Faible', anchor: 'levels'},
                        {additionalData:true, level: -1},
                        {additionalData:true, xpCost: 2 }
                    ],
                    normal : [
                        {lang: 'fr', label: 'Banal', anchor: 'levels'},
                        {additionalData:true, level: 0},
                        {additionalData:true, xpCost: 2 }
                    ],
                    strong : [
                        {lang: 'fr', label: 'Fort', anchor: 'levels'},
                        {additionalData:true, level: 1},
                        {additionalData:true, xpCost: 4 }
                    ],
                    really_strong : [
                        {lang: 'fr', label: 'Exceptionnel', anchor: 'levels'},
                        {additionalData:true, level: 2},
                        {additionalData:true, xpCost: 6 }
                    ],
                    champion : [
                        {lang: 'fr', label: 'Champion', anchor: 'levels'},
                        {additionalData:true, level: 3},
                        {additionalData:true, xpCost: 8 }
                    ],
                    superhuman : [
                        {lang: 'fr', label: 'Surhumain', anchor: 'levels'},
                        {additionalData:true, level: 4},
                        {additionalData:true, xpCost: 10 }
                    ],
                    apostle : [
                        {lang: 'fr', label: 'Apôtre', anchor: 'levels'},
                        {additionalData:true, level: 5},
                        {additionalData:true, xpCost: 12 }
                    ],
                    godlike : [
                        {lang: 'fr', label: 'Divin', anchor: 'levels'},
                        {additionalData:true, level: 6},
                        {additionalData:true, xpCost: 0 }
                    ],
                }}
            ]
        },
        skill_area : {
            definition: [
                {lang: 'fr', label: 'Les axes de compétence', articleId: '3a11f3e6-bdfc-4970-bbdb-ea58f6d303e7'},
                {additionalData:true, translated:true, list: {
                    combat : [
                        {lang: 'fr', label: 'Combat & Magie', anchor: 'combat',
                         description:['Détermine à quel point votre personnage est habitué à se battre.','Chaque compétence débloquée vous permet de maitriser un style de combat, qu\'il soit physique ou magique']},
                        {lang: 'en', label: 'Combat & Magic', anchor: 'combat',
                         description:['Describe how well you well your character can fight.','Each unlocked skill allows you to use a fighting or magic style']},
                        {additionalData:true, order: 0}
                    ],
                    explore : [
                        {lang: 'fr', label: 'Exploration', anchor: 'explore',
                         description:['Une grande partie de vos aventures vous mènera en dehors des murs des cités.', 'Cet axe de compétence permet de déterminer avec quelle efficacité vous faites face à l\'inconnu.']},
                        {lang: 'en', label: 'Explore', anchor: 'explore',
                         description:['A big part of your adventures will lead you outside the city walls', 'This skill set describes how well you manage in this type of environment']},
                        {additionalData:true, order: 1}
                    ],
                    intrigue : [
                        {lang: 'fr', label: 'Intrigue', anchor: 'intrigue',
                         description:['Regroupe toutes les compétences citadines de votre personnage.', 'Contient aussi toutes ses connaissances académiques.']},
                        {lang: 'en', label: 'Intrigue', anchor: 'intrigue',
                         description:['Regroup skills you lean inside a city', 'Also contains academical knowledge.']},
                        {additionalData:true, order: 2}
                    ]
                }},
                {additionalData:true, translated:true, levels: {
                    unknown : [
                        {lang: 'fr', label: 'Non appris', anchor: 'level'},
                        {additionalData:true, level: 0},
                        {additionalData:true, xpCost: 2}
                    ],
                    learned : [
                        {lang: 'fr', label: 'Novice', anchor: 'level'},
                        {additionalData:true, level: 1},
                        {additionalData:true, xpCost: 4}
                    ],
                    advanced : [
                        {lang: 'fr', label: 'Rompu', anchor: 'level'},
                        {additionalData:true, level: 2},
                        {additionalData:true, xpCost: 6}
                    ],
                    expert : [
                        {lang: 'fr', label: 'Expert', anchor: 'level'},
                        {additionalData:true, level: 3},
                        {additionalData:true, xpCost: 8}
                    ],
                    master : [
                        {lang: 'fr', label: 'Maître', anchor: 'level'},
                        {additionalData:true, level: 4},
                        {additionalData:true, xpCost: 10}
                    ],
                    great_master : [
                        {lang: 'fr', label: 'Grand maître', anchor: 'level'},
                        {additionalData:true, level: 5},
                        {additionalData:true, xpCost: 12}
                    ],
                    divine : [
                        {lang: 'fr', label: 'Divin', anchor: 'level'},
                        {additionalData:true, level: 6},
                        {additionalData:true, xpCost: 0}
                    ]
                }},
                {additionalData:true, translated:true, skills: {
                    melee: [
                        {lang: 'fr', label: 'Armes de mélée', articleId: ''},
                        {additionalData:true, skillType:'combat' }
                    ],
                    ranged: [
                        {lang: 'fr', label: 'Armes à distance', articleId: ''},
                        {additionalData:true, skillType:'combat' }
                    ],
                    improvised: [
                        {lang: 'fr', label: 'Armes improvisées', articleId: ''},
                        {additionalData:true, skillType:'combat' }
                    ],
                    prayer: [
                        {lang: 'fr', label: 'Prières en combat', articleId: ''},
                        {additionalData:true, skillType:'combat' }
                    ],
                    arcane: [
                        {lang: 'fr', label: 'Cercles d\'incantation', articleId: ''},
                        {additionalData:true, skillType:'combat' }
                    ],
                    songs: [
                        {lang: 'fr', label: 'Mélodie de bataille', articleId: ''},
                        {additionalData:true, skillType:'combat' }
                    ],
                    stealth: [
                        {lang: 'fr', label: 'Furtivité', articleId: ''},
                        {additionalData:true, skillType:'explore' }
                    ],
                    contracts: [
                        {lang: 'fr', label: 'Créer des contrats', articleId: ''},
                        {additionalData:true, skillType:'intrigue' }
                    ],
                    poisons: [
                        {lang: 'fr', label: 'Connaissance des poisons', articleId: ''},
                        {additionalData:true, skillType:'intrigue' }
                    ],
                    weapon_handling: [
                        {lang: 'fr', label: 'Maintenance de l\'équipement', articleId: ''},
                        {additionalData:true, skillType:'explore' }
                    ],
                    bartering: [
                        {lang: 'fr', label: 'Marchandage', articleId: ''},
                        {additionalData:true, skillType:'intrigue' }
                    ],
                    weapon_handling: [
                        {lang: 'fr', label: 'Maintenance de l\'équipement', articleId: ''},
                        {additionalData:true, skillType:'explore' }
                    ],
                    animal_knowledge: [
                        {lang: 'fr', label: 'Connaissance des animaux', articleId: ''},
                        {additionalData:true, skillType:'explore' }
                    ],
                    on_the_road: [
                        {lang: 'fr', label: 'Sur les routes', articleId: ''},
                        {additionalData:true, skillType:'explore' }
                    ],
                    street_art: [
                        {lang: 'fr', label: 'Arts de rue', articleId: ''},
                        {additionalData:true, skillType:'intrigue' }
                    ],
                    empathy: [
                        {lang: 'fr', label: 'Empathie', articleId: ''},
                        {additionalData:true, skillType:'intrigue' }
                    ],
                    chatter: [
                        {lang: 'fr', label: 'Baratin', articleId: ''},
                        {additionalData:true, skillType:'intrigue' }
                    ],
                    plant_knowledge: [
                        {lang: 'fr', label: 'Connaissance des plantes', articleId: ''},
                        {additionalData:true, skillType:'explore' }
                    ],
                    fatigue_resistance: [
                        {lang: 'fr', label: 'Résistance à la fatigue', articleId: ''},
                        {additionalData:true, skillType:'explore' }
                    ],
                    law_knowledge: [
                        {lang: 'fr', label: 'Connaissance de la loi', articleId: ''},
                        {additionalData:true, skillType:'intrigue' }
                    ],
                    leadership: [
                        {lang: 'fr', label: 'Commandement', articleId: ''},
                        {additionalData:true, skillType:'intrigue' }
                    ],
                    find_hidden_stuff: [
                        {lang: 'fr', label: 'Trouve les objets dissimulés', articleId: ''},
                        {additionalData:true, skillType:'explore' }
                    ],
                    tracking: [
                        {lang: 'fr', label: 'Pistage', articleId: ''},
                        {additionalData:true, skillType:'explore' }
                    ],
                    magic_ritual: [
                        {lang: 'fr', label: 'Rituels magiques', articleId: ''},
                        {additionalData:true, skillType:'explore' }
                    ],
                    myth_knowledge: [
                        {lang: 'fr', label: 'Connaissance des mythes et légendes', articleId: ''},
                        {additionalData:true, skillType:'explore' }
                    ],
                    crypted_language: [
                        {lang: 'fr', label: 'Language codé', articleId: ''},
                        {additionalData:true, skillType:'intrigue' }
                    ],
                    survival: [
                        {lang: 'fr', label: 'Survivre n\'importe où', articleId: ''},
                        {additionalData:true, skillType:'explore' }
                    ],
                    well_informed: [
                        {lang: 'fr', label: 'Réseaux d\'informateurs', articleId: ''},
                        {additionalData:true, skillType:'intrigue' }
                    ],
                    theft: [
                        {lang: 'fr', label: 'Vols et sabotages', articleId: ''},
                        {additionalData:true, skillType:'intrigue' }
                    ],
                    spirit_knowledge: [
                        {lang: 'fr', label: 'Connaissance des esprits', articleId: ''},
                        {additionalData:true, skillType:'explore' }
                    ],
                    spread_belief: [
                        {lang: 'fr', label: 'Promulguer sa foi', articleId: ''},
                        {additionalData:true, skillType:'intrigue' }
                    ],
                    corruption_ward: [
                        {lang: 'fr', label: 'Rempart contre la corruption', articleId: ''},
                        {additionalData:true, skillType:'explore' }
                    ],
                    corruption_detector: [
                        {lang: 'fr', label: 'Détection de la corruption', articleId: ''},
                        {additionalData:true, skillType:'intrigue' }
                    ],
                    logistic: [
                        {lang: 'fr', label: 'Logistique', articleId: ''},
                        {additionalData:true, skillType:'intrigue' }
                    ],
                    speak_with_animals: [
                        {lang: 'fr', label: 'Language animal', articleId: ''},
                        {additionalData:true, skillType:'explore' }
                    ],
                    intimidate: [
                        {lang: 'fr', label: 'Initimidation', articleId: ''},
                        {additionalData:true, skillType:'intrigue' }
                    ],
                    drawPowerRitual: [
                        {lang: 'fr', label: 'Rituel de puissance', articleId: '99efcf22-12f7-4b9b-b722-2176d50bda0b', anchor: 'drawPowerRitual'},
                        {additionalData:true, skillType:'explore' },
                        {additionalData:true, available:false }
                    ],
                    powerTransfer: [
                        {lang: 'fr', label: 'Transfert de pouvoir', articleId: '99efcf22-12f7-4b9b-b722-2176d50bda0b', anchor: 'powerTransfer'}, 
                        {additionalData:true, skillType:'intrigue' },
                        {additionalData:true, available:false }
                    ],
                    spiritDialogue: [
                        {lang: 'fr', label: 'Dialogue intérieur', articleId: '99efcf22-12f7-4b9b-b722-2176d50bda0b', anchor: 'spiritDialogue'}, 
                        {additionalData:true, skillType:'intrigue' },
                        {additionalData:true, available:false }
                    ],
                    spiritDetection: [
                        {lang: 'fr', label: 'Détection des esprits', articleId: '99efcf22-12f7-4b9b-b722-2176d50bda0b', anchor: 'spiritDetection'},
                        {additionalData:true, skillType:'explore' },
                        {additionalData:true, available:false }
                    ],
                    spiritEvocation: [
                        {lang: 'fr', label: 'Evocation des esprits', articleId: '99efcf22-12f7-4b9b-b722-2176d50bda0b', anchor: 'spiritEvocation'},
                        {additionalData:true, skillType:'intrigue' },
                        {additionalData:true, available:false }
                    ],
                    spiritRealm: [
                        {lang: 'fr', label: 'Royaume des esprits', articleId: '99efcf22-12f7-4b9b-b722-2176d50bda0b', anchor: 'spiritRealm'},
                        {additionalData:true, skillType:'explore' },
                        {additionalData:true, available:false }
                    ]
                }}
            ]
        },
        jobs : {
            assassin: [
                {lang: 'fr', label: 'Assassin', articleId: '08138091-3806-49a6-888d-55e21bb432f8'},
                {additionalData:true, inCombat: 'ranged', whileExploring: 'stealth', duringIntrigue: 'contracts' },
                {additionalData:true, battle: { main: 1, secondary: 2 } },
                {additionalData:true, forMonsters:false },
                {additionalData:true, translated:true, 
                    activities: {
                        assassination_art : [
                            {lang: 'fr', label: 'L\'art de l\'assassinat', anchor: 'skill_1'},
                            {additionalData:true, category: 'physical'}
                        ],
                        contracts_methodology : [
                            {lang: 'fr', label: 'Elaboration de contrats', anchor: 'skill_2'},
                            {additionalData:true, category: 'intellectual'}
                        ],
                        hide_in_the_shadows : [
                            {lang: 'fr', label: 'Maitre des ombres', anchor: 'skill_3'},
                            {additionalData:true, category: 'behavior'}
                        ]
                    }
                }
            ],
            butcher: [
                {lang: 'fr', label: 'Artisan boucher', articleId: '0545a83e-a83d-4e02-8223-2249dbb26323'},
                {additionalData:true, inCombat: 'improvised', whileExploring: 'animal_knowledge', duringIntrigue: 'bartering' },
                {additionalData:true, battle: { main: 2, secondary: 5 } },
                {additionalData:true, forMonsters:false },
                {additionalData:true, translated:true, 
                    activities: {
                        blade_sharpening : [
                            {lang: 'fr', label: 'Aiguise et manie différentes lames', anchor: 'skill_1'},
                            {additionalData:true, category: 'physical'}
                        ],
                        animal_knowledge : [
                            {lang: 'fr', label: 'Connaissance des animaux', anchor: 'skill_2'},
                            {additionalData:true, category: 'intellectual'}
                        ],
                        sell_products : [
                            {lang: 'fr', label: 'Vend ses produits au marché', anchor: 'skill_3'},
                            {additionalData:true, category: 'behavior'}
                        ]
                    }
                }
            ],
            entertainer: [
                {lang: 'fr', label: 'Saltimbanque', articleId: '8691fa5e-c5f7-4776-80d7-a00adebaff6f'},
                {additionalData:true, inCombat: 'songs', whileExploring: 'on_the_road', duringIntrigue: 'street_art' },
                {additionalData:true, battle: { main: 5, secondary: 1 } },
                {additionalData:true, forMonsters:false },
                {additionalData:true, translated:true, 
                    activities: {
                        street_art : [
                            {lang: 'fr', label: 'Art de rue', anchor: 'skill_1'},
                            {additionalData:true, category: 'physical'}
                        ],
                        keen_eye : [
                            {lang: 'fr', label: 'Rien ne lui échappe', anchor: 'skill_2'},
                            {additionalData:true, category: 'intellectual'}
                        ],
                        silver_tongue : [
                            {lang: 'fr', label: 'Langue d\'argent', anchor: 'skill_3'},
                            {additionalData:true, category: 'behavior'}
                        ]
                    }
                }
            ],
            farmer: [
                {lang: 'fr', label: 'Fermier', articleId: '12cf441d-6619-4f2f-88b8-02456fc95988'},
                {additionalData:true, inCombat: 'melee', whileExploring: 'fatigue_resistance', duringIntrigue: 'plant_knowledge' },
                {additionalData:true, battle: { main: 0, secondary: 2 } },
                {additionalData:true, forMonsters:false },
                {additionalData:true, translated:true, 
                    activities: {
                        field_works : [
                            {lang: 'fr', label: 'Travail dans les champs', anchor: 'skill_1'},
                            {additionalData:true, category: 'physical'}
                        ],
                        plant_knowledge : [
                            {lang: 'fr', label: 'Connaissance des plantes', anchor: 'skill_2'},
                            {additionalData:true, category: 'intellectual'}
                        ],
                        sell_products : [
                            {lang: 'fr', label: 'Vend ses produits', anchor: 'skill_3'},
                            {additionalData:true, category: 'behavior'}
                        ]
                    }
                }
            ],
            guard: [
                {lang: 'fr', label: 'Garde', articleId: 'c8c988f6-df17-4762-9f17-6e903e6521f5'},
                {additionalData:true, inCombat: 'melee', whileExploring: 'find_hidden_stuff', duringIntrigue: 'law_knowledge' },
                {additionalData:true, battle: { main: 0, secondary: 1 } },
                {additionalData:true, forMonsters:false },
                {additionalData:true, translated:true, 
                    activities: {
                        protect_citizens : [
                            {lang: 'fr', label: 'Protéger les citoyens', anchor: 'skill_1'},
                            {additionalData:true, category: 'physical'}
                        ],
                        inspect_people : [
                            {lang: 'fr', label: 'Inspecter les gens suspects', anchor: 'skill_2'},
                            {additionalData:true, category: 'intellectual'}
                        ],
                        enforce_law : [
                            {lang: 'fr', label: 'Faire respecter la loi', anchor: 'skill_3'},
                            {additionalData:true, category: 'behavior'}
                        ]
                    }
                }
            ],
            knight: [
                {lang: 'fr', label: 'Chevalier', articleId: '3f219ee3-2650-44e1-89b5-bf679822e10d'},
                {additionalData:true, inCombat: 'melee', whileExploring: 'tracking', duringIntrigue: 'leadership' },
                {additionalData:true, battle: { main: 0, secondary: 5 } },
                {additionalData:true, forMonsters:false },
                {additionalData:true, translated:true, 
                    activities: {
                        empire_shield : [
                            {lang: 'fr', label: 'Le bouclier de l\'empire', anchor: 'skill_1'},
                            {additionalData:true, category: 'physical'}
                        ],
                        nobility_armed_wing : [
                            {lang: 'fr', label: 'Le bras armé de la noblesse', anchor: 'skill_2'},
                            {additionalData:true, category: 'intellectual'}
                        ],
                        light_in_the_darness : [
                            {lang: 'fr', label: 'La lumière dans les ténèbres', anchor: 'skill_3'},
                            {additionalData:true, category: 'behavior'}
                        ]
                    }
                }
            ],
            magician: [
                {lang: 'fr', label: 'Magicien', articleId: '94797d3c-b76a-4abb-ba09-d5a7d5a3d380'},
                {additionalData:true, inCombat: 'arcane', whileExploring: 'magic_ritual', duringIntrigue: 'crypted_language' },
                {additionalData:true, battle: { main: 3, secondary: 2 } },
                {additionalData:true, forMonsters:false },
                {additionalData:true, translated:true, 
                    activities: {
                        attuned_to_magic : [
                            {lang: 'fr', label: 'Harmonisation magique', anchor: 'skill_1'},
                            {additionalData:true, category: 'physical'}
                        ],
                        myth_knowledge : [
                            {lang: 'fr', label: 'Connaissance des mythes et légendes', anchor: 'skill_2'},
                            {additionalData:true, category: 'intellectual'}
                        ],
                        language_of_the_ancient : [
                            {lang: 'fr', label: 'La langue des anciens', anchor: 'skill_3'},
                            {additionalData:true, category: 'behavior'}
                        ]
                    }
                }
            ],
            mercenary: [
                {lang: 'fr', label: 'Mercenaire', articleId: '8518ed5c-f4aa-4e94-becd-3d774f278ef6'},
                {additionalData:true, inCombat: 'melee', whileExploring: 'on_the_road', duringIntrigue: 'well_informed' },
                {additionalData:true, battle: { main: 0, secondary: 2 } },
                {additionalData:true, forMonsters:false },
                {additionalData:true, translated:true, 
                    activities: {
                        survives_everywhere : [
                            {lang: 'fr', label: 'Survivre n\'importe où', anchor: 'skill_1'},
                            {additionalData:true, category: 'physical'}
                        ],
                        always_well_informed : [
                            {lang: 'fr', label: 'Toujours bien informés', anchor: 'skill_2'},
                            {additionalData:true, category: 'intellectual'}
                        ],
                        play_with_the_law : [
                            {lang: 'fr', label: 'Aux frontières de la légalité', anchor: 'skill_3'},
                            {additionalData:true, category: 'behavior'}
                        ]
                    }
                }
            ],
            priest: [
                {lang: 'fr', label: 'Prêtre', articleId: '8279b9ba-952d-47d4-b89d-c7a096827ec3'},
                {additionalData:true, inCombat: 'prayer', whileExploring: 'spirit_knowledge', duringIntrigue: 'spread_belief' },
                {additionalData:true, battle: { main: 4, secondary: 5 } },
                {additionalData:true, forMonsters:false },
                {additionalData:true, translated:true, 
                    activities: {
                        shield_against_corruption : [
                            {lang: 'fr', label: 'Rempart contre la corruption', anchor: 'skill_1'},
                            {additionalData:true, category: 'physical'}
                        ],
                        speak_with_spirits : [
                            {lang: 'fr', label: 'Converser avec les esprits', anchor: 'skill_2'},
                            {additionalData:true, category: 'intellectual'}
                        ],
                        spread_belief : [
                            {lang: 'fr', label: 'Promulguer sa foi', anchor: 'skill_3'},
                            {additionalData:true, category: 'behavior'}
                        ]
                    }
                }
            ],
            sentinel: [
                {lang: 'fr', label: 'Sentinelle', articleId: 'c3013b9d-ba94-4cf2-ba2c-9ef8931f2499'},
                {additionalData:true, inCombat: 'ranged', whileExploring: 'fatigue_resistance', duringIntrigue: 'logistic' },
                {additionalData:true, battle: { main: 1, secondary: 0 } },
                {additionalData:true, forMonsters:false },
                {additionalData:true, translated:true, 
                    activities: {
                        protect_citizens : [
                            {lang: 'fr', label: 'Protéger les citoyens', anchor: 'skill_1'},
                            {lang: 'en', label: 'Protect citizens', anchor: 'skill_1'},
                            {additionalData:true, category: 'physical'}
                        ],
                        support_from_shadows : [
                            {lang: 'fr', label: 'Support depuis les ombres', anchor: 'skill_2'},
                            {lang: 'en', label: 'Support from shadows', anchor: 'skill_2'},
                            {additionalData:true, category: 'intellectual'}
                        ],
                        night_watch : [
                            {lang: 'fr', label: 'Un oiseau de nuit', anchor: 'skill_3'},
                            {lang: 'en', label: 'Night watch', anchor: 'skill_3'},
                            {additionalData:true, category: 'behavior'}
                        ]
                    }
                }
            ],
            templar: [
                {lang: 'fr', label: 'Templier', articleId: '9b6a934d-8ab1-48a9-9dd7-7f36c902bd12'},
                {additionalData:true, inCombat: 'melee', whileExploring: 'corruption_ward', duringIntrigue: 'empathy' },
                {additionalData:true, battle: { main: 0, secondary: 4 } },
                {additionalData:true, forMonsters:false },
                {additionalData:true, translated:true, 
                    activities: {
                        protect_innoncent : [
                            {lang: 'fr', label: 'Protéger la veuve et l\'orphelin', anchor: 'skill_1'},
                            {additionalData:true, category: 'physical'}
                        ],
                        purge_heretics : [
                            {lang: 'fr', label: 'Purger les hérétiques', anchor: 'skill_2'},
                            {additionalData:true, category: 'intellectual'}
                        ],
                        spread_belief : [
                            {lang: 'fr', label: 'Répandre la bonne parole', anchor: 'skill_3'},
                            {additionalData:true, category: 'behavior'}
                        ]
                    }
                }
            ],
            wildman: [
                {lang: 'fr', label: 'Sauvageon', articleId: 'a0ac1e09-c656-42e6-81b5-9e6928b4c683'},
                {additionalData:true, inCombat: 'ranged', whileExploring: 'survival', duringIntrigue: 'intimidate' },
                {additionalData:true, battle: { main: 1, secondary: 2 } },
                {additionalData:true, forMonsters:false },
                {additionalData:true, translated:true, 
                    activities: {
                        hunter_gatherer : [
                            {lang: 'fr', label: 'Chasseur cueilleur', anchor: 'skill_1'},
                            {additionalData:true, category: 'physical'}
                        ],
                        protects_his_territory : [
                            {lang: 'fr', label: 'Protéger son territoire', anchor: 'skill_2'},
                            {additionalData:true, category: 'intellectual'}
                        ],
                        animal_behavior : [
                            {lang: 'fr', label: 'Comportement animal', anchor: 'skill_3'},
                            {additionalData:true, category: 'behavior'}
                        ]
                    }
                }
            ],
            monster_predator: [
                {lang: 'fr', label: 'Prédateur', articleId: 'a6856f85-3d7a-4d33-9a3e-7aff9c6e15d7'},
                {additionalData:true, inCombat: '', whileExploring: 'survival', duringIntrigue: 'intimidate' },
                {additionalData:true, battle: { main: 0, secondary: 1 } },
                {additionalData:true, forMonsters:true },
                {additionalData:true, translated:true, 
                    activities: {
                        hunter_gatherer : [
                            {lang: 'fr', label: 'Chasser ses proies', anchor: 'skill_1'},
                            {additionalData:true, category: 'physical'}
                        ],
                        protects_his_territory : [
                            {lang: 'fr', label: 'Protéger son territoire', anchor: 'skill_2'},
                            {additionalData:true, category: 'intellectual'}
                        ],
                        animal_behavior : [
                            {lang: 'fr', label: 'Comportement animal', anchor: 'skill_3'},
                            {additionalData:true, category: 'behavior'}
                        ]
                    }
                }
            ]
        },
        spirit : {
            definition: [
                {lang: 'fr', label: 'L\'esprit gardien', articleId: '99efcf22-12f7-4b9b-b722-2176d50bda0b'},
                {additionalData:true, translated:true, benefits: {
                    attributes : [
                        {lang: 'fr', label: 'Transcendance', anchor: 'attributes'},
                        {additionalData:true, related: ['hunger', 'stigmata']},
                        {additionalData:true, unlockable: [
                            ['vigor', 'speed', 'versatility', 'focus', 'perspicacity', 'socialEase'],
                            [],
                            ['vigor', 'speed', 'versatility', 'focus', 'perspicacity', 'socialEase'],
                            [],
                            ['vigor', 'speed', 'versatility', 'focus', 'perspicacity', 'socialEase'],
                            []
                        ]}
                    ],
                    skills : [
                        {lang: 'fr', label: 'Mémoire ancestrale', anchor: 'skills'},
                        {additionalData:true, related: ['personality', 'hunger']},
                        {additionalData:true, unlockable: [
                            ['combat', 'explore', 'intrigue'],
                            [],
                            ['combat', 'explore', 'intrigue'],
                            [],
                            ['combat', 'explore', 'intrigue'],
                            []
                        ]}
                    ],
                    abilities : [
                        {lang: 'fr', label: 'Collaboration', anchor: 'abilities'},
                        {additionalData:true, related: ['hunger', 'personality']},
                        {additionalData:true, unlockable: [
                            ['proactiveSpiritLvl1', 'elementalInfusionLvl1', 'elementalAttackLvl1'],
                            ['proactiveSpiritLvl2', 'elementalInfusionLvl2', 'elementalAttackLvl2'],
                            ['proactiveSpiritLvl3', 'elementalInfusionLvl3', 'elementalAttackLvl3'],
                            [],
                            [],
                            []
                        ]}
                    ],
                    health : [
                        {lang: 'fr', label: 'Immortalité', anchor: 'health'},
                        {additionalData:true, related: ['stigmata', 'hunger']},
                        {additionalData:true, unlockable: [
                            ['health'],
                            ['health'],
                            ['health'],
                            ['health'],
                            ['health'],
                            ['health']
                        ]}
                    ],
                    attunement : [
                        {lang: 'fr', label: 'Lien élémentaire', anchor: 'attunement'},
                        {additionalData:true, related: ['stigmata', 'personality']},
                        {additionalData:true, unlockable: [
                            ['elementResist', 'arcaneAttunement', 'circleHarmonization', 'spiritProtection', 'drawPowerFromElement'],
                            ['elementResist', 'circleHarmonization'],
                            ['elementResist', 'circleHarmonization', 'spiritProtection', 'harmonization', 'drawPowerFromElement'],
                            ['elementResist'],
                            ['elementResist', 'spiritProtection', 'drawPowerFromElement'],
                            ['elementResist', 'harmonization']
                        ]}
                    ],
                    link : [
                        {lang: 'fr', label: 'Dualité', anchor: 'link'},
                        {additionalData:true, related: ['personality', 'stigmata']},
                        {additionalData:true, unlockable: [
                            ['drawPowerRitualLvl1', 'spiritDialogueLvl1', 'powerTransferLvl1', 'spiritDetectionLvl1', 'spiritEvocationLvl1', 'spiritRealmLvl1'],
                            ['drawPowerRitualLvl2', 'spiritDialogueLvl2', 'powerTransferLvl2', 'spiritDetectionLvl2', 'spiritEvocationLvl2', 'spiritRealmLvl2'],
                            ['drawPowerRitualLvl3', 'spiritDialogueLvl3', 'powerTransferLvl3', 'spiritDetectionLvl3', 'spiritEvocationLvl3', 'spiritRealmLvl3'],
                            [],
                            [],
                            []
                        ]}
                    ]
                }},
                {additionalData:true, translated:true, drawbacks: {
                    stigmata : [
                        {lang: 'fr', label: ' Stigmates', anchor: 'stigmata'},
                        {additionalData:true, unlockable: [
                            ['stigmataLvl1'],
                            ['stigmataLvl2'],
                            ['stigmataLvl3'],
                            ['stigmataLvl4'],
                            ['stigmataLvl5']
                        ]}
                    ],
                    personality : [
                        {lang: 'fr', label: 'Dissociation mentale', anchor: 'personality'},
                        {additionalData:true, unlockable: [
                            ['angry', 'restless', 'lazy', 'fearful'],
                            [],
                            ['angry', 'restless', 'lazy', 'fearful'],
                            [],
                            ['angry', 'restless', 'lazy', 'fearful']
                        ]}
                    ],
                    hunger : [
                        {lang: 'fr', label: 'Dépendance à l\'énergie', anchor: 'hunger'},
                        {additionalData:true, unlockable: [
                            ['hunger'],
                            ['hunger'],
                            ['hunger'],
                            ['hunger'],
                            ['hunger']
                        ]}
                    ]
                }},
                {additionalData:true, translated:true, allPerks: {
                    vigor: [
                        {lang: 'fr', label: 'Vigueur transcendée', anchor: 'attributes',
                        description:['Chaque prise de ce bénéfice augmente le niveau maximum de l\'attribut Vigueur.']}
                    ],
                    speed: [
                        {lang: 'fr', label: 'Vitesse transcendée', anchor: 'attributes',
                        description:['Chaque prise de ce bénéfice augmente le niveau maximum de l\'attribut Vitesse.']}
                    ],
                    versatility: [
                        {lang: 'fr', label: 'Versatilité transcendée', anchor: 'attributes',
                        description:['Chaque prise de ce bénéfice augmente le niveau maximum de l\'attribut Versatilité.']}
                    ],
                    focus: [
                        {lang: 'fr', label: 'Concentration transcendée', anchor: 'attributes',
                        description:['Chaque prise de ce bénéfice augmente le niveau maximum de l\'attribut Concentration.']}
                    ],
                    perspicacity: [
                        {lang: 'fr', label: 'Perspicacité transcendée', anchor: 'attributes',
                        description:['Chaque prise de ce bénéfice augmente le niveau maximum de l\'attribut Perspicacité.']}
                    ],
                    socialEase: [
                        {lang: 'fr', label: 'Aisance sociale transcendée', anchor: 'attributes',
                        description:['Chaque prise de ce bénéfice augmente le niveau maximum de l\'attribut Aisance sociale.']}
                    ],
                    combat: [
                        {lang: 'fr', label: 'Mémoire de combats', anchor: 'skills',
                        description:['Chaque prise de ce bénéfice augmente le niveau maximum de l\'axe de compétence Combat.']}
                    ],
                    explore: [
                        {lang: 'fr', label: 'Mémoire d\'explorations', anchor: 'skills',
                        description:['Chaque prise de ce bénéfice augmente le niveau maximum de l\'axe de compétence Exploration.']}
                    ],
                    intrigue: [
                        {lang: 'fr', label: 'Mémoire d\'intrigues', anchor: 'skills',
                        description:['Chaque prise de ce bénéfice augmente le niveau maximum de l\'axe de compétence Intrigue.']}
                    ],
                    health: [
                        {lang: 'fr', label: 'Lien de vie', anchor: 'health',
                        description:['Chaque prise de ce bénéfice augmente les points de vie de l\'hôte de 3.']}
                    ],
                    elementResist: [
                        {lang: 'fr', label: 'Résistance élémentaire', anchor: 'elementResist',
                        description:['Chaque prise de ce bénéfice augmente la réistance aux dégats de 1.', 'Concerne les dégats élémentaires d\'un type possédé par l\'esprit gardien.']}
                    ],
                    arcaneAttunement: [
                        {lang: 'fr', label: 'Harmonisations arcaniques', anchor: 'arcaneAttunement',
                        description:['Permet d\'emprunter les liens élémentaires de son esprit gardien lors de l\'harmonisation arcanique d\'un cercle d\'incantation.', 'Les sorts utilisants les éléments de l\'esprit ne subissent plus de réduction d\'effet.']}
                    ],
                    circleHarmonization: [
                        {lang: 'fr', label: 'Harmonisations multiples du cercle', anchor: 'circleHarmonization',
                        description:['Permet au magicien de combiner plusieurs énergies à l\'intérieur d\'un même cercle d\'incantation. L\'esprit gardien l\'aidant à maintenir la précédente harmonisation.']}
                    ],
                    spiritProtection: [
                        {lang: 'fr', label: 'Résistance au drain', anchor: 'spiritProtection',
                        description:['Chaque prise de ce bénéfice réduit de 1 les dégats du drain dès lors qu\'un esprit mineur ayant effectué le miracle possède un élément en commun avec l\'esprit gardien.']}
                    ],
                    drawPowerFromElement: [
                        {lang: 'fr', label: 'Regain de puissance', anchor: 'drawPowerFromElement',
                        description:['Permet de récupérer un point de pouvoir supplémentaire lorsqu\'il reçoit des blessures occasionés par un des éléments de l\'esprit.']}
                    ],
                    harmonization: [
                        {lang: 'fr', label: 'Harmonisations supplémentaires', anchor: 'harmonization',
                        description:['L\'hôte peut choisir un élémentaire supplémentaire auquel l\'esprit gardien sera lié.']}
                    ],
                    drawPowerRitualLvl1: [
                        {lang: 'fr', label: 'Rituel de puissance I', anchor: 'drawPowerRitual',
                        description:['Récupération du premier jeton pouvoir une fois par jour après un rituel de 10 minutes.']},
                        {additionalData:true, givesAbility: { skill: 'drawPowerRitual', type: 'explore'}}
                    ],
                    drawPowerRitualLvl2: [
                        {lang: 'fr', label: 'Rituel de puissance II', anchor: 'drawPowerRitual',
                        description:['Permet de monter à deux jetons de pouvoir à la fin du rituel.']},
                        {additionalData:true, needs: ['drawPowerRitualLvl1']}
                    ],
                    drawPowerRitualLvl3: [
                        {lang: 'fr', label: 'Rituel de puissance III', anchor: 'drawPowerRitual',
                        description:['Permet de monter à trois jetons de pouvoir à la fin du rituel.']},
                        {additionalData:true, needs: ['drawPowerRitualLvl2']}
                    ],
                    powerTransferLvl1: [
                        {lang: 'fr', label: 'Transfert de pouvoir I', anchor: 'powerTransfer',
                        description:['En touchant un allié, au prix d\'une action mentale simple, il vous est possible de transférer un point de pouvoir.']},
                        {additionalData:true, givesAbility: { skill: 'powerTransfer', type: 'intrigue'}}
                    ],
                    powerTransferLvl2: [
                        {lang: 'fr', label: 'Transfert de pouvoir II', anchor: 'powerTransfer',
                        description:['Il ne vous est plus nécessaire de toucher votre allié, l\'action devient une action mentale rapide.']},
                        {additionalData:true, needs: ['powerTransferLvl1']}
                    ],
                    powerTransferLvl3: [
                        {lang: 'fr', label: 'Transfert de pouvoir III', anchor: 'powerTransfer',
                        description:['Vous pouvez à présent transférer autant de points de pouvoir que vous désirez.','Avec l\'accord de l\'allié, vous pouvez aussi récupérer ses points de pouvoir.']},
                        {additionalData:true, needs: ['powerTransferLvl2']}
                    ],
                    spiritDialogueLvl1: [
                        {lang: 'fr', label: 'Dialogue intérieur I', anchor: 'spiritDialogue',
                        description:['En vous concentrant, vous êtes capable de déterminer l\'humeur actuelle de votre esprit gardien.','S\'il est agité, vous pouvez dépenser un point de pouvoir pour le calmer.']},
                        {additionalData:true, givesAbility: { skill: 'spiritDialogue', type: 'intrigue'}}
                    ],
                    spiritDialogueLvl2: [
                        {lang: 'fr', label: 'Dialogue intérieur II', anchor: 'spiritDialogue',
                        description:['Vous pouvez effectuer un véritable dialogue avec votre esprit gardien et ainsi comprendre ce qui le fait réagir.']},
                        {additionalData:true, needs: ['spiritDialogueLvl1']}
                    ],
                    spiritDialogueLvl3: [
                        {lang: 'fr', label: 'Dialogue intérieur III', anchor: 'spiritDialogue',
                        description:['Lorsque votre esprit s\'agite, vous pouvez en réaction tenter de lui intimer de se calmer.']},
                        {additionalData:true, needs: ['spiritDialogueLvl2']}
                    ],
                    spiritDetectionLvl1: [
                        {lang: 'fr', label: 'Détection des esprits I', anchor: 'spiritDetection',
                        description:['En vous concentrant, vous êtes capable de déterminer si des esprits gardiens inconnus sont dans les environs.']},
                        {additionalData:true, givesAbility: { skill: 'spiritDetection', type: 'explore'}}
                    ],
                    spiritDetectionLvl2: [
                        {lang: 'fr', label: 'Détection des esprits II', anchor: 'link',
                        description:['Vous n\'êtes plus limités aux esprits ayant choisi de se loger dans un être vivant.']},
                        {additionalData:true, needs: ['spiritDetectionLvl1']}
                    ],
                    spiritDetectionLvl3: [
                        {lang: 'fr', label: 'Détection des esprits III', anchor: 'link',
                        description:['Vous n\'avez plus besoin de vous concentrer pour détecter leur présence. La concentration reste nécessaire pour en savoir plus.']},
                        {additionalData:true, needs: ['spiritDetectionLvl2']}
                    ],
                    spiritEvocationLvl1: [
                        {lang: 'fr', label: 'Evocation des esprits I', anchor: 'spiritEvocation',
                        description:['En touchant quelqu\'un, vous êtes capables de faire apparaitre son esprit gardien ainsi que le votre sur le plan réel.','Les esprits peuvent se mettre à parler si l\'un des protagoniste à maitrisé le dialogue intérieur au niveau 2.']},
                        {additionalData:true, givesAbility: { skill: 'spiritEvocation', type: 'intrigue'}}
                    ],
                    spiritEvocationLvl2: [
                        {lang: 'fr', label: 'Evocation des esprits II', anchor: 'spiritEvocation',
                        description:['Vous n\'êtes plus limités obligés de toucher la personne. Elle doit néanmoins rester à portée de voix.']},
                        {additionalData:true, needs: ['spiritEvocationLvl1']}
                    ],
                    spiritEvocationLvl3: [
                        {lang: 'fr', label: 'Evocation des esprits III', anchor: 'spiritEvocation',
                        description:['Vous pouvez dissimuler votre propre esprit lorsqu\'une évocation d\'esprit est en cours.','Vous pouvez en effectuer plusieurs en parallèle.']},
                        {additionalData:true, needs: ['spiritEvocationLvl2']}
                    ],
                    spiritRealmLvl1: [
                        {lang: 'fr', label: 'Royaume des esprits I', anchor: 'spiritRealm',
                        description:['En effectuant un rituel de 10 minutes, vous êtes capables de vous projeter dans le monde des esprits.','Vous pouvez percevoir l\'état émotionel et les souvenirs forts de ses résidents.']},
                        {additionalData:true, givesAbility: { skill: 'spiritRealm', type: 'explore'}}
                    ],
                    spiritRealmLvl2: [
                        {lang: 'fr', label: 'Royaume des esprits II', anchor: 'spiritRealm',
                        description:['Vous pouvez à présent vous déplacer dans le monde des esprits.','Les notions de distances n\'étant pas les mêmes, vous vous déplacez quasi instantannément dans un rayon d\'un km autour de votre corps.']},
                        {additionalData:true, needs: ['spiritRealmLvl1']}
                    ],
                    spiritRealmLvl3: [
                        {lang: 'fr', label: 'Royaume des esprits III', anchor: 'spiritRealm',
                        description:['La notion de temps devient elle aussi floue. Pour un point de pouvoir, vous pouvez essayer de vous projeter dans le futur ou le passé.']},
                        {additionalData:true, needs: ['spiritRealmLvl2']}
                    ],
                    proactiveSpiritLvl1: [
                        {lang: 'fr', label: 'Esprit gardien proactif I', anchor: 'proactiveSpirit',
                        description:['Permet de diviser par deux les besoins pour certaines actions.', 'A ce niveau, il vous est impossible de cacher l\'énergie élémentaire utilisée pour améliorer l\'effet.']},
                        {additionalData:true, spiritManeuver: { maneuver: 'proactiveSpirit', types: ['explore', 'intrigue'] }}
                    ],
                    proactiveSpiritLvl2: [
                        {lang: 'fr', label: 'Esprit gardien proactif II', anchor: 'proactiveSpirit',
                        description:['Permet de diviser par cinq les besoins.','Il vous est possible de cacher l\'origine de l\'amélioration.']},
                        {additionalData:true, needs: ['proactiveSpiritLvl1']}
                    ],
                    proactiveSpiritLvl3: [
                        {lang: 'fr', label: 'Esprit gardien proactif III', anchor: 'proactiveSpirit',
                        description:['Les besoins peuvent être réduits par vingt.','Si vous vous limitez à une réduction par deux, vous récupérez votre jeton de pouvoir après l\'avoir dépensé.']},
                        {additionalData:true, needs: ['proactiveSpiritLvl2']}
                    ],
                    elementalInfusionLvl1: [
                        {lang: 'fr', label: 'Infusion élémentaire I', anchor: 'elementalInfusion',
                        description:['Permet d\'augmenter l\'effet de certaines actions, ou de produire des effets secondaires.','Il vous est impossible de cacher l\'énergie élémentaire utilisée pour améliorer l\'effet.','Utile hors combat']},
                        {additionalData:true, spiritManeuver: { maneuver: 'elementalInfusion', types: ['explore', 'intrigue'] }}
                    ],
                    elementalInfusionLvl2: [
                        {lang: 'fr', label: 'Infusion élémentaire II', anchor: 'elementalInfusion',
                        description:['L\'augmentation d\'effet a toujours lieu.','Vous pouvez choisir un effet secondaire si vous le désirez.','Il vous à présent est possible de cacher l\'origine de l\'amélioration']},
                        {additionalData:true, needs: ['elementalInfusionLvl1']}
                    ],
                    elementalInfusionLvl3: [
                        {lang: 'fr', label: 'Infusion élémentaire III', anchor: 'elementalInfusion',
                        description:['Vous pouvez choisir plusieurs effets secondaires.','Si vous voulez pas bénéficier de l\'augmentation d\'effet, le point de pouvoir nécessaire vous est remboursé en fin d\'action.']},
                        {additionalData:true, needs: ['elementalInfusionLvl2']}
                    ],
                    elementalAttackLvl1: [
                        {lang: 'fr', label: 'Attaque élémentaire I', anchor: 'elementalAttack',
                        description:['Lors d\'une attaque physique, les dégats infligés deviennent d\'un des éléments de l\'esprit gardien.']},
                        {additionalData:true, spiritManeuver: { maneuver: 'elementalAttack', types: ['combat'] }}
                    ],
                    elementalAttackLvl2: [
                        {lang: 'fr', label: 'Attaque élémentaire II', anchor: 'elementalAttack',
                        description:['Les dégats sont augmentés de 1.']},
                        {additionalData:true, needs: ['elementalAttackLvl1']}
                    ],
                    elementalAttackLvl3: [
                        {lang: 'fr', label: 'Attaque élémentaire III', anchor: 'elementalAttack',
                        description:['Les dégats sont augmentés de 2.']},
                        {additionalData:true, needs: ['elementalAttackLvl2']}
                    ],
                    stigmataLvl1: [
                        {lang: 'fr', label: 'Stigmates I', anchor: 'stigmata',
                        description:['Des mutations apparaissent sur votre corps.','Elles sont en adéquation avec les élements de votre esprit gardien.','A ce niveau, les mutations sont très facilement dissimulable sous des vêtements. Vous ne serez pas inquiétez sauf si l\'on vous voit nu(e).']}
                    ],
                    stigmataLvl2: [
                        {lang: 'fr', label: 'Stigmates II', anchor: 'stigmata',
                        description:['Lors d\'utilisation de points de pouvoir, vos mutations s\'agitent.','Un oeil averti pourra vous percer à jour. (Difficulté: Difficile)']},
                        {additionalData:true, needs: ['stigmataLvl1']}
                    ],
                    stigmataLvl3: [
                        {lang: 'fr', label: 'Stigmates III', anchor: 'stigmata',
                        description:['Les mutations deviennent franchement visible. Il vous devient impossible de totalement les cacher sous des vêtements.', 'Quelqu\'un vous fixant à des chances de vous percer à jour. (Difficulté: Difficile).','Si vous être en train d\'utiliser une capacité spéciale, la difficulté devient Normale.']},
                        {additionalData:true, needs: ['stigmataLvl2']}
                    ],
                    stigmataLvl4: [
                        {lang: 'fr', label: 'Stigmates IV', anchor: 'stigmata',
                        description:['L\'utilisation de capacités spéciales devient vraiment spectaculaire. Il devient impossible de vous confondre avec un humain normal.','L\'énergie dégagée a tendance à ameuter les personnes aux alentours.']},
                        {additionalData:true, needs: ['stigmataLvl3']}
                    ],
                    stigmataLvl5: [
                        {lang: 'fr', label: 'Stigmates V', anchor: 'stigmata',
                        description:['Il devient difficile de deviner que vous ayez un jour eu une apparence humaine.','L\'empire vous range à présent dans la catégorie Monstres.']},
                        {additionalData:true, needs: ['stigmataLvl4']}
                    ],
                    angry: [
                        {lang: 'fr', label: 'Esprit colérique', anchor: 'angry',
                        description:['Lorsque l\'hôte perd le contrôle, une explosion de rage émerge en lui qu\'il redirige vers la source de frustration la plus proche.']}
                    ],
                    restless: [
                        {lang: 'fr', label: 'Esprit impatient', anchor: 'restless',
                        description:['Lorsque l\'hôte perd le contrôle, il devient incapable de rester à attendre. Il lui faut agir dans la précipitation pour résoudre l\'impasse actuelle.']}
                    ],
                    lazy: [
                        {lang: 'fr', label: 'Esprit paresseux', anchor: 'lazy',
                        description:['Lorsque l\'hôte perd le contrôle, il devient incapable de prendre les devants. Toute action de sa part doit être déclenché par un stimuli externe.']}
                    ],
                    fearful: [
                        {lang: 'fr', label: 'Esprit craintif', anchor: 'fearful',
                        description:['Lorsque l\'hôte perd le contrôle, ses peurs sont décuplées et il devient incapable d\'agir rationnellement.']}
                    ],
                    hunger: [
                        {lang: 'fr', label: 'Faim', anchor: 'hunger',
                        description:['Chaque fois que cet inconvénient est pris, la voracité de l\'esprit gardien grandit.', 
                                     'Lorsque l\'esprit gardien est affamé, l\'hôte ressent un phénomène de manque, similaire à celui d\'un drogué n\'ayant pas pris sa dose.',
                                     'Si la jauge de faim atteint son niveau maximum (20), l\'hôte fait un bad trip. Il a alors toutes les peines du monde à effectuer les tâches les plus simples.']}
                    ],
                }},
                {additionalData:true, translated:true, spiritManeuvers: {
                    proactiveSpirit : [
                        {lang: 'fr', label: 'Esprit gardien proactif', anchor: 'proactiveSpirit'}
                    ],
                    elementalInfusion : [
                        {lang: 'fr', label: 'Infusion élémentaire', anchor: 'elementalInfusion'}
                    ],
                    elementalAttack : [
                        {lang: 'fr', label: 'Attaque élémentaire', anchor: 'elementalAttack'}
                    ],
                }}
            ],
            cards: [
                {lang: 'fr', label: 'Dérangements de l\'esprit gardien', articleId: 'c05a580c-dc34-45c6-be3d-f4159329a8f2'},
                {additionalData:true, translated:true, labels: {
                    apply_negative : [
                        {lang: 'fr', label: 'Appliquez cet effet si l\'une des deux jauges faim ou agitation dépasse la valeur de la carte' }
                    ],
                    apply_positive : [
                        {lang: 'fr', label: 'Appliquez cet effet si pendant la journée vous avez rassasié ou satisfait votre esprit et que les deux jauges sont à présent en dessous de la valeur de la carte' }
                    ]
                }},
                {additionalData:true, translated:true, effects: {
                    power_health : [
                        {lang: 'fr', label: 'Crampes'}
                    ],
                    power_missing : [
                        {lang: 'fr', label: 'Apathique'}
                    ],
                    event_confused : [
                        {lang: 'fr', label: 'Désorienté'}
                    ],
                    initiative_slow : [
                        {lang: 'fr', label: 'Au ralenti'}
                    ],
                    success_minimum : [
                        {lang: 'fr', label: 'Sans énergie'}
                    ],
                    skull_augmented : [
                        {lang: 'fr', label: 'Fébrile'}
                    ]
                }},
                {additionalData:true, translated:true, quantities: {
                    once : [
                        {lang: 'fr', label: 'S\'applique au maximum une fois par jet de dés' },
                        {additionalData:true, max: 1}
                    ],
                    all_dice : [
                        {lang: 'fr', label: 'S\'applique à chaque occurence sur les jets de dés' },
                        {additionalData:true, max: 6}
                    ]
                }},
                {additionalData:true, translated:true, status: {
                    positive : [
                        {lang: 'fr', label: 'Satisfait' }
                    ],
                    ignored : [
                        {lang: 'fr', label: 'Endormi' }
                    ],
                    negative : [
                        {lang: 'fr', label: 'Agité' }
                    ],
                }}
            ]
        },
        maneuvers : {
            definition: [
                {lang: 'fr', label: 'Les manœuvres', articleId: 'c18e7eb3-b905-4416-a0aa-a92230a9c3ad', anchor: 'maneuver'},
                {additionalData:true, translated:true, levels: {
                    unknown : [
                        {lang: 'fr', label: 'Non appris', anchor: 'maneuver_level'},
                        {additionalData:true, level: 0},
                        {additionalData:true, interaction: {advantageModifier: -1, masteryModifier: -1} }
                    ],
                    learned : [
                        {lang: 'fr', label: 'Novice', anchor: 'maneuver_level'},
                        {additionalData:true, level: 1},
                        {additionalData:true, interaction: {advantageModifier: -1, masteryModifier: -1} }
                    ],
                    advanced : [
                        {lang: 'fr', label: 'Rompu', anchor: 'maneuver_level'},
                        {additionalData:true, level: 2},
                        {additionalData:true, interaction: {advantageModifier: 0, masteryModifier: 0} }
                    ],
                    expert : [
                        {lang: 'fr', label: 'Expert', anchor: 'maneuver_level'},
                        {additionalData:true, level: 3},
                        {additionalData:true, interaction: {advantageModifier: 0, masteryModifier: 0} }
                    ],
                    master : [
                        {lang: 'fr', label: 'Maître', anchor: 'maneuver_level'},
                        {additionalData:true, level: 4},
                        {additionalData:true, interaction: {advantageModifier: 1, masteryModifier: 1} }
                    ],
                    great_master : [
                        {lang: 'fr', label: 'Grand maître', anchor: 'maneuver_level'},
                        {additionalData:true, level: 5},
                        {additionalData:true, interaction: {advantageModifier: 1, masteryModifier: 1} }
                    ],
                    divine : [
                        {lang: 'fr', label: 'Divin', anchor: 'maneuver_level'},
                        {additionalData:true, level: 6},
                        {additionalData:true, interaction: {advantageModifier: 2, masteryModifier: 2} }
                    ]
                }}
            ]
        },
        monsters : {
            augments: [
                {lang: 'fr', label: 'Améliorations spécifiques au monstres', articleId: ''}, //FIXME
                {additionalData:true, translated:true, menu: {
                    elementalResist: [
                        {lang: 'fr', label: 'Résistances élémentaires'},
                        {additionalData:true, displayOrder: 10}
                    ],
                    elementalAttunement: [
                        {lang: 'fr', label: 'Harmonisations élémentaires'},
                        {additionalData:true, displayOrder: 11}
                    ],
                    classic: [
                        {lang: 'fr', label: 'Améliorations classiques'},
                        {additionalData:true, displayOrder: 20}
                    ],
                    invocation: [
                        {lang: 'fr', label: 'Associés aux invocations'},
                        {additionalData:true, displayOrder: 30}
                    ],
                    other: [
                        {lang: 'fr', label: 'Autres améliorations'},
                        {additionalData:true, displayOrder: 99}
                    ],
                }},
                {additionalData:true, translated:true, list: {
                    healthAugment : [
                        {lang: 'fr', label: 'Vie améliorée', anchor: 'healthAugment'},
                        {additionalData:true, menu: 'classic'},
                        {additionalData:true, maxLevel: 10}
                    ],

                    // Elemental resist
                    primaryResist : [
                        {lang: 'fr', label: 'Résistance élémentaire: Lumière', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalResist'},
                        {additionalData:true, maxLevel: 5}
                    ],
                    fireResist : [
                        {lang: 'fr', label: 'Résistance élémentaire: Feu', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalResist'},
                        {additionalData:true, maxLevel: 5}
                    ],
                    electricityResist : [
                        {lang: 'fr', label: 'Résistance élémentaire: Electricité', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalResist'},
                        {additionalData:true, maxLevel: 5}
                    ],
                    windResist : [
                        {lang: 'fr', label: 'Résistance élémentaire: Vent', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalResist'},
                        {additionalData:true, maxLevel: 5}
                    ],
                    moonResist : [
                        {lang: 'fr', label: 'Résistance élémentaire: Lune', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalResist'},
                        {additionalData:true, maxLevel: 5}
                    ],
                    animalResist : [
                        {lang: 'fr', label: 'Résistance élémentaire: Animal', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalResist'},
                        {additionalData:true, maxLevel: 5}
                    ],
                    mindResist : [
                        {lang: 'fr', label: 'Résistance élémentaire: Esprit', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalResist'},
                        {additionalData:true, maxLevel: 5}
                    ],
                    shadowResist : [
                        {lang: 'fr', label: 'Résistance élémentaire: Ombre', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalResist'},
                        {additionalData:true, maxLevel: 5}
                    ],
                    iceResist : [
                        {lang: 'fr', label: 'Résistance élémentaire: Glace', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalResist'},
                        {additionalData:true, maxLevel: 5}
                    ],
                    waterResist : [
                        {lang: 'fr', label: 'Résistance élémentaire: Eau', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalResist'},
                        {additionalData:true, maxLevel: 5}
                    ],
                    plantResist : [
                        {lang: 'fr', label: 'Résistance élémentaire: Plante', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalResist'},
                        {additionalData:true, maxLevel: 5}
                    ],
                    earthResist : [
                        {lang: 'fr', label: 'Résistance élémentaire: Terre', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalResist'},
                        {additionalData:true, maxLevel: 5}
                    ],
                    metalResist : [
                        {lang: 'fr', label: 'Résistance élémentaire: Metal', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalResist'},
                        {additionalData:true, maxLevel: 5}
                    ],


                    // Elemental attunement
                    primaryAttunement : [
                        {lang: 'fr', label: 'Harmonisation élémentaire: Lumière', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalAttunement'},
                        {additionalData:true, maxLevel: 1}
                    ],
                    fireAttunement : [
                        {lang: 'fr', label: 'Harmonisation élémentaire: Feu', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalAttunement'},
                        {additionalData:true, maxLevel: 1}
                    ],
                    electricityAttunement : [
                        {lang: 'fr', label: 'Harmonisation élémentaire: Electricité', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalAttunement'},
                        {additionalData:true, maxLevel: 1}
                    ],
                    windAttunement : [
                        {lang: 'fr', label: 'Harmonisation élémentaire: Vent', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalAttunement'},
                        {additionalData:true, maxLevel: 1}
                    ],
                    moonAttunement : [
                        {lang: 'fr', label: 'Harmonisation élémentaire: Lune', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalAttunement'},
                        {additionalData:true, maxLevel: 1}
                    ],
                    animalAttunement : [
                        {lang: 'fr', label: 'Harmonisation élémentaire: Animal', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalAttunement'},
                        {additionalData:true, maxLevel: 1}
                    ],
                    mindAttunement : [
                        {lang: 'fr', label: 'Harmonisation élémentaire: Esprit', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalAttunement'},
                        {additionalData:true, maxLevel: 1}
                    ],
                    shadowAttunement : [
                        {lang: 'fr', label: 'Harmonisation élémentaire: Ombre', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalAttunement'},
                        {additionalData:true, maxLevel: 1}
                    ],
                    iceAttunement : [
                        {lang: 'fr', label: 'Harmonisation élémentaire: Glace', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalAttunement'},
                        {additionalData:true, maxLevel: 1}
                    ],
                    waterAttunement : [
                        {lang: 'fr', label: 'Harmonisation élémentaire: Eau', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalAttunement'},
                        {additionalData:true, maxLevel: 1}
                    ],
                    plantAttunement : [
                        {lang: 'fr', label: 'Harmonisation élémentaire: Plante', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalAttunement'},
                        {additionalData:true, maxLevel: 1}
                    ],
                    earthAttunement : [
                        {lang: 'fr', label: 'Harmonisation élémentaire: Terre', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalAttunement'},
                        {additionalData:true, maxLevel: 1}
                    ],
                    metalAttunement : [
                        {lang: 'fr', label: 'Harmonisation élémentaire: Metal', anchor: 'elementalResist'},
                        {additionalData:true, menu: 'elementalAttunement'},
                        {additionalData:true, maxLevel: 1}
                    ],

                    // Invoc augment
                    invocationBeing : [
                        {lang: 'fr', label: 'Invocation élémentaire', anchor: 'invocationBeing'},
                        {additionalData:true, menu: 'invocation'},
                        {additionalData:true, maxLevel: 7}
                    ]
                }}
            ]
        }
    },
    item: {
        weapon: {
            definition: [
                {lang: 'fr', label: 'Les armes', articleId: '1b1b344c-83e7-42fa-a085-127f5069a274'},
                {additionalData:true, translated:true, sizes: {
                    none : [
                        {lang: 'fr', label: 'Sans arme', anchor: 'no_weapon'},
                        {lang: 'en', label: 'Without weapon', anchor: 'no_weapon'},
                        {additionalData:true, baseSpeed: 8},
                        {additionalData:true, baseDamage: 3},
                        {additionalData:true, basePrice: 0}
                    ],
                    small_weapon : [
                        {lang: 'fr', label: 'Arme de petite taille', anchor: 'small_size'},
                        {lang: 'en', label: 'Small weapon', anchor: 'small_size'},
                        {additionalData:true, baseSpeed: 10},
                        {additionalData:true, baseDamage: 4},
                        {additionalData:true, basePrice: 100}
                    ],
                    medium_weapon : [
                        {lang: 'fr', label: 'Arme de taille moyenne', anchor: 'medium_size'},
                        {lang: 'en', label: 'Medium weapon', anchor: 'medium_size'},
                        {additionalData:true, baseSpeed: 12},
                        {additionalData:true, baseDamage: 5},
                        {additionalData:true, basePrice: 200}
                    ],
                    large_weapon : [
                        {lang: 'fr', label: 'Arme de grande taille', anchor: 'large_size'},
                        {lang: 'en', label: 'Large weapon', anchor: 'large_size'},
                        {additionalData:true, baseSpeed: 15},
                        {additionalData:true, baseDamage: 6},
                        {additionalData:true, basePrice: 300}
                    ]
                }},
                {additionalData:true, translated:true, rarity: {
                    bad: [
                        {lang: 'fr', label: 'Mauvaise qualité'},
                        {additionalData:true, affixCost: 0},
                        {additionalData:true, additionalCost: -50}
                    ],
                    standard: [
                        {lang: 'fr', label: 'Qualité standard'},
                        {additionalData:true, affixCost: 1},
                        {additionalData:true, additionalCost: 0}
                    ],
                    fine: [
                        {lang: 'fr', label: 'Qualité supérieure'},
                        {additionalData:true, affixCost: 2},
                        {additionalData:true, additionalCost: 500}
                    ],
                    masterwork: [
                        {lang: 'fr', label: 'Chef d\'œuvre'},
                        {additionalData:true, affixCost: 3},
                        {additionalData:true, additionalCost: 1500}
                    ],
                    relic: [
                        {lang: 'fr', label: 'Relique'},
                        {additionalData:true, affixCost: 4},
                        {additionalData:true, additionalCost: 3000}
                    ]
                }}
            ],
            damages: [
                {lang: 'fr', label: 'Dégats des armes', articleId: '1b1b344c-83e7-42fa-a085-127f5069a274', anchor:'damage'}
            ],
            common: {
                barehanded : [
                    {lang: 'fr', label: 'A mains nues', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'mains_nues'},
                    {additionalData:true, size: 'none'},
                    {additionalData:true, category: 'improvised'},
                    {additionalData:true, element: 'animal'},
                    {additionalData:true, affixes: []}
                ],
                small_improvised : [
                    {lang: 'fr', label: 'Petite arme improvisée', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_vase'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'improvised'},
                    {additionalData:true, element: 'plant'},
                    {additionalData:true, affixes: []}
                ],
                medium_improvised : [
                    {lang: 'fr', label: 'Arme improvisée de taille raisonnable', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'stool'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'improvised'},
                    {additionalData:true, element: 'plant'},
                    {additionalData:true, affixes: []}
                ],
                large_improvised : [
                    {lang: 'fr', label: 'Grosse arme improvisée', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'big_box'},
                    {additionalData:true, size: 'large_weapon'},
                    {additionalData:true, category: 'improvised'},
                    {additionalData:true, element: 'plant'},
                    {additionalData:true, affixes: []}
                ],
                sai: [
                    {lang: 'fr', label:'Saï', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_melee_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['piercing', 'leftHand']}
                ],
                dagger: [
                    {lang: 'fr', label:'Dague', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_melee_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['assassin', 'leftHand']}
                ],
                hand_axe: [
                    {lang: 'fr', label:'Hachette', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_melee_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['powered', 'leftHand']}
                ],
                curved_dagger: [
                    {lang: 'fr', label:'Lame courbe', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_melee_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['weaponMaster', 'leftHand']}
                ],
                light_hammer: [
                    {lang: 'fr', label:'Marteau', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_melee_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'earth'},
                    {additionalData:true, affixes: ['brutal', 'leftHand']}
                ],
                light_mace: [
                    {lang: 'fr', label:'Massue', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_melee_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'earth'},
                    {additionalData:true, affixes: ['harasser', 'leftHand']}
                ],
                kriss: [
                    {lang: 'fr', label:'Kriss', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_melee_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['lucky', 'leftHand']}
                ],
                knuckles: [
                    {lang: 'fr', label:'Gant épineux', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_melee_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['ambidextrous', 'leftHand']}
                ],
                shortsword: [
                    {lang: 'fr', label:'Epée courte', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_melee_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['sharpened', 'leftHand']}
                ],
                sickle: [
                    {lang: 'fr', label:'Faucille', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_melee_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['thirsty', 'leftHand']}
                ],
                tonfa: [
                    {lang: 'fr', label:'Tonfa', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_melee_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'plant'},
                    {additionalData:true, affixes: ['barbarian', 'leftHand']}
                ],
                club: [
                    {lang: 'fr', label:'Gourdin', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_melee_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'plant'},
                    {additionalData:true, affixes: ['reliable', 'leftHand']}
                ],
                kama: [
                    {lang: 'fr', label:'Kama', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_melee_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['strategist', 'leftHand']}
                ],
                knife: [
                    {lang: 'fr', label:'Couteau', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_melee_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['light', 'leftHand']}
                ],
                rapier: [
                    {lang: 'fr', label:'Rapière', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_melee_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['piercing']}
                ],
                estoc: [
                    {lang: 'fr', label:'Estoc', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_melee_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['assassin']}
                ],
                battle_axe: [
                    {lang: 'fr', label:'Hache de bataille', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_melee_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['powered']}
                ],
                serrated_blade: [
                    {lang: 'fr', label:'Lame dentelée', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_melee_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['weaponMaster']}
                ],
                warhammer: [
                    {lang: 'fr', label:'Marteau de guerre', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_melee_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['brutal']}
                ],
                flail: [
                    {lang: 'fr', label:'Fléau', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_melee_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['harasser']}
                ],
                scimitar: [
                    {lang: 'fr', label:'Scimitar', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_melee_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['lucky']}
                ],
                claws: [
                    {lang: 'fr', label:'Gantelet griffes', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_melee_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['ambidextrous']}
                ],
                longsword: [
                    {lang: 'fr', label:'Epée longue', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_melee_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['sharpened']}
                ],
                saber: [
                    {lang: 'fr', label:'Sabre', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_melee_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['thirsty']}
                ],
                spear: [
                    {lang: 'fr', label:'Lance', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_melee_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['reach']}
                ],
                staff: [
                    {lang: 'fr', label:'Baton', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_melee_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'plant'},
                    {additionalData:true, affixes: ['barbarian']}
                ],
                nunchaku: [
                    {lang: 'fr', label:'Nunchaku', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_melee_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'plant'},
                    {additionalData:true, affixes: ['reliable']}
                ],
                whip: [
                    {lang: 'fr', label:'Fouet', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_melee_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'animal'},
                    {additionalData:true, affixes: ['strategist']}
                ],
                pickaxe: [
                    {lang: 'fr', label:'Pioche', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_melee_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['light']}
                ],
                trident: [
                    {lang: 'fr', label:'Trident', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'large_melee_weapon'},
                    {additionalData:true, size: 'large_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['piercing']}
                ],
                great_axe: [
                    {lang: 'fr', label:'Hache à deux mains', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'large_melee_weapon'},
                    {additionalData:true, size: 'large_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['powered']}
                ],
                halberd: [
                    {lang: 'fr', label:'Hallebarde', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'large_melee_weapon'},
                    {additionalData:true, size: 'large_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['weaponMaster']}
                ],
                massive_hammer: [
                    {lang: 'fr', label:'Marteau à deux mains', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'large_melee_weapon'},
                    {additionalData:true, size: 'large_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'earth'},
                    {additionalData:true, affixes: ['brutal']}
                ],
                heavy_mace: [
                    {lang: 'fr', label:'Masse à deux mains', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'large_melee_weapon'},
                    {additionalData:true, size: 'large_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'earth'},
                    {additionalData:true, affixes: ['harasser']}
                ],
                flamberge: [
                    {lang: 'fr', label:'Flamberge', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'large_melee_weapon'},
                    {additionalData:true, size: 'large_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['lucky']}
                ],
                greatsword: [
                    {lang: 'fr', label:'Epée à deux mains', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'large_melee_weapon'},
                    {additionalData:true, size: 'large_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['sharpened']}
                ],
                scythe: [
                    {lang: 'fr', label:'Faux', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'large_melee_weapon'},
                    {additionalData:true, size: 'large_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['thirsty']}
                ],
                glaive: [
                    {lang: 'fr', label:'Vouge', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'large_melee_weapon'},
                    {additionalData:true, size: 'large_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['reach']}
                ],
                fauchard: [
                    {lang: 'fr', label:'Fauchard', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'large_melee_weapon'},
                    {additionalData:true, size: 'large_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['barbarian']}
                ],
                great_club: [
                    {lang: 'fr', label:'Merlin', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'large_melee_weapon'},
                    {additionalData:true, size: 'large_weapon'},
                    {additionalData:true, category: 'melee'},
                    {additionalData:true, element: 'plant'},
                    {additionalData:true, affixes: ['reliable']}
                ],
                hand_crossbow: [
                    {lang: 'fr', label:'Arbalète de poing', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_ranged_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'plant'},
                    {additionalData:true, affixes: ['assassin']}
                ],
                throwing_dagger: [
                    {lang: 'fr', label:'Dagues de lancer', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_ranged_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['skirmisher']}
                ],
                bola: [
                    {lang: 'fr', label:'Bola', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_ranged_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'plant'},
                    {additionalData:true, affixes: ['hunter']}
                ],
                shuriken: [
                    {lang: 'fr', label:'Shuriken', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_ranged_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['sharpened']}
                ],
                blowgun: [
                    {lang: 'fr', label:'Sarbacane', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_ranged_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'plant'},
                    {additionalData:true, affixes: ['thirsty']}
                ],
                light_grenade: [
                    {lang: 'fr', label:'Petite grenade', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_ranged_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'animal'},
                    {additionalData:true, affixes: ['reliable']}
                ],
                sling: [
                    {lang: 'fr', label:'Fronde', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_ranged_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'earth'},
                    {additionalData:true, affixes: ['trickster']}
                ],
                dart: [
                    {lang: 'fr', label:'Fléchettes', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'small_ranged_weapon'},
                    {additionalData:true, size: 'small_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'animal'},
                    {additionalData:true, affixes: ['light']}
                ],
                light_crossbow: [
                    {lang: 'fr', label:'Arbalète légère', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_ranged_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'plant'},
                    {additionalData:true, affixes: ['piercing']}
                ],
                throwing_axe: [
                    {lang: 'fr', label:'Hache de lancer', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_ranged_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['disruptor']}
                ],
                boomerang: [
                    {lang: 'fr', label:'Boomerang', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_ranged_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'plant'},
                    {additionalData:true, affixes: ['hunter']}
                ],
                javelin: [
                    {lang: 'fr', label:'Javeline', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_ranged_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'plant'},
                    {additionalData:true, affixes: ['predator']}
                ],
                chakram: [
                    {lang: 'fr', label:'Chakram', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_ranged_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['lucky']}
                ],
                shortbow: [
                    {lang: 'fr', label:'Arc court', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_ranged_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'plant'},
                    {additionalData:true, affixes: ['sniper']}
                ],
                starknife: [
                    {lang: 'fr', label:'Lamétoile', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_ranged_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['sharpened']}
                ],
                grenade: [
                    {lang: 'fr', label:'Grenade artisanale', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'medium_ranged_weapon'},
                    {additionalData:true, size: 'medium_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'animal'},
                    {additionalData:true, affixes: ['reliable']}
                ],
                heavy_crossbow: [
                    {lang: 'fr', label:'Arbalète lourde', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'large_ranged_weapon'},
                    {additionalData:true, size: 'large_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'plant'},
                    {additionalData:true, affixes: ['piercing']}
                ],
                hurlbat: [
                    {lang: 'fr', label:'Hurlbat', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'large_ranged_weapon'},
                    {additionalData:true, size: 'large_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['disruptor']}
                ],
                pilum: [
                    {lang: 'fr', label:'Pilum', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'large_ranged_weapon'},
                    {additionalData:true, size: 'large_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'metal'},
                    {additionalData:true, affixes: ['predator']}
                ],
                longbow: [
                    {lang: 'fr', label:'Arc long', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'large_ranged_weapon'},
                    {additionalData:true, size: 'large_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'plant'},
                    {additionalData:true, affixes: ['sniper']}
                ],
                slingstaff: [
                    {lang: 'fr', label:'Baton fronde', articleId: '7a018196-6050-4079-ad75-6aeb5afedce0', anchor:'large_ranged_weapon'},
                    {additionalData:true, size: 'large_weapon'},
                    {additionalData:true, category: 'ranged'},
                    {additionalData:true, element: 'earth'},
                    {additionalData:true, affixes: ['trickster']}
                ]
            }
        },
        armor: {
            definition: [
                {lang: 'fr', label: 'Les armures', articleId: 'bcb3c253-b47f-49f8-8fb8-34c2c7a3cbd7'},
                {additionalData:true, translated:true, protectionLevels: {
                    none : [
                        {lang: 'fr', label: 'Sans protection', anchor: 'protection_levels'},
                        {additionalData:true, value: 0},
                        {additionalData:true, basePrice: 60}
                    ],
                    light : [
                        {lang: 'fr', label: 'Protection légère', anchor: 'protection_levels'},
                        {additionalData:true, value: 1},
                        {additionalData:true, basePrice: 200}
                    ],
                    medium : [
                        {lang: 'fr', label: 'Protection moyenne', anchor: 'protection_levels'},
                        {additionalData:true, value: 2},
                        {additionalData:true, basePrice: 400}
                    ],
                    heavy : [
                        {lang: 'fr', label: 'Protection lourde', anchor: 'protection_levels'},
                        {additionalData:true, value: 3},
                        {additionalData:true, basePrice: 600}
                    ],
                    full : [
                        {lang: 'fr', label: 'Protection totale', anchor: 'protection_levels'},
                        {additionalData:true, value: 4},
                        {additionalData:true, basePrice: 800}
                    ]
                }},
                {additionalData:true, translated:true, slots: {
                    head : [
                        {lang: 'fr', label: 'Tête', anchor: 'tete'},
                        {lang: 'en', label: 'Head', anchor: 'tete'},
                        {additionalData:true, sortValue: '010'}
                    ],
                    back : [
                        {lang: 'fr', label: 'Dos', anchor: 'dos'},
                        {lang: 'en', label: 'Back', anchor: 'dos'},
                        {additionalData:true, sortValue: '020'},
                        {additionalData:true, allowProtection: {
                            limiter: 'versatility',
                            against: ['water','ice','fire','wind','electricity']
                        }}
                    ],
                    left_hand : [
                        {lang: 'fr', label: 'Deuxième main', anchor: 'main_gauche'},
                        {lang: 'en', label: 'Left hand', anchor: 'main_gauche'},
                        {additionalData:true, sortValue: '040'}
                    ],
                    chest : [
                        {lang: 'fr', label: 'Torse', anchor: 'torse'},
                        {lang: 'en', label: 'Chest', anchor: 'torse'},
                        {additionalData:true, sortValue: '050'},
                        {additionalData:true, allowProtection: {
                            limiter: 'vigor',
                            against: ['metal', 'animal', 'earth','plant']
                        }}
                    ],
                    hands : [
                        {lang: 'fr', label: 'Mains', anchor: 'mains'},
                        {lang: 'en', label: 'Hands', anchor: 'mains'},
                        {additionalData:true, sortValue: '060'}
                    ],
                    neck : [
                        {lang: 'fr', label: 'Parure', anchor: 'cou'},
                        {lang: 'en', label: 'Set', anchor: 'cou'},
                        {additionalData:true, sortValue: '070'},
                        {additionalData:true, allowProtection: {
                            limiter: 'socialEase',
                            against: ['primary', 'shadow', 'mind', 'moon']
                        }}
                    ],
                    feet : [
                        {lang: 'fr', label: 'Pieds', anchor: 'pieds'},
                        {lang: 'en', label: 'Feet', anchor: 'pieds'},
                        {additionalData:true, sortValue: '080'}
                    ],
                    fingers : [
                        {lang: 'fr', label: 'Doigts', anchor: 'doigts'},
                        {lang: 'en', label: 'Fingers', anchor: 'doigts'},
                        {additionalData:true, sortValue: '090'}
                    ],
                    unset : [
                        {lang: 'fr', label: 'Non défini', anchor: 'slots'},
                        {lang: 'en', label: 'Unset', anchor: 'slots'},
                        {additionalData:true, sortValue: '200'}
                    ]
                }},
                {additionalData:true, translated:true, rarity: {
                    bad: [
                        {lang: 'fr', label: 'Ordinaire'},
                        {additionalData:true, affixCost: 0},
                        {additionalData:true, additionalCost: -50},
                    ],
                    standard: [
                        {lang: 'fr', label: 'Bonne qualité'},
                        {additionalData:true, affixCost: 1},
                        {additionalData:true, additionalCost: 0},
                    ],
                    fine: [
                        {lang: 'fr', label: 'Qualité supérieure'},
                        {additionalData:true, affixCost: 2},
                        {additionalData:true, additionalCost: 500},
                    ],
                    masterwork: [
                        {lang: 'fr', label: 'Chef d\'œuvre'},
                        {additionalData:true, affixCost: 3},
                        {additionalData:true, additionalCost: 1500},
                    ],
                    relic: [
                        {lang: 'fr', label: 'Relique'},
                        {additionalData:true, affixCost: 4},
                        {additionalData:true, additionalCost: 3000},
                    ]
                }}
            ],
            common: {
                // Chest armors
                //--------------------
                chest: [
                    {lang: 'fr', label: 'Armures principales', articleId: ''},
                    {additionalData:true, translated:true, list: {
                        cloth : [
                            {lang: 'fr', label: 'Vêtements'},
                            {additionalData:true, base: { 
                                slot: 'chest',
                                protectionLevel: 'none'
                            }},
                            {additionalData:true, affixes: []}
                        ],
                        light_armor : [
                            {lang: 'fr', label: 'Armure légère'},
                            {additionalData:true, base: { 
                                slot: 'chest',
                                protectionLevel: 'light'
                            }},
                            {additionalData:true, affixes: []}
                        ],
                        medium_armor : [
                            {lang: 'fr', label: 'Armure intermédiaire'},
                            {additionalData:true, base: { 
                                slot: 'chest',
                                protectionLevel: 'medium'
                            }},
                            {additionalData:true, affixes: []}
                        ],
                        heavy_armor : [
                            {lang: 'fr', label: 'Armure lourde'},
                            {additionalData:true, base: { 
                                slot: 'chest',
                                protectionLevel: 'heavy'
                            }},
                            {additionalData:true, affixes: []}
                        ],
                        full_armor : [
                            {lang: 'fr', label: 'Armure mythique'},
                            {additionalData:true, base: { 
                                slot: 'chest',
                                protectionLevel: 'full'
                            }},
                            {additionalData:true, affixes: []}
                        ],
                    }}
                ],
                // Necklaces and sets
                //--------------------
                necklace: [
                    {lang: 'fr', label: 'Colliers et parures', articleId: ''},
                    {additionalData:true, translated:true, list: {
                        simple_necklace : [
                            {lang: 'fr', label: 'Collier simple'},
                            {additionalData:true, base: { 
                                slot: 'neck',
                                protectionLevel: 'none'
                            }},
                            {additionalData:true, affixes: []}
                        ],
                        dream_catcher: [
                            {lang: 'fr', label: 'Attrape rèves'},
                            {additionalData:true, base: { 
                                slot: 'neck',
                                protectionLevel: 'none'
                            }},
                            {additionalData:true, affixes: ['elementalArmor_mind']}
                        ],
                        big_necklace : [
                            {lang: 'fr', label: 'Collier imposant'},
                            {additionalData:true, base: { 
                                slot: 'neck',
                                protectionLevel: 'light'
                            }},
                            {additionalData:true, affixes: []}
                        ],
                        small_set : [
                            {lang: 'fr', label: 'Parure sommaire'},
                            {additionalData:true, base: { 
                                slot: 'neck',
                                protectionLevel: 'medium'
                            }},
                            {additionalData:true, affixes: []}
                        ],
                        complete_set : [
                            {lang: 'fr', label: 'Parure complète'},
                            {additionalData:true, base: { 
                                slot: 'neck',
                                protectionLevel: 'heavy'
                            }},
                            {additionalData:true, affixes: []}
                        ],
                        set_and_tatoos : [
                            {lang: 'fr', label: 'Parure et tatouages'},
                            {additionalData:true, base: { 
                                slot: 'neck',
                                protectionLevel: 'full'
                            }},
                            {additionalData:true, affixes: []}
                        ],
                    }}
                ],
                // Cape and shoulders
                //--------------------
                back: [
                    {lang: 'fr', label: 'Capes et épaulettes', articleId: ''},
                    {additionalData:true, translated:true, list: {
                        simple_shoulderpad : [
                            {lang: 'fr', label: 'Epaulettes basiques', articleId: ''},
                            {additionalData:true, base: { 
                                slot: 'back',
                                protectionLevel: 'none'
                            }},
                            {additionalData:true, affixes: []}
                        ],
                        armored_shoulderpad : [
                            {lang: 'fr', label: 'Epaulettes renforcées', articleId: ''},
                            {additionalData:true, base: { 
                                slot: 'back',
                                protectionLevel: 'light'
                            }},
                            {additionalData:true, affixes: []}
                        ],
                        shoulderpad_and_cape : [
                            {lang: 'fr', label: 'Cape et épaulettes', articleId: ''},
                            {additionalData:true, base: { 
                                slot: 'back',
                                protectionLevel: 'medium'
                            }},
                            {additionalData:true, affixes: []}
                        ],
                        full_cape : [
                            {lang: 'fr', label: 'Cape intégrale', articleId: ''},
                            {additionalData:true, base: { 
                                slot: 'back',
                                protectionLevel: 'heavy'
                            }},
                            {additionalData:true, affixes: []}
                        ],
                        prominent_shoulder_pad : [
                            {lang: 'fr', label: 'Imposantes spalières', articleId: ''},
                            {additionalData:true, base: { 
                                slot: 'back',
                                protectionLevel: 'full'
                            }},
                            {additionalData:true, affixes: []}
                        ],
                    }}
                ],
                // Hand armors
                //--------------------
                hands: [
                    {lang: 'fr', label: 'Gantelets et brassards', articleId: ''},
                    {additionalData:true, translated:true, list: {
                        armband : [
                            {lang: 'fr', label: 'Brassards', articleId: ''},
                            {additionalData:true, base: { 
                                slot: 'hands'
                            }},
                            {additionalData:true, affixes: []}
                        ],
                        gauntlet : [
                            {lang: 'fr', label: 'Gantelets', articleId: ''},
                            {additionalData:true, base: { 
                                slot: 'hands'
                            }},
                            {additionalData:true, affixes: []}
                        ],
                    }}
                ],
                // Head armors
                //--------------------
                head: [
                    {lang: 'fr', label: 'Chapeau et casques', articleId: ''},
                    {additionalData:true, translated:true, list: {
                        hat : [
                            {lang: 'fr', label: 'Chapeau', articleId: ''},
                            {additionalData:true, base: { 
                                slot: 'head'
                            }},
                            {additionalData:true, affixes: []}
                        ],
                        hood : [
                            {lang: 'fr', label: 'Capuche', articleId: ''},
                            {additionalData:true, base: { 
                                slot: 'head'
                            }},
                            {additionalData:true, affixes: []}
                        ],
                        crown : [
                            {lang: 'fr', label: 'Couronne', articleId: ''},
                            {additionalData:true, base: { 
                                slot: 'head'
                            }},
                            {additionalData:true, affixes: ['elementalResist_plant']}
                        ],
                        helm : [
                            {lang: 'fr', label: 'Casque', articleId: ''},
                            {additionalData:true, base: { 
                                slot: 'head'
                            }},
                            {additionalData:true, affixes: ['head_critical_1']}
                        ],
                        full_helm : [
                            {lang: 'fr', label: 'Casque intégral', articleId: ''},
                            {additionalData:true, base: { 
                                slot: 'head'
                            }},
                            {additionalData:true, affixes: ['head_critical_2']}
                        ],
                    }}
                ],
                // Left hand
                //--------------------
                left_hand: [
                    {lang: 'fr', label: 'En main secondaire', articleId: ''},
                    {additionalData:true, translated:true, list: {
                        small_shield : [
                            {lang: 'fr', label: 'Targe', articleId: ''},
                            {additionalData:true, base: { 
                                slot: 'left_hand'
                            }},
                            {additionalData:true, affixes: ['shield_level_1']}
                        ],
                        medium_shield : [
                            {lang: 'fr', label: 'Bouclier d\'infanterie', articleId: ''},
                            {additionalData:true, base: { 
                                slot: 'left_hand'
                            }},
                            {additionalData:true, affixes: ['shield_level_2']}
                        ],
                        large_shield : [
                            {lang: 'fr', label: 'Pavois', articleId: ''},
                            {additionalData:true, base: { 
                                slot: 'left_hand'
                            }},
                            {additionalData:true, affixes: ['shield_level_3']}
                        ],
                        magician_orb : [
                            {lang: 'fr', label: 'Orbe de magicien', articleId: ''},
                            {additionalData:true, base: { 
                                slot: 'left_hand'
                            }},
                            {additionalData:true, affixes: ['orb_arcane_1']}
                        ],
                        divine_symbol : [
                            {lang: 'fr', label: 'Symbole divin', articleId: ''},
                            {additionalData:true, base: { 
                                slot: 'left_hand'
                            }},
                            {additionalData:true, affixes: ['divine_symbol_1']}
                        ],
                    }}
                ]

            }
        },
        misc: {
            definition: [
                {lang: 'fr', label: 'Les armures', articleId: 'bcb3c253-b47f-49f8-8fb8-34c2c7a3cbd7'},
                {additionalData:true, translated:true, slots: {
                    useable : [
                        {lang: 'fr', label: 'Consommables', anchor: 'useable'},
                        {lang: 'en', label: 'Useables', anchor: 'useable'},
                        {additionalData:true, sortValue: '100'}
                    ],
                    bagOnly : [
                        {lang: 'fr', label: 'Dans le sac', anchor: 'sac'},
                        {lang: 'en', label: 'In the bag', anchor: 'sac'},
                        {additionalData:true, sortValue: '110'}
                    ],
                    unset : [
                        {lang: 'fr', label: 'Non défini', anchor: 'slots'},
                        {lang: 'en', label: 'Unset', anchor: 'slots'},
                        {additionalData:true, sortValue: '200'}
                    ]
                }},
                {additionalData:true, translated:true, rarity: {
                    bad: [
                        {lang: 'fr', label: 'Ordinaire'},
                        {additionalData:true, affixCost: 0},
                        {additionalData:true, additionalCost: 0},
                    ],
                    standard: [
                        {lang: 'fr', label: 'Bonne qualité'},
                        {additionalData:true, affixCost: 1},
                        {additionalData:true, additionalCost: 100},
                    ],
                    fine: [
                        {lang: 'fr', label: 'Qualité supérieure'},
                        {additionalData:true, affixCost: 2},
                        {additionalData:true, additionalCost: 300},
                    ],
                    masterwork: [
                        {lang: 'fr', label: 'Chef d\'œuvre'},
                        {additionalData:true, affixCost: 3},
                        {additionalData:true, additionalCost: 600},
                    ],
                    relic: [
                        {lang: 'fr', label: 'Relique'},
                        {additionalData:true, affixCost: 4},
                        {additionalData:true, additionalCost: 1000},
                    ]
                }}
            ],
            common: {
                potion : [
                    {lang: 'fr', label: 'Potion', anchor: 'potion'},
                    {lang: 'en', label: 'Potion', anchor: 'potion'},
                    {additionalData:true, base: { 
                        slot: 'useable',
                        basePrice: 10
                    }},
                    {additionalData:true, affixes: []}
                ],
                precious : [
                    {lang: 'fr', label: 'Objet précieux', anchor: 'precieux'},
                    {lang: 'en', label: 'Precious item', anchor: 'precieux'},
                    {additionalData:true, base: { 
                        slot: 'bagOnly',
                        basePrice: 100
                    }},
                    {additionalData:true, affixes: []}
                ]
            }
        }
    },
    affixes: {
        definition: [
            {lang: 'fr', label: 'Les affixes', articleId: 'b92c451c-3ba6-49cf-b7ec-56381012957c'},
            {additionalData:true, translated:true, menu: {
                damageModificator: [
                    {lang: 'fr', label: 'Modificateur de dégâts'},
                    {additionalData:true, displayOrder: 10}
                ],
                elementModificator: [
                    {lang: 'fr', label: 'Modificateur du type de dégâts'},
                    {additionalData:true, displayOrder: 11}
                ],
                battleEnhancement: [
                    {lang: 'fr', label: 'Capacités de combat'},
                    {additionalData:true, displayOrder: 20}
                ],
                battleEnhancement_melee: [
                    {lang: 'fr', label: 'Capacités de combat (mêlée)'},
                    {additionalData:true, displayOrder: 21}
                ],
                battleEnhancement_ranged: [
                    {lang: 'fr', label: 'Capacités de combat (distance)'},
                    {additionalData:true, displayOrder: 22}
                ],
                battleEnhancement_improvised: [
                    {lang: 'fr', label: 'Capacités de combat (outils)'},
                    {additionalData:true, displayOrder: 23}
                ],
                elementalArmor: [
                    {lang: 'fr', label: 'Armures élémentaires'},
                    {additionalData:true, displayOrder: 30}
                ],
                elementalResist: [
                    {lang: 'fr', label: 'Résistances élémentaires'},
                    {additionalData:true, displayOrder: 40}
                ],
                spellTrigger: [
                    {lang: 'fr', label: 'Déclencheur de sort'},
                    {additionalData:true, displayOrder: 80}
                ],
                other: [
                    {lang: 'fr', label: 'Autres affixes'},
                    {additionalData:true, displayOrder: 99}
                ],
                onlyForSpells: [
                    {lang: 'fr', label: 'Réservés à des effets magiques'},
                    {additionalData:true, displayOrder: -1}
                ],
            }}
        ],
        weapons: [
            {lang: 'fr', label: 'Affixes pour les armes', articleId: '1b1b344c-83e7-42fa-a085-127f5069a274', anchor:'weapon_affix'},
            {additionalData:true, translated:true, list: {
                sharpened: [
                    {lang: 'fr', label: 'affutée', anchor:'damage_affix'},
                    {additionalData:true, menu: 'damageModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        damageModifiers: [{ penetration:1 }]
                    }}
                ],
                assassin: [
                    {lang: 'fr', label: 'assassine', anchor:'damage_affix'},
                    {additionalData:true, menu: 'damageModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        damageModifiers: [{ flat:2, on: 'no_armor'}]
                    }}
                ],
                thirsty: [
                    {lang: 'fr', label: 'assoiffée', anchor:'damage_affix'},
                    {additionalData:true, menu: 'damageModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        onAttackSuccess: [{ powerGain:2, on: 'incredible' }]
                    }}
                ],
                lucky: [
                    {lang: 'fr', label: 'chanceuse', anchor:'damage_affix'},
                    {additionalData:true, menu: 'damageModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        damageModifiers: [{ multiplier: { beforeArmor: 1.5, afterArmor: 1}, on: 'incredible'}]
                    }}
                ],
                reliable: [
                    {lang: 'fr', label: 'fiable', anchor:'damage_affix'},
                    {additionalData:true, menu: 'damageModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        damageModifiers: [
                            { multiplier: { beforeArmor: 1, afterArmor: 2}, on: 'partial'},
                            { multiplier: { beforeArmor: 0.5, afterArmor: 1}, on: 'incredible'}
                        ]
                    }}
                ],
                piercing: [
                    {lang: 'fr', label: 'perforante', anchor:'damage_affix'},
                    {additionalData:true, menu: 'damageModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        damageModifiers: [{ penetration:2, on: 'big_armor'}]
                    }}
                ],
                vicious: [
                    {lang: 'fr', label: 'vicieuse', anchor:'damage_affix'},
                    {additionalData:true, menu: 'damageModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        damageModifiers: [{ flat:1 }],
                        onAttackSuccess: [{ selfDamage:1 }]
                    }}
                ],
        
                primaryImbue: [
                    {lang: 'fr', label: 'lumineuse', anchor:'damage_affix'},
                    {additionalData:true, menu: 'elementModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 2,
                        damageModifiers: [{ element:'primary' }]
                    }}
                ],
                fireImbue: [
                    {lang: 'fr', label: 'enfammée', anchor:'damage_affix'},
                    {additionalData:true, menu: 'elementModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 2,
                        damageModifiers: [{ element:'fire' }]
                    }}
                ],
                electricityImbue: [
                    {lang: 'fr', label: 'crépitante', anchor:'damage_affix'},
                    {additionalData:true, menu: 'elementModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 2,
                        damageModifiers: [{ element:'electricity' }]
                    }}
                ],
                windImbue: [
                    {lang: 'fr', label: 'des tempètes', anchor:'damage_affix'},
                    {additionalData:true, menu: 'elementModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 2,
                        damageModifiers: [{ element:'wind' }]
                    }}
                ],
                moonImbue: [
                    {lang: 'fr', label: 'crépusculaire', anchor:'damage_affix'},
                    {additionalData:true, menu: 'elementModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 2,
                        damageModifiers: [{ element:'moon' }]
                    }}
                ],
                animalImbue: [
                    {lang: 'fr', label: 'en os', anchor:'damage_affix'},
                    {additionalData:true, menu: 'elementModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 0,
                        damageModifiers: [{ element:'animal' }]
                    }}
                ],
                mindImbue: [
                    {lang: 'fr', label: 'immatérielle', anchor:'damage_affix'},
                    {additionalData:true, menu: 'elementModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 2,
                        damageModifiers: [{ element:'mind' }]
                    }}
                ],
                shadowImbue: [
                    {lang: 'fr', label: 'abyssale', anchor:'damage_affix'},
                    {additionalData:true, menu: 'elementModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 2,
                        damageModifiers: [{ element:'shadow' }]
                    }}
                ],
                iceImbue: [
                    {lang: 'fr', label: 'glaciale', anchor:'damage_affix'},
                    {additionalData:true, menu: 'elementModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 2,
                        damageModifiers: [{ element:'ice' }]
                    }}
                ],
                waterImbue: [
                    {lang: 'fr', label: 'aqueuse', anchor:'damage_affix'},
                    {additionalData:true, menu: 'elementModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 2,
                        damageModifiers: [{ element:'water' }]
                    }}
                ],
                plantImbue: [
                    {lang: 'fr', label: 'en bois', anchor:'damage_affix'},
                    {additionalData:true, menu: 'elementModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 0,
                        damageModifiers: [{ element:'plant' }]
                    }}
                ],
                earthImbue: [
                    {lang: 'fr', label: 'en pierre', anchor:'damage_affix'},
                    {additionalData:true, menu: 'elementModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 0,
                        damageModifiers: [{ element:'earth' }]
                    }}
                ],
                metalImbue: [
                    {lang: 'fr', label: 'en métal', anchor:'damage_affix'},
                    {additionalData:true, menu: 'elementModificator'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 0,
                        damageModifiers: [{ element:'metal' }]
                    }}
                ],
                
                ambidextrous: [
                    {lang: 'fr', label: 'de l\'ambidextre', articleId: '7f46febb-5e52-4b40-9606-5e8a0688b9e7', anchor:'second_weapon'},
                    {additionalData:true, menu: 'battleEnhancement_melee'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        categories: ['melee'],
                        maneuverModifiers: [{maneuver: 'second_weapon', category: 'melee', advantage: 1, mastery: 1}]
                    }}
                ],
                barbarian: [
                    {lang: 'fr', label: 'du barbare', articleId: '7f46febb-5e52-4b40-9606-5e8a0688b9e7', anchor:'swipe'},
                    {additionalData:true, menu: 'battleEnhancement_melee'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        categories: ['melee'],
                        maneuverModifiers: [{maneuver: 'swipe', category: 'melee', advantage: 1, mastery: 1}]
                    }}
                ],
                brutal: [
                    {lang: 'fr', label: 'de brute', articleId: '7f46febb-5e52-4b40-9606-5e8a0688b9e7', anchor:'bucker_smash'},
                    {additionalData:true, menu: 'battleEnhancement_melee'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        categories: ['melee'],
                        maneuverModifiers: [{maneuver: 'bucker_smash', category: 'melee', advantage: 1, mastery: 1}]
                    }}
                ],
                duelist: [
                    {lang: 'fr', label: 'du dueliste', articleId: '7f46febb-5e52-4b40-9606-5e8a0688b9e7', anchor:'finesse'},
                    {additionalData:true, menu: 'battleEnhancement_melee'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        categories: ['melee'],
                        maneuverModifiers: [{maneuver: 'finesse', category: 'melee', advantage: 1, mastery: 1}]
                    }}
                ],
                harasser: [
                    {lang: 'fr', label: 'de l\'harceleur', articleId: '7f46febb-5e52-4b40-9606-5e8a0688b9e7', anchor:'harassing'},
                    {additionalData:true, menu: 'battleEnhancement_melee'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        categories: ['melee'],
                        maneuverModifiers: [{maneuver: 'harassing', category: 'melee', advantage: 1, mastery: 1}]
                    }}
                ],
                mountainDweller: [
                    {lang: 'fr', label: 'du montagnard', articleId: '7f46febb-5e52-4b40-9606-5e8a0688b9e7', anchor:'overthrow'},
                    {additionalData:true, menu: 'battleEnhancement_melee'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        categories: ['melee'],
                        maneuverModifiers: [{maneuver: 'overthrow', category: 'melee', advantage: 1, mastery: 1}]
                    }}
                ],
                powered: [
                    {lang: 'fr', label: 'de puissance', articleId: '7f46febb-5e52-4b40-9606-5e8a0688b9e7', anchor:'powerAttack'},
                    {additionalData:true, menu: 'battleEnhancement_melee'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        categories: ['melee'],
                        maneuverModifiers: [{maneuver: 'powerAttack', category: 'melee', advantage: 1, mastery: 1}]
                    }}
                ],
                strategist: [
                    {lang: 'fr', label: 'du stratège', articleId: '7f46febb-5e52-4b40-9606-5e8a0688b9e7', anchor:'hit_and_run'},
                    {additionalData:true, menu: 'battleEnhancement_melee'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        categories: ['melee'],
                        maneuverModifiers: [{maneuver: 'hit_and_run', category: 'melee', advantage: 1, mastery: 1}]
                    }}
                ],
                weaponMaster: [
                    {lang: 'fr', label: 'du maitre d\'armes', articleId: '7f46febb-5e52-4b40-9606-5e8a0688b9e7', anchor:'disarm'},
                    {additionalData:true, menu: 'battleEnhancement_melee'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        categories: ['melee'],
                        maneuverModifiers: [{maneuver: 'disarm', category: 'melee', advantage: 1, mastery: 1}]
                    }}
                ],

                skirmisher: [
                    {lang: 'fr', label: 'du tirailleur', articleId: 'd96faae1-f392-48e3-a1b0-0714ee945714', anchor:'harassing'},
                    {additionalData:true, menu: 'battleEnhancement_ranged'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        categories: ['ranged'],
                        maneuverModifiers: [{maneuver: 'harassing', category: 'ranged', advantage: 1, mastery: 1}]
                    }}
                ],
                disruptor: [
                    {lang: 'fr', label: 'du perturbateur', articleId: 'd96faae1-f392-48e3-a1b0-0714ee945714', anchor:'interruptingshot'},
                    {additionalData:true, menu: 'battleEnhancement_ranged'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        categories: ['ranged'],
                        maneuverModifiers: [{maneuver: 'interruptingshot', category: 'ranged', advantage: 1, mastery: 1}]
                    }}
                ],
                hunter: [
                    {lang: 'fr', label: 'du chasseur', articleId: 'd96faae1-f392-48e3-a1b0-0714ee945714', anchor:'crippleshot'},
                    {additionalData:true, menu: 'battleEnhancement_ranged'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        categories: ['ranged'],
                        maneuverModifiers: [{maneuver: 'crippleshot', category: 'ranged', advantage: 1, mastery: 1}]
                    }}
                ],
                prodigy: [
                    {lang: 'fr', label: 'du prodige', articleId: 'd96faae1-f392-48e3-a1b0-0714ee945714', anchor:'multishot'},
                    {additionalData:true, menu: 'battleEnhancement_ranged'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        categories: ['ranged'],
                        maneuverModifiers: [{maneuver: 'multishot', category: 'ranged', advantage: 1, mastery: 1}]
                    }}
                ],
                predator: [
                    {lang: 'fr', label: 'du prédateur', articleId: 'd96faae1-f392-48e3-a1b0-0714ee945714', anchor:'finesse'},
                    {additionalData:true, menu: 'battleEnhancement_ranged'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        categories: ['ranged'],
                        maneuverModifiers: [{maneuver: 'finesse', category: 'ranged', advantage: 1, mastery: 1}]
                    }}
                ],
                sniper: [
                    {lang: 'fr', label: 'du snipeur', articleId: 'd96faae1-f392-48e3-a1b0-0714ee945714', anchor:'sniper'},
                    {additionalData:true, menu: 'battleEnhancement_ranged'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        categories: ['ranged'],
                        maneuverModifiers: [{maneuver: 'sniper', category: 'ranged', advantage: 1, mastery: 1}]
                    }}
                ],
                trickster: [
                    {lang: 'fr', label: 'du filou', articleId: 'd96faae1-f392-48e3-a1b0-0714ee945714', anchor:'ricochet'},
                    {additionalData:true, menu: 'battleEnhancement_ranged'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        categories: ['ranged'],
                        maneuverModifiers: [{maneuver: 'ricochet', category: 'ranged', advantage: 1, mastery: 1}]
                    }}
                ],

                potter: [
                    {lang: 'fr', label: 'du potier', articleId: '51988368-abc3-439d-ac35-a0952a9ac3ff', anchor:'splash_damage'},
                    {additionalData:true, menu: 'battleEnhancement_improvised'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        categories: ['improvised'],
                        maneuverModifiers: [{maneuver: 'splash_damage', category: 'improvised', advantage: 1, mastery: 1}]
                    }}
                ],
                blacksmith: [
                    {lang: 'fr', label: 'du forgeron', articleId: '51988368-abc3-439d-ac35-a0952a9ac3ff', anchor:'perfect_tool'},
                    {additionalData:true, menu: 'battleEnhancement_improvised'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        categories: ['improvised'],
                        maneuverModifiers: [{maneuver: 'perfect_tool', category: 'improvised', advantage: 1, mastery: 1}]
                    }}
                ],

                light: [
                    {lang: 'fr', label: 'légère', anchor:'other_affix'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 1,
                        speedModifier: -1
                    }}
                ],
                leftHand: [
                    {lang: 'fr', label: 'main gauche', anchor:'other_affix'},
                    {additionalData:true, weaponUpgrade: {
                        sizes: ['small_weapon'],
                        categories: ['melee'],
                        cost: 0,
                        leftHand: true
                    }}
                ],
                reach: [
                    {lang: 'fr', label: 'longue allonge', anchor:'other_affix'},
                    {additionalData:true, weaponUpgrade: {
                        sizes: ['medium_weapon', 'large_weapon'],
                        categories: ['melee', 'improvised'],
                        cost: 1,
                        reach: true
                    }}
                ],

                // Only for spells (have a cost of 99 otherwise)
                elementalPower1: [
                    {lang: 'fr', label: 'puissance élémentaire I', anchor:'damage_affix'},
                    {additionalData:true, menu: 'onlyForSpells'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 99,
                        damageModifiers: [{ flat:1 }]
                    }}
                ],
                elementalPower2: [
                    {lang: 'fr', label: 'puissance élémentaire II', anchor:'damage_affix'},
                    {additionalData:true, menu: 'onlyForSpells'},
                    {additionalData:true, weaponUpgrade: {
                        cost: 99,
                        damageModifiers: [{ flat:1 }]
                    }}
                ]

            }},
        ],

        armors: [
            {lang: 'fr', label: 'Affixes pour les armures', articleId: 'bcb3c253-b47f-49f8-8fb8-34c2c7a3cbd7', anchor:'armor_affix'},
            {additionalData:true, translated:true, list: {
                elementalArmor_water: [
                    {lang: 'fr', label: 'Cape imperméable'},
                    {additionalData:true, menu: 'elementalArmor'},
                    {additionalData:true, armorUpgrade: {
                        slots: ['back'],
                        cost: 1,
                        limiter: 'versatility',
                        elementalArmor: ['water']
                    }}
                ],
                elementalArmor_ice: [
                    {lang: 'fr', label: 'Cape chauffante'},
                    {additionalData:true, menu: 'elementalArmor'},
                    {additionalData:true, armorUpgrade: {
                        slots: ['back'],
                        cost: 1,
                        limiter: 'versatility',
                        elementalArmor: ['ice']
                    }}
                ],
                elementalArmor_fire: [
                    {lang: 'fr', label: 'Cape ignifugée'},
                    {additionalData:true, menu: 'elementalArmor'},
                    {additionalData:true, armorUpgrade: {
                        slots: ['back'],
                        cost: 1,
                        limiter: 'versatility',
                        elementalArmor: ['fire']
                    }}
                ],
                elementalArmor_wind: [
                    {lang: 'fr', label: 'Cape ample'},
                    {additionalData:true, menu: 'elementalArmor'},
                    {additionalData:true, armorUpgrade: {
                        slots: ['back'],
                        cost: 1,
                        limiter: 'versatility',
                        elementalArmor: ['wind']
                    }}
                ],
                elementalArmor_electricity: [
                    {lang: 'fr', label: 'Cape isolante'},
                    {additionalData:true, menu: 'elementalArmor'},
                    {additionalData:true, armorUpgrade: {
                        slots: ['back'],
                        cost: 1,
                        limiter: 'versatility',
                        elementalArmor: ['electricity']
                    }}
                ],
        
                elementalArmor_metal: [
                    {lang: 'fr', label: 'Plastron renforcé'},
                    {additionalData:true, menu: 'elementalArmor'},
                    {additionalData:true, armorUpgrade: {
                        slots: ['chest'],
                        cost: 1,
                        limiter: 'vigor',
                        elementalArmor: ['metal']
                    }}
                ],
                elementalArmor_plant: [
                    {lang: 'fr', label: 'Couverte de lierre'},
                    {additionalData:true, menu: 'elementalArmor'},
                    {additionalData:true, armorUpgrade: {
                        slots: ['chest'],
                        cost: 1,
                        limiter: 'vigor',
                        elementalArmor: ['plant']
                    }}
                ],
                elementalArmor_animal: [
                    {lang: 'fr', label: 'Trophés de chasse'},
                    {additionalData:true, menu: 'elementalArmor'},
                    {additionalData:true, armorUpgrade: {
                        slots: ['chest'],
                        cost: 1,
                        limiter: 'vigor',
                        elementalArmor: ['animal']
                    }}
                ],
                elementalArmor_earth: [
                    {lang: 'fr', label: 'Sous-couche rembourée'},
                    {additionalData:true, menu: 'elementalArmor'},
                    {additionalData:true, armorUpgrade: {
                        slots: ['chest'],
                        cost: 1,
                        limiter: 'vigor',
                        elementalArmor: ['earth']
                    }}
                ],
        
                elementalArmor_primary: [
                    {lang: 'fr', label: 'Symbole d\'Acaria'},
                    {additionalData:true, menu: 'elementalArmor'},
                    {additionalData:true, armorUpgrade: {
                        slots: ['neck'],
                        cost: 1,
                        limiter: 'socialEase',
                        elementalArmor: ['primary']
                    }}
                ],
                elementalArmor_mind: [
                    {lang: 'fr', label: 'Attrape rèves'},
                    {additionalData:true, menu: 'elementalArmor'},
                    {additionalData:true, armorUpgrade: {
                        slots: ['neck'],
                        cost: 1,
                        limiter: 'socialEase',
                        elementalArmor: ['mind']
                    }}
                ],
                elementalArmor_moon: [
                    {lang: 'fr', label: 'Pierre de lune'},
                    {additionalData:true, menu: 'elementalArmor'},
                    {additionalData:true, armorUpgrade: {
                        slots: ['neck'],
                        cost: 1,
                        limiter: 'socialEase',
                        elementalArmor: ['moon']
                    }}
                ],
                elementalArmor_shadow: [
                    {lang: 'fr', label: 'Talisman contre la mort'},
                    {additionalData:true, menu: 'elementalArmor'},
                    {additionalData:true, armorUpgrade: {
                        slots: ['neck'],
                        cost: 1,
                        limiter: 'socialEase',
                        elementalArmor: ['shadow']
                    }}
                ],
        
                elementalResist_water: [
                    {lang: 'fr', label: 'Bénédiction des eaux'},
                    {additionalData:true, menu: 'elementalResist'},
                    {additionalData:true, armorUpgrade: {
                        cost: 2,
                        limiter: 'perspicacity',
                        elementalResist: ['water']
                    }}
                ],
                elementalResist_ice: [
                    {lang: 'fr', label: 'Bénédiction des glaces'},
                    {additionalData:true, menu: 'elementalResist'},
                    {additionalData:true, armorUpgrade: {
                        cost: 2,
                        limiter: 'perspicacity',
                        elementalResist: ['ice']
                    }}
                ],
                elementalResist_fire: [
                    {lang: 'fr', label: 'Bénédiction des flammes'},
                    {additionalData:true, menu: 'elementalResist'},
                    {additionalData:true, armorUpgrade: {
                        cost: 2,
                        limiter: 'perspicacity',
                        elementalResist: ['fire']
                    }}
                ],
                elementalResist_wind: [
                    {lang: 'fr', label: 'Bénédiction des vents'},
                    {additionalData:true, menu: 'elementalResist'},
                    {additionalData:true, armorUpgrade: {
                        cost: 2,
                        limiter: 'perspicacity',
                        elementalResist: ['wind']
                    }}
                ],
                elementalResist_electricity: [
                    {lang: 'fr', label: 'Bénédiction de la foudre'},
                    {additionalData:true, menu: 'elementalResist'},
                    {additionalData:true, armorUpgrade: {
                        cost: 2,
                        limiter: 'perspicacity',
                        elementalResist: ['electricity']
                    }}
                ],
                elementalResist_metal: [
                    {lang: 'fr', label: 'Bénédiction des forges'},
                    {additionalData:true, menu: 'elementalResist'},
                    {additionalData:true, armorUpgrade: {
                        cost: 2,
                        limiter: 'perspicacity',
                        elementalResist: ['metal']
                    }}
                ],
                elementalResist_plant: [
                    {lang: 'fr', label: 'Bénédiction de la nature'},
                    {additionalData:true, menu: 'elementalResist'},
                    {additionalData:true, armorUpgrade: {
                        cost: 2,
                        limiter: 'perspicacity',
                        elementalResist: ['plant']
                    }}
                ],
                elementalResist_animal: [
                    {lang: 'fr', label: 'Bénédiction du chasseur'},
                    {additionalData:true, menu: 'elementalResist'},
                    {additionalData:true, armorUpgrade: {
                        cost: 2,
                        limiter: 'perspicacity',
                        elementalResist: ['animal']
                    }}
                ],
                elementalResist_earth: [
                    {lang: 'fr', label: 'Bénédiction des roches'},
                    {additionalData:true, menu: 'elementalResist'},
                    {additionalData:true, armorUpgrade: {
                        cost: 2,
                        limiter: 'perspicacity',
                        elementalResist: ['earth']
                    }}
                ],
                elementalResist_primary: [
                    {lang: 'fr', label: 'Bénédiction d\'Acaria'},
                    {additionalData:true, menu: 'elementalResist'},
                    {additionalData:true, armorUpgrade: {
                        cost: 2,
                        limiter: 'perspicacity',
                        elementalResist: ['primary']
                    }}
                ],
                elementalResist_mind: [
                    {lang: 'fr', label: 'Bénédiction des songes'},
                    {additionalData:true, menu: 'elementalResist'},
                    {additionalData:true, armorUpgrade: {
                        cost: 2,
                        limiter: 'perspicacity',
                        elementalResist: ['mind']
                    }}
                ],
                elementalResist_moon: [
                    {lang: 'fr', label: 'Bénédiction des étoiles'},
                    {additionalData:true, menu: 'elementalResist'},
                    {additionalData:true, armorUpgrade: {
                        cost: 2,
                        limiter: 'perspicacity',
                        elementalResist: ['moon']
                    }}
                ],
                elementalResist_shadow: [
                    {lang: 'fr', label: 'Bénédiction des ombres'},
                    {additionalData:true, menu: 'elementalResist'},
                    {additionalData:true, armorUpgrade: {
                        cost: 2,
                        limiter: 'perspicacity',
                        elementalResist: ['shadow']
                    }}
                ],

                head_critical_1: [
                    {lang: 'fr', label: 'Absorption critique I'},
                    {additionalData:true, armorUpgrade: {
                        slots: ['head'],
                        cost: 2,
                        limiter: 'vigor'
                    }}
                ],
                head_critical_2: [
                    {lang: 'fr', label: 'Absorption critique II'}, 
                    {additionalData:true, armorUpgrade: {
                        slots: ['head'],
                        cost: 3,
                        limiter: 'vigor'
                    }}
                ],
                head_critical_3: [
                    {lang: 'fr', label: 'Absorption critique III'}, 
                    {additionalData:true, armorUpgrade: {
                        slots: ['head'],
                        cost: 4,
                        limiter: 'vigor'
                    }}
                ],
                
                shield_level_1: [
                    {lang: 'fr', label: 'Bouclier léger'}, 
                    {additionalData:true, menu: 'battleEnhancement'},
                    {additionalData:true, armorUpgrade: {
                        slots: ['left_hand'],
                        cost: 1,
                        limiter: 'versatility',
                        combat: {
                            on: 'defense',
                            types: ['melee','ranged'],
                            advantage: 0,
                            mastery: 1
                        }
                    }}
                ],
                shield_level_2: [
                    {lang: 'fr', label: 'Bouclier lourd'}, 
                    {additionalData:true, menu: 'battleEnhancement'},
                    {additionalData:true, armorUpgrade: {
                        slots: ['left_hand'],
                        cost: 2,
                        limiter: 'versatility',
                        combat: {
                            on: 'defense',
                            types: ['melee','ranged', 'magic'],
                            advantage: 0,
                            mastery: 1
                        }
                    }}
                ],
                shield_level_3: [
                    {lang: 'fr', label: 'Pavois'}, 
                    {additionalData:true, menu: 'battleEnhancement'},
                    {additionalData:true, armorUpgrade: {
                        slots: ['left_hand'],
                        cost: 3,
                        limiter: 'versatility',
                        combat: {
                            on: 'defense',
                            types: ['melee','ranged', 'magic'],
                            advantage: 1,
                            mastery: 1
                        }
                    }}
                ],
                orb_arcane_1: [
                    {lang: 'fr', label: 'Catalyseur magique'}, 
                    {additionalData:true, menu: 'battleEnhancement'},
                    {additionalData:true, armorUpgrade: {
                        slots: ['left_hand'],
                        cost: 2,
                        limiter: 'perspicacity',
                        combat: {
                            on: 'offense',
                            types: ['arcane'],
                            advantage: 0,
                            mastery: 1
                        }
                    }}
                ],
                divine_symbol_1: [
                    {lang: 'fr', label: 'Symbole divin'}, 
                    {additionalData:true, menu: 'battleEnhancement'},
                    {additionalData:true, armorUpgrade: {
                        slots: ['left_hand'],
                        cost: 2,
                        limiter: 'socialEase',
                        combat: {
                            on: 'offense',
                            types: ['prayer'],
                            advantage: 0,
                            mastery: 1
                        }
                    }}
                ],

                noPenalty: [
                    {lang: 'fr', label: 'N\'encombre pas'}, 
                    {additionalData:true, menu: 'other'},
                    {additionalData:true, armorUpgrade: {
                        cost: -6
                    }}
                ],

            }},
        ],

        miscs: [
            {lang: 'fr', label: 'Affixes pour les consommables', articleId: '', anchor:''}, //FIXME
            {additionalData:true, translated:true, list: {
                potion_heal: [
                    {lang: 'fr', label: 'Potion de soins'},
                    {additionalData:true, menu: 'spellTrigger'},
                    {additionalData:true, miscUpgrade: {
                        slots: ['useable'],
                        cost: 1,
                        spell: {
                            ref: 'elementalHeal',
                            caracs: {
                                effectHeal: 0,
                                range: 0,
                                areaOfEffect: 0,
                                clone: 0
                            }
                        }
                    }}
                ]
            }}
        ]
    }
};

const waCache = {}; // For storing list, wich should be calculated only once.

export const initShortcuts = () => {
    const module = game.modules.get("world-anvil");
    waCache.shortcuts = {};
    retrieveEntries(JOURNAL_ENTRIES, waCache.shortcuts);
  
    module.helpers.registerShortcuts(waCache.shortcuts);
    waCache.allEntries = expandObject(waCache.shortcuts);
}

export const generateWALinks = (html) => {
    const helpers = game.modules.get("world-anvil").helpers;
    helpers.substituteShortcutsAndHandleClicks(html);
    helpers.substituteShortcutsAndHandleClicks(html, {classes: ['.wa-tooltip', '.wa-frame'], changeLabels: false});
    helpers.substituteShortcutsAndHandleClicks(html, {classes: ['.wa-contextmenu'], changeLabels: false, contextmenu: true});
}

export const attributeList = () => {

    const nodePath = 'character.attributes.definition';
    if( ! waCache.attributeList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                order: value.order
            };
        };

        waCache.attributeList = buildListFromNodeChilds(mapMethod, nodePath, 'list');
        waCache.attributeList.sort( (a,b) => a.order - b.order );
    }
    return waCache.attributeList;
}

export const attributeLevels = () => {

    const nodePath = 'character.attributes.definition';
    if( ! waCache.attributeLevels && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                level: value.level,
                xpCost: value.xpCost
            };
        };

        waCache.attributeLevels = buildListFromNodeChilds(mapMethod, nodePath, 'levels');
        waCache.attributeLevels.sort( (a,b) => a.level - b.level );
    }
    return waCache.attributeLevels;
}

export const maneuverLevels = () => {

    const nodePath = 'character.maneuvers.definition';
    if( ! waCache.maneuverLevels && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                level: value.level,
                interaction: value.interaction
            };
        };

        waCache.maneuverLevels = buildListFromNodeChilds(mapMethod, nodePath, 'levels');
        waCache.maneuverLevels.sort( (a,b) => a.level - b.level );
    }
    return waCache.maneuverLevels;
}

export const monsterAugmentMenus = () => {

    const nodePath = 'character.monsters.augments';
    if( ! waCache.monsterAugmentMenus && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                displayOrder: value.displayOrder
            };
        }
        waCache.monsterAugmentMenus = buildListFromNodeChilds(mapMethod, nodePath, 'menu');;
        waCache.monsterAugmentMenus.sort( (a,b) => a.displayOrder - b.displayOrder );
    }
    return waCache.monsterAugmentMenus;
}

export const monsterAugmentList = () => {

    const nodePath = 'character.monsters.augments';
    if( ! waCache.monsterAugmentList && nodeIsRegistered(nodePath) ) {

        const menus = monsterAugmentMenus();
        const otherMenu = menus.find( m => m.key === 'other');

        const mapMethod = ([key, value]) => {
            return {
                maxLevel: value.maxLevel,
                menu: menus.find( m => m.key === value.menu ) ?? otherMenu
            };
        };

        waCache.monsterAugmentList = buildListFromNodeChilds(mapMethod, nodePath, 'list');
    }
    return waCache.monsterAugmentList;
}

export const eventTypeList = () => {

    const nodePath = 'interaction.system.events';
    if( ! waCache.eventTypeList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {};
        };

        waCache.eventTypeList = buildListFromNodeChilds(mapMethod, nodePath, 'types');
    }
    return waCache.eventTypeList;
}

export const eventTimingList = () => {

    const nodePath = 'interaction.system.events';
    if( ! waCache.eventTimingList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {};
        };

        waCache.eventTimingList = buildListFromNodeChilds(mapMethod, nodePath, 'timings');
    }
    return waCache.eventTimingList;
}

export const eventOnRollList = () => {

    const nodePath = 'interaction.system.events';
    if( ! waCache.eventOnRollList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                icon: 'systems/acariaempire/resources/cards/event/onroll/' + key + '.webp'
            };
        };

        waCache.eventOnRollList = buildListFromNodeChilds(mapMethod, nodePath, 'onrolls');
    }
    return waCache.eventOnRollList;
}

export const eventCostList = () => {

    const nodePath = 'interaction.system.events';
    if( ! waCache.eventCostList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                min: value.min,
                max: value.max
            };
        };

        waCache.eventCostList = buildListFromNodeChilds(mapMethod, nodePath, 'costs');
    }
    return waCache.eventCostList;
}

export const combatSystems = () => {

    const nodePath = 'combat.system';
    if( ! waCache.combatSystems && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                system: key
            };
        };

        waCache.combatSystems = buildListFromNode(mapMethod, nodePath);
    }

    return waCache.combatSystems;
}

export const meleeManeuvers = () =>  {

    const nodePath = 'combat.system.melee';
    if( ! waCache.meleeManeuvers && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                maneuver: key,
                icon: 'systems/acariaempire/resources/capacities/maneuvers/melee/' + key+ '.png',
                levelIcons: levelIcons()
            };
        };

        waCache.meleeManeuvers = buildListFromNodeChilds(mapMethod, nodePath, 'maneuvers');
    }
    return waCache.meleeManeuvers;
}

export const rangedManeuvers = () =>  {

    const nodePath = 'combat.system.ranged';
    if( ! waCache.rangedManeuvers && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                maneuver: key,
                icon: 'systems/acariaempire/resources/capacities/maneuvers/ranged/' + key+ '.png',
                levelIcons: levelIcons()
            };
        };

        waCache.rangedManeuvers = buildListFromNodeChilds(mapMethod, nodePath, 'maneuvers');
    }
    return waCache.rangedManeuvers;
}

export const improvisedManeuvers = () =>  {

    const nodePath = 'combat.system.improvised';
    if( ! waCache.improvisedManeuvers && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                maneuver: key,
                martialArt: value.martialArt,
                icon: 'systems/acariaempire/resources/capacities/maneuvers/improvised/' + key+ '.png',
                levelIcons: levelIcons()
            };
        };

        waCache.improvisedManeuvers = buildListFromNodeChilds(mapMethod, nodePath, 'maneuvers');
    }
    return waCache.improvisedManeuvers;
}


export const magicSystems = () => {

    const nodePath = 'magic.system';
    if( ! waCache.magicSystems && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                system: key
            };
        };

        waCache.magicSystems = buildListFromNode(mapMethod, nodePath);
    }

    return waCache.magicSystems;
}

export const spellCategories = () => {
    const nodePath = 'magic.effects';
    if( ! waCache.spellCategories && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {

            const spellMaping =([spellKey, spellValue]) => {
                return {
                    icon: 'systems/acariaempire/resources/capacities/spells/' + key + '/' + spellKey + '.png',
                    affixes: spellValue.affixes ?? []
                };
            };
            const spells = !value.spells ? [] : buildListFromNode(spellMaping, nodePath + '.' + key + '.spells' );
            spells.forEach( s => {
                s.shortcut = nodePath + '.' + key;
                s.shortcutChild = 'spells.' + s.key;
                s.relatedEffect = key;
            });

            return {
                effect: key,
                offensive: value.offensive ? true : false,
                caracteristics: value.caracteristics,
                levelIcons: levelIcons(),
                spells: spells
            };
        };

        waCache.spellCategories = buildListFromNode(mapMethod, nodePath);
    }

    return waCache.spellCategories;
}

export const allSpellsDefinition = () => {
    if( ! waCache.allSpellsDefinition && spellCategories() ) {

        const spells = spellCategories().reduce( (list, current) => {
            return list.concat( current.spells );
        }, []);

        waCache.allSpellsDefinition = {
            levelIcons: levelIcons(),
            spells: spells
        };
    }
    return waCache.allSpellsDefinition;
}

export const spellShapes = () => {
    const nodePath = 'magic.shapes';
    if( ! waCache.spellShapes && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {

            const generateSpellDesc = (cloneLevel, rangeLevel, areaLevel, durationLevel) => {

                const base = value.spellDescription;
                const title = base.cloneDesc.description[cloneLevel ?? 0] + base.rangeDesc.description[rangeLevel ?? 0];
                
                let details = [];
                if( areaLevel || areaLevel == 0 ) {
                    const areaDetails = base.areaDesc.description[areaLevel];
                    if( areaDetails != '' ) { details.push( areaDetails); }
                }

                if( durationLevel || durationLevel == 0 ) {
                    const durationDetails = base.durationDesc.description[durationLevel];
                    if( durationDetails != '' ) { details.push( durationDetails); }
                }
                
                const infos = base.additionalInfo?.description ?? [];
                if( infos.length > 0 ) {
                    details = details.concat(infos);
                }

                return {
                    title: title,
                    details: details
                };
            }


            return {
                icon: 'systems/acariaempire/resources/capacities/shapes/' + key+ '.png',
                generateSpellDesc: generateSpellDesc
            };
        };

        waCache.spellShapes = buildListFromNode(mapMethod, nodePath);
    }

    return waCache.spellShapes;
}

export const arcaneSpellCategories = () => {

    if( ! waCache.arcaneSpellCategories && spellCategories() ) {
        waCache.arcaneSpellCategories = spellCategories().filter(e => e.effect != 'effectHeal');
    }

    return waCache.arcaneSpellCategories;
}

export const arcaneSpellModifiers = () => {
    const nodePath = 'magic.modifiers';
    if( ! waCache.arcaneSpellModifiers && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                modifier: key
            };
        };

        waCache.arcaneSpellModifiers = buildListFromNode(mapMethod, nodePath);
    }

    return waCache.arcaneSpellModifiers;
}

export const arcaneManeuvers = () =>  {

    const nodePath = 'magic.system.arcane';
    if( ! waCache.arcaneManeuvers && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                maneuver: key,
                icon: 'systems/acariaempire/resources/capacities/maneuvers/arcane/' + key+ '.png',
                levelIcons: levelIcons()
            };
        };

        waCache.arcaneManeuvers = buildListFromNodeChilds(mapMethod, nodePath, 'maneuvers');
    }
    return waCache.arcaneManeuvers;
}

export const prayerManeuvers = () =>  {

    const nodePath = 'magic.system.divine';
    if( ! waCache.prayerManeuvers && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                maneuver: key,
                icon: 'systems/acariaempire/resources/capacities/maneuvers/prayer/' + key+ '.png',
                levelIcons: levelIcons()
            };
        };

        waCache.prayerManeuvers = buildListFromNodeChilds(mapMethod, nodePath, 'maneuvers');
    }
    return waCache.prayerManeuvers;
}

export const divinityList = () => {

    const nodePath = 'magic.divinities';
    if( ! waCache.divinityList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                divinity: key,
                icon: 'systems/acariaempire/resources/divinity/' + key+ '.png',
                elements: value.elements ?? []
            };
        };

        waCache.divinityList = buildListFromNode(mapMethod, nodePath);
    }

    return waCache.divinityList;
}

export const elementList = () => {

    const nodePath = 'magic.elements';
    if( ! waCache.elementList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {

            const spellList = Object.values( value.spells ).reduce( (list, current) => {
                return list.concat(current);
            }, []);

            return {
                element: key,
                icon: 'systems/acariaempire/resources/cards/event/element/' + key+ '.png',
                arcanePenalty: value.arcanePenalty,
                arcaneNeighbor: value.arcaneNeighbor, 
                spellShape: value.spellShape,
                spells: spellList
            };
        };

        waCache.elementList = buildListFromNode(mapMethod, nodePath);
    }
    return waCache.elementList;
}

export const arcaneCirclePenalties = () => {

    if( ! waCache.arcaneCirclePenalties && elementList() ) {

        waCache.arcaneCirclePenalties = new Map();
        
        const elementsWithoutPrimary = elementList().filter( e => {
            return e.element != 'primary';
        });

        // Add primary element to map since its a little different from the other ones
        let penalties = new Map();
        elementsWithoutPrimary.forEach(e => {
            penalties.set( e.element, e.arcanePenalty );
        });
        waCache.arcaneCirclePenalties.set( 'primary', penalties );

        // Add other elements
        elementsWithoutPrimary.forEach( rootElement => {
            
            penalties = new Map();
            penalties.set( rootElement.key, 0 );
            penalties.set( 'primary', rootElement.arcanePenalty ); // primary is not among the neighbors

            const toExplore = [];
            const exploreElement = (element) => {
                const basePenalty = penalties.get(element.key);
                element.arcaneNeighbor.forEach( neighbor => {

                    const currentPenalty = penalties.get(neighbor);
                    if( !penalties.has(neighbor) || currentPenalty > basePenalty + 1 ) {
                        penalties.set( neighbor, basePenalty + 1 );
                        toExplore.push( neighbor );
                    }
                });
            };

            // Do the first two neighbors
            exploreElement( rootElement );

            let security = 0;
            while( toExplore.length > 0 && security < 1000 ) {
                security++;
                const elementKey = toExplore.shift();
                const element = elementList().find( e => e.key === elementKey );
                exploreElement(element);
            }
            if( security == 1000 ) {
                throw 'Something went wrong while calculating arcaneCirclePenalties';
            }

            waCache.arcaneCirclePenalties.set( rootElement.key, penalties );
        });

    }
    return waCache.arcaneCirclePenalties;
}

export const spiritElementList = () => {

    const nodePath = 'magic.system.divine';
    if( ! waCache.spiritElementList && nodeIsRegistered(nodePath) && elementList() ) {

        const mapMethod = ([key, value]) => {

            const data = {};
            const elementDef = elementList().find( el => el.key === key );
            if( elementDef ) {
                mergeObject( data, elementDef );
                data.keepMapLinks = true;
            } else { //if( key == 'any' ) { 
                data.element = key;
                data.icon = 'systems/acariaempire/resources/cards/event/element/' + key + '.png';
                data.spells = allSpellsDefinition().spells;
            }
            return data;
        };
        waCache.spiritElementList = buildListFromNodeChilds(mapMethod, nodePath, 'element');
    }
    return waCache.spiritElementList;
}

export const spiritEffectList = () => {

    const nodePath = 'magic.system.divine';
    if( ! waCache.spiritEffectList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                effect: key,
                offensive: value.offensive,
                icon: 'systems/acariaempire/resources/cards/event/effect/' + key + '.svg'
            };
        };

        waCache.spiritEffectList = buildListFromNodeChilds(mapMethod, nodePath, 'effect');
    }
    return waCache.spiritEffectList;
}

export const spiritBonusList = () => {

    const nodePath = 'magic.system.divine';
    if( ! waCache.spiritBonusList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                bonus: key,
                icon:  'systems/acariaempire/resources/cards/event/bonus/' + key + '.svg'
            };
        };

        waCache.spiritBonusList = buildListFromNodeChilds(mapMethod, nodePath, 'bonus');
    }
    return waCache.spiritBonusList;
}

export const songsMelodies = () =>  {

    const nodePath = 'magic.system.songs';
    if( ! waCache.songsMelodies && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                melody: key,
                icon: 'systems/acariaempire/resources/capacities/songs/' + key + '.png'
            };
        };

        waCache.songsMelodies = buildListFromNodeChilds(mapMethod, nodePath, 'melodies');
    }
    return waCache.songsMelodies;
}

export const songsManeuvers = () =>  {

    const nodePath = 'magic.system.songs';
    if( ! waCache.songsManeuvers && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                maneuver: key,
                icon: 'systems/acariaempire/resources/capacities/maneuvers/songs/' + key+ '.png',
                levelIcons: levelIcons()
            };
        };

        waCache.songsManeuvers = buildListFromNodeChilds(mapMethod, nodePath, 'maneuvers');
    }
    return waCache.songsManeuvers;
}

export const jobList = () => {

    const nodePath = 'character.jobs';
    if( ! waCache.jobList && nodeIsRegistered(nodePath) ) {

        const combatSkillList = skillList().filter( s => s.skillType == 'combat' );
        const exploreSkillList = skillList().filter( s => s.skillType == 'explore' );
        const intrigueSkillList = skillList().filter( s => s.skillType == 'intrigue' );

        const mapMethod = ([key, value]) => {
            const activities = Object.entries(value.activities).map( ([k, v]) => mergeObject({ key: k }, v) );
            return {
                job: key,
                icon: 'systems/acariaempire/resources/job/' + key+ '.png',
                activities: activities,
                battle: value.battle ?? {},
                forMonsters: value.forMonsters ?? false,
                inCombat: combatSkillList.find( s => s.key === value.inCombat ) ?? null,
                whileExploring: exploreSkillList.find( s => s.key === value.whileExploring ) ?? null,
                duringIntrigue: intrigueSkillList.find( s => s.key === value.duringIntrigue ) ?? null
            };
        };

        waCache.jobList = buildListFromNode(mapMethod, nodePath);
    }
    return waCache.jobList;
}

export const skillAreas = () => {

    const nodePath = 'character.skill_area.definition';
    if( ! waCache.skillAreas && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                order: value.order
            };
        };

        waCache.skillAreas = buildListFromNodeChilds(mapMethod, nodePath, 'list');
        waCache.skillAreas.sort( (a,b) => a.order - b.order );
    }
    return waCache.skillAreas;
}

export const skillLevels = () => {

    const nodePath = 'character.skill_area.definition';
    if( ! waCache.skillLevels && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                level: value.level,
                xpCost: value.xpCost
            };
        };

        waCache.skillLevels = buildListFromNodeChilds(mapMethod, nodePath, 'levels');
        waCache.skillLevels.sort( (a,b) => a.level - b.level );
    }
    return waCache.skillLevels;
}

export const skillList = () => {

    const nodePath = 'character.skill_area.definition';
    if( ! waCache.skillList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                skillType: value.skillType,
                available: value.available ?? true
            };
        };

        waCache.skillList = buildListFromNodeChilds(mapMethod, nodePath, 'skills');
    }
    return waCache.skillList;
}


export const weaponSizeList = () => {

    const nodePath = 'item.weapon.definition';
    if( ! waCache.weaponSizeList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                size: key,
                baseSpeed: value.baseSpeed,
                baseDamage: value.baseDamage,
                basePrice: value.basePrice
            };
        };

        waCache.weaponSizeList = buildListFromNodeChilds(mapMethod, nodePath, 'sizes');
    }
    return waCache.weaponSizeList;
}

export const weaponRarityList = () => {

    const nodePath = 'item.weapon.definition';
    if( ! waCache.weaponRarityList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {

            const result = { 
                affixCost: value.affixCost,
                additionalCost: value.additionalCost
            };
            return result;
        };

        waCache.weaponRarityList = buildListFromNodeChilds(mapMethod, nodePath, 'rarity');
    }

    return waCache.weaponRarityList;
}

export const weaponMaxRarity = () => {

    if( ! waCache.weaponMaxRarity ) {

        waCache.weaponMaxRarity = weaponRarityList().reduce( (result, rarity) => {
            return rarity.affixCost > result ? rarity.affixCost : result;
        }, 0);
    }

    return waCache.weaponMaxRarity;
}

export const commonWeaponList = () => {

    const nodePath = 'item.weapon.common';
    if( ! waCache.commonWeaponList && nodeIsRegistered(nodePath) ) {
        
        const mapMethod = ([key, value]) => {
            const sizeDef = weaponSizeList().find(s => s.size === value.size);
            if( !sizeDef ) { throw `Can't build commonWeaponList. Unknown size (for weapon) : ${value.size} (${key})`; }

            const affixesBase = value.affixes ?? [];
            const affixes = affixesBase.map( base => {
                const affix = affixList().find(af => af.key === base);
                if( !affix ) { throw `Can't build commonWeaponList. Unknown affix (for weapon) : ${base} (${key})`; }
                return duplicate(affix);
            });

            const wpnCategory = value.category;
            if( ! ['melee', 'ranged', 'improvised'].includes(wpnCategory) ) {
                throw `Can't build commonWeaponList. Unknown category (for weapon) : ${wpnCategory} (${key})`;
            }

            const elementKey = value.element;
            const element = elementList().find( e => e.key === elementKey );
            if( !element ) {
                throw `Can't build commonWeaponList. Unknown element (for weapon) : ${elementKey} (${key})`;
            }

            return {
                weapon: key,
                icon: 'systems/acariaempire/resources/items/weapon/' + key+ '.webp', 
                size: sizeDef,
                category: value.category,
                element: element,
                affixes: affixes
            };
        };

        waCache.commonWeaponList = buildListFromNode(mapMethod, nodePath);
    }
    return waCache.commonWeaponList;
}

export const armorProtectionLevels = () => {

    const nodePath = 'item.armor.definition';
    if( ! waCache.armorProtectionLevels && nodeIsRegistered(nodePath) ) {
        
        const mapMethod = ([key, value]) => {

            return {
                value: value.value,
                basePrice: value.basePrice
            };
        };

        waCache.armorProtectionLevels = buildListFromNodeChilds(mapMethod, nodePath, 'protectionLevels');
        waCache.armorProtectionLevels.sort( (a,b) => a.value - b.value );
    }
    return waCache.armorProtectionLevels;
}

export const armorRarityList = () => {

    const nodePath = 'item.armor.definition';
    if( ! waCache.armorRarityList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {

            const result = { 
                affixCost: value.affixCost,
                additionalCost: value.additionalCost
            };
            return result;
        };

        waCache.armorRarityList = buildListFromNodeChilds(mapMethod, nodePath, 'rarity');
    }

    return waCache.armorRarityList;
}

export const armorMaxRarity = () => {

    if( ! waCache.armorMaxRarity ) {

        waCache.armorMaxRarity = armorRarityList().reduce( (result, rarity) => {
            return rarity.affixCost > result ? rarity.affixCost : result;
        }, 0);
    }

    return waCache.armorMaxRarity;
}

export const armorSlotList = () => {

    const nodePath = 'item.armor.definition';
    if( ! waCache.armorSlotList && nodeIsRegistered(nodePath) ) {
        
        const mapMethod = ([key, value]) => {

            return {
                icon: 'systems/acariaempire/resources/items/slots/' + key+ '.png',
                sortValue: value.sortValue,
                allowProtection: value.allowProtection ?? null
            };
        };

        waCache.armorSlotList = buildListFromNodeChilds(mapMethod, nodePath, 'slots');
        waCache.armorSlotList.sort( (a,b) => a.sortValue - b.sortValue );
    }
    return waCache.armorSlotList;
}

export const commonArmorList = () => {

    const nodePath = 'item.armor.common';
    if( ! waCache.commonArmorList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {

            const slotDef = armorSlotList().find( c => c.key === value.base.slot );
            if( !slotDef ) { throw `Can't build commonArmorList. Unknown slot : ${value.base.slot} (${key})`; }
            
            const affixesBase = value.affixes ?? [];
            const affixes = affixesBase.map( base => {
                const affix = affixList().find(af => af.key === base);
                if( !affix ) { throw `Can't build commonArmorList. Unknown affix (for armor) : ${base} (${key})`; }
                return duplicate(affix);
            });

            const result = {
                icon: 'systems/acariaempire/resources/items/armor/' + value.base.slot + '/' + key+ '.webp', 
                slot: slotDef,
                protectionLevel: null,
                affixes: affixes
            }
            
            const allowProtect = slotDef.allowProtection;
            if( allowProtect ) {
                const protectLevelDef = armorProtectionLevels().find( c => c.key === value.base.protectionLevel );
                if( !protectLevelDef ) { throw `Can't build commonArmorList. Unavailable protection level : ${value.base.protectionLevel} (${key})`; }
                
                result.protectionLevel = protectLevelDef;
            }

            return result;
        };

        // Items have been put in sub sections in order to set only once articleId and anchorId
        waCache.commonArmorList = [];
        const root = getProperty(waCache.allEntries, nodePath);
        Object.keys( root ).forEach( sublevel => {
            const path = nodePath + '.' + sublevel;
            waCache.commonArmorList = waCache.commonArmorList.concat( 
                buildListFromNodeChilds(mapMethod, path, 'list')
            );
        });

    }
    return waCache.commonArmorList;
}


export const miscSlotList = () => {

    const nodePath = 'item.misc.definition';
    if( ! waCache.miscSlotList && nodeIsRegistered(nodePath) ) {
        
        const mapMethod = ([key, value]) => {

            return {
                icon: 'systems/acariaempire/resources/items/slots/' + key+ '.png',
                sortValue: value.sortValue
            };
        };

        waCache.miscSlotList = buildListFromNodeChilds(mapMethod, nodePath, 'slots');
        waCache.miscSlotList.sort( (a,b) => a.sortValue - b.sortValue );
    }
    return waCache.miscSlotList;
}

export const miscRarityList = () => {

    const nodePath = 'item.misc.definition';
    if( ! waCache.miscRarityList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {

            const result = { 
                affixCost: value.affixCost,
                additionalCost: value.additionalCost
            };
            return result;
        };

        waCache.miscRarityList = buildListFromNodeChilds(mapMethod, nodePath, 'rarity');
    }

    return waCache.miscRarityList;
}

export const miscMaxRarity = () => {

    if( ! waCache.miscMaxRarity ) {

        waCache.miscMaxRarity = miscRarityList().reduce( (result, rarity) => {
            return rarity.affixCost > result ? rarity.affixCost : result;
        }, 0);
    }

    return waCache.miscMaxRarity;
}

export const commonMiscList = () => {

    const nodePath = 'item.misc';
    if( ! waCache.commonMiscList && nodeIsRegistered(nodePath) ) {
        
        const mapMethod = ([key, value]) => {

            const slotDef = miscSlotList().find( c => c.key === value.base.slot );
            if( !slotDef ) { throw `Can't build commonArmorList. Unknown slot : ${value.base.slot} (${key})`; }
            
            const affixesBase = value.affixes ?? [];
            const affixes = affixesBase.map( base => {
                const affix = affixList().find(af => af.key === base);
                if( !affix ) { throw `Can't build commonMiscList. Unknown affix (for misc) : ${base} (${key})`; }
                return duplicate(affix);
            });

            return {
                icon: 'systems/acariaempire/resources/items/misc/' + value.base.slot + '/' + key+ '.webp', 
                basePrice: value.base.basePrice ?? 0,
                slot: slotDef,
                affixes: affixes
            };
        };

        waCache.commonMiscList = buildListFromNodeChilds(mapMethod, nodePath, 'common');
    }
    return waCache.commonMiscList;
}


export const affixList = () => {

    if( ! waCache.affixList ) {

        waCache.affixList = weaponAffixList()
                    .concat( armorAffixList() )
                    .concat( miscAffixList() );

        waCache.affixList.sort( (a,b) => a.name.localeCompare(b.name) );
    }
    return waCache.affixList;
}

export const affixMenuList = () => {

    const nodePath = 'affixes.definition';
    if( ! waCache.affixMenuList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                displayOrder: value.displayOrder
            };
        }
        waCache.affixMenuList = buildListFromNodeChilds(mapMethod, nodePath, 'menu');;

        waCache.affixMenuList.sort( (a,b) => a.displayOrder - b.displayOrder );
    }
    return waCache.affixMenuList;
}

export const weaponAffixList = () => {

    const nodePath = 'affixes.weapons';
    if( ! waCache.weaponAffixList && nodeIsRegistered(nodePath) ) {

        const affixMenus = affixMenuList();
        const otherMenu = affixMenus.find( m => m.key === 'other');

        const mapMethod = ([key, value]) => {

            const result = { 
                forWeapon: true,
                menu: affixMenus.find( m => m.key === value.menu ) ?? otherMenu
            };

            const source = value.weaponUpgrade ?? {};
            result.weaponUpgrade = {
                sizes: source.sizes ?? ['small_weapon', 'medium_weapon', 'large_weapon'],
                categories: source.categories ?? ['melee', 'ranged', 'improvised'],
                cost: source.cost,
                leftHand: source.leftHand ?? false,
                reach: source.reach ?? false,
                speedModifier: source.speedModifier ?? 0,
                maneuverModifiers : duplicate( source.maneuverModifiers ?? []),
                damageModifiers: [], // Need to set default values. Set separately
                onAttackSuccess: [] // Need to set default values. Set separately
            };

            result.weaponUpgrade.damageModifiers = (source.damageModifiers ?? []).map( modifier => {
                return {
                    flat: modifier.flat ?? 0,
                    multiplier: modifier.multiplier ?? { beforeArmor: 1, afterArmor: 1},
                    penetration: modifier.penetration ?? 0,
                    on: modifier.on ?? null,
                    element: modifier.element ?? null
                };
            });

            result.weaponUpgrade.onAttackSuccess = (source.onAttackSuccess ?? []).map( modifier => {
                return {
                    selfDamage: modifier.selfDamage ?? 0,
                    powerGain: modifier.powerGain ?? 0,
                    on: modifier.on ?? null
                };
            });

            return result;
        };

        waCache.weaponAffixList = buildListFromNodeChilds(mapMethod, nodePath, 'list');
    }

    return waCache.weaponAffixList;
}

export const armorAffixList = () => {

    const nodePath = 'affixes.armors';
    if( ! waCache.armorAffixList && nodeIsRegistered(nodePath) ) {

        const affixMenus = affixMenuList();
        const otherMenu = affixMenus.find( m => m.key === 'other');

        const mapMethod = ([key, value]) => {
            const result = { 
                forArmor: true,
                menu: affixMenus.find( m => m.key === value.menu ) ?? otherMenu,
                armorUpgrade: value.armorUpgrade
            };
            return result;
        };

        waCache.armorAffixList = buildListFromNodeChilds(mapMethod, nodePath, 'list');
    }
    return waCache.armorAffixList;
}

export const miscAffixList = () => {

    const nodePath = 'affixes.miscs';
    if( ! waCache.miscAffixList && nodeIsRegistered(nodePath) ) {

        const affixMenus = affixMenuList();
        const otherMenu = affixMenus.find( m => m.key === 'other');

        const mapMethod = ([key, value]) => {
            const result = { 
                forMisc: true,
                menu: affixMenus.find( m => m.key === value.menu ) ?? otherMenu,
                miscUpgrade: value.miscUpgrade
            };
            return result;
        };

        waCache.miscAffixList = buildListFromNodeChilds(mapMethod, nodePath, 'list');
    }
    return waCache.miscAffixList;
}

/**
 * When the guardian spirit is growing, he will give some benefits to its host
 * Benefits are stored by categories. The host can spend energy to upgrade a category.
 * When a category levels up, one of its benefit can be chosen.
 */
export const guardianSpiritBenefitList = () => {

    const nodePath = 'character.spirit.definition';
    if( ! waCache.guardianSpiritBenefitList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                benefit: key,
                related: value.related,  // The related drawbacks
                unlockable: value.unlockable // The available perks when leveling up
            };
        };

        waCache.guardianSpiritBenefitList = buildListFromNodeChilds(mapMethod, nodePath, 'benefits');
    }
    return waCache.guardianSpiritBenefitList;
}

/**
 * Each time the benefit category levels up, one drawback category grows.
 * When drawback category levels, on of its drawnbacks has to be chosen
 */
export const guardianSpiritDrawbackList = () => {

    const nodePath = 'character.spirit.definition';
    if( ! waCache.guardianSpiritDrawbackList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                drawback: key,
                unlockable: value.unlockable // The available perks when leveling up
            };
        };

        waCache.guardianSpiritDrawbackList = buildListFromNodeChilds(mapMethod, nodePath, 'drawbacks');
    }
    return waCache.guardianSpiritDrawbackList;
}

/**
 * Guardian spirit perks definitions. For benefits and drawbacks.
 */
export const guardianSpiritPerkList = () => {

    const nodePath = 'character.spirit.definition';
    if( ! waCache.guardianSpiritPerkList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                perk: key, 
                icon: 'systems/acariaempire/resources/actor/sheet/spirit/' + key+ '.png',
                needs: value.needs ?? [],
                givesAbility : value.givesAbility ?? null,
                spiritManeuver : value.spiritManeuver ?? null
            };
        };

        waCache.guardianSpiritPerkList = buildListFromNodeChilds(mapMethod, nodePath, 'allPerks');
    }
    return waCache.guardianSpiritPerkList;
}

/**
 * Some perks gives access to new Explore and Intrigue ability.
 */
export const unlockableGuardianSpiritAbilityList = () => {

    if( ! waCache.unlockableGuardianSpiritAbilityList && guardianSpiritPerkList() ) {

        waCache.unlockableGuardianSpiritAbilityList = [];
        guardianSpiritPerkList().filter(p => {
            return p.givesAbility != null;
        }).forEach( perk => {
            const ability = mergeObject({perk: perk.key}, perk.givesAbility);
            if( !waCache.unlockableGuardianSpiritAbilityList.find( a => a.skill === ability.skill ) ) {
                waCache.unlockableGuardianSpiritAbilityList.push(ability);
            }
        });
    }
    return waCache.unlockableGuardianSpiritAbilityList;
}

/**
 * Some perks gives access to new maneuvers which could be used when doing a combat, explore or maneuver actions.
 * Maneuvers needs power to be used, and are used in correlation with another ability.
 */
export const guardianSpiritPerksWithManeuversList = () => {

    if( ! waCache.guardianSpiritPerksWithManeuversList && guardianSpiritPerkList() ) {

        waCache.guardianSpiritPerksWithManeuversList = [];
        guardianSpiritPerkList().filter(p => {
            return p.spiritManeuver != null;
        }).forEach( perk => {
            const maneuver = mergeObject({perk: perk.key}, perk.spiritManeuver);
            if( !waCache.guardianSpiritPerksWithManeuversList.find( m => m.maneuver === maneuver.maneuver ) ) {
                waCache.guardianSpiritPerksWithManeuversList.push(maneuver);
            }
        });
    }
    return waCache.guardianSpiritPerksWithManeuversList;
}

export const guardianSpiritManeuverList = () => {

    const nodePath = 'character.spirit.definition';
    if( ! waCache.guardianSpiritManeuverList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                maneuver: key
            };
        };

        waCache.guardianSpiritManeuverList = buildListFromNodeChilds(mapMethod, nodePath, 'spiritManeuvers');
    }
    return waCache.guardianSpiritManeuverList;
}

export const guardianSpiritTroubleEffectList = () => {

    const nodePath = 'character.spirit.cards';
    if( ! waCache.guardianSpiritTroubleEffectList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                positiveIcon: 'systems/acariaempire/resources/cards/trouble/effect/' + key+ '_positive.png',
                negativeIcon: 'systems/acariaempire/resources/cards/trouble/effect/' + key+ '_negative.png'
            };
        };

        waCache.guardianSpiritTroubleEffectList = buildListFromNodeChilds(mapMethod, nodePath, 'effects');
    }
    return waCache.guardianSpiritTroubleEffectList;

}

export const guardianSpiritTroubleQuantityList = () => {

    const nodePath = 'character.spirit.cards';
    if( ! waCache.guardianSpiritTroubleQuantityList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {
                max: value.max
            };
        };

        waCache.guardianSpiritTroubleQuantityList = buildListFromNodeChilds(mapMethod, nodePath, 'quantities');
    }
    return waCache.guardianSpiritTroubleQuantityList;
}

export const guardianSpiritTroubleLabelList = () => {

    const nodePath = 'character.spirit.cards';
    if( ! waCache.guardianSpiritTroubleLabelList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {};
        };

        waCache.guardianSpiritTroubleLabelList = buildListFromNodeChilds(mapMethod, nodePath, 'labels');
    }
    return waCache.guardianSpiritTroubleLabelList;
}

export const guardianSpiritTroubleStatusList = () => {

    const nodePath = 'character.spirit.cards';
    if( ! waCache.guardianSpiritTroubleStatusList && nodeIsRegistered(nodePath) ) {

        const mapMethod = ([key, value]) => {
            return {};
        };

        waCache.guardianSpiritTroubleStatusList = buildListFromNodeChilds(mapMethod, nodePath, 'status');
    }
    return waCache.guardianSpiritTroubleStatusList;
}

/** Used in many list. So stored independently */
const levelIcons = () => {
    return [0, 1, 2, 3, 4, 5, 6].map( level => {
        return 'systems/acariaempire/resources/capacities/levels/' + level+ '.png';
    });
};

const nodeIsRegistered = (nodePath) => {
    return getProperty(waCache.allEntries, nodePath) ? true : false;
}

const buildListFromNode = (mapMethod, nodePath) => {

    const node = getProperty(waCache.allEntries, nodePath);
    const list = Object.entries(node).map(([key, value]) => {
        const base = mapMethod( [key, value]);
        base.key = key;
        base.name = value.label;
        if( base.keepMapLinks ) {
            base.keepMapLinks = undefined;
        } else {
            base.articleId = value.articleId;
            base.shortcut = nodePath + '.' + key;
        }

        const desc = value.description;
        if(desc) { base.description = Array.isArray(desc) ? desc : [desc]; }

        return base;
    }).sort( (a,b) => a.name.localeCompare(b.name) );

    return list;
}

const buildListFromNodeChilds = (mapMethod, nodePath, childRootPath) => {

    const parentNode = getProperty(waCache.allEntries, nodePath);
    const node = getProperty(parentNode, childRootPath);
    const list = Object.entries(node).map(([key, value]) => {
        const base = mapMethod( [key, value]);
        base.key = key;
        base.name = value.label ?? parentNode.label;
        if( base.keepMapLinks ) {
            base.keepMapLinks = undefined;
        } else {
            base.articleId = value.articleId ?? parentNode.articleId;
            base.shortcut = nodePath;
            base.shortcutChild = childRootPath + '.' + key;
        }

        const desc = value.description;
        if(desc) { base.description = Array.isArray(desc) ? desc : [desc]; }

        return base;
    }).sort( (a,b) => a.name.localeCompare(b.name) );

    return list;
}


const retrieveEntries = (obj, res, parent = '') => {
    const keys = Object.keys(obj);

    /** Loop throw the object keys and check if there is any object there */
    keys.forEach(key => {
        const path = parent ? parent + '.' + key : key;
        const value = obj[key];

        if(value instanceof Array) {
            res[path] = contentForMyLang(game.i18n.lang, value);

        } else if( typeof obj[key] !== 'object') {
            // Should not happen. Leaf should all be arrays in our case
            throw 'Incorrect entry stucture here : ' + path;

        } else {
            // If object found then recursively call the function with updpated parent
            retrieveEntries(obj[key], res, path);
        }
    });
}

const contentForMyLang = (lang, property) => {

    const defaultShortcut = property.find(e => e.lang == 'fr');
    let result = property.find(e => e.lang === lang) ?? defaultShortcut;

    property.filter(e => e.additionalData == true).forEach(data => {

        const translated = data.translated == true;
        const entries = Object.entries(data).filter(e => ! ['additionalData', 'translated'].includes(e[0]) );

        entries.forEach(([key, value]) => {
            if(translated) {
                let child = {};
                retrieveEntries(value, child);
                Object.entries(child).forEach( ([childKey, childValue]) => {
                    result[key + '.' + childKey] = childValue;
                });
                
            } else {
                result[key] = value;
            }
        });
    });
    return duplicate(result);
}
