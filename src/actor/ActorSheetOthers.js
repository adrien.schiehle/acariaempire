import {translate} from '../tools/Translator.js';

/**
 * Prepare data for XP tab in actor sheet : giving XP to players
 * @param {AEActor} actor Displayed actor
 * @param {object} updatedData Sheet data. This tab related data will be stored inside updatedData.gui.xp
 */
export const dataForXP = (actor, updatedData) => {

    const xp = actor.xp;
    updatedData.gui.xp = {
        all: xp.all,
        attribute: xp.attribute,
        skill: xp.skill,
        power: xp.power,
        free: xp.free,
        granted: xp.granted,
        energy: translate( 'AESYSTEM.actor.spirit.xpResume' )
                    .replace('LEFT', '' + actor.guardianSpirit.energyLeft ) 
                    .replace('TOTAL', '' + actor.guardianSpirit.totalEnergyRetrieved ) 
    };
}

/**
 * Prepare data for Trouble tab in actor sheet
 * @param {AEActor} actor Displayed actor
 * @param {object} updatedData Sheet data. This tab related data will be stored inside updatedData.gui.troubles
 */
export const dataForTroubles = (actor, updatedData) => {

    const cards = actor.cards.troubles;
    updatedData.gui.troubles = {
        summary: translate('AESYSTEM.actor.sheet.troubles.summary').replace('NB', cards.length),
        cards: cards
    };
}

/**
 * Bind actions for Trouble tab.
 * @param {AEActorSheet} sheet parent
 * @param {HTMLElement} html Sheet content
 */
export const bindTroubleActions = ( sheet, html ) => {
    html.find(".troubles .key .trouble-show").click(event => onClickTroubleShow(sheet, event));
    if (sheet.options.editable) {
        html.find(".troubles .actions .trouble-discard").click(event => onClickTroubleDiscard(sheet, event));
    }
}

/**
 * Bind actions for XP tab.
 * @param {AEActorSheet} sheet parent
 * @param {HTMLElement} html Sheet content
 */
export const bindXPActions = ( sheet, html ) => {
    if (sheet.options.editable) {
        html.find('.ae-xp .xp-line .tokenButton').click(event => onClickManageCharacterXP(sheet, event));
    }
}

const onClickTroubleShow = async (sheet, event) => {

    event.preventDefault();
    const a = event.currentTarget;
    const troubleId = a.parentElement.parentElement.dataset.cardId;

    const trouble = sheet.actor.cards.troubles.find( t => t.card.id === troubleId );
    trouble?.card.sheet.render(true);
}

const onClickTroubleDiscard = async (sheet, event) => {

    event.preventDefault();
    const a = event.currentTarget;
    const troubleId = a.parentElement.parentElement.dataset.cardId;

    await sheet.actor.cards.discardTrouble([troubleId]);
    sheet.render(true);
}


const onClickManageCharacterXP = async (sheet, event) => {
    event.preventDefault();

    const a = event.currentTarget;
    const action = a.dataset.action;
    const xpType = a.dataset.xp;

    if( xpType == 'energy') { // Special case for energy -> It goes to the spirit
        if(  action == 'plus' ) {
            await sheet.actor.guardianSpirit.grantEnergy(1);
        } else {
            await sheet.actor.guardianSpirit.looseEnergy(1);
        }

    } else {
        await sheet.actor.grantXP(xpType, action == 'plus' ? 1 : -1);
    }
    
}

