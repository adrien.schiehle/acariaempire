import { COMBAT_CAPACITY_EFFECT } from '../tools/activeEffects.js';

export const BATTLE_METHOD = {
    melee: 'melee', 
    ranged: 'ranged', 
    improvised: 'improvised',
    arcane: 'arcane', 
    prayer: 'prayer', 
    songs: 'songs'
}

export const DEFENSE_METHOD = {
    melee: 'melee', 
    ranged: 'ranged', 
    magic: 'magic'
}

/**
 * Calculate advantage taking into account if the skill is known or not.
 * @param {int} combatLevel Actor current combat level
 * @param {int} attributeLevel Actor related attribute level
 * @param {boolean} known If the related skill is mastered
 * @returns advantagevalue
 */
 const baseAdvantageValue = ( combatLevel, attributeLevel, known ) => {
    let result = combatLevel + attributeLevel;
    if( !known ) { result -= 1; }
    return result;
}

/**
 * Calculate mastery taking into account if the skill is known or not.
 * @param {boolean} known If the related skill is mastered
 * @returns mastery value
 */
const baseMasteryValue = ( known ) => {
    return known ? 0 : -2;
}
/**
 * Calculate battle score while taking into account only attribute and abilities.
 * Other potential modifiers are ignored.
 * @param {AEActor} actor current actor 
 * @param {string} battleMethod battle method key
 * @returns {object} In the form of {initialAdvantage: int, initialMastery: int, maneuverMaxLevel: int}
 */
const initialOffenseBoost = (actor, battleMethod) => {

    // Retrieve related attribute
    let attributeKey;
    if( battleMethod == 'melee' ) { attributeKey = 'vigor'; }
    else if( battleMethod == 'ranged' ) { attributeKey = 'focus'; }
    else if( battleMethod == 'improvised' ) { attributeKey = 'versatility'; }
    else if( battleMethod == 'arcane' ) { attributeKey = 'perspicacity'; }
    else if( battleMethod == 'prayer' ) { attributeKey = 'socialEase'; }
    else if( battleMethod == 'songs' ) { attributeKey = 'versatility'; }
    const attributeLevel = actor.getAttributeLevel(attributeKey);

    const combatData = actor.system.abilities.combat;
    const combatLevel = combatData.level;
    const known = combatData.unlocked.includes(battleMethod);
    return {
        initialAdvantage: baseAdvantageValue(combatLevel, attributeLevel, known),
        initialMastery: baseMasteryValue(known),
        maneuverMaxLevel: known ? combatLevel : 0
    };
}


/**
 * Calculate battle score while taking into account only attribute and abilities.
 * Other potential modifiers are ignored.
 * @param {AEActor} actor current actor 
 * @param {string} defenseMethod Which way the actor defends itself
 * @returns {object} In the form of {initialAdvantage: int, initialMastery: int}
 */
 const initialDefenseBoost = (actor, defenseMethod) => {

    // Retrieve related attribute
    const abilities = [];
    if( defenseMethod == 'melee') { 
        abilities.push({skill: 'melee', attribute: 'vigor'});
        abilities.push({skill: 'improvised', attribute: 'versatility'}); 

    } else if( defenseMethod == 'ranged') { 
        abilities.push({skill: 'ranged', attribute: 'focus'});
        abilities.push({skill: 'improvised', attribute: 'versatility'});
    
    } else if( defenseMethod == 'magic') { 
        abilities.push({skill: 'arcane', attribute: 'perspicacity'});
        abilities.push({skill: 'prayer', attribute: 'socialEase'});
    }

    const combatData = actor.system.abilities.combat;
    const combatLevel = combatData.level;

    const choice = abilities.map( a => {
        const skillKey = a.skill;
        const attributeKey = a.attribute;

        const known = combatData.unlocked.includes(skillKey);
        const attributeLevel = actor.getAttributeLevel(attributeKey);
        const advantage = baseAdvantageValue(combatLevel, attributeLevel, known);
        const mastery = baseMasteryValue(known);
        return [advantage, mastery];

    }).reduce( (selected, current) => {
        // Priority is chosen
        const selectedSum = selected[0] + selected[1];
        const currentSum = current[0] + current[1];
        return selectedSum > currentSum ? selected : current;
    });

    return {
        initialAdvantage: choice[0],
        initialMastery: choice[1]
    };
}

/**
 * Done after each change inside actor.
 * Reload active effect list related to battle abilies.
 * @param {AEActorBattle} parent 
 * @returns TRUE if some effects have changed
 */
 const refreshActorDataWithCurrentBattleAbilities = async (parent) => {

    // Convenience funtions
    //----------------------
    const currentEffects = parent.effects;
    const newActiveEffects = [];
    const removedActiveEffectIds = [];

    const actor = parent._actor;

    const manageBattleEffects = ({mode, check, changes=[], parameters={}}) => {
        const currentOnes = currentEffects.filter( e => e.mode === mode );
        const removingEffects = currentOnes.length > 1 || ( currentOnes.length == 1 && !check( currentOnes[0]) );
        const addingNewEffect = removingEffects ? !check( null ) : !check( currentOnes[0] );
    
        if( removingEffects ) { 
            currentOnes.forEach( e => removedActiveEffectIds.push( e.effectId) ); 
        }
        if( addingNewEffect ) { 
            newActiveEffects.push( 
                activeEffectDataForBattle(mode, changes, parameters)
            ); 
        }
    }

    /**
     * Build an Active effect that will alter actor offensive/defensive capacities
     * Also remove old one if it has changed.
     * Data are stored inside newActiveEffects and removedActiveEffectIds and will be handled in one go at the end of this method
     * @param {string} style 'offense' or 'defense'
     * @param {string} type For offense : The combat ability key, For defense : 'melee', 'ranged' or 'magic'
     * @param {object[]} abilities Abilitis which can be used for this type of defense. (Will choose the better one)
     */
     const combatEffect = ( style, type ) => {

        const newParameters = {
            style: style,
            type: type
        };
        const newValue = style + ':' + type;

        const effectIsCorrect = (effect) => {
            const oldValue = (effect?.parameters?.style ?? '') + ':' + (effect?.parameters?.type ?? '');
            return oldValue === newValue;
        };

        manageBattleEffects({
            mode: style + '.' + type,
            check: effectIsCorrect,
            changes: [{
                mode: CONST.ACTIVE_EFFECT_MODES.CUSTOM,
                key: COMBAT_CAPACITY_EFFECT.key, 
                priority: COMBAT_CAPACITY_EFFECT.priority, 
                value: style + ':' + type
            }],
            parameters: newParameters
        });
    }

    // Offense effects (one for each skill)
    combatEffect( 'offense', 'melee' );
    combatEffect( 'offense', 'ranged' );
    combatEffect( 'offense', 'improvised' );
    combatEffect( 'offense', 'arcane' );
    combatEffect( 'offense', 'prayer' );
    combatEffect( 'offense', 'songs' );

    // Defense effects ( melee, ranged, magic )
    combatEffect( 'defense', 'melee' );
    combatEffect( 'defense', 'ranged' );
    combatEffect( 'defense', 'magic' );

    // Refresh active effects
    const removingEffects = removedActiveEffectIds.length > 0;
    const addingNewffects = newActiveEffects.length > 0;
    if( removingEffects ) { await actor.deleteEmbeddedDocuments("ActiveEffect", removedActiveEffectIds ); }
    if( addingNewffects ) { await actor.createEmbeddedDocuments("ActiveEffect", newActiveEffects ); }

    return removingEffects || addingNewffects;
}

/**
 * Build ActiveEfectData for one of the battle modes and the wanted modifiers
 * @param {string} mode A key for one of the effect type . ex: offense.melee
 * @param {object[]} modifiers What will be modified on character
 * @param {object[]} parameters Optional. Some infos that will be added in the guardianSpirit flag
 * @returns {ActiveEfectData} data that could be used to create a new Active effect on actor
 */
 const activeEffectDataForBattle = (mode, modifiers, parameters = {}) => {

    const battle = mergeObject( {mode: mode}, parameters );
    const activeEffectData = {
        label: 'Battle modifier from skills', // Won't be displayed. So no need to have a translated label.
        // icon: xxxx , No need for an icon either
        changes: modifiers,
        flags: {
            acariaempire : {
                battle: battle
            }
        }
    };
    return activeEffectData;
}



/**
 * For managing battles capacities of an actor
 * Doesn't contain state variables. Only here for convenience use.
 */
export class AEActorBattle {

    constructor(actor) {

        this._actor = actor;
    }

    get actorData() {

        return this._actor.system;
    }

    get melee() {
        return this.forOffense(BATTLE_METHOD.melee);
    }

    get ranged() {
        return this.forOffense(BATTLE_METHOD.ranged);
    }

    get improvised() {
        return this.forOffense(BATTLE_METHOD.improvised);
    }

    get arcane() {
        return this.forOffense(BATTLE_METHOD.arcane);
    }

    get prayer() {
        return this.forOffense(BATTLE_METHOD.prayer);
    }

    get songs() {
        return this.forOffense(BATTLE_METHOD.songs);
    }

    forOffense(skillType) {
        return mergeObject( 
            initialOffenseBoost(this._actor, skillType),
            this.actorData.battle.offense[skillType]
        );
    }

    forDefense(defenseMethod) {
        return mergeObject( 
            initialDefenseBoost(this._actor, defenseMethod),
            this.actorData.battle.defense[defenseMethod]
        );
    }

    /**
     * Reload all guardian spirit effects
     * @returns TRUE if some change were done
     */
    async reloadEffects() {
        return refreshActorDataWithCurrentBattleAbilities(this);
    }

    get effects() {
        const allEffects = this._actor.effects ?? [];
        return allEffects.filter( e => {
            return e.getFlag('acariaempire', 'battle');
        }).map( e => {
            const dataInFlag = e.getFlag('acariaempire', 'battle');
            const mode = dataInFlag.mode;

            return {
                effectId: e.id,
                mode: mode,
                effect: e,
                parameters: dataInFlag
            };
        });
    }
}
