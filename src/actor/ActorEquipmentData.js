import { translate } from '../tools/Translator.js'
import { AEEquipmentItem } from '../items/EquipmentItem.js';
import { AEArmorItem } from '../items/ArmorItem.js';
import { AEWeaponItem } from '../items/WeaponItem.js';
import { AEMiscellaneousItem } from '../items/MiscellaneousItem.js';
import { commonWeaponList } from '../tools/WaIntegration.js';
import { modifyDamageWithAffixes } from '../tools/affixes.js';
import { MANEUVER_EFFECT, OVERLOAD_EFFECT } from '../tools/activeEffects.js';
import { EFFECT_PRIORITY } from '../tools/constants.js';

const EQUIPPABLE_SLOTS = [
    'head', 'back', 'weapon', 'left_hand', 'chest', 'hands', 
    'neck', 'feet', 'ring_1', 'ring_2'
];

/**
 * Done after each actor change.
 * Reload active effect list related to equipment. (Mainly armor overload for now)
 * @param {AEActorEquipmentData} parent 
 * @returns TRUE if some effects have changed
 */
const refreshActorDataWithCurrentEquipment = async (parent) => {


    // Convenience funtions
    //----------------------
    const currentEffects = parent.effects;
    const newActiveEffects = [];
    const removedActiveEffectIds = [];

    const manageEquipementEffects = ({item, check, changes=[], parameters={}}) => {
        const currentOnes = currentEffects.filter( e => e.itemId === item.id );
        const removingEffects = currentOnes.length > 1 || ( currentOnes.length == 1 && !check( currentOnes[0]) );
        const addingNewEffect = removingEffects ? !check( null ) : !check( currentOnes[0] );
    
        if( removingEffects ) { 
            currentOnes.forEach( e => removedActiveEffectIds.push( e.effectId) ); 
        }
        if( addingNewEffect ) { 
            newActiveEffects.push( 
                activeEffectDataFromItem(item, changes, parameters)
            ); 
        }
    }
    
    const actor = parent._actor;

    // Removing effects from equipment that are not equipped anymore.
    //---------------------------------
    // - Items can't be dropped when they are equipped. No need to call this method when a item is dropped
    const currentEquipmentList = parent.all.filter( e => e.isEquipped );
    const currentEquipmentId = currentEquipmentList.map( e => e.item.id );
    currentEffects.forEach( effect => {
        if( !currentEquipmentId.includes( effect.itemId ) ) {
            removedActiveEffectIds.push( effect.effectId );
        }
    });

    // Weapons
    const weaponList = currentEquipmentList.filter( a => a.item.isWeapon );
    weaponList.forEach( weapon => {

        const maneuverModifiers = weapon.affixes.reduce( (modifiers, current) => {
            return modifiers.concat( current.weaponUpgrade.maneuverModifiers );
         }, []);
        const maneuverString = JSON.stringify(maneuverModifiers);
        const effectIsCorrect = (effect) => {
            const maneuvers = effect?.parameters?.maneuverModifiers;
            return maneuverString === JSON.stringify(maneuvers);
        }

        const changes = [];
        for( const modifier of maneuverModifiers ) {
            const maneuverPath = modifier.category + '.maneuvers.' + modifier.maneuver;
            const valueString = maneuverPath + ':' + ( modifier.advantage ?? 0 ) + ':' + ( modifier.mastery ?? 0 );
            changes.push({
                mode: CONST.ACTIVE_EFFECT_MODES.CUSTOM, 
                key: MANEUVER_EFFECT.key,
                value: valueString, 
                priority: EFFECT_PRIORITY.classic
            });
        }

        manageEquipementEffects({
            item: weapon.item,
            check: effectIsCorrect,
            changes: changes,
            parameters: {
                maneuverModifiers: maneuverModifiers
            }
        });
    });

    // Armors
    const armorList = currentEquipmentList.filter( a => a.item.isArmor );
    armorList.forEach( armor => {

        const limiter = armor.limiter;
        const limiterString = JSON.stringify(limiter);
        
        const protections = armor.protections;
        const protectionsString = JSON.stringify(protections);

        const elementResistances = armor.elementResistances;
        const elementResistString = JSON.stringify(elementResistances);


        const combatModifiers = armor.combatModifiers;
        const combatModifiersString = JSON.stringify(combatModifiers);

        const effectIsCorrect = (effect) => {
            const effectLimiter =  effect?.parameters?.limiter ?? { apply:false, attribute: '', step: 99 };
            const effectProtect = effect?.parameters?.protections ?? [];
            const effectResists = effect?.parameters?.elementResistances ?? [];
            const effectCombat = effect?.parameters?.combatModifiers ?? [];

            return limiterString === JSON.stringify(effectLimiter)
                && protectionsString === JSON.stringify(effectProtect)
                && elementResistString === JSON.stringify(effectResists)
                && combatModifiersString === JSON.stringify(effectCombat);
        };

        const changes = [];
        if( limiter.apply ) {
            const value = limiter.attribute + ':' + limiter.step;
            changes.push({
                mode: CONST.ACTIVE_EFFECT_MODES.CUSTOM, 
                key: OVERLOAD_EFFECT.key, 
                value: value, 
                priority: OVERLOAD_EFFECT.priority
            });
        }
        protections.forEach( p => {
            changes.push({
                mode: CONST.ACTIVE_EFFECT_MODES.UPGRADE, 
                key: 'system.elements.' + p.element + '.armor', 
                value: p.value, 
                priority: EFFECT_PRIORITY.upgrade
            });
        });
        elementResistances.forEach( r => {
            changes.push({
                mode: CONST.ACTIVE_EFFECT_MODES.UPGRADE, 
                key: 'system.elements.' + r.element + '.resist', 
                value: r.value, 
                priority: EFFECT_PRIORITY.upgrade
            });
        });
        combatModifiers.forEach( m => {
            const baseKey = 'system.battle.' + m.on + '.' + m.type;
            if( m.advantage ) {
                changes.push({
                    mode: CONST.ACTIVE_EFFECT_MODES.ADD, 
                    key: baseKey + '.advantage',
                    value: m.advantage, 
                    priority: EFFECT_PRIORITY.classic
                });
            }
            if( m.mastery ) {
                changes.push({
                    mode: CONST.ACTIVE_EFFECT_MODES.ADD, 
                    key: baseKey + '.mastery',
                    value: m.mastery, 
                    priority: EFFECT_PRIORITY.classic
                });
            }
        });

        manageEquipementEffects({
            item: armor.item,
            check: effectIsCorrect,
            changes: changes,
            parameters: {
                limiter: limiter, 
                protections: protections, 
                elementResistances: elementResistances,
                combatModifiers: combatModifiers
            }
        });


    });

    // Refresh active effects
    const removingEffects = removedActiveEffectIds.length > 0;
    const addingNewffects = newActiveEffects.length > 0;
    if( removingEffects ) { await actor.deleteEmbeddedDocuments("ActiveEffect", removedActiveEffectIds ); }
    if( addingNewffects ) { await actor.createEmbeddedDocuments("ActiveEffect", newActiveEffects ); }

    return removingEffects || addingNewffects;
}


/**
 * Build ActiveEfectData from an equipment and the wanted modifiers
 * @param {string} itemId The item id. For finding it back
 * @param {object[]} modifiers What will be modified on character
 * @param {object[]} parameters Optional. Some infos that will be added in the guardianSpirit flag
 * @returns {ActiveEfectData} data that could be used to create a new Active effect on actor
 */
const activeEffectDataFromItem = (item, modifiers, parameters = {}) => {

    const equipment = mergeObject( {item: item.id}, parameters );
    const activeEffectData = {
        label: item.name,
        icon: item.img,
        changes: modifiers,
        flags: {
            acariaempire : {
                equipment: equipment
            }
        }
    };
    return activeEffectData;
}

/**
 * For managing arcane circles during scenes
 */
export class AEActorEquipmentData {

    constructor(actor) {

        this._actor = actor;
    }

    /** @returns List of owned cards as Item. */
    get asItems() {
        return this._actor.items.filter(item => item.isEquipment);
    }

    /** @returns {AEWeaponItem[]} All weapon items */
    get weapons() {
        return this._actor.items.filter(item => {
            return item.isWeapon;
        }).map(item => {
            return item.asWeapon();
        });
    }

    /** @returns {AEArmorItem[]} All armor items */
    get armors() {
        return this._actor.items.filter(item => {
            return item.isArmor;
        }).map(item => {
            return item.asArmor();
        });
    }

    /** @returns {AEMiscellaneousItem[]} All misc items */
    get others() {
        return this._actor.items.filter(item => {
            return item.isMisc;
        }).map(item => {
            return item.asMisc();
        });
    }

    /** @returns {AEEquipmentItem[]} All equipment items */
    get all() {
        let result = [];
        result = result.concat(this.weapons);
        result = result.concat(this.armors);
        result = result.concat(this.others);
        return result;
    }

    /** Similar info as if it was a weapon attack */
    get barehandedAttack() {
        if( ! this._barehandedAttack ) {
            const def = commonWeaponList().find( w => w.key === 'barehanded' );
            this._barehandedAttack = mergeObject(
                { damage : modifyDamageWithAffixes(def.size.baseDamage, []) },
                def
            );
        }
        return this._barehandedAttack;        
    }

    get currentlyEquipped() {

        const result = {};
        EQUIPPABLE_SLOTS.forEach( s => {
            result[s] = this.itemOnSlot(s)
        });
        return result;
    }

    get mainWeapon() {
        return this.itemOnSlot('weapon');
    }

    get currentArmorProtection() {
        return Object.entries(this._actor.system.elements).map( ([element, data]) => {
            return {
                element: element,
                value: data.armor
            };
        });
    }

    /** @param {*} slot : See EQUIPPABLE_SLOTS. */
    itemOnSlot(slot) {

        if( ['weapon', 'left_hand'].includes(slot) ) {

            const equippedWeapons = this.weapons.filter(w => w.isEquipped);
            const mainGauche = equippedWeapons.length >=2 ? equippedWeapons.find(w => w.isAllowedOnLeftHand) : null;
            if(slot === 'left_hand' && mainGauche) { 
                return mainGauche; 
            }
            if(slot === 'weapon') {
                return equippedWeapons.find(w => w != mainGauche) ?? null;
            }
        } else if( ['ring_1', 'ring_2'].includes(slot) ) {
            const firstRing = this.others.find( o => o.isEquipped && o.slot.key === 'fingers' );
            if( slot == 'ring_1') {
                return firstRing;
            } else {
                return this.others.find( o => o.isEquipped && o.slot.key === 'fingers' && o.item.id != firstRing?.item.id );
            }
        }

        return this.all.find(i => i.slot.key === slot && i.isEquipped) ?? null;
    }

    async equipItem(equipment) {

        // Some verifications
        if( !equipment || equipment.item.actor.id != this._actor.id ) { return ui.notifications.warn(translate('AESYSTEM.item.sheet.equipment.notOwned')); }
        if( equipment.isEquipped ) { return ui.notifications.info( translate('AESYSTEM.item.sheet.equipment.alreadyEquipped') ); }

        if ( equipment.slot.key == 'weapon') {
            await this.itemOnSlot('weapon')?._equip(false); // Do not unequip left hand

        } else if ( equipment.slot.key == 'left_hand') {
            await this.itemOnSlot('left_hand')?._equip(false); // Weapon have a basic slot : weapon. But they still should be unequipped when equipping a shield

        } else if ( equipment.slot.key == 'fingers') {
            await this.itemOnSlot('ring_2')?._equip(false);

        } else {
            const equipped = this.all.find(e => e.isEquipped && e.slot.key == equipment.slot.key);
            await equipped?._equip(false);
        }

        await equipment._equip(true);
    }

    async equipOnSecondHand(equipment) {

        // Some verifications
        if( !equipment || equipment.item.actor.id != this._actor.id ) { return ui.notifications.warn(translate('AESYSTEM.item.sheet.equipment.notOwned')); }
        if( equipment.isEquipped ) { return ui.notifications.info(translate('AESYSTEM.item.sheet.equipment.alreadyEquipped')); }

        const onLeftHand = this.currentlyEquipped.left_hand;
        if(onLeftHand) {
            await onLeftHand._equip(false);
        }

        await equipment._equip(true);
    }

    async unequipItem(equipment) {

        // Some verifications
        if( !equipment || equipment.item.actor.id != this._actor.id ) { return ui.notifications.warn(translate('AESYSTEM.item.sheet.equipment.notOwned')); }
        if( !equipment.isEquipped ) { return ui.notifications.info(translate('AESYSTEM.item.sheet.equipment.alreadyNotEquipped')); }

        await equipment._equip(false);
    }

    /*------------------------------------------
        Equipment active effects (mainly overloads for now)
    --------------------------------------------*/

    async reloadEffects() {
        return refreshActorDataWithCurrentEquipment(this);
    }

    get effects() {
        const allEffects = this._actor.effects ?? [];
        return allEffects.filter( e => {
            return e.getFlag('acariaempire', 'equipment');
        }).map( e => {
            const dataInFlag = e.getFlag('acariaempire', 'equipment');
            const itemId = dataInFlag.item;
            const item = this.asItems.find( i => i.id === itemId );

            return {
                effectId: e.id,
                itemId: itemId,
                effect: e,
                item: item,
                parameters: dataInFlag
            };
        });
    }


}
