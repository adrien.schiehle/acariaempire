import { ALL_CARDS_TYPES, TROUBLE_STATUS } from '../tools/constants.js';

/**
 * If some spirits are retrieved when n communion is present, we create one.
 * @param {AECardsData} parent the instance
 */
const createCommunionIfNone= async (parent) => {
    const divine = parent._actor.divine;
    const communion = divine.communion; 

    if( communion.spiritPower == 99 ) {

        const divineMastery = parent._actor.battle.prayer.mastery;
        await divine.alterCommunion({currentSpiritMaxPower: divineMastery});
    }
};

/**
 * For managing arcane circles during scenes
 */
export class AECardsData {

    constructor(actor) {

        this._actor = actor;

        const cardStacks = game.modules.get('ready-to-use-cards').cardStacks;
        this._playerHand = null;
        this._playerPile = null;

        const players = this._actor.playersOwningIt;
        if( players.length == 0 ) {
            this._playerHand = cardStacks.gmHand;
            this._playerPile = cardStacks.gmRevealedCards;
        } else {
            this._playerHand = cardStacks.findPlayerHand(players[0]);
            this._playerPile = cardStacks.findRevealedCards(players[0]);
        }
    }

    /** @returns List of owned event cards as AEEventCard.  */
    get events() {
        return this._playerHand.stack.cards.filter(c => c.type === ALL_CARDS_TYPES.event).map( c => c.impl );
    }

    /** @returns List of owned spirit cards as AEEventCard.  */
    get spirits() {
        return this._playerPile.stack.cards.filter(c => c.type === ALL_CARDS_TYPES.event).map( c => c.impl );
    }

    /** @returns List of owned spirit cards as AEEventCard.  */
    get troubles() {
        return this._playerPile.stack.cards.filter(c => c.type === ALL_CARDS_TYPES.trouble).map( c => c.impl );
    }

    /**
     * Draw some spirit cards and store them as embedded entities
     * @param {int} number The amount of spirits that should be drawn. By default: 1
     * @returns An array of all drawn cards
     */
    async drawSpirits(number=1) {
        
        const cardStacks = game.modules.get('ready-to-use-cards').cardStacks;
        const cards = await this._playerPile.drawCards(cardStacks.decks.event, number);
        await createCommunionIfNone(this);
        return cards;
    }

    /** 
     * Draw some event cards and store them as embedded entities
     * @returns The embeded entities as Card
    */
     async drawEvents(number=1) {
        const cardStacks = game.modules.get('ready-to-use-cards').cardStacks;
        return this._playerHand.drawCards(cardStacks.decks.event, number);
    }

    /** 
     * Draw some trouble cards and store inside the revealed cards
     * Also update TroubleCard with status set to negative if needed
     * @returns {AECard[]} The drawn cards
    */
     async drawTroubles(number=1) {

        const cardStacks = game.modules.get('ready-to-use-cards').cardStacks;
        const drawnCards = await this._playerPile.drawCards(cardStacks.decks.trouble, number);
        
        // Set the status position to negative if needed
        const troubleData = this._actor.system.guardianSpirit.troubles;
        const hunger = troubleData.hunger.value;
        const restlessness = troubleData.restlessness.value;
        for( const card of drawnCards ) {
            const trouble = card.impl;
            if( trouble.step <= hunger || trouble.step <= restlessness ) {
                await trouble.changeStatus(TROUBLE_STATUS.negative);
            }
        }

        return drawnCards;
    }

    /**
     * Discard some spirits and send a message telling which ons have been discarded
     * @param {object[]} cardIds A list of all spirit cards that should be discarded. Cardsm which are not owned or aren't a spirit will be ignored
     * @returns An array of all discarded cards
     */
    async discardSpirits(cardIds) {
        return this._playerPile.discardCards(cardIds);
    }

    /** Some actions require to discards all spirits in one go */
    async discardAllSpirits() {
        const cardIds = this.spirits.map( c => c.card.id );
        return this._playerPile.discardCards(cardIds);
    }

    /**
     * Discard some trouble cards
     * @param {string[]} troubleIds Trouble card Ids
     */
    async discardTrouble(troubleIds) {
        return this._playerPile.discardCards(troubleIds);
    }


    /** Shortcut to display play Hand */
    renderDrawnSpirits() {
        this._playerPile.stack.sheet.render(true);
    }

    /** Shortcut to display play Hand */
    renderDrawnEvents() {
        this._playerHand.stack.sheet.render(true);
    }

}
