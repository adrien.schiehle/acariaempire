import {translate} from '../tools/Translator.js';
import {spritDisplayedOnActorSheet} from '../tools/gmactions.js';

/** @returns Liste of available choices for this actor */
export const loadMenuChoices = (actor) => {

    let result = [];
    const gm = game.user.isGM;
    const gmActor = actor.isGMActor;
    if( !gmActor ) {

        const minimumRights = gm || actor.testUserPermission(game.user, CONST.DOCUMENT_PERMISSION_LEVELS.LIMITED);
        if( minimumRights ) {
            result.push('description');
        }

        // Actor details available for owned characters
        const canSeeDetails = gm || actor.testUserPermission(game.user, CONST.DOCUMENT_PERMISSION_LEVELS.OBSERVER);
        if( canSeeDetails ) {
            result.push('attributes', 'abilities', 'equipments', 'bags', 'melee', 'ranged', 'improvised', 'arcane', 'divine', 'songs');
        }

        // Spirit related stuff are only here is the player has awoken
        const guardianSpiritDisplayed = canSeeDetails && spritDisplayedOnActorSheet() && !actor.isMonster;
        if( guardianSpiritDisplayed ) {
            result.push('spirit', 'troubles');
        }

        // Tab only available for monsters
        const monsterDetails = canSeeDetails && actor.isMonster;
        if( monsterDetails ) {
            result.push('monster');
        }

        // NPC can have a shop
        const merchantDetails = actor.isNPC && ( gm || actor.merchant.hasShop );
        if( merchantDetails ) {
            result.push('merchant');
        }

        // GMs can also manage character XP
        if( gm ) { 
            result.push('xp'); 
        }
    }

    return result;
}

const customNavigatorLine = (line, selection) => {
    return {
        choice: line,
        cssClass: line != selection ? '' : ' active',
        label: translate('AESYSTEM.actor.sheet.tab.' + line),
        icon: 'systems/acariaempire/resources/actor/sheet/tab/' + line + '.png'
    };
}

export const customNavigator = (menuChoices, selection, position) => {

    const result = {
        nav: menuChoices.map( line => customNavigatorLine(line, selection) ),
        tabs: {},
        size: {
            maxheight: position.height - 200
        }
    };

    menuChoices.forEach( line => result.tabs[line] = line === selection );
    return result;
}


export const dataForActorHeader = (actor, updatedData, headerData) => {

    // XP details change on toggle
    const TOKEN_IMG = {
        advantage: 'systems/acariaempire/resources/actor/tokens/success.png',
        mastery: 'systems/acariaempire/resources/actor/tokens/chance.png',
        power: 'systems/acariaempire/resources/actor/tokens/power.png',
        health: 'systems/acariaempire/resources/actor/tokens/health.png',
        xp: 'systems/acariaempire/resources/actor/sheet/xp_all.png',
        energy: 'systems/acariaempire/resources/actor/sheet/xp_energy.png'
    };
    const TOKEN_LABEL = {
        min : translate('AESYSTEM.actor.sheet.header.token.min'),
        current : translate('AESYSTEM.actor.sheet.header.token.current'),
        max : translate('AESYSTEM.actor.sheet.header.token.max'),
        free : translate('AESYSTEM.actor.sheet.header.token.free'),
        attribute : translate('AESYSTEM.actor.sheet.header.token.attribute'),
        skill : translate('AESYSTEM.actor.sheet.header.token.skill'),
        power : translate('AESYSTEM.actor.sheet.header.token.power'),
        available : translate('AESYSTEM.actor.sheet.header.token.available'),
        accumulated : translate('AESYSTEM.actor.sheet.header.token.accumulated'),
        damage : translate('AESYSTEM.actor.sheet.header.token.damage'),
        fatigue : translate('AESYSTEM.actor.sheet.header.token.fatigue')
    };

    const fatigue = actor.currentFatigue;
    const realMaxHealth = actor.system.health.max + fatigue;
    const maxHealthDesc =  '' + realMaxHealth + ( !fatigue ? '' : ' => ' + actor.system.health.max );

    const allTokens = [
        {id:'advantage', onLeft: true, img: TOKEN_IMG.advantage, value: actor.advantage, 
            onSelected: [
                { title: TOKEN_LABEL.min, value: actor.system.advantage.min },
                { title: TOKEN_LABEL.current, value: actor.advantage, editName: 'system.advantage.value' },
                { title: TOKEN_LABEL.max, value: actor.system.advantage.max }
            ]},
        {id:'mastery', onLeft: true, img: TOKEN_IMG.mastery, value: actor.mastery, 
            onSelected: [
                { title: TOKEN_LABEL.min, value: actor.system.mastery.min },
                { title: TOKEN_LABEL.current, value: actor.mastery, editName: 'system.mastery.value' },
                { title: TOKEN_LABEL.max, value: actor.system.mastery.max }
            ]},
        {id:'power', onLeft: true, img: TOKEN_IMG.power, value: actor.power, 
            onSelected: [
                { title: TOKEN_LABEL.current, value: actor.power, editName: 'system.power.value' },
                { title: TOKEN_LABEL.max, value: actor.system.power.max }
            ]},
        {id:'health', onLeft: false, img: TOKEN_IMG.health, value: actor.health, 
            onSelected: [
                { title: TOKEN_LABEL.damage, value: actor.health, editName: 'system.health.value' },
                { title: TOKEN_LABEL.fatigue, value: fatigue, editName: 'system.health.fatigue' },
                { title: TOKEN_LABEL.max, value: maxHealthDesc }
            ]},
        {id:'spirit', onLeft: false, img: TOKEN_IMG.energy, value: actor.guardianSpirit.energyLeft, 
            onSelected: [
                { title: TOKEN_LABEL.available, value: actor.guardianSpirit.energyLeft },
                { title: TOKEN_LABEL.accumulated, value: actor.guardianSpirit.totalEnergyRetrieved }
            ]},
        {id:'xp', onLeft: false, img: TOKEN_IMG.xp, value: actor.xp.all , 
            onSelected: [
                { title: TOKEN_LABEL.free, value: actor.xp.free },
                { title: TOKEN_LABEL.attribute, value: actor.xp.attribute },
                { title: TOKEN_LABEL.skill, value: actor.xp.skill },
                { title: TOKEN_LABEL.power, value: actor.xp.power }
            ]}
    ];

    // Add some additional possibility for xp tab depending on how many xp is leftfor this actor
    const xpTokens = allTokens.find( t => t.id == 'xp');
    const xpOnSelected = xpTokens.onSelected.filter( s => s.value != 0 );
    if( xpOnSelected.length == 0 ) {
        xpOnSelected.push( { title: TOKEN_LABEL.current, value: actor.xp.all } );
    }
    if( xpOnSelected.length < 4 ) {
        xpOnSelected.push( { title: TOKEN_LABEL.accumulated, value: actor.xp.granted } );
    }
    xpTokens.onSelected = xpOnSelected;


    allTokens.forEach( t => {
        // Retrieve current selection
        t.selected = t.id === headerData.selectedIcon;
        t.css = t.selected ? 'selected' : '';

        // Add editable boolean on each selectable value
        t.onSelected = t.onSelected ?? [];
        t.onSelected.forEach( s => {
            s.editable = s.editName ? true : false;
        });
    })
    const currentSelection = allTokens.find( t => t.selected );

    // Tokens
    updatedData.gui.header = {
        background: actor.isMonster ? 'monster' : 'human',
        leftTokens: allTokens.filter( t => t.onLeft ),
        bottomTokens: allTokens.filter( t => !t.onLeft ),
        displaySelected: currentSelection ? true : false,
        tokenDisplay: currentSelection?.onSelected ?? []
    };
}

/**
 * Bind navigation buttons.
 * @param {AEActorSheet} sheet parent
 * @param {HTMLElement} html Sheet content
 */
 export const bindHeaderButtons = ( sheet, html ) => {

    html.find(".sheet-header .ae-token").click(event => onClickDisplayHeaderDetails(sheet, event));
    html.find(".main-nav .nav-item").click(event => onClickMenuNav(sheet, event));
}

const onClickMenuNav = async (sheet, event) => {

    event.preventDefault();
    const a = event.currentTarget;
    const action = a.dataset.tab;

    sheet._currentMenu = action;
    sheet.object.render();
}

/** Event for toggling a header details (May be health, advantage, xp, etc..) **/
const onClickDisplayHeaderDetails = async (sheet, event) => {
    event.preventDefault();

    const a = event.currentTarget;
    const id = a.dataset.id;
    const wasSelected = sheet._displayData.header.selectedIcon === id;
    sheet._displayData.header.selectedIcon = wasSelected ? null : id;
    sheet.object.render();
}
