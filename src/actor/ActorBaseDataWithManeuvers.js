import {translate} from '../tools/Translator.js';
import { maneuverLevels } from '../tools/WaIntegration.js';
import {retrieveXP, giveXP} from './actor.js';

/**
 * For managing attack abilities with maneuvers. Should be extended
 */
export class AEActorBaseDataWithManeuvers {

    constructor(actor, attackScore, listingManeuverMethod, storingSuffix) {

        this._actor = actor;
        this._attackScore = attackScore;
        this._listingManeuverMethod = listingManeuverMethod;
        this._storingSuffix = storingSuffix;
    }

    get actorData() {

        return this._actor.system;
    }

    get attackScore() {
        return this._attackScore;
    }

    get maneuvers() {

        const maxLevel = this.attackScore.maneuverMaxLevel;
        const base = getProperty(this.actorData, this._storingSuffix)?.maneuvers ?? {};

        const maneuvers = this._listingManeuverMethod().map(m => {

            const node = getProperty(base, m.maneuver);

            const xp = node?.xp ?? 0;
            const level = node?.level ?? 0;
            const def = maneuverLevels().find( m => m.level === level );

            const data = {
                level: mergeObject({
                    xp: xp,
                    maxLevel: maxLevel,
                    icon: m.levelIcons[level]
                }, def),
                advantageModifier: def.interaction.advantageModifier,
                masteryModifier: def.interaction.masteryModifier
            };
            data.level.interaction = null; // Unlinking interaction that have childs

            // Add potential activeEffects
            if( node?.activeEffects ) {
                data.advantageModifier += node.activeEffects.advantage;
                data.masteryModifier += node.activeEffects.mastery;
            }

            return [m.maneuver, mergeObject(data, m)];
        });

        return maneuvers;
    }

    /**
     * @returns A maneuver definition, as described in songsManeuvers(), with advantage and mastery modifiers in addition
     */
     getManeuver(maneuverName) {
        for( let [key, maneuver] of this.maneuvers ) {
            if( key === maneuverName ) {
                return maneuver;
            }
        }
        throw maneuverName + ' is not a valid maneuver';
    }

    async spendXPToManeuver(maneuverName) {

        this._actor._assertIsOwner();

        const updateData = {};
        const maneuver = this.getManeuver(maneuverName);


        // Verif on max level
        const currentXP = maneuver?.level?.xp ?? 0;
        const currentLevel = maneuver?.level?.level ?? 0;
        if( currentLevel >= this.attackScore.maneuverMaxLevel ) {
            return ui.notifications.info(translate('AESYSTEM.actor.xp.' + this._storingSuffix + '.cappedByMastery'));
        }

        // Build updateData
        const willLevelUp = currentXP >= currentLevel;
        const data = {
            xp: willLevelUp ? 0 : currentXP + 1,
            level: willLevelUp ? currentLevel + 1 : currentLevel
        };

        retrieveXP(this._actor, 'power', updateData, 1);
        updateData['system.' + this._storingSuffix + '.maneuvers.' + maneuverName] = data;
        await this._actor.update(updateData);
    }

    async forgetManeuver(maneuverName) {
        
        this._actor._assertGMRemovingXP()

        const updateData = {};
        const maneuver = this.getManeuver(maneuverName);

        const currentXP = maneuver?.level?.xp ?? 0;
        const currentLevel = maneuver?.level?.level ?? 0;

        let givenXP = currentLevel * (currentLevel+1) / 2; // n.(n+1) / 2
        givenXP += currentXP;
        
        giveXP(this._actor, 'power', updateData, givenXP);
        updateData['system.' + this._storingSuffix + '.maneuvers.' + maneuverName] = {
            xp: 0,
            level: 0
        };
        await this._actor.update(updateData);
    }
}
