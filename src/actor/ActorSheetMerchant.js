import { AEEquipmentItem } from '../items/EquipmentItem.js';
import { askGMToBuyItem, askGMToSellItem } from '../sockets/MerchantTransactions.js';
import { translate } from '../tools/Translator.js';
import { attributeList } from '../tools/WaIntegration.js';

const SHOPTAB_CHOICE = {
    setup: 'setup',
    sell: 'sell',
    buy: 'buy',
    noShop: 'noShop'
};

/**
 * Prepare data for Guardian spirit tab in actor sheet
 * @param {AEActor} actor Displayed actor
 * @param {object} updatedData Sheet data. This tab related data will be stored inside updatedData.gui.merchant
 * @param {object} sheetState Where current tab state is stored
 */
 export const dataForMerchant = (actor, updatedData, sheetState) => {

    initSheetState(sheetState);

    updatedData.gui.merchant = {
        hasShop: actor.merchant.hasShop,
        viewedByPlayerWithCharacter: !game.user.isGM && game.user.character
    };

    initAvailableChoices(updatedData, sheetState);
    encureSheetStateIsAvailable(updatedData, sheetState);

    initChoiceSetup(actor, updatedData);
    initChoiceBuyItems(actor, updatedData);
    initChoiceSellItems(actor, updatedData);
}

/**
 * On first init, GMs will see the settings tab. Players will see the sellables
 * @param {object} sheetState Where current tab state is stored
 */
const initSheetState = (sheetState) => {

    if( sheetState.choice ) { return; }

    if( game.user.isGM ) {
        sheetState.choice = SHOPTAB_CHOICE.setup;
    } else {
        sheetState.choice = SHOPTAB_CHOICE.buy;
    }
}

/**
 * When manipulating Actor data, it may be possible that the current selection is not available anymore.
 * In that case, first available choice is selected.
 * @param {*} sheetState 
 * @param {*} updatedData 
 */
const encureSheetStateIsAvailable = (updatedData, sheetState) => {

    const data = updatedData.gui.merchant; 
    const choice = data.choices.find( c => c.key === sheetState.choice );
    if( !choice ) {
        const newChoice = data.choices[0].key;
        data.choices[0].active = true;
        data[newChoice].displayed = true;
        sheetState.choice = newChoice;
    }
}

/**
 * Prepare updatedData.gui.merchant.choices and init children for each choice
 * Init available choices for this sheet. Stored inside updatedData.gui.merchant.choices
 * Will used already calculated data like .hasShop or .viewedByPlayerWithCharacter
 * @param {object} updatedData Sheet data. This tab related data will be stored inside updatedData.gui.merchant
 * @param {object} sheetState Where current tab state is stored
 */
const initAvailableChoices = (updatedData, sheetState) => {

    const data = updatedData.gui.merchant; 
    const keys = [];

    if( !data.hasShop ) { 
        // Only choice if no shop has been built
        keys.push(SHOPTAB_CHOICE.noShop); 

    } else {
        // First choice : Setup
        if( game.user.isGM ) { keys.push(SHOPTAB_CHOICE.setup); }

        // Second choice : Buy items
        keys.push(SHOPTAB_CHOICE.buy);

        // Third choice : Sell items
        if( data.viewedByPlayerWithCharacter ) { keys.push(SHOPTAB_CHOICE.sell); }
    }
    

    // .choices: []
    const choices = keys.map(choice => {
        return {
            key: choice,
            active: choice === sheetState.choice,
            title: translate('AESYSTEM.actor.sheet.merchant.choice.' + choice)
        };
    });
    updatedData.gui.merchant.choices = choices;

    // .xxxx.displayed: true/false
    for( const choice of Object.values(SHOPTAB_CHOICE) ) {
        updatedData.gui.merchant[choice] = {
            displayed: choice === sheetState.choice && ( choices.findIndex( c => c.key === choice ) != -1 )
        }
    }
}

/**
 * Prepare updatedData.gui.merchant.setup
 * @param {AEActor} actor Displayed actor
 * @param {object} updatedData Sheet data. This tab related data will be stored inside updatedData.gui.merchant
 */
 const initChoiceSetup = (actor, updatedData) => {

    const data = updatedData.gui.merchant; 
    if( !data.setup.displayed ) { return; }

    data.setup.compendiums = retrieveAllItemCompendiums();
    fillCompendiumsCurrentAmount(actor, data.setup.compendiums);

    data.setup.prices = retrieveCurrentPrices(actor).map( p => {
        return foundry.utils.mergeObject({
            title: translate('AESYSTEM.actor.sheet.merchant.price.' + p.key),
            hint: p.min ? ( 'Min: ' + p.min + '%' ) : ( 'Max: ' + p.max + '%' )
        }, p);
    });
}

/**
 * Prepare updatedData.gui.merchant.buy
 * @param {AEActor} actor Displayed actor
 * @param {object} updatedData Sheet data. This tab related data will be stored inside updatedData.gui.merchant
 */
 const initChoiceBuyItems = (actor, updatedData) => {

    const data = updatedData.gui.merchant; 
    if( !data.buy.displayed ) { return; }

    const playerMainActor = game.user.character;

    // Selling items
    const sellingPrice = actor.merchant.base.settings?.sellingPrice ?? 100;
    const buttonTitle = translate('AESYSTEM.actor.sheet.merchant.button.buy');

    data.buy.sellables = actor.merchant.sellableItems.map( (d, index) => {

        const source = d.source;
        const eqt = d.eqt;
        const realPrice = Math.floor(eqt.cost * sellingPrice / 100.0);
        let buttonCss = '';
        if( data.viewedByPlayerWithCharacter && playerMainActor.coins < realPrice ) {
            buttonCss = 'red';
        }


        return {
            index: index,
            source: source,
            eqt: eqt,
            item: eqt.item,
            button: {
                displayed: data.viewedByPlayerWithCharacter,
                css: buttonCss,
                title: buttonTitle
            },
            limiter: retrieveLimiterDataFromEqt(eqt, data.viewedByPlayerWithCharacter),
            realPrice: realPrice
        };
    });

    data.buy.coins = retrieveCoinsData();
}

/**
 * Prepare updatedData.gui.merchant.sell
 * @param {AEActor} actor Displayed actor
 * @param {object} updatedData Sheet data. This tab related data will be stored inside updatedData.gui.merchant
 */
const initChoiceSellItems = (actor, updatedData) => {

    const data = updatedData.gui.merchant; 
    if( !data.sell.displayed ) { return; }

    // Main actor bag
    const buyingPrice = actor.merchant.base.settings?.buyingPrice ?? 50;
    const buttonTitle = translate('AESYSTEM.actor.sheet.merchant.button.sell');

    const playerMainActor = game.user.character;
    data.sell.bagItems = playerMainActor.equipment.all.filter( eqt => {
        return !eqt.isEquipped;
    }).map( (eqt, index) => {

        const realPrice = Math.floor(eqt.cost * buyingPrice / 100.0);
        return {
            index: index,
            eqt: eqt,
            item: eqt.item,
            realPrice: realPrice,
            button: {
                displayed: true,
                css: '',
                title: buttonTitle
            },
            limiter: retrieveLimiterDataFromEqt(eqt, data.viewedByPlayerWithCharacter)
        };
    });

    data.sell.coins = retrieveCoinsData();
}

/**
 * Build .coins childs. Not directly put inside updatedData.
 * It's called on different parts and will be added bellow
 * @returns {object} coins data
 */
const retrieveCoinsData = () => {

    const coins = {
        displayed: !game.user.isGM && game.user.character
    }

    if( coins.displayed ) {
        const playerMainActor = game.user.character;
        coins.desc = translate('AESYSTEM.actor.sheet.merchant.coins.currentlyHave').replace('AMOUNT', '' + playerMainActor.coins);
    }
    return coins;    
}

/**
 * Retrive current prices from actor data.
 * @param {AEActor} actor Displayed actor
 * @returns 
 */
const retrieveCurrentPrices = (actor) => {
    return [{
        key: 'sellingPrice',
        value: actor.merchant.base.settings?.sellingPrice ?? 100,
        min: 70,
        max: 200
    }, {
        key: 'buyingPrice',
        value: actor.merchant.base.settings?.buyingPrice ?? 50,
        min: 0,
        max: 70
    }];
}


/**
 * Compute limiter info to display inside a buy/sell eqt line
 * @param {AEEquipmentItem} eqt eqt displayed on this buy/sell line
 * @param {boolean} isViewedByPlayerWithCharacter if the gui is viewed by a player with a main character set
 * @returns {object} limiter data to add to the sellable/bagItem object
 */
 const retrieveLimiterDataFromEqt = (eqt, isViewedByPlayerWithCharacter ) => {

    const limiter = { apply: false, css: '' };
    if( eqt.item.isArmor ) { 
        foundry.utils.mergeObject(limiter, eqt.limiter); 
    }
    if( limiter.apply ) {

        const attribute = attributeList().find( a => a.key === limiter.attribute );
        limiter.details = translate('AESYSTEM.actor.sheet.merchant.buy.restriction')
                            .replace('ATTRIBUTE', attribute.name )
                            .replace('STEP', limiter.step );

        if( isViewedByPlayerWithCharacter ) {
            const level = game.user.character.getAttributeLevel(limiter.attribute);
            if( level < limiter.step ) { 
                limiter.css = 'red'; 
            }
        }
    }
    return limiter;
}


/**
 * Loop on very compendium and select the ones wich Equipment items
 * @returns List of selected compendiums, refenrenced as {name:<string>, path:<string>, size:<int>}
 */
const retrieveAllItemCompendiums = () => {

    const result = [];
    for( const [path, cEntry] of game.packs.entries() ) {
        if( cEntry.metadata.system && cEntry.metadata.system != 'acariaempire' ) { continue; }
        if( cEntry.metadata.type != 'Item' ) { continue; }
        if( cEntry.index.size == 0 ) { continue; }

        const forbiddenItemTypes = ['eventcard', 'trouble'];
        const allowed = Array.from( cEntry.index.values() ).reduce( (test, current) => {
            return test && !forbiddenItemTypes.includes(current.type);
        }, true);
        if( !allowed ) { continue; }

        result.push({
            name: cEntry.metadata.label,
            path: path,
            size: cEntry.index.size
        });
    };
    result.sort( (a,b) => a.name.localeCompare(b.name) );
    return result;
}

/**
 * Add a .amount on allItemCompendiums elements.
 * .amount is retrieved from actor data. If not found inside actor data, amount is set to zero
 * @param {AEActor} actor Displayed actor
 * @param {object[]} allItemCompendiums Compendiums retrieved in retrieveAllItemCompendiums()
 */
const fillCompendiumsCurrentAmount = (actor, allItemCompendiums) => {

    const usedCompendiums = actor.merchant.data?.settings?.compendiums ?? [];
    allItemCompendiums.forEach( compendium => {
        const used = usedCompendiums.find( u => u.path === compendium.path );
        if( used ) {
            compendium.amount = Math.clamped( used.amount, 0, compendium.size );
        } else {
            compendium.amount = 0;
        }
    });
}

/**
 * Bind merchant buttons.
 * @param {AEActorSheet} sheet parent
 * @param {HTMLElement} html Sheet content
 */
 export const bindMerchantButtons = ( sheet, html ) => {

    // Choices
    html.find(".ae-merchant .choose-tab").click(event => onClickToggleChoice(sheet, event));

    // No shop
    html.find(".ae-merchant .build-shop").click(event => onClickBuildShop(sheet, event));

    // Setup
    html.find(".ae-merchant .update-prices").click(event => onClickUpdatePrices(sheet, event));
    html.find(".ae-merchant .save-shop").click(event => onClickSaveShop(sheet, event));
    html.find(".ae-merchant .delete-shop").click(event => onClickDeleteShop(sheet, event));

    // Buying items
    html.find(".ae-merchant .compendium-show").click(event => onClickShowCompendium(sheet, event));
    html.find(".ae-merchant .item-img").click(event => onClickShowItem(sheet, event));
    html.find(".ae-merchant .item-click").click(event => onClickShopLineButton(sheet, event));
}

const onClickBuildShop = async (sheet, event) => {
    event.preventDefault();
    await sheet.actor.merchant.buildShop();
}

const onClickUpdatePrices = async (sheet, event) => {
    event.preventDefault();
    const a = event.currentTarget;

    // Retrieving all amount values
    const amountMap = new Map();
    const htmlPrices = a.parentElement;
    for(const child of htmlPrices.children ) {
        if( !child.classList.contains('price') ) { continue; }

        const input = child.getElementsByClassName("amount")[0];
        const value = parseInt(input.value);
        if( value ) {
            amountMap.set(child.dataset.key, value);
        }
    }

    // Update prices directly (Easy enough to not need a custom method in ActorMerchantData for that)
    const prices = retrieveCurrentPrices(sheet.actor);
    const updateData = {};
    prices.forEach( p => {
        if( amountMap.has(p.key) ) {
            updateData['system.shop.settings.' + p.key] = Math.clamped( amountMap.get(p.key), p.min, p.max );
        }
    });
    sheet._displayData.merchant.choice = SHOPTAB_CHOICE.buy;
    await sheet.actor.update( updateData );
}

const onClickSaveShop = async (sheet, event) => {
    event.preventDefault();
    const a = event.currentTarget;

    // Retrieving all amount values
    const amountMap = new Map();
    const htmlCompendiums = a.parentElement;
    for(const child of htmlCompendiums.children ) {
        if( !child.classList.contains('compendium') ) { continue; }

        const input = child.getElementsByClassName("amount")[0];
        const value = parseInt(input.value);
        if( value ) {
            amountMap.set(child.dataset.path, value);
        }
    }

    // Rebuild the compendiums attribute for the actor
    const compendiums = retrieveAllItemCompendiums().filter(c => amountMap.has(c.path));
    const formattedData = compendiums.map( c => {
        return {
            path: c.path,
            amount: Math.clamped( amountMap.get(c.path), 0, c.size )
        };
    });

    sheet._displayData.merchant.choice = SHOPTAB_CHOICE.buy;
    await sheet.actor.merchant.reloadShop( formattedData );
}

const onClickDeleteShop = async (sheet, event) => {
    event.preventDefault();
    await sheet.actor.merchant.deleteShop();
}

const onClickToggleChoice = async (sheet, event) => {
    event.preventDefault();
    const choice = event.currentTarget.dataset.choice;
    sheet._displayData.merchant.choice = choice;
    sheet.render();
}

const onClickShowCompendium = async (sheet, event) => {
    event.preventDefault();
    const path = event.currentTarget.parentElement.parentElement.dataset.path;
    const pack = game.packs.find(p => p.collection === path);
    for( const app of ( pack?.apps ?? [] ) ) {
        app.render(true);
    }
}

const onClickShowItem = async (sheet, event) => {
    event.preventDefault();
    const element = event.currentTarget.parentElement;
    const index = parseInt( element.dataset.index );
    const type = element.dataset.type;

    let eqtItem;
    if( type == 'sellable' ) {
        eqtItem = sheet.actor.merchant.sellableItems[index].eqt;
    } else {
        const playerMainActor = game.user.character;
        const bagItems = playerMainActor.equipment.all.filter(eqt => !eqt.isEquipped);
        eqtItem = bagItems[index];
    }
    
    eqtItem?.item.sheet.render(true);
}

const onClickShopLineButton = async (sheet, event) => {
    event.preventDefault();
    const element = event.currentTarget.parentElement;
    const index = parseInt( element.dataset.index );
    const source = element.dataset.source;
    const type = element.dataset.type;

    if( type == 'sellable' ) {
        await buyItem(sheet, index, source);
    } else {
        await sellItem(sheet, index);
    }
}

const buyItem = async (sheet, index, source) => {

    const eqtItem = sheet.actor.merchant.sellableItems[index].eqt;

    if( eqtItem ) {
        const percent = sheet.actor.merchant.base.settings?.sellingPrice ?? 100;
        const price = Math.floor(eqtItem.cost * percent / 100.0);

        const msg = await askGMToBuyItem(game.user.character, sheet.actor, index, price, source );
        ui.notifications.info(msg);
        sheet.render();
    }
}

const sellItem = async (sheet, index) => {

    const playerMainActor = game.user.character;
    const bagItems = playerMainActor.equipment.all.filter(eqt => !eqt.isEquipped);
    const eqtItem = bagItems[index];

    if( eqtItem ) {
        const item = eqtItem.item;

        const percent = sheet.actor.merchant.base.settings?.buyingPrice ?? 50;
        const price = Math.floor(eqtItem.cost * percent / 100.0);

        const msg = await askGMToSellItem(playerMainActor, sheet.actor, item, price );
        ui.notifications.info(msg);
        sheet.render();
    }
}
