import { TROUBLE_STATUS } from '../tools/constants.js';
import { assertGMOnly, removingXPAllowed } from '../tools/gmactions.js';
import {translate} from '../tools/Translator.js';
import { arcaneCirclePenalties, elementList, guardianSpiritBenefitList, guardianSpiritDrawbackList, guardianSpiritManeuverList, guardianSpiritPerkList, guardianSpiritPerksWithManeuversList, unlockableGuardianSpiritAbilityList } from '../tools/WaIntegration.js';

const MAX_SPIRIT_LEVEL = 6;
const HEALTH_BOOST_BY_PERK = 3;

const retrieveEnergy = (parent, updateData) => {

    const energyLeft = parent.energyLeft;
    if( energyLeft < 1 ) {
        ui.notifications.warn(translate('AESYSTEM.actor.spirit.noEnergyLeft'));
        throw 'No Energy left';
    }

    updateData['system.guardianSpirit.energy'] = parent.energyLeft -1;
}

const spendEnergyOnCapacity = (capacity) => {
    let newXP = capacity.xp + 1;
    let newLevel = capacity.level;
    let levelChanged = false;
    if( newXP > newLevel ) { 
        newLevel++;
        newXP -= newLevel;
        levelChanged = true;
    }

    if( newLevel > MAX_SPIRIT_LEVEL || ( newLevel == MAX_SPIRIT_LEVEL && newXP > 0 )  ) {
        const msg = translate('AESYSTEM.actor.spirit.maxReached').replace('CAPA', capacity.name);
        ui.notifications.warn(msg);
        throw msg;
    }

    return {
        xp: newXP,
        level: newLevel,
        levelChanged: levelChanged
    };
}

const filterAvailablePerks = (rawCapacity, capacityDefinition) => {

    if( rawCapacity.unlocked.length >= rawCapacity.level ) { // One perk can be unlocked at each level
        return [];
    }
    
    const resultSet = new Set();
    
    // Remove the ones that have already been learned
    let toRemoveFromPool = duplicate( rawCapacity.unlocked );
    for( let level = 1; level <= rawCapacity.level && level <= capacityDefinition.unlockable.length; level++ ) {
        
        const newPerks = capacityDefinition.unlockable[level-1];
        for( let perk of newPerks ) {
            const index = toRemoveFromPool.indexOf(perk);
            if( index != -1 ) {
                toRemoveFromPool.splice(index, 1);
            } else {
                resultSet.add(perk);
            }
        }
    }

    

    // Remove the ones that have a specific condition to be learned
    const result = [...resultSet].filter( perkKey => {
        const perk = guardianSpiritPerkList().find( p => p.perk === perkKey );

        let conditionOK = true;
        for( const need of perk.needs ) {
            conditionOK = conditionOK && rawCapacity.unlocked.includes(need);
        }

        // Special case: Personnality. Only two different personnalities can be taken
        if( rawCapacity.key == 'personality' && rawCapacity.unlocked.length >= 2 ) {
            conditionOK = conditionOK && rawCapacity.unlocked.includes(perkKey);
        }

        return conditionOK;
    });

    result.sort();
    return result;
}

const impactDrawbacks = (parent, updateData, levelingBenefit, newLevel) => {

    const drawbackKey = [1, 3, 5].includes(newLevel) ? levelingBenefit.related[0] : levelingBenefit.related[1];
    const drawback = parent.drawbacks.find( d => d.key === drawbackKey );

    const calc = spendEnergyOnCapacity(drawback);

    const base = parent.actorData.guardianSpirit?.drawbacks ?? [];
    const newDrawbackList = base.filter( d => d.key != drawbackKey );
    newDrawbackList.push( {key: drawbackKey, level: calc.level, xp: calc.xp, unlocked: drawback.unlocked } );

    updateData['system.guardianSpirit.drawbacks'] = newDrawbackList;
}

const countPerkOnBenefit = (actorData, perkKey, benefitKey) => {
    const benefits = actorData.guardianSpirit?.benefits ?? [];
    const unlockedPerks = benefits.find( b => b.key === benefitKey )?.unlocked ?? [];

    return unlockedPerks.filter( p => p === perkKey ).length;
}

const countPerkOnDrawback = (actorData, perkKey, drawbackKey) => {
    const drawbacks = actorData.guardianSpirit?.drawbacks ?? [];
    const unlockedPerks = drawbacks.find( b => b.key === drawbackKey )?.unlocked ?? [];

    return unlockedPerks.filter( p => p === perkKey ).length;
}

const automaticallyLearnSinglePerks = async (parent) => {

    for( const benefit of parent.benefits ) {
        if( benefit.availablePerks.length == 1 ) {
            await parent.learnNewPerkOnBenefit(benefit.key, benefit.availablePerks[0]);
        }
    }

    for( const drawback of parent.drawbacks ) {
        if( drawback.availablePerks.length == 1 ) {
            await parent.learnNewPerkOnDrawback(drawback.key, drawback.availablePerks[0]);
        }
    }
}

/**
 * Done after each change inside actor.
 * Reload active effect list related to guardian spirit.
 * @param {AEActorGuardianSpiritData} parent 
 * @returns TRUE if some effects have changed
 */
const refreshActorDataWithCurrentPerks = async (parent) => {


    // Convenience funtions
    //----------------------
    const spiritElements = parent.harmonizations;
    const currentEffects = parent.effects;
    const newActiveEffects = [];
    const removedActiveEffectIds = [];

    const managePerkEffects = ({perk, check, changes=[], parameters={}}) => {
        const currentOnes = currentEffects.filter( e => e.perkId === perk );
        const removingEffects = currentOnes.length > 1 || ( currentOnes.length == 1 && !check( currentOnes[0]) );
        const addingNewEffect = removingEffects ? !check( null ) : !check( currentOnes[0] );
    
        if( removingEffects ) { 
            currentOnes.forEach( e => removedActiveEffectIds.push( e.effectId) ); 
        }
        if( addingNewEffect ) { 
            newActiveEffects.push( 
                activeEffectDataFromPerk(perk, changes, parameters)
            ); 
        }
    }
    
    const actor = parent._actor;
    const actorData = parent.actorData;

    // Max health
    {
        const heathBoost = HEALTH_BOOST_BY_PERK * countPerkOnBenefit(actorData, 'health', 'health');
        const effectIsCorrect = (effect) => {
            const boost = effect?.parameters?.heathBoost ?? 0;
            return boost === heathBoost;
        };
        managePerkEffects({
            perk: 'health',
            check: effectIsCorrect,
            changes: [{key: 'system.health.max', value: heathBoost, mode: CONST.ACTIVE_EFFECT_MODES.ADD}],
            parameters: {heathBoost: heathBoost}
        });
    }

    // Attribute boosts
    {
        const attributes = Object.keys(actorData.attributes);
        attributes.forEach( attributeName => {
            const attributePath = 'system.attributes.' + attributeName + '.max';
            const attributeBoost = countPerkOnBenefit(actorData, attributeName, 'attributes');
            const effectIsCorrect = (effect) => {
                const boost = effect?.parameters?.attributeBoost ?? 0;
                return boost === attributeBoost;
            };
            managePerkEffects({
                perk: attributeName,
                check: effectIsCorrect,
                changes: [{key: attributePath, value: attributeBoost, mode: CONST.ACTIVE_EFFECT_MODES.ADD}],
                parameters: {attributeBoost: attributeBoost}
            });
        });
    }


    // Skills boost
    {
        const skillTypes = ['combat', 'explore', 'intrigue'];
        skillTypes.forEach( skillType => {
            const skillPath = 'system.abilities.' + skillType + '.max';
            const skillTypeBoost = countPerkOnBenefit(actorData, skillType, 'skills');
            const effectIsCorrect = (effect) => {
                const boost = effect?.parameters?.skillTypeBoost ?? 0;
                return boost === skillTypeBoost;
            };
            managePerkEffects({
                perk: skillType,
                check: effectIsCorrect,
                changes: [{key: skillPath, value: skillTypeBoost, mode: CONST.ACTIVE_EFFECT_MODES.ADD}],
                parameters: {skillTypeBoost: skillTypeBoost}
            });
        });
    }

    // ElementalResist boost
    {
        const resistBoost = countPerkOnBenefit(actorData, 'elementResist', 'attunement') ;
        const effectIsCorrect = (effect) => {
            const boost = effect?.parameters?.resistBoost ?? 0;
            if( boost != resistBoost ) { return false; }
            if( resistBoost == 0 ) { return true; } // No need to add an effect when there is no resists
    
            const elements = effect?.parameters?.elements ?? [];
            return elements.length == spiritElements.length &&
                spiritElements.reduce( (ok, element) => {
                    return ok && elements.includes(element);
                }, true);
        };
        const changes = spiritElements.map( e => {
            return {
                key: 'system.elements.' + e + '.resist', 
                value: resistBoost, 
                mode: CONST.ACTIVE_EFFECT_MODES.ADD
            };
        });
        managePerkEffects({
            perk: 'elementResist',
            check: effectIsCorrect,
            changes: changes,
            parameters: {resistBoost: resistBoost, elements: spiritElements}
        }); 
    }

    // PowerGain on 
    {
        const powerGain = countPerkOnBenefit(actorData, 'drawPowerFromElement', 'attunement');
        const effectIsCorrect = (effect) => {
            const boost = effect?.parameters?.powerGain ?? 0;
            if( boost != powerGain ) { return false; }
            if( powerGain == 0 ) { return true; } // No need to add an effect when there is no resists
    
            const elements = effect?.parameters?.elements ??  [];
            return elements.length == spiritElements.length &&
                spiritElements.reduce( (ok, element) => {
                    return ok && elements.includes(element);
                }, true);
        };
        const changes = spiritElements.map( e => {
            return {
                key: 'system.elements.' + e + '.powerGain', 
                value: powerGain, 
                mode: CONST.ACTIVE_EFFECT_MODES.ADD
            };
        });
        managePerkEffects({
            perk: 'drawPowerFromElement',
            check: effectIsCorrect,
            changes: changes,
            parameters: {powerGain: powerGain, elements: spiritElements}
        }); 
    }

    // Drain resist
    {
        const drainResist = countPerkOnBenefit(actorData, 'spiritProtection', 'attunement');
        const effectIsCorrect = (effect) => {
            const boost = effect?.parameters?.drainResist ?? 0;
            if( boost != drainResist ) { return false; }
            if( drainResist == 0 ) { return true; } // No need to add an effect when there is no resists
    
            const elements = effect?.parameters?.elements ?? [];
            return elements.length == spiritElements.length &&
                spiritElements.reduce( (ok, element) => {
                    return ok && elements.includes(element);
                }, true);
        };
        const changes = spiritElements.map( e => {
            return {
                key: 'system.elements.' + e + '.drainResist', 
                value: drainResist, 
                mode: CONST.ACTIVE_EFFECT_MODES.ADD
            };
        });
        managePerkEffects({
            perk: 'spiritProtection',
            check: effectIsCorrect,
            changes: changes,
            parameters: {drainResist: drainResist, elements: spiritElements}
        }); 
    }

    // Max circle Harmonizations
    {
        const maxHarmonizations = countPerkOnBenefit(actorData, 'circleHarmonization', 'attunement');
        const effectIsCorrect = (effect) => {
            const boost = effect?.parameters?.maxHarmonizations ?? 0;
            return boost === maxHarmonizations;
        };
        managePerkEffects({
            perk: 'circleHarmonization',
            check: effectIsCorrect,
            changes: [{key: 'system.arcane.maxCircleHarmonizations', value: maxHarmonizations, mode: CONST.ACTIVE_EFFECT_MODES.ADD}],
            parameters: {maxHarmonizations: maxHarmonizations}
        }); 
    }

    // Arcane attunements
    {
        const unlocked = countPerkOnBenefit(actorData, 'arcaneAttunement', 'attunement') > 0;
        const attunements = unlocked ? spiritElements : [];
        const effectIsCorrect = (effect) => {
            const elements = effect?.parameters?.elements ??  [];
            return elements.length == attunements.length &&
                attunements.reduce( (ok, element) => {
                    return ok && elements.includes(element);
                }, true);
        };
        const changes = attunements.map( e => {
            return {
                key: 'system.elements.' + e + '.attuned', 
                value: 1, 
                mode: CONST.ACTIVE_EFFECT_MODES.OVERRIDE
            };
        });
        managePerkEffects({
            perk: 'arcaneAttunement',
            check: effectIsCorrect,
            changes: changes,
            parameters: {elements: attunements}
        }); 
    }


    // Unlocked abilities
    {
        const allSpiritAbilities = unlockableGuardianSpiritAbilityList();
        const learnedAbilities = parent.learnedAbilities;

        // Remove the ones that aren't here anymore
        allSpiritAbilities.filter( ability => {
            const known = learnedAbilities.findIndex( a => a.perk === ability.perk ) != -1;
            if( known ) { return false; }
            return currentEffects.find( e => e.perkId === ability.perk );
        }).forEach( ability => {
            const effectId = currentEffects.find( e => e.perkId === ability.perk ).effectId;
            removedActiveEffectIds.push( effectId );
        });

        // For the existant ones
        learnedAbilities.forEach( ability => {
            const perk = ability.perk;
            const skillTYpe = ability.type;
            const newSkill = ability.skill;
    
            const effectIsCorrect = (effect) => {
                const skill = effect?.parameters?.newSkill ?? null;
                return skill === newSkill;
            };
            managePerkEffects({
                perk: perk,
                check: effectIsCorrect,
                changes: [{ key: 'system.abilities.' + skillTYpe + '.unlocked', value: newSkill, mode: CONST.ACTIVE_EFFECT_MODES.ADD }],
                parameters: {newSkill: newSkill}
            }); 
    
        });
    }

    // Drawback : Hunger
    {
        const unlockedPerks = parent.drawbacks.find( b => b.key === 'hunger' )?.unlocked ?? [];
        const hungerLevel = unlockedPerks.length;
        const effectIsCorrect = (effect) => {
            const val = effect?.parameters?.hungerLevel ?? 0;
            return hungerLevel === val ;
        };
        managePerkEffects({
            perk: 'hunger',
            check: effectIsCorrect,
            changes: [{key: 'system.guardianSpirit.troubles.hunger.eachDay', value: hungerLevel, mode: CONST.ACTIVE_EFFECT_MODES.ADD}],
            parameters: {hungerLevel: hungerLevel}
        }); 
    }

    // Drawback : Restlessness
    {
        const unlockedPerks = parent.drawbacks.find( b => b.key === 'personality' )?.unlocked ?? [];
        const personalityLevel = unlockedPerks.length;
        const effectIsCorrect = (effect) => {
            const val = effect?.parameters?.personalityLevel ?? 0;
            return personalityLevel === val ;
        };
        managePerkEffects({
            perk: 'restless',
            check: effectIsCorrect,
            changes: [{key: 'system.guardianSpirit.troubles.restlessness.eachDay', value: personalityLevel, mode: CONST.ACTIVE_EFFECT_MODES.ADD}],
            parameters: {personalityLevel: personalityLevel}
        }); 
    }

    // Refresh active effects
    const removingEffects = removedActiveEffectIds.length > 0;
    const addingNewffects = newActiveEffects.length > 0;
    if( removingEffects ) { await actor.deleteEmbeddedDocuments("ActiveEffect", removedActiveEffectIds ); }
    if( addingNewffects ) { await actor.createEmbeddedDocuments("ActiveEffect", newActiveEffects ); }

    return removingEffects || addingNewffects;
}


/**
 * Build ActiveEfectData from a perk and the wanted modifiers
 * @param {string} perk The perk Key
 * @param {object[]} modifiers What will be modified on character
 * @param {object[]} parameters Optional. Some infos that will be added in the guardianSpirit flag
 * @returns {ActiveEfectData} data that could be used to create a new Active effect on actor
 */
const activeEffectDataFromPerk = (perk, modifiers, parameters = {}) => {

    const perkDef = guardianSpiritPerkList().find( p => p.key === perk );
    if( !perkDef ) { throw 'Unknwown perk ' + perk; }

    const guardianSpirit = mergeObject( {perk: perk}, parameters );
    const activeEffectData = {
        label: perkDef.name,
        icon: perkDef.icon,
        changes: modifiers,
        flags: {
            acariaempire : {
                guardianSpirit: guardianSpirit
            }
        }
    };
    return activeEffectData;
}

/**
 * For managing arcane circles during scenes
 */
export class AEActorGuardianSpiritData {

    constructor(actor) {

        this._actor = actor;
    }

    get actorData() {

        return this._actor.system;
    }

    get awakened() {
        return this.actorData.guardianSpirit?.awakened ?? false;
    }

    get name() {
        return this.actorData.guardianSpirit?.name ?? '';
    }

    get harmonizations() {
        const base = this.actorData.guardianSpirit?.harmonizations ?? [];
        return duplicate(base);
    }


    get benefits() {

        const benefitsDefinition = guardianSpiritBenefitList();
        const current = this.actorData.guardianSpirit?.benefits ?? []; // {key:'', level: 0, xp: 0, unlocked: [] }

        return benefitsDefinition.map( b => {

            const actual = current.find( c => c.key === b.benefit ) ?? { key: b.benefit, level: 0, xp: 0, unlocked: [] };

            const copy = { 
                key: actual.key, 
                level: actual.level, 
                maxLevel: MAX_SPIRIT_LEVEL, 
                xp: actual.xp, 
                unlocked: duplicate(actual.unlocked),
                availablePerks: filterAvailablePerks(actual, b)
            };
            return mergeObject(copy, b);
        })
    }

    get drawbacks() {

        const drawbacksDefinition = guardianSpiritDrawbackList();
        const current = this.actorData.guardianSpirit?.drawbacks ?? []; // {key:'', level: 0, xp: 0, unlocked: [] }

        return drawbacksDefinition.map( d => {

            const actual = current.find( c => c.key === d.drawback ) ?? { key: d.drawback, level: 0, xp: 0, unlocked: [] };
            const copy = { 
                key: actual.key, 
                level: actual.level, 
                maxLevel: MAX_SPIRIT_LEVEL,
                xp: actual.xp, 
                unlocked: duplicate(actual.unlocked),
                availablePerks: filterAvailablePerks(actual, d)
            };
            return mergeObject(copy, d);
        })
    }

    get learnedAbilities() {
        const result = [];
        const unlockableAbilities = unlockableGuardianSpiritAbilityList();

        this.benefits.forEach( b => {
            b.unlocked.forEach( u => {
                const ability = unlockableAbilities.find( ability => ability.perk === u );
                if( ability && ! result.includes(ability) ) {
                    result.push(ability);
                }
            });
        });
        return result;
    }

    /*------------------------------------------
        Guardian spirit troubles
    --------------------------------------------*/

    get canContractTroubles() {
        const troubleData = this.actorData.guardianSpirit.troubles;
        const eachDay = troubleData.hunger.eachDay + troubleData.restlessness.eachDay;
        return eachDay > 0;
    }

    async updateTrougleGaugeWithDayPassing(amountOfDays=1) {

        const troubleData = this.actorData.guardianSpirit.troubles;
        const hunger = troubleData.hunger;
        const restlessness = troubleData.restlessness;

        const updateData = {};
        updateData['system.guardianSpirit.troubles.hunger.value'] = Math.clamped(hunger.value + hunger.eachDay * amountOfDays, hunger.min, hunger.max);
        updateData['system.guardianSpirit.troubles.restlessness.value'] = Math.clamped(restlessness.value + restlessness.eachDay * amountOfDays, restlessness.min, restlessness.max);
        await this._actor.update(updateData);
    }

    /*------------------------------------------
        Guardian spirit active effects
    --------------------------------------------*/

    /**
     * Reload all guardian spirit effects
     * @returns TRUE if some change were done
     */
    async reloadEffects() {
        return refreshActorDataWithCurrentPerks(this);
    }

    get effects() {
        const allEffects = this._actor.effects ?? [];
        return allEffects.filter( e => {
            return e.getFlag('acariaempire', 'guardianSpirit');
        }).map( e => {
            const dataInFlag = e.getFlag('acariaempire', 'guardianSpirit');
            const perkId = dataInFlag.perk;
            const perkDef = guardianSpiritPerkList().find( p => p.key === perkId );

            return {
                effectId: e.id,
                perkId: perkId,
                effect: e,
                perk: perkDef,
                parameters: dataInFlag
            };
        });
    }


    /*----------------------------------------
      Boost part
     ---------------------------------------*/
    get canBeHarmonized() {
        const hamonisationAmount = countPerkOnBenefit(this.actorData, 'harmonization', 'attunement') + ( this.awakened ? 1 : 0 );

        return this.harmonizations.length < hamonisationAmount;
    }

    getAvailableManeuvers(skillType) {
        const result = [];
        this.benefits.forEach( benefit => {
            benefit.unlocked.forEach( perkKey => {

                const perk = guardianSpiritPerksWithManeuversList().find( p => {
                    const hasManeuver = p.perk === perkKey;
                    const availableForSkillType = p.types.includes(skillType);
                    return hasManeuver && availableForSkillType;
                });
                if( perk && !result.find( m => m.maneuver === perk.maneuver ) ) {
                    const maneuver = guardianSpiritManeuverList().find( m => m.maneuver === perk.maneuver );
                    result.push( mergeObject( {}, maneuver ) );
                }
            });
        });
        return result;
    }


    /*----------------------------------------
      Energy spending part
     ---------------------------------------*/
    get energyLeft() {
        return this.actorData.guardianSpirit?.energy ?? 0;
    }

    get totalEnergyRetrieved() {
        return this.actorData.guardianSpirit?.totalEnergy ?? 0;
    }

    async grantEnergy(amount) {
        const val = parseInt(amount);
        if( assertGMOnly() ) { return ui.notifications.warn( translate('AESYSTEM.gm.restricted')); }

        const newAmount = this.energyLeft + val;
        const newTotal = this.totalEnergyRetrieved + val;

        const updateData = {};
        updateData['system.guardianSpirit.energy'] = newAmount;
        updateData['system.guardianSpirit.totalEnergy'] = newTotal;
        await this._actor.update(updateData);
    }

    async looseEnergy(amount) {
        const val = Math.min( this.energyLeft, parseInt(amount) );
        if( assertGMOnly() ) { return ui.notifications.warn( translate('AESYSTEM.gm.restricted')); }

        const newAmount = this.energyLeft - val;
        const newTotal = this.totalEnergyRetrieved - val;

        const updateData = {};
        updateData['system.guardianSpirit.energy'] = newAmount;
        updateData['system.guardianSpirit.totalEnergy'] = newTotal;
        await this._actor.update(updateData);
    }

    async bigReset() {
        if( !removingXPAllowed() ) { return ui.notifications.warn( translate('AESYSTEM.gm.restricted')); }

        const updateData = {};
        updateData['system.guardianSpirit.awakened'] = false;
        updateData['system.guardianSpirit.name'] = '';
        updateData['system.guardianSpirit.energy'] = this.totalEnergyRetrieved;
        updateData['system.guardianSpirit.totalEnergy'] = this.totalEnergyRetrieved;
        updateData['system.guardianSpirit.harmonizations'] = [];
        updateData['system.guardianSpirit.benefits'] = [];
        updateData['system.guardianSpirit.drawbacks'] = [];
        await this._actor.update(updateData);
    }

    async awakeSpirit() {
        this._actor._assertIsOwner();

        if( this.awakened ) { return ui.notifications.warn(translate('AESYSTEM.actor.spirit.alreadyAwakened')); }

        const spiritNames = ['Abbaz' ,'Abboq' ,'Crait' ,'Craz' ,'Creerbe' ,'Cyim' ,'Cyir' ,'Deez' ,'Dilbaik' ,'Doljot' ,'Drer' ,'Drullit' ,'Duq' ,'Duz' ,'Ebrem' ,'Elpul' ,'Gnalnu' ,'Gneec' ,'Graat' ,'Grekjo' ,'Gropqic' ,'Ilrus' ,'Ippep' ,'Ippias' ,'Jekjuc' ,'Kran' ,'Kroltap' ,'Krubla' ,'Krut' ,'Kupha' ,'Kyabqu' ,'Kyan' ,'Kyiarjom' ,'Kyukbai' ,'Lupras' ,'Ogfuk' ,'Oklir' ,'Poltut' ,'Qaarmus' ,'Qigbip' ,'Qrikkaac' ,'Qrippas' ,'Qrogfez' ,'Qrullir' ,'Qrut' ,'Rairle' ,'Raq' ,'Rukbat' ,'Szam' ,'Sziz' ,'Szoz' ,'Szum' ,'Tabbeec' ,'Tekru' ,'Tilruk' ,'Trepnub' ,'Tukbak' ,'Tuprab' ,'Tyan' ,'Tyekjix' ,'Tyin' ,'Ughiax' ,'Vaknet' ,'Vax' ,'Vullaak' ,'Vuphut' ,'Vuz' ,'Xibjuz' ,'Xon' ,'Xux' ,'Yiux' ,'Yobkul' ,'Zakhux' ,'Zil' ,'Zol' ,'Zon' ,'Zreel' ,'Zribjum'];
        const index = Math.floor(Math.random() * spiritNames.length);

        const updateData = {};
        retrieveEnergy(this, updateData);
        updateData['system.guardianSpirit.name'] = spiritNames[index];
        updateData['system.guardianSpirit.awakened'] = true;
        await this._actor.update(updateData);
    }

    async addSpiritHarmonization(element) {
        this._actor._assertIsOwner();

        if( !this.canBeHarmonized ) { return ui.notifications.warn(translate('AESYSTEM.actor.spirit.alreadyHarmonized')); }
        if( !elementList().find(e => e.element === element ) ) { return ui.notifications.error(translate('AESYSTEM.actor.spirit.invalidHarmonization').replace('ELEMENT', element) ); }

        const newHarmonizations = duplicate( this.harmonizations );
        if( ! newHarmonizations.includes(element) ) {
            newHarmonizations.push(element);
        }

        const updateData = {};
        updateData['system.guardianSpirit.harmonizations'] = newHarmonizations;
        await this._actor.update(updateData);
    }

    async spendXPOnBenefit(benefitKey) {
        this._actor._assertIsOwner();

        const benefit = this.benefits.find(b => b.key === benefitKey );
        if( !benefit ) { return ui.notifications.error(translate('AESYSTEM.actor.spirit.invalidBenefit').replace('BENEFIT', benefitKey)); }

        const calc = spendEnergyOnCapacity(benefit);

        const base = this.actorData.guardianSpirit?.benefits ?? [];
        const newBenefitList = base.filter( b => b.key != benefitKey );
        newBenefitList.push( {key: benefitKey, level: calc.level, xp: calc.xp, unlocked: benefit.unlocked } );

        const updateData = {};
        retrieveEnergy(this, updateData);
        updateData['system.guardianSpirit.benefits'] = newBenefitList;

        if( calc.levelChanged ) { // Also add change on drawbacks if necessary
            impactDrawbacks(this, updateData, benefit, calc.level); 
        }
        await this._actor.update(updateData);

        await automaticallyLearnSinglePerks(this);
    }

    async learnNewPerkOnBenefit(benefitKey, perkKey) {
        this._actor._assertIsOwner();

        const benefit = this.benefits.find(b => b.key === benefitKey );
        if( !benefit ) { return ui.notifications.error(translate('AESYSTEM.actor.spirit.invalidBenefit').replace('BENEFIT', benefitKey)); }

        if( !benefit.availablePerks.includes(perkKey) ) { return ui.notifications.warn(translate('AESYSTEM.actor.spirit.invalidBenefitPerk').replace('PERK', perkKey).replace('BENEFIT', benefitKey)); }

        const newUnlocked = duplicate( benefit.unlocked );
        newUnlocked.push(perkKey);
        newUnlocked.sort();

        const base = this.actorData.guardianSpirit?.benefits ?? [];
        const newBenefitList = base.filter( b => b.key != benefitKey );
        newBenefitList.push( {key: benefitKey, level: benefit.level, xp: benefit.xp, unlocked: newUnlocked } );

        const updateData = {};
        updateData['system.guardianSpirit.benefits'] = newBenefitList;
        await this._actor.update(updateData);
    }


    async learnNewPerkOnDrawback(drawbackKey, perkKey) {
        this._actor._assertIsOwner();

        const drawback = this.drawbacks.find(d => d.key === drawbackKey );
        if( !drawback ) { return ui.notifications.error(translate('AESYSTEM.actor.spirit.invalidDrawback').replace('DRAWBACK', drawbackKey)); }

        if( !drawback.availablePerks.includes(perkKey) ) { return ui.notifications.warn(translate('AESYSTEM.actor.spirit.invalidDrawbackPerk').replace('PERK', perkKey).replace('DRAWBACK', drawbackKey)); }

        const newUnlocked = duplicate( drawback.unlocked );
        newUnlocked.push(perkKey);
        newUnlocked.sort();

        const base = this.actorData.guardianSpirit?.drawbacks ?? [];
        const newDrawbackList = base.filter( d => d.key != drawbackKey );
        newDrawbackList.push( {key: drawbackKey, level: drawback.level, xp: drawback.xp, unlocked: newUnlocked } );

        const updateData = {};
        updateData['system.guardianSpirit.drawbacks'] = newDrawbackList;
        await this._actor.update(updateData);
    }

}
