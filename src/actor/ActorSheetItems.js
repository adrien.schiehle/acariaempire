import {translate} from '../tools/Translator.js';
import { AEActorSheet } from './ActorSheet.js';

/**
 * Prepare data for Equipment tab in actor sheet
 * @param {AEActor} actor Displayed actor
 * @param {object} updatedData Sheet data. This tab related data will be stored inside updatedData.gui.equipment
 */
export const dataForEquipment = (actor, updatedData) => {

    const currentEquipment = actor.equipment.currentlyEquipped;

    // Slots
    const positions = [ // Those are the ideal poisition
        [110, 0], [270, 0], [0, 220], [233, 220], [110, 110], 
        [0, 140], [270, 80], [135, 435], [270, 350], [270, 435]
    ];
    for( let index = 1; index < positions.length; index++ ) {
        // Relative position should take into account each previously put element
        positions[index][1]-= 65 * index; // 65px is the height of each element
    }

    const slots = Object.entries(currentEquipment).map( ([key, value]) => {

        const defaultIcon = 'systems/acariaempire/resources/actor/equipment/' + key + '.png';

        const position = positions.shift();

        return {
            header: translate('AESYSTEM.actor.sheet.equipment.' + key + '.header'),
            slot: key,
            itemEquipped: value != null,
            itemId: value?.item.id ?? null,
            itemName: value?.item.name ?? translate('AESYSTEM.actor.sheet.equipment.slot.' + key + '.default'),
            itemIcon: value?.item.img ?? defaultIcon,
            left: position[0],
            top: position[1]
        };
    });

    // Spell effects
    const combat = game.combats?.active;
    const spellEffects = actor.spellEffects.map( se => {

        const data = mergeObject({}, se);
        data.labelCss = se.offensive ? 'red' : 'green';
        data.removeDisplayed = game.user.isGM;

        const spellTrack = combat?.findSpellTrack(se.spellId) ?? null;
        
        // - data.detail
        data.detail = { 
            displayed: spellTrack ? true : false 
        };
        if( data.detail.displayed ) {
            data.initiative = spellTrack.initiative;
            data.detail.label = translate('AESYSTEM.actor.sheet.equipment.effect.spellDetail')
                            .replace('ACTOR', spellTrack.actor.name)
                            .replace('UNTIL', spellTrack.initiative);
        }

        // - data.stack
        data.stack = {
            displayed: se.stacks.amount > 0,
            amount: se.stacks.amount
        };
        if( data.stack.displayed ) {
            data.stack.label = translate('AESYSTEM.actor.sheet.equipment.effect.spellStacks')
                            .replace('STACKS', '' + se.stacks.amount);
            data.stack.css = game.user.isGM ? 'stack-change' : '';
        }
        return data;

    }).sort( (a,b) => {
        // Sorted by spell duration with ones outside tracker on top.
        const aInit = a.initiative ?? 0;
        const bInit = b.initiative ?? 0;
        if( aInit != bInit ) { return aInit - bInit; }
        
        const aLabel = a.effect.label;
        const bLabel = b.effect.label;
        return aLabel.localeCompare(bLabel);
    });

    updatedData.gui.equipment = {
        slots: slots,
        spellEffects: spellEffects,
        effectsDisplayed: spellEffects.length > 0
    }
}


/**
 * Prepare data for Bag tab in actor sheet
 * @param {AEActor} actor Displayed actor
 * @param {object} updatedData Sheet data. This tab related data will be stored inside updatedData.gui.bag
 */
 export const dataForBags = (actor, updatedData) => {

    const allItems = actor.equipment.all;
    
    const nbItemsOnOneLine = 11;
    let nbItems = nbItemsOnOneLine;
    while(nbItems < allItems.length) {
        nbItems += nbItemsOnOneLine;
    }

    updatedData.gui.bag = {
        items: actor.equipment.all.sort( (a,b) => {
            return a.compare(b);
        }).map(e => {
            return {
                display: true,
                css: e?.isEquipped ? 'equipped' : 'filled',
                itemId: e?.item.id ?? null,
                itemName: e?.item.name ?? translate('AESYSTEM.actor.sheet.equipment.' + key + '.default'),
                itemIcon: e?.item.img ?? defaultIcon,
                canBeEquipped: e.canBeEquipped
            };
        })
    } ?? [];

    for(let i = allItems.length; i < nbItems; i++) {
        updatedData.gui.bag.items.push({
            display: false,
            css: ''
        });
    }

    // Also display coins
    updatedData.gui.bag.coins = {
        amount: actor.coins,
        title: translate('AESYSTEM.actor.sheet.equipment.coins.title'),
        editable: game.user.isGM
    }
}


/**
 * Bind actions for Equipment and Bags tabs.
 * If editable, activates Context menus
 * @param {AEActorSheet} sheet parent
 * @param {HTMLElement} html Sheet content
 */
 export const bindItemsActions = ( sheet, html ) => {
    html.find(".equipment-slots .slot-item").dblclick( event => onDoubleClickDisplayOnChat(sheet, event));
    html.find(".bag-slot .bag-item").dblclick( event => onDoubleClickDisplayOnChat(sheet, event));

    if (sheet.options.editable) {
        html.find(".ae-equipments .spell-line .effect-remove").click(event => onClickRemoveSpell(sheet, event));
        html.find(".ae-equipments .spell-line .stack-change").click(event => onClickModifyStack(sheet, event, 1));
        html.find(".ae-equipments .spell-line .stack-change").contextmenu(event => onClickModifyStack(sheet, event, -1));
        bindItemsContextMenu( sheet, html );
    }
}

const bindItemsContextMenu = (sheet, html) => {

    const relatedItem = (li => {
        const id = li.data("id") ?? null;
        const item = sheet.actor.equipment.asItems.find( i => i.id === id ) ?? null;
        return item;
    });

    const relatedEquipment = (li => {
        const item = relatedItem(li);
        if( !item?.isEquipment ) { return null; }
        return item.asEquipement();
    });

    const actions = [{
        name: "AESYSTEM.actor.sheet.equipment.popup.show",
        icon: '<i class="far fa-eye"></i>',
        condition: li => {
            return relatedItem(li) != null;
        },
        callback: async li => {
            relatedItem(li)?.sheet.render(true);
        }
    }];

    if(sheet.options.editable) {

        actions.push({
            name: "AESYSTEM.actor.sheet.equipment.popup.onChat",
            icon: '<i class="fas fa-hand-point-right"></i>',
            condition: li => {
                return relatedItem(li) != null;
            },
            callback: async li => {
                relatedEquipment(li)?.showOnChat();
            }
        });
        
        actions.push({
            name: "AESYSTEM.actor.sheet.equipment.popup.unequip",
            icon: '<i class="fas fa-hand-point-down"></i>',
            condition: li => {
                const equipment = relatedEquipment(li);
                return equipment && equipment.isEquipped;
            },
            callback: async li => {
                await sheet.actor.equipment.unequipItem(relatedEquipment(li));
                sheet.render();
            }
        });
        
        actions.push({
            name: "AESYSTEM.actor.sheet.equipment.popup.equip",
            icon: '<i class="fas fa-hand-holding"></i>',
            condition: li => {
                const equipment = relatedEquipment(li);
                return equipment?.canBeEquipped && !equipment.isEquipped;
            },
            callback: async li => {
                await sheet.actor.equipment.equipItem(relatedEquipment(li));
                sheet.render();
            }
        });
        
        actions.push({
            name: "AESYSTEM.actor.sheet.equipment.popup.equipSecondHand",
            icon: '<i class="fas fa-hand-holding"></i>',
            condition: li => {
                const equipment = relatedEquipment(li);
                return equipment?.item.isWeapon && equipment.isAllowedOnLeftHand && !equipment.isEquipped;
            },
            callback: async li => {
                await sheet.actor.equipment.equipOnSecondHand(relatedEquipment(li));
                sheet.render();
            }
        });

        actions.push({
            name: "AESYSTEM.actor.sheet.equipment.popup.drop",
            icon: '<i class="fas fa-trash"></i>',
            condition: li => {
                const equipment = relatedEquipment(li);
                return equipment && !equipment.isEquipped;
            },
            callback: async li => {

                const equipment = relatedEquipment(li);
                await Dialog.confirm({
                    title: translate('AESYSTEM.actor.sheet.equipment.popup.dropConfirmTitle').replace('ITEM', equipment.item.name),
                    content: translate('AESYSTEM.actor.sheet.equipment.popup.dropConfirmText'),
                    yes: async () => {
                        return relatedItem(li)?.delete();
                    }
                });
                sheet.render();
            }
        });
        
    }

    new ContextMenu(html, ".bag-item", actions);
    new ContextMenu(html, ".slot-item", actions);
}

const onDoubleClickDisplayOnChat = async (sheet, event) => {
    event.preventDefault();
    const a = event.currentTarget;
    const itemId = a.dataset.id;
    if( !itemId ) { return; } //May be an empty slot

    const eqt = sheet.actor.equipment.all.find( e => e.item.id === itemId );
    await eqt?.showOnChat();
}

const onClickRemoveSpell = async (sheet, event) => {
    event.preventDefault();
    const a = event.currentTarget;
    const spellId = a.parentElement.dataset.id;
    await sheet.actor.removeSpellEffects([spellId]);
    sheet.render();
}

const onClickModifyStack = async (sheet, event, amountOffset) => {
    event.preventDefault();
    const a = event.currentTarget;
    const currentAmount = parseInt(a.dataset.amount);
    const spellId = a.parentElement.parentElement.dataset.id;
    await sheet.actor.updateSpellStackAmount(spellId, currentAmount + amountOffset);
    sheet.render();
}


export const onDropRetrieveItem = async (sheet, event) => {

    try {
        const parsed = JSON.parse(event.dataTransfer.getData('text/plain'));
        if( parsed?.type != "Item" ) {
            throw translate('AESYSTEM.actor.sheet.drag.itemOnly');
        }

        const item = await onDropLoadItem(fromUuidSync(parsed.uuid));
        if( !item ) {  throw 'Item not found'; } //Should not happen
        if( !item.isEquipment ) { throw translate('AESYSTEM.actor.sheet.drag.itemOnly'); }
        if( !game.user.isGM ) { throw translate('AESYSTEM.gm.restricted'); }

        await sheet.actor.createEmbeddedDocuments("Item", [item._source]);
    } catch( err ) {
        ui.notifications.warn(err);
    }
}


/**
* Before letting the classic ActorSheet._onDrop do its job, we need to assert which type of items it is.
* For that, we need to load the item.data part.
**/
const onDropLoadItem = async (parsed) => {

    // Retrieve item data
    let item;
    if( parsed.pack ) {
        let pack = game.packs.find(p => p.collection === parsed.pack);
        let index = await pack.getIndex();
        let entry = index.get(parsed._id);
        if( ! entry ) { throw 'Entry not found'; } //Should not happen

        item = await pack.getDocument(entry._id);

    } else if( ! parsed.data ) {
        item = game.items.get(parsed.id);
    } else {
        item = parsed;
    }

    return item;
}
