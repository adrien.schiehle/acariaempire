import {retrieveXP, giveXP} from './actor.js';
import {translate} from '../tools/Translator.js';
import {divinityList, elementList, prayerManeuvers} from '../tools/WaIntegration.js';
import { AEActorBaseDataWithManeuvers } from './ActorBaseDataWithManeuvers.js';

export const MAX_SPIRIT_LEVEL = 6;

/**
 * For managing divine communions during scenes
 */
export class AEActorDivineData extends AEActorBaseDataWithManeuvers {

    constructor(actor) {
        super(actor, actor.battle.prayer, prayerManeuvers, 'divine');
    }

    get pacts() {
        const divinities = divinityList();
        const elements = elementList();
        const pacts = this.actorData.divine?.pacts ?? [];
        return pacts.map(p => {
            const divinity = divinities.find(d => p.divinity === d.divinity);
            const divinityElems = (divinity?.elements ?? []).map(e => {
                const elem = elements.find(el => el.element === e);
                return {
                    name: e,
                    icon: elem?.icon
                };
            });
            return {
                key: p.divinity,
                divinity: p.divinity,
                name: divinity?.name ?? translate('AESYSTEM.actor.sheet.divine.notFound').replace('DIVINITY', p.divinity),
                icon: divinity?.icon ?? '',
                elements: divinityElems,
                level: p.level
            };
        });
    }

    getPact(divinityName) {
        const pact = this.pacts.find( pact => pact.name === divinityName);
        return pact ?? null;
    }

    async spendXPToPact(pactName) {
 
        this._actor._assertIsOwner();

        if( this._actor.getTotalXPLeft('power') <= 0 ) { return ui.notifications.info(translate('AESYSTEM.actor.xp.power.noXpLeft')); }

        const pacts = duplicate(this.actorData.divine?.pacts ?? []);
        const pact = pacts.find(pact => pact.divinity === pactName);

        if(! pact ) { throw 'Pact not found : ' + pactName; }

        if(pact.level >= this._actor.battle.prayer.maneuverMaxLevel ) { return ui.notifications.info(translate('AESYSTEM.actor.xp.divine.cappedByMastery')); }
        pact.level += 1;

        const updateData = {};
        retrieveXP(this._actor, 'power', updateData, 1);
        updateData['system.divine.pacts'] = pacts;
        await this._actor.update(updateData);
    }

    async removeXPFromPact(pactName) {
 
        this._actor._assertIsOwner();

        const pacts = duplicate(this.actorData.divine?.pacts ?? []);
        const pact = pacts.find(pact => pact.divinity === pactName);

        if(! pact ) { throw 'Pact not found : ' + pactName; }

        if(pact.level <= 0) { return ui.notifications.info(translate('AESYSTEM.actor.xp.divine.alreadyLevel0')); }
        pact.level -= 1;

        const updateData = {};
        giveXP(this._actor, 'power', updateData, 1);
        updateData['system.divine.pacts'] = pacts.filter(p => p.level != 0);
        await this._actor.update(updateData);
    }

    async formNewPact(pactName) {
 
        this._actor._assertIsOwner();

        if( this._actor.getTotalXPLeft('power') <= 0 ) { return ui.notifications.info(translate('AESYSTEM.actor.xp.power.noXpLeft')); }

        const pacts = duplicate(this.actorData.divine?.pacts ?? []);
        const existingPact = pacts.find(pact => pact.divinity === pactName);

        if(existingPact != null) { return ui.notifications.info(translate('AESYSTEM.actor.xp.divine.alreadyFormed')); }

        const divinity = divinityList().find(d => d.divinity === pactName);
        if(!divinity) { return ui.notifications.info(translate('AESYSTEM.actor.xp.divine.unknownDivinity').replace('DIVINITY', pactName)); }

        pacts.push({
            divinity: pactName,
            level: 1
        });

        const updateData = {};
        retrieveXP(this._actor, 'power', updateData, 1);
        updateData['system.divine.pacts'] = pacts;
        await this._actor.update(updateData);
    }

    get communion() {
        const c= this.actorData.divine?.communion;

        const nbSpirits = this._actor.cards.spirits.length ?? 0;
        const spiritPower = nbSpirits == 0 ? Math.max(this._actor.battle.prayer.maneuverMaxLevel, 0)
                                           :  c?.spiritPower ?? 0;
        const allowed = nbSpirits == 0 || (c?.canCallSpirits ?? true);
        const dissatisfaction = c?.dissatisfaction ?? 0;
        return {
            spiritPower: spiritPower,
            canCallSpirits: allowed,
            calledSpirits: nbSpirits,
            dissatisfaction: dissatisfaction
        };
    }

    async deleteCurrentCommunion() {
        await this.alterCommunion({newCommunion: true, revokeAllSpirits: true})
    }

    /**
     * Modify the current communion.
     */
     async alterCommunion({
            additionalSpirits=0, 
            currentSpiritMaxPower=null, 
            dissatisfaction=0,
            revokeAllSpirits=false, 
            blockFurtherCalls=false, 
            newCommunion=false, 
            consumedSpirits=[]}={}) {
        
        const updateData = {};

        // For when players draw spirit cards without doing a real communion
        if( currentSpiritMaxPower == null ) {
            currentSpiritMaxPower = this._actor.battle.prayer.maneuverMaxLevel;
        }

        // Modifying spirit card hand
        //---------------------------
        if( consumedSpirits.length > 0 ) {
            await this._actor.cards.discardSpirits(consumedSpirits);
        }
        
        if( revokeAllSpirits ) { await this._actor.cards.discardAllSpirits(); }

        if( additionalSpirits > 0 ) { await this._actor.cards.drawSpirits(additionalSpirits); }

        // Altering communion data
        //---------------------------
        const currentComunion = this.communion;

        // If called spirits are of a lower level
        if( currentComunion.spiritPower > currentSpiritMaxPower ) {
            updateData['system.divine.communion.spiritPower'] = currentSpiritMaxPower;
        }

        // On epic fail
        if( blockFurtherCalls ) {
            updateData['system.divine.communion.canCallSpirits'] = false;
        }

        // Reseting communion data
        if( newCommunion ) {
            updateData['system.divine.communion.spiritPower'] = currentSpiritMaxPower;
            updateData['system.divine.communion.canCallSpirits'] = true;
        }

        // Handling dissatisfaction
        if( newCommunion || currentComunion.dissatisfaction != dissatisfaction ) {
            updateData['system.divine.communion.dissatisfaction'] = dissatisfaction;
        }

        // Update actor
        if(Object.keys(updateData).length > 0) { 
            await this._actor.update(updateData); 
        }
    }

    calculateDrain(element, effect, bonus, spiritPower, pactName=null) {

        // Drain level
        const masteryGap = Math.max( 0, spiritPower - this._actor.battle.prayer.maneuverMaxLevel );
        const drainReductionFromBonus = bonus == 'drain' ? 1 : 0;

        const pacts = this.pacts.filter( pact => {
            if(pactName != null) {
                return pact.key === pactName;
            }
            return pact.elements.findIndex( e => e.name === element) != -1;

        });
        let selectedPact = pacts.length > 0 ? pacts[0] : null;
        for( let index = 1; index < pacts.length; index++) {
            if( pacts[index].level > selectedPact.level ) {
                selectedPact = pacts[index];
            }
        }

        const pactPower = selectedPact?.level ?? 0;

        const drainLevel = Math.max( 0, spiritPower + masteryGap - drainReductionFromBonus - pactPower );

        // Drain damage
        const calculateDamage = (level) => {
            if( level == 0 ) { return 0; }
            if( level == 1 ) { return 1; }
            if( level == 2 ) { return 2; }
            if( level == 3 ) { return 5; }
            if( level == 4 ) { return 10; }
            return 99;
        };

        const drainResist = element? this.actorData.elements[element].drainResist : 0;
        const drainDamage = Math.max( 0, calculateDamage(drainLevel) - drainResist);

        return {
            level: drainLevel,
            damage: Math.min( this._actor.health, drainDamage ),
            pactIcon: selectedPact?.icon ?? null
        };
    }

}
