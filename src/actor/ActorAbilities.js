import { removingXPAllowed } from '../tools/gmactions.js';
import {translate} from '../tools/Translator.js';
import {jobList, skillAreas, skillLevels, skillList, unlockableGuardianSpiritAbilityList} from '../tools/WaIntegration.js';
import {retrieveXP, giveXP} from './actor.js';

const SKILLTYPE = {
    combat: 'combat',
    explore: 'explore',
    intrigue: 'intrigue'
};

const adventurerSkills = (parent, skillType, areaDefinition=null) => {

    const actorData = parent.actorData;
    let skillBase;
    if( skillType === SKILLTYPE.combat ) {
        skillBase = actorData.abilities?.combat;

    } else if( skillType === SKILLTYPE.explore ) {
        skillBase = actorData.abilities?.explore;
        
    } else if( skillType === SKILLTYPE.intrigue ) {
        skillBase = actorData.abilities?.intrigue;
    } 

    const maxLevel = getProperty(parent.actorData.abilities, skillType).max;
    const data = {
        type: skillType,
        level: skillBase?.level ?? 0,
        maxLevel: maxLevel,
        xp: skillBase?.xp ?? 0,
        unlocked: skillBase?.unlocked ?? []
    };

    if( areaDefinition != null ) {
        mergeObject(data, areaDefinition);

        data.levelUpInfo = {
            xp: data.xp,
            maxLevel: maxLevel
        };
        mergeObject(data.levelUpInfo, skillLevels().find( level => level.level ==data.level ));
    }
    return data;
}

const fillSkillInfos = (ability) => {
    
    const unlockableAbilities = unlockableGuardianSpiritAbilityList();
    const unlockedKeys = ability.unlocked;

    const data = {};
    mergeObject(data, ability);
    data.unlocked = unlockedKeys.map( skillKey => {
        const skillDef = skillList().find( s => s.key === skillKey ) ?? {key: skillKey, name: 'Unknown'} ;
        const data = {
            spiritSkill: unlockableAbilities.findIndex( a => a.skill === skillKey ) != -1
        };
        return mergeObject(data, skillDef);
    });
    return data;
}

const buildAbilities = (parent, areas) => {
    const result = areas.map( area => {
        return adventurerSkills(parent, area.key, area);
    }).map( ability => {
        return fillSkillInfos(ability);
    });

    return result;
}


const addXPOnAbility = (parent, skillType, minusXP = false) => {

    const skillBase = parent._actor._source.system.abilities[skillType];;

    const addedXP = skillBase.xp + ( minusXP ? -1 : 1 );
    const nextLevelXP = (skillBase.level + 1) * 2;
    let newLevel;
    let newXP;
    if(addedXP >= nextLevelXP) {
        newLevel = skillBase.level + 1;
        newXP = 0;
    } else if( addedXP < 0 ) {
        if( skillBase.level <= 0 ) { throw 'Can\'t remove XP'; }
        newLevel = skillBase.level - 1;
        newXP = skillBase.level * 2 - 1;
    } else {
        newLevel = skillBase.level;
        newXP = addedXP;
    }

    return {
        level: newLevel,
        xp: newXP,
        unlocked: skillBase.unlocked
    };
}


const deduceAdventurerLevel = (parent, newValues) => {

    const tab = duplicate(newValues);
    for( let skillType of [SKILLTYPE.combat, SKILLTYPE.explore, SKILLTYPE.intrigue]) {
        if( ! newValues.find( v => v.type === skillType ) ) {
            tab.push( adventurerSkills(parent, skillType) );
        }
    }
    
    const minFromValues = tab.reduce( (min, current) => {
        if( min > current.level ) {
            return current.level;
        }
        return min;
    }, 99);
    return minFromValues;
}



/**
 * For managing job and skills for this actor.
 * Doesn't contain state variables. Only here for convenience use.
 */
export class AEActorAbilities {

    constructor(actor) {

        this._actor = actor;
    }

    get actorData() {

        return this._actor.system;
    }

    get ref() {

        return this.actorData.abilities?.startingAs;
    }

    get def() {
        if( this.isSet && !this._def ) {
            this._def = duplicate(jobList().find(j => j.job === this.ref) ?? {});
        }
        return this._def;
    }

    get isSet() {
        if( ! this.ref ) { return false; }
        return this.ref != '';
    }

    get canLearnNewSkill() {

        if( ! this.isSet ) {
            return false;
        }

        const unlockedSkills = this.allAbilities.reduce( (amount, data) => {
            return amount + data.unlocked.filter(u => !u.spiritSkill).length;
        }, 0 );
        return unlockedSkills < this.level + 3; // The three first ones are given when starting.
    }

    get name() {

        return this.def.name;
    }

    get icon() {

        return this.def.icon;
    }

    get level() {

        return this.actorData.abilities?.level ?? 0;
    }

    get combatAbility() {
        const areas = skillAreas().filter( area => area.key === SKILLTYPE.combat);
        return buildAbilities(this, areas)[0];
    }

    get exploreAbility() {
        const areas = skillAreas().filter( area => area.key === SKILLTYPE.explore);
        return buildAbilities(this, areas)[0];
    }

    get intrigueAbility() {
        const areas = skillAreas().filter( area => area.key === SKILLTYPE.intrigue);
        return buildAbilities(this, areas)[0];
    }

    get allAbilities() {
        return buildAbilities(this, skillAreas());
    }

    skillIsKnown(skillType, skillKey) {
        let ability;
        if( skillType === SKILLTYPE.combat ) { ability = this.combatAbility; }
        else if( skillType === SKILLTYPE.explore ) { ability = this.exploreAbility; }
        else { ability = this.intrigueAbility; }

        return ability.unlocked.findIndex( u => u.key === skillKey ) != -1;
    }

    get xpHasBeenSpent() {

        for( let ability of this.allAbilities ) {
            if( ability.level != 0 || ability.xp != 0 ) {
                return true;
            }
        }
        return false;
    }

    async removeStartingJob() {

        this._actor._assertIsOwner();

        if( ! this.isSet ) { return ui.notifications.warn(translate('AESYSTEM.actor.xp.job.noJobToChange')); }

        if( ! removingXPAllowed() ) { 
            return ui.notifications.warn(translate('AESYSTEM.actor.xp.job.changeRestricted')); 
        }

        if( this.xpHasBeenSpent ) {
            return ui.notifications.warn(translate('AESYSTEM.actor.xp.job.xpSpent')); 
        }

        const updateData = {};
        updateData['system.abilities.startingAs'] = '';
        updateData['system.abilities.level'] = 0;
        updateData['system.abilities.combat'] = { level: 0, xp: 0, unlocked: [] };
        updateData['system.abilities.explore'] = { level: 0, xp: 0, unlocked: [] };
        updateData['system.abilities.intrigue'] = { level: 0, xp: 0, unlocked: [] };
        await this._actor.update(updateData);
    }

    async changeStartingJob(newJob) {

        this._actor._assertIsOwner();

        if( this.isSet ) { return ui.notifications.warn(translate('AESYSTEM.actor.xp.job.alreadyHasOne')); }

        const job = jobList().find(j => j.job === newJob);
        if(! job) { return ui.notifications.warn(translate('AESYSTEM.actor.xp.job.unknownJob').replace('JOB',newJob)); }

        const unlockedCombatSkill = job.inCombat? [job.inCombat.key] : [];
        const unlockedExploreSkill = job.whileExploring? [job.whileExploring.key] : [];
        const unlockedIntrigueSkill = job.duringIntrigue? [job.duringIntrigue.key] : [];

        const updateData = {};
        updateData['system.abilities.startingAs'] = newJob;
        updateData['system.abilities.level'] = 0;
        updateData['system.abilities.combat'] = { level: 0, xp: 0, unlocked: unlockedCombatSkill };
        updateData['system.abilities.explore'] = { level: 0, xp: 0, unlocked: unlockedExploreSkill };
        updateData['system.abilities.intrigue'] = { level: 0, xp: 0, unlocked: unlockedIntrigueSkill };
        await this._actor.update(updateData);
    }

    async unlockNewSkill(skillType, newSkillRef) {

        this._actor._assertIsOwner();

        const skillDef = skillList().find( s => s.skillType === skillType && s.key === newSkillRef );
        if( !skillDef ) { return ui.notifications.warn('INVALID ' + skillType + ' SKILL ' + newSkillRef); }
    
        const spiritSkill = unlockableGuardianSpiritAbilityList().findIndex( a => a.skill === newSkillRef ) != -1;
        if( !spiritSkill && !this.canLearnNewSkill ) { return ui.notifications.warn(translate('AESYSTEM.actor.xp.ability.cantGetNewOne')); }

        const knownSkills = this._actor._source.system.abilities[skillType].unlocked;
        if( knownSkills.includes(newSkillRef) ) {  return ui.notifications.warn(translate('AESYSTEM.actor.xp.ability.cantGetNewOne')); }
    
        const updateData = {};
        updateData['system.abilities.' + skillType + '.unlocked'] = knownSkills.concat(newSkillRef);
        await this._actor.update(updateData);
    }

    async forgetUnlockedSkill(skillType, skillRef) {

        this._actor._assertIsOwner();

        if( ! removingXPAllowed() ) { 
            return ui.notifications.warn(translate('AESYSTEM.actor.xp.ability.forgetRestricted')); 
        }
    
        const knownSkills = this._actor._source.system.abilities[skillType].unlocked;
        if( ! knownSkills.includes(skillRef) ) { 
            return ui.notifications.warn(translate('AESYSTEM.actor.xp.ability.invalidSkill').replace('SKILL',skillRef)); 
        }
        
        const updateData = {};
        updateData['system.abilities.' + skillType + '.unlocked'] = knownSkills.filter(u => u != skillRef);
        await this._actor.update(updateData);
    }

    async spendXPToAbility(ability) {

        this._actor._assertIsOwner();

        const newAbilityValue = addXPOnAbility(this, ability);
        const newValWithTypeSet = mergeObject({type: ability}, newAbilityValue);

        const updateData = {};
        retrieveXP(this._actor, 'skill', updateData, 1);
        updateData['system.abilities.' + ability ] = newAbilityValue;
        updateData['system.abilities.level'] = deduceAdventurerLevel(this, [newValWithTypeSet]);

        await this._actor.update(updateData);
    }

    async reinitAbility(ability) {

        if( ! removingXPAllowed() ) { 
            return ui.notifications.warn(translate('AESYSTEM.actor.xp.job.changeRestricted')); 
        }

        const levelInfo = this.allAbilities.find(a => a.key === ability)?.levelUpInfo;
        if(!levelInfo) { throw 'Unkown skill area ' + ability; }

        const retrievedAmount = skillLevels().reduce( (amount, current) => {
            return current.level < levelInfo.level ? amount + current.xpCost : amount;
        }, levelInfo.xp);

        const initialUnlockedSkills = [];
        if( ability === SKILLTYPE.combat && this.def.inCombat ) {
            initialUnlockedSkills.push( this.def.inCombat.key );
        } else if( ability === SKILLTYPE.explore && this.def.whileExploring ) {
            initialUnlockedSkills.push( this.def.whileExploring.key );
        } else if( this.def.duringIntrigue ) {
            initialUnlockedSkills.push( this.def.duringIntrigue.key );
        }

        const updateData = {};
        giveXP(this._actor, 'skill', updateData, retrievedAmount);
        updateData['system.abilities.' + ability + '.level' ] = 0;
        updateData['system.abilities.' + ability + '.xp' ] = 0;
        updateData['system.abilities.' + ability + '.unlocked' ] = initialUnlockedSkills;
        updateData['system.abilities.level'] = 0;
        await this._actor.update(updateData);
    }

}
