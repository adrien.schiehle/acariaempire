import {ARCANE_MAX_LEVEL, PACT_MAX_LEVEL, MELODY_MAX_LEVEL} from './actor.js';

import {translate} from '../tools/Translator.js';
import {arcaneSpellModifiers, arcaneSpellCategories, divinityList, elementList, magicSystems, combatSystems} from '../tools/WaIntegration.js';
import {removingXPAllowed} from '../tools/gmactions.js';
import * as utils from './ActorSheetUtils.js';

/**
 * Prepare data for Melee tab in actor sheet
 * @param {AEActor} actor Displayed actor
 * @param {object} updatedData Sheet data. This tab related data will be stored inside updatedData.gui.melee
 */
export const dataForMelee = (actor, updatedData, meleeData) => {

    const maneuvers = utils.prepareManeuverData( actor, actor.melee.maneuvers, meleeData.currentManeuver );

    const weapon = actor.equipment.mainWeapon ?? null;

    // Also add some shortcuts for easy rolls
    const actions = [
        { action: 'battle', category: 'melee', 
          label: translate('AESYSTEM.actor.sheet.melee.actions.attack') }
    ];

    updatedData.gui.melee = {
        system: combatSystems().find(s=> s.system == 'melee'),
        capacity: utils.battleCapacity(actor, 'vigor', 'melee'),
        maneuvers: maneuvers,
        actions: actions,
        weapon: {
            displayed: weapon?.isMeleeWeapon ?? false,
            header: translate('AESYSTEM.actor.sheet.melee.header.weapon'),
            name: weapon?.item.name
        }
    };
}

/**
 * Prepare data for Ranged tab in actor sheet
 * @param {AEActor} actor Displayed actor
 * @param {object} updatedData Sheet data. This tab related data will be stored inside updatedData.gui.ranged
 */
 export const dataForRanged = (actor, updatedData, rangedData) => {

    const maneuvers = utils.prepareManeuverData( actor, actor.ranged.maneuvers, rangedData.currentManeuver );

    const weapon = actor.equipment.mainWeapon ?? null;

    // Also add some shortcuts for easy rolls
    const actions = [
        { action: 'battle', category: 'ranged', 
          label: translate('AESYSTEM.actor.sheet.ranged.actions.attack') }
    ];

    updatedData.gui.ranged = {
        system: combatSystems().find(s=> s.system == 'ranged'),
        capacity: utils.battleCapacity(actor, 'focus', 'ranged'),
        maneuvers: maneuvers,
        actions: actions,
        weapon: {
            displayed: weapon?.isRangedWeapon ?? false,
            header: translate('AESYSTEM.actor.sheet.ranged.header.weapon'),
            name: weapon?.item.name
        }
    };
}

/**
 * Prepare data for Improvised tab in actor sheet
 * @param {AEActor} actor Displayed actor
 * @param {object} updatedData Sheet data. This tab related data will be stored inside updatedData.gui.improvised
 */
export const dataForImprovised = (actor, updatedData, improvisedData) => {

    const maneuvers = utils.prepareManeuverData( actor, actor.improvised.maneuvers, improvisedData.currentManeuver );

    const weapon = actor.equipment.mainWeapon ?? null;

    // Also add some shortcuts for easy rolls
    const actions = [
        { action: 'battle', category: 'improvised', subchoice: 'improvised',
          label: translate('AESYSTEM.actor.sheet.improvised.actions.attack') },
        { action: 'battle', category: 'improvised', subchoice: 'barehanded', 
          label: translate('AESYSTEM.actor.sheet.improvised.actions.barehanded') },
    ];

    updatedData.gui.improvised = {
        system: combatSystems().find(s=> s.system == 'improvised'),
        capacity: utils.battleCapacity(actor, 'versatility', 'improvised'),
        improvisedManeuvers: maneuvers.filter(m => !m.martialArt),
        barehandedManeuvers: maneuvers.filter(m => m.martialArt),
        actions: actions,
        weapon: {
            displayed: weapon?.isImprovisedWeapon ?? false,
            header: translate('AESYSTEM.actor.sheet.improvised.header.weapon'),
            name: weapon?.item.name
        }
    };
}

/**
 * Prepare data for Arcane tab in actor sheet
 * @param {AEActor} actor Displayed actor
 * @param {object} updatedData Sheet data. This tab related data will be stored inside updatedData.gui.arcane
 */
export const dataForArcaneKnowledge = (actor, updatedData, arcaneData) => {

    const removingXP = removingXPAllowed();
    const editAllowed = actor.allowedToEdit;
    const xpLeft = actor.getTotalXPLeft('power') > 0;
    const maneuvers = utils.prepareManeuverData( actor, actor.arcane.maneuvers, arcaneData.currentManeuver );

    const modifiers = arcaneSpellModifiers().map( m => {
        const knowledgeLevel = actor.arcane.getKnowledge(m.modifier);
        return {
            key: m.modifier,
            shortcut: m.shortcut,
            level: knowledgeLevel,
            showXpButton: editAllowed && xpLeft,
            showMinusXPButton: removingXP && (knowledgeLevel > 0),
            xptab: utils.buildXPBubblesForAlternateCapacities(knowledgeLevel, ARCANE_MAX_LEVEL)
        };
    });

    const effects = arcaneSpellCategories().map( e => {
        const knowledgeLevel = actor.arcane.getKnowledge(e.effect);
        return {
            key: e.effect,
            shortcut: e.shortcut,
            level: knowledgeLevel,
            showXpButton: editAllowed && xpLeft,
            showMinusXPButton: removingXP && (knowledgeLevel > 0),
            xptab: utils.buildXPBubblesForAlternateCapacities(knowledgeLevel, ARCANE_MAX_LEVEL)
        };
    });

    // Also add some shortcuts for easy rolls
    const actions = [
        { action: 'battle', category: 'arcane', subchoice: 'spell', 
          label: translate('AESYSTEM.actor.sheet.arcane.actions.castSpell') }, 
        { action: 'battle', category: 'arcane', subchoice: 'circle', 
          label: translate('AESYSTEM.actor.sheet.arcane.actions.buildCircle') }
    ];

    updatedData.gui.arcane = {
        system: magicSystems().find(s=> s.system == 'arcane'),
        capacity: utils.battleCapacity(actor, 'perspicacity', 'arcane'),
        effects : effects,
        modifiers : modifiers,
        actions: actions,
        maneuvers: maneuvers
    };
}

/**
 * Prepare data for Prayer tab in actor sheet
 * @param {AEActor} actor Displayed actor
 * @param {object} updatedData Sheet data. This tab related data will be stored inside updatedData.gui.divine
 */
export const dataForDivinePacts = (actor, updatedData, divineData) => {

    const removingXP = removingXPAllowed();
    const editAllowed = actor.allowedToEdit;
    const xpLeft = actor.getTotalXPLeft('power') > 0;
    const maneuvers = utils.prepareManeuverData( actor, actor.divine.maneuvers, divineData.currentManeuver );

    const donePacts = actor.divine.pacts.map( (pact) => {
        const data = duplicate(pact);
        return mergeObject(data, {
            done: true,
            showXpButton: editAllowed && xpLeft,
            showMinusXPButton: removingXP && (pact.level > 0),
            xptab: utils.buildXPBubblesForAlternateCapacities(pact.level, PACT_MAX_LEVEL)
        });
    }).sort((a,b) => {
        return a.name.localeCompare(b.name);
    });

    const availablePacts = divinityList().filter(d => {
        const pact = donePacts.find(p => p.divinity === d.divinity);
        return ! pact;
    }).map(d => {
        const divinityElems = d.elements.map(e => {
            const elem = elementList().find(el => el.element === e);
            return {
                name: e,
                icon: elem?.icon
            };
        });
        const result = duplicate(d);
        result.elements = divinityElems;
        result.done = false;
        result.showXpButton = editAllowed && xpLeft;
        return result;
    }).sort((a,b) => {
        return a.name.localeCompare(b.name);
    });

    // Also add some shortcuts for easy rolls
    const actions = [
        { action: 'battle', category: 'prayer', subchoice: 'communion', 
            label: translate('AESYSTEM.actor.sheet.divine.actions.communion') }, 
        { action: 'battle', category: 'prayer', subchoice: 'miracle', 
            label: translate('AESYSTEM.actor.sheet.divine.actions.miracle') }, 
        { action: 'battle', category: 'prayer', subchoice: 'release', 
            label: translate('AESYSTEM.actor.sheet.divine.actions.release') }
    ];
    
    updatedData.gui.divine = {
        system: magicSystems().find(s=> s.system == 'divine'),
        capacity: utils.battleCapacity(actor, 'socialEase', 'prayer'),
        pacts: donePacts.concat(availablePacts),
        actions: actions,
        maneuvers: maneuvers
    };
}

/**
 * Prepare data for Songs tab in actor sheet
 * @param {AEActor} actor Displayed actor
 * @param {object} updatedData Sheet data. This tab related data will be stored inside updatedData.gui.songs
 */
export const dataForSongs = (actor, updatedData, songsData) => {
    const removingXP = removingXPAllowed();
    const editAllowed = actor.allowedToEdit;
    const xpLeft = actor.getTotalXPLeft('power') > 0;
    const maneuvers = utils.prepareManeuverData( actor, actor.songs.maneuvers, songsData.currentManeuver );

    const melodies = actor.songs.melodies.map( ([key, melody]) => {
        return {
            key: key,
            level: melody.level,
            shortcut: melody.shortcut,
            shortcutChild: melody.shortcutChild,
            showXpButton: editAllowed && xpLeft,
            showMinusXPButton: removingXP && (melody.level > 0),
            xptab: utils.buildXPBubblesForAlternateCapacities(melody.level, MELODY_MAX_LEVEL)
        }
    });

    // Also add some shortcuts for easy rolls
    const actions = [
        { action: 'battle', category: 'songs', 
            label: translate('AESYSTEM.actor.sheet.songs.actions.play') }
    ];

    updatedData.gui.songs = {
        system: magicSystems().find(s=> s.system == 'songs'),
        capacity: utils.battleCapacity(actor, 'versatility', 'songs'),
        melodies: melodies,
        maneuvers: maneuvers,
        actions: actions
    };
}


/**
 * Bind buttons for every battle tables
 * @param {AEActorSheet} sheet parent
 * @param {HTMLElement} html Sheet content
 */
export const bindBattleActions = ( sheet, html ) => {

    if ( !sheet.options.editable ) { return; }
    
    new ForManeuverClicks(sheet, sheet.actor.melee, sheet._displayData.melee, 'ae-melee').bindActions(html);
    new ForManeuverClicks(sheet, sheet.actor.ranged, sheet._displayData.ranged, 'ae-ranged').bindActions(html);
    new ForManeuverClicks(sheet, sheet.actor.improvised, sheet._displayData.improvised, 'ae-improvised').bindActions(html);
    new ForManeuverClicks(sheet, sheet.actor.arcane, sheet._displayData.arcane, 'ae-arcanes').bindActions(html);
    new ForManeuverClicks(sheet, sheet.actor.divine, sheet._displayData.divine, 'ae-divine').bindActions(html);
    new ForManeuverClicks(sheet, sheet.actor.songs, sheet._displayData.songs, 'ae-songs').bindActions(html);

    html.find(".interaction .key .launch-dice").click(event => onClickRollBattleDice(sheet, event));
    html.find(".ae-arcanes .arcane .xp-action").click(event => onClickArcaneKnowledgeControl(sheet, event));

    html.find(".ae-divine .pact .xp-action").click(event => onClickDivinePactsControl(sheet, event));
    html.find(".ae-divine .pact .form-pact").click(event => onClickDivineFormPactsControl(sheet, event));

    html.find('.ae-songs .melody .xp-action').click(event => onClickSongsMelodyModify(sheet, event));
}

const onClickRollBattleDice = async (sheet, event) => {

    const a = event.currentTarget;
    const category = a.parentElement.dataset.category;
    const subchoice = a.parentElement.dataset.subchoice;

    const tokenId = sheet.actor.tokenId;
    if( ! tokenId ) return ui.notifications.warn(translate('AESYSTEM.actor.noTokenOnScene'));

    game.aesystem.triggerRollPanel({
        tokenId: tokenId,
        battle: category,
        battleSubchoice: subchoice
    });
}

/** Event for modifying arcane knowledge **/
const onClickArcaneKnowledgeControl = async (sheet, event) => {
    sheet._assertEditPermissions();

    event.preventDefault();
    const a = event.currentTarget;
    const action = a.dataset.action;
    const knowledge = a.parentElement.parentElement.parentElement.dataset.knowledge;

    if ( action == 'removeXP' ) {
        await sheet.actor.arcane.removeXPToKnowledge(knowledge);

    } else if ( action == 'addXP' ) {
        await sheet.actor.arcane.spendXPToKnowledge(knowledge);
    }
    sheet.actor.render();
}


/** Event for modifying existing divine pacts **/
const onClickDivinePactsControl = async (sheet, event) => {
    sheet._assertEditPermissions();

    event.preventDefault();
    const a = event.currentTarget;
    const action = a.dataset.action;
    const pact = a.parentElement.parentElement.parentElement.dataset.pact;

    if ( action == 'removeXP' ) {
        await sheet.actor.divine.removeXPFromPact(pact);

    } else if ( action == 'addXP' ) {
        await sheet.actor.divine.spendXPToPact(pact);
    }
    sheet.actor.render();
}

/** Event for creating new divine pacts **/
const onClickDivineFormPactsControl = async (sheet, event) => {
    sheet._assertEditPermissions();

    event.preventDefault();
    const a = event.currentTarget;
    const pact = a.parentElement.dataset.pact;

    await sheet.actor.divine.formNewPact(pact);
    sheet.actor.render();
}

/** Event for modifying songs melodies **/
const onClickSongsMelodyModify = async (sheet, event) => {
    sheet._assertEditPermissions();

    event.preventDefault();
    const a = event.currentTarget;
    const action = a.dataset.action;
    const melody = a.parentElement.parentElement.parentElement.dataset.melody;

    if ( action == 'removeXP' ) {
        await sheet.actor.songs.removeXPFromMelody(melody);

    } else if ( action == 'addXP' ) {
        await sheet.actor.songs.spendXPToMelody(melody);
    }
    sheet.render();
}


/** Group of listeners needed by each maneuver type */
class ForManeuverClicks {

    constructor(sheet, actorAbilityData, displayData, parentDivClass) {
        this._sheet = sheet;
        this._actorAbilityData = actorAbilityData;
        this._displayData = displayData;
        this._parentDivClass = parentDivClass;
    }

    bindActions(html) {
        const basePath = '.' + this._parentDivClass;
        html.find(basePath).on("click",  ".maneuver.selectable",                    this._onClickSelectManeuver.bind(this));
        html.find(basePath + ' .maneuver').on("click",  ".evolve-button.available", this._onClickSpendXPOnManeuver.bind(this));
        html.find(basePath + ' .maneuver').on("click",  ".evolve-button.forget",    this._onClickForgetManeuver.bind(this));
    }

    async _onClickSelectManeuver(event) {
        this._sheet._assertEditPermissions();
        event.preventDefault();

        const a = event.currentTarget;
        const maneuverKey = a.dataset.maneuver;
        this._displayData.currentManeuver = maneuverKey;

        this._sheet.render();
    }

    async _onClickSpendXPOnManeuver(event) {
        this._sheet._assertEditPermissions();
        event.preventDefault();

        await this._actorAbilityData.spendXPToManeuver(this._displayData.currentManeuver);
        this._sheet.render();
    }

    async _onClickForgetManeuver(event) {
        this._sheet._assertEditPermissions();
        event.preventDefault();

        await this._actorAbilityData.forgetManeuver(this._displayData.currentManeuver);
        this._sheet.render();
    }
}

