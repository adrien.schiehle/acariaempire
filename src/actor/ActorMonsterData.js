import { monsterAugmentList } from '../tools/WaIntegration.js';
import { monsterEffects } from '../tools/monsters.js';

/**
 * Done after each actor change.
 * Reload active effect list related to monster augments
 * @param {AEActorMonsterData} parent 
 * @returns TRUE if some effects have changed
 */
 const refreshActorDataWithCurrentAugments = async (parent) => {

    const actor = parent._actor;
    const currentEffects = parent.effects;
    const currentAugments = parent.allAugments.filter( a => a.level > 0 );

    // Removing effects from augments.
    //---------------------------------
    const effectsToRemove = [];
    currentEffects.forEach( effect => {

        const augment = currentAugments.find( a => a.key === effect.augmentId );

        const shoudBeRemoved = !augment || augment.level != effect.level;
        if( shoudBeRemoved ) {
            effectsToRemove.push( effect );
        }
    });
    const removedActiveEffectIds = effectsToRemove.map( e => e.effectId );

    // Check which one should be added
    //-------------------------------
    const augmentsToAdd = currentAugments.reduce( (res, augment) => {

        const insideRemoveList = effectsToRemove.findIndex( e => e.augmentId === augment.key ) != -1;
        const insideCurrentList = currentEffects.findIndex( e => e.augmentId === augment.key ) != -1;
        if(  insideRemoveList || ! insideCurrentList ) {
            res.push(augment);
        }
        return res;
    }, []);


    // Transform them into active effect
    //--------------------------------
    const newActiveEffects = augmentsToAdd.map( augment => {

        const augmentDef = monsterEffects[augment.key];
        const changes = [{
            mode: CONST.ACTIVE_EFFECT_MODES.CUSTOM, 
            key: monsterEffects.CHANGE_KEY,
            value: augment.key + ':' + augment.level, 
            priority: augmentDef.priority
        }];

        const activeEffectData = {
            label: augment.name,
            changes: changes,
            flags: {
                acariaempire : {
                    monster: {
                        augment: augment.key,
                        level: augment.level
                    }
                }
            }
        };
        return activeEffectData;
    });

    // Refresh active effects
    const removingEffects = removedActiveEffectIds.length > 0;
    const addingNewffects = newActiveEffects.length > 0;
    if( removingEffects ) { await actor.deleteEmbeddedDocuments("ActiveEffect", removedActiveEffectIds ); }
    if( addingNewffects ) { await actor.createEmbeddedDocuments("ActiveEffect", newActiveEffects ); }

    return removingEffects || addingNewffects;
}



/**
 * For managing NPC merchant shops
 */
export class AEActorMonsterData {

    constructor(actor) {
        this._actor = actor;
    }

    get base() {
        const data = this._actor.system.monster;
        if( !data ) { 'This character type \'' + this._actor.type + '\' has no access to monster part'; }
        return data;
    }


    get allAugments() {
        const currentAugments = this.base.augments ?? [];

        return monsterAugmentList().map( augment => {
            let level = currentAugments.find(ca => ca.augment === augment.key )?.level ?? 0;
            const data =  {
                level: Math.min(level, augment.maxLevel)
            };
            return foundry.utils.mergeObject(data, augment);
        });
    }

    get threat() {
        return this.base.threat + this._actor.getTotalXPLeft('all');
    }

    /*------------------------------------------
        Monster active effects
    --------------------------------------------*/
    async reloadEffects() {
        return refreshActorDataWithCurrentAugments(this);
    }

    get effects() {
        const allEffects = this._actor.effects ?? [];
        return allEffects.filter( e => {
            return e.getFlag('acariaempire', 'monster');
        }).map( e => {
            const dataInFlag = e.getFlag('acariaempire', 'monster');
            return {
                effectId: e.id,
                augmentId : dataInFlag.augment,
                effect: e,
                level: dataInFlag.level
            };
        });
    }


    /**
     * Change a monster augmentation level
     * @param {string} augmentKey key as defined in monsterAugmentList
     * @param {int} level new augment level
     */
    async setAugment( augmentKey, level ) {

        const rawAugments = this._actor._source.system.monster.augments ?? [];
        const newAugments = rawAugments.reduce( (res, a) => {
            if( a.augment != augmentKey ) { res.push( a ); }
            return res;
        }, []);
        if( level > 0 ) {
            newAugments.push( {augment: augmentKey, level: level} );
        }

        const updateData = {};
        updateData['system.monster.augments'] = newAugments;
        await this._actor.update(updateData);
    }

}

