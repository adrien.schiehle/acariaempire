import {elementList, guardianSpiritPerkList} from '../tools/WaIntegration.js';
import {removingXPAllowed} from '../tools/gmactions.js';
import * as utils from './ActorSheetUtils.js';
import { translate } from '../tools/Translator.js';

/**
 * Prepare data for Guardian spirit tab in actor sheet
 * @param {AEActor} actor Displayed actor
 * @param {object} updatedData Sheet data. This tab related data will be stored inside updatedData.gui.spirit
 */
export const dataForSpirit = (actor, updatedData, spiritData) => {
    
    const guardianSpirit = actor.guardianSpirit;
    const rawBenefits = guardianSpirit.benefits;
    const rawDrawbacks = guardianSpirit.drawbacks;

    const needToChooseElement = guardianSpirit.canBeHarmonized;
    
    const selectedBenefit = rawBenefits.find( b => b.key === spiritData.currentSelection ) ?? null;
    const selectedDrawbacks = selectedBenefit ? [
        rawDrawbacks.find( d => selectedBenefit.related[0] === d.key ),
        rawDrawbacks.find( d => selectedBenefit.related[1] === d.key )
    ] : [];

    const willBenefitEvolve = checkIfBenefitWillEvolve(selectedBenefit);
    const progressingDrawbackKey = findProgressingDrawbackKey(selectedBenefit, willBenefitEvolve);

    // Convenience functions
    const prepareBenefitData = (benefit) => {

        const selectable = guardianSpirit.awakened && !needToChooseElement;
        const canSpendXP = guardianSpirit.energyLeft > 0 && benefit.availablePerks.length == 0;

        const data = {
            title: translate('AESYSTEM.actor.sheet.guardian.benefit.title').replace('BENEFIT', benefit.name).replace('LEVEL', benefit.level),
            css: !selectable ? 'transparent' : ( benefit ==  selectedBenefit ? 'selected' : 'selectable' ),
            knownPerksInfo: benefit.unlocked.map( p => fillPerksInfo(p) ),
            availablePerksInfo: benefit.availablePerks.map( p => fillPerksInfo(p) ),
            detailDisplayed: benefit == selectedBenefit
        };

        adjustPerkTooltips(data.knownPerksInfo);
        adjustPerkTooltips(data.availablePerksInfo);

        if( data.detailDisplayed ) {
            data.button = {
                css: canSpendXP ? 'available' : '',
                title: translate('AESYSTEM.actor.sheet.guardian.benefit.button.' + (willBenefitEvolve ? 'levelUp' : 'feed') )
            };
            data.xptab = utils.buildXPBubblesForAbility(benefit, {bluePoint: true});
        }

        return mergeObject( data, benefit );
    };

    const prepareDrawbackData = (drawback) => {

        const available = guardianSpirit.awakened && !needToChooseElement;
        const index = selectedDrawbacks.findIndex( d => d.key === drawback.key);

        let css = '';
        if( !available ) {
            css = 'transparent';
        } else {
            if( index == 0 ) { css = 'first'; }
            else if( index == 1 ) { css = 'second'; }
        }

        const data = {
            title: translate('AESYSTEM.actor.sheet.guardian.drawback.title').replace('DRAWBACK', drawback.name).replace('LEVEL', drawback.level),
            css: css,
            knownPerksInfo: drawback.unlocked.map( p => fillPerksInfo(p) ),
            availablePerksInfo: drawback.availablePerks.map( p => fillPerksInfo(p) ),
            detailDisplayed: index != -1
        };

        adjustPerkTooltips(data.knownPerksInfo);
        adjustPerkTooltips(data.availablePerksInfo);

        if( data.detailDisplayed ) {

            const missingXPForEvolving = drawback.level + 1 - drawback.xp;
            const willDrawbackProgress = progressingDrawbackKey === drawback.key;
            const willDrawbackEvolve = willDrawbackProgress && missingXPForEvolving <= 1;
    
            if( willDrawbackEvolve ) {
                data.css = data.css + ' red';
                data.evolving = translate('AESYSTEM.actor.sheet.guardian.drawback.comment.levelUp');

            } else if( willDrawbackProgress ) {
                data.evolving = translate('AESYSTEM.actor.sheet.guardian.drawback.comment.progressing');

            } else {
                data.evolving = translate('AESYSTEM.actor.sheet.guardian.drawback.comment.noChange');
            }
            data.xptab = utils.buildXPBubblesForAbility(drawback, {redPoint: willDrawbackProgress});
        }

        return mergeObject( data, drawback );
    };

    updatedData.gui.spirit = {
        resetBtn: {
            displayed : removingXPAllowed() && guardianSpirit.awakened && guardianSpirit.totalEnergyRetrieved > 0,
            label: translate('AESYSTEM.actor.sheet.guardian.state.button.reset')
        },
        awakening: {
            done: guardianSpirit.awakened,
            title: translate('AESYSTEM.actor.sheet.guardian.state.comment.' + (guardianSpirit.awakened ? 'awakened' : 'sleeping') ),
            awakeBtn: {
                css : guardianSpirit.energyLeft > 0 ? 'available' : '',
                label: translate('AESYSTEM.actor.sheet.guardian.state.button.awake')
            },
            harmonizations: elementList().map( e => prepareHarmonization(guardianSpirit, needToChooseElement, e) ),
            spiritName: guardianSpirit.name
        },
        drawbacks: {
            title: translate('AESYSTEM.actor.sheet.guardian.drawback.list'),
            tab: rawDrawbacks.map( d => prepareDrawbackData(d) )
        },
        benefits: {
            title: translate('AESYSTEM.actor.sheet.guardian.benefit.list'),
            tab: rawBenefits.map( b => prepareBenefitData(b))
        }
    };
}

/**
 * Bind actions for Trouble tab.
 * @param {AEActorSheet} sheet parent
 * @param {HTMLElement} html Sheet content
 */
export const bindSpiritActions = ( sheet, html ) => {

    html.find(".ae-spirit .benefits .benefit.selectable").click(event => onClickSelectSpiritBenefit(sheet, event));
    html.find(".ae-spirit .benefits .perk").contextmenu(event => onRightClickDisplayPerkOnWA(sheet, event));
    html.find(".ae-spirit .drawbacks .perk").contextmenu(event => onRightClickDisplayPerkOnWA(sheet, event));

    if ( !sheet.options.editable ) { return; }

    html.find(".ae-spirit .current-state .awake.available").click(event => onClickAwakeSpirit(sheet, event));
    html.find(".ae-spirit .current-state .reset").click(event => onClickResetSpirit(sheet, event));
    html.find(".ae-spirit .current-state .harmonization.selectable").click(event => onClickAddSpiritHarmonization(sheet, event));

    html.find(".ae-spirit .benefits .evolve-button.available").click(event => onClickSpendXPOnSpiritBenefit(sheet, event));
    html.find(".ae-spirit .benefits .perk.new-perk").click(event => onClickLearnNewSpiritBenefitPerk(sheet, event));
    html.find(".ae-spirit .drawbacks .perk.new-perk").click(event => onClickLearnNewSpiritDrawbackPerk(sheet, event));

}

const fillPerksInfo = (perk) => {
    return mergeObject( {}, guardianSpiritPerkList().find( perkDef => perkDef.perk === perk ) );
}

const checkIfBenefitWillEvolve = (benefit) => {
    if(!benefit) { return false; }
    const missingXP = benefit.level + 1 - benefit.xp;
    return missingXP <= 1;
}

const findProgressingDrawbackKey = (selectedBenefit, willBenefitEvolve) => {
    if( !willBenefitEvolve ) { return null; }
    const nextLevel = selectedBenefit.level + 1;

    return [1, 3, 5].includes(nextLevel) ? selectedBenefit.related[0] : selectedBenefit.related[1];
}

const prepareHarmonization = (guardianSpirit, needToChooseElement, element) => {
    const harmonized = guardianSpirit.harmonizations.includes( element.element );
    const data = {
        css: harmonized ? 'harmonized' : (needToChooseElement ? 'selectable' : '')
    };
    return mergeObject( data, element );
}

const adjustPerkTooltips = (perkTab) => {

    const nbPerks = perkTab.length;
    for( let index = 0; index < nbPerks; index++ ) {

        const perk = perkTab[index];
        if( nbPerks == 6 && index == 0 ) {
            perk.left = '-50px';
        } else if( nbPerks == 6 && index == 5 ) {
            perk.left = '-130px';
        } else if( nbPerks == 5 && index == 0 ) {
            perk.left = '-70px';
        } else if( nbPerks == 5 && index == 4 ) {
            perk.left = '-110px';
        } else  {
            perk.left = '-90px';
        }
    }
}


const onClickAwakeSpirit = async (sheet, event) => {
    event.preventDefault();
    sheet._assertEditPermissions();

    await sheet.actor.guardianSpirit.awakeSpirit();
    sheet.render(true);
}

const onClickResetSpirit = async (sheet, event) => {
    event.preventDefault();

    sheet._displayData.spirit.currentSelection = null;
    await sheet.actor.guardianSpirit.bigReset();
    sheet.render(true);
}

const onClickAddSpiritHarmonization = async (sheet, event) => {
    event.preventDefault();
    sheet._assertEditPermissions();
    
    const a = event.currentTarget;
    const elem = a.dataset.elem;
    await sheet.actor.guardianSpirit.addSpiritHarmonization(elem);
    sheet.render(true);
}

const onClickSelectSpiritBenefit = async (sheet, event) => {
    event.preventDefault();
    
    const a = event.currentTarget;
    const benefitKey = a.dataset.benefit;
    if( sheet._displayData.spirit.currentSelection != benefitKey ) {
        sheet._displayData.spirit.currentSelection = benefitKey;
    } else {
        sheet._displayData.spirit.currentSelection = null;
    }
    sheet.render(true);
}

const onClickSpendXPOnSpiritBenefit = async (sheet, event) => {
    event.preventDefault();
    sheet._assertEditPermissions();
    
    await sheet.actor.guardianSpirit.spendXPOnBenefit(sheet._displayData.spirit.currentSelection);
    sheet.render(true);
}

const onClickLearnNewSpiritBenefitPerk = async (sheet, event) => {
    event.preventDefault();
    sheet._assertEditPermissions();
    
    const a = event.currentTarget;
    const perkKey = a.dataset.perk;

    await sheet.actor.guardianSpirit.learnNewPerkOnBenefit(sheet._displayData.spirit.currentSelection, perkKey);
    sheet.render(true);
}

const onClickLearnNewSpiritDrawbackPerk = async (sheet, event) => {
    event.preventDefault();
    sheet._assertEditPermissions();
    
    const a = event.currentTarget;
    const perkKey = a.dataset.perk;
    const drawbackKey = a.parentElement.parentElement.dataset.drawback;

    await sheet.actor.guardianSpirit.learnNewPerkOnDrawback(drawbackKey, perkKey);
    sheet.render(true);
}

const onRightClickDisplayPerkOnWA = async (sheet, event) => {
    event.preventDefault();

    const waHelpers = game.modules.get("world-anvil").helpers;
    await waHelpers.displayWALink(event);
}

