import { rangedManeuvers } from '../tools/WaIntegration.js';
import { AEActorBaseDataWithManeuvers } from './ActorBaseDataWithManeuvers.js';

/**
 * For managing ranged attacks
 */
export class AEActorRangedData extends AEActorBaseDataWithManeuvers {

    constructor(actor) {
        super(actor, actor.battle.ranged, rangedManeuvers, 'ranged');
    }

}
