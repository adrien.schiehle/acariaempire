import {translate} from '../tools/Translator.js';
import {songsManeuvers, songsMelodies} from '../tools/WaIntegration.js';
import {retrieveXP, giveXP} from './actor.js';
import { AEActorBaseDataWithManeuvers } from './ActorBaseDataWithManeuvers.js';

/**
 * For managing magic fo emotions
 */
export class AEActorSongsData extends AEActorBaseDataWithManeuvers {

    constructor(actor) {
        super(actor, actor.battle.songs, songsManeuvers, 'songs');
    }

    get melodies() {

        const base = this.actorData.songs?.melodies ?? {};
        const defaultValue = 0;

        const melodies = songsMelodies().map(m => {
            const result = duplicate(m);
            result.level = getProperty(base, m.melody) ?? defaultValue;
            return [m.melody, result];
        });

        return melodies;
    }

    get currentPerformance() {
        const perf = this.actorData.songs?.currentPerformance;
        if( perf && perf.type != '' ) {

            const melody = this.getMelody(perf.type);
            return {
                melodyKey: melody.melody,
                melodyName: melody.name,
                level: perf.level,
                maxLevel: Math.min( this._actor.battle.songs.maneuverMaxLevel, melody.level),
                shortcut: melody.shortcut,
                shortcutChild: melody.shortcutChild,
                dissonance: perf.dissonance ?? 0
            };
        }
        return null;
    }

    /**
     * @returns A melody definition, as described in songsMelodies(), with a level attribute in addition
     */
    getMelody(melodyName) {
        for( let [key, melody] of this.melodies ) {
            if( key === melodyName ) {
                return melody;
            }
        }
        throw melodyName + ' is not a valid melody';
    }

    async spendXPToMelody(melodyName) {
 
        this._actor._assertIsOwner();

        if( this._actor.getTotalXPLeft('power') <= 0 ) {
            ui.notifications.info(translate('AESYSTEM.actor.xp.power.noXpLeft'));
            return; // Not an error since there is no control prior to this.
        }

        const updateData = {};
        const currentMelody = this.getMelody(melodyName);
        // Level 1 shoudl always be accessible
        if(currentMelody.level != 0 && currentMelody.level >= this._actor.battle.songs.maneuverMaxLevel ) {
            ui.notifications.info(translate('AESYSTEM.actor.xp.songs.cappedByMastery'));
            return; // Not an error since there is no control prior to this.
        }

        retrieveXP(this._actor, 'power', updateData, 1);
        updateData['system.songs.melodies.' + melodyName] = currentMelody.level + 1;
        await this._actor.update(updateData);
    }

    async removeXPFromMelody(melodyName) {
 
        this._actor._assertIsOwner();

        const updateData = {};

        const currentMelody = this.getMelody(melodyName);

        if(currentMelody.level <= 0) {
            ui.notifications.info(translate('AESYSTEM.actor.xp.songs.alreadyLevel0'));
            return; // Not an error since there is no control prior to this.
        }

        giveXP(this._actor, 'power', updateData, 1);
        updateData['system.songs.melodies.' + melodyName] = currentMelody.level - 1;
        await this._actor.update(updateData);
    }

    async alterCurrentPerformance(melodyName, level, dissonance) {

        const updateData = {};
        this.getMelody(melodyName);// Will throw an exception if no melody with this name

        updateData['system.songs.currentPerformance'] = {
            type: melodyName,
            level: level,
            dissonance: dissonance
        };
        await this._actor.update(updateData);
    }

    async stopCurrentPerformance() {

        const updateData = {};
        updateData['system.songs.currentPerformance'] = {
            type: '',
            level: 0
        };
        await this._actor.update(updateData);
    }

}
