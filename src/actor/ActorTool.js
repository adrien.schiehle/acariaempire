import { ACTOR_GM_NAME } from '../tools/constants.js';
import { translate } from '../tools/Translator.js';

const findActorToken = (tokenId) => {
    const token = canvas.tokens.placeables.find(t => t.id === tokenId);
    return token ?? null;
}

const findTokenWithActorId = ( actorId, allowActorWithoutToken ) => {
    
    let token = null;
    const baseActor = game.actors.get(actorId);
    if( baseActor && ( allowActorWithoutToken || baseActor.tokenId ) ) {
        token = canvas.tokens.placeables.find( t => t.document.actorId === actorId );
    }
    return token ?? null;
}

const findPlayerWithTheLeastEventsAndChooseItsMainCharacter = ( players ) => {
    
    const activePlayerList = players.filter( u => u.active );
    
    if( activePlayerList.length == 0 ) {
        return null;
    }

    return activePlayerList.reduce( (selected, player) => {
        
        if(!selected) { return player.character; }
        const lower = player.character.cards.events.length < selected.cards.events.length;
        return lower ? player.character : selected;
    }, null);
}


export class AEActorTools {

    constructor() {

        this._gmActor = game.actors.find(actor => actor.name === ACTOR_GM_NAME);
        if( ! this._gmActor) {
            this._createGM();
        }
    }

    async _createGM() {
        const perms = {};
        perms["default"] = CONST.DOCUMENT_PERMISSION_LEVELS.OWNER;

        this._gmActor = await Actor.create({   
            type: 'character',
            name: ACTOR_GM_NAME,
            img: 'systems/acariaempire/resources/actor/gmIcon.png', 
            permission: perms
        });
    }

    /**
     * Priority given to the current selection if it's allowded.
     * Then try from the tokenId if specified
     * Then try from the actorId if specified. (will take its first token instance)
     * Then try to get the mainPlayer first token if it's not the user is not a GM
     * @returns The placeable Token
     */
     getToken({tokenId=null, actorId=null, allowCurrentSelection=true, allowActorWithoutToken=false}={}) {

        let token = null;

        // First : Try to get the first selected actor
        const canUseCurrentSelection = allowCurrentSelection && canvas.tokens.controlled.length > 0;
        if( canUseCurrentSelection ) { token = canvas.tokens.controlled[0]; }

        // If not found, try to retrieve actor by the given id
        if ( !token && tokenId) { token = findActorToken(tokenId); }

        // If not found, try to retrieve the first token from the given charater
        if( !token && actorId ) { token = findTokenWithActorId(actorId, allowActorWithoutToken); }

        // If still not found, try to get the token of the main characer of the player
        const canRetrieveDefaultPlayerToken = allowCurrentSelection && !game.user.isGM;
        if( !token && canRetrieveDefaultPlayerToken ) { token = findTokenWithActorId(this.defaultActor.id, allowActorWithoutToken); }

        return token;
    }

    /**
     * Convert the retrieved token to a AEActor
     * @returns AEActor
     */
    getActorOrTokenActor({tokenId=null, actorId=null, allowCurrentSelection=true}={}) {

        const token = this.getToken({
            tokenId: tokenId,
            actorId: actorId,
            allowCurrentSelection: allowCurrentSelection
        });

        return token?.actor ?? null;
    }

    get currentTargetActorToken() {
        const targetTokens = Array.from(game.user.targets) ?? [];
        if( targetTokens.length > 0 ) {
            return targetTokens[0].actor;
        }
        return null;
    }

    selectToken({tokenId=null, actorId=null, onlyHim=true}={}) {

        const token = this.getToken({tokenId: tokenId, actorId: actorId, allowCurrentSelection: false});
        if( ! token ) { return; }

        if( onlyHim ) {
            canvas.tokens.releaseAll();
        }
        token.control();
    }

    getCharacter(actorId) {

        let actor = null;
        
        if (actorId) { // First : Try to retrieve actor by the given id
            actor = game.actors.get(actorId);
        }

        if( !actor ) { // If not found, try to get the first selected actor

            if( canvas.tokens.controlled.length > 0 ) { 
                actor = canvas.tokens.controlled[0].actor;
                // FIXME we shouldn't need this anymore. actor = game.actors.get(actor.id); // Then switch from the token to the rela actor

            } else { // If nothing worked, try to get the current speaker
                const speaker = ChatMessage.getSpeaker();
                actor = ChatMessage.getSpeakerActor(speaker);
            }
        }

        return actor;
    }

    get allTokens() {
        const tokens = canvas.tokens.placeables.filter(t => t instanceof Token);
        return tokens.filter(t => t.actor).map(t => t.actor);
    }

    get gmActor() {
        return this._gmActor;
    }

    get defaultActor() {
        if(game.user.isGM) return this.gmActor;

        return game.user.character;
    }

    get defaultToken() {

        let result = null;
        const actor = game.user.character; // GM doesn't have a default character
        if(actor) {
            const tokens = actor.getActiveTokens();
            if( tokens.length > 0 ) {
                result = tokens[0].actor;
            }
        }

        return result;
    }

    chooseActorForCardDraw(actionMainActor, mainDefender=null, cardsAreForDefense=false) {

        const actionFromPlayerSide = actionMainActor?.prototypeToken.disposition == 1;
        const actionFromEnnemySide = actionMainActor?.prototypeToken.disposition == -1;

        let targetingPlayerSide = mainDefender ? (mainDefender.prototypeToken.disposition == 1) : actionFromEnnemySide;
        let targetingEnnemySide = mainDefender ? (mainDefender.prototypeToken.disposition == -1) : actionFromPlayerSide;

        // People on the same side => Opposing side take the cards
        if( actionFromPlayerSide && targetingPlayerSide ) {
            targetingPlayerSide = false;
            targetingEnnemySide = true;

        } else if( actionFromEnnemySide && targetingEnnemySide ) {
            targetingPlayerSide = true;
            targetingEnnemySide = false;
        }
        
        // First case, cards are for the ennemy side : GM draw the cards
        const forGM = cardsAreForDefense ? targetingEnnemySide : actionFromEnnemySide;
        if( forGM ) { 
            return this.gmActor; 
        }

        const forPlayers = cardsAreForDefense ? targetingPlayerSide : actionFromPlayerSide;
        if( forPlayers ) {
            const protagonist = cardsAreForDefense ? mainDefender : actionMainActor;

            const allPlayers = game.users.contents.filter( u => !u.isGM );
            const relatedPlayers = protagonist?.playersOwningIt ?? [];

            // Only give cards to players have a character and currently in game
            const availablePlayers = (relatedPlayers.length > 0 ? relatedPlayers : allPlayers).filter(u => {
                return u.active && u.character;
            });

            if( availablePlayers.length > 0 ) {
                return findPlayerWithTheLeastEventsAndChooseItsMainCharacter(availablePlayers);

            } else {
                ui.notifications.warn(translate('AESYSTEM.actor.warnNoPlayerConnected'));
                return null;
            }
        }

        // Last case : A neutral party is involved => No card is drawn
        return null;
    }

}