import {translate} from '../tools/Translator.js';
import {generateWALinks, attributeList} from '../tools/WaIntegration.js';
import * as utils from './ActorSheetUtils.js';
import * as headers from './ActorSheetHeaders.js';
import * as attributes from './ActorSheetAttributes.js';
import * as abilities from './ActorSheetAbilities.js';
import * as items from './ActorSheetItems.js';
import * as battles from './ActorSheetBattles.js';
import * as spirits from './ActorSheetSpirits.js';
import * as others from './ActorSheetOthers.js';
import * as merchant from './ActorSheetMerchant.js';
import * as monster from './ActorSheetMonster.js';


/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class AEActorSheet extends ActorSheet {

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["acariaempire", "sheet", "actor"],
            template: "systems/acariaempire/resources/actor/sheet/actor-sheet.hbs",
            width: 680,
            height: 770,
            dragDrop: [
                {dragSelector: ".ae-items .bag-layer", dropSelector: null},
                {dragSelector: ".attribute .key", dropSelector: null},
                {dragSelector: ".equipment-slots .slot-item", dropSelector: null},
                {dragSelector: ".bag-slot .bag-item", dropSelector: null},
                {dragSelector: ".interaction .key", dropSelector: null},
                {dragSelector: ".unlocked-skill .skill-name", dropSelector: null},
                {dragSelector: ".ability-col .ability-title", dropSelector: null}
            ]
        });
    }

    constructor(...args) {
        super(...args);
        this._menuChoices = headers.loadMenuChoices(this.actor);
        this._currentMenu = this._menuChoices[0];
        this._displayData = {
            learnNewSkill: {
                display: false,
                skillType: 'combat',
                label: ''
            },
            header: {
                selectedIcon: null
            }, 
            spirit: {
                currentSelection: null
            },
            melee: {
               currentManeuver: null 
            },
            ranged: {
                currentManeuver: null 
            },
            improvised: {
                currentManeuver: null 
            },
            arcane: {
                currentManeuver: null 
            },
            divine: {
                currentManeuver: null 
            },
            songs: {
                currentManeuver: null 
            },
            merchant: {},
            monster: {}
        };
    }

    showEventPanel() {
        if( this._menuChoices.includes('events') ) {
            this._currentMenu = 'events';
        }
        this.render(true);
    }

    showTroublePanel() {
        if( this._menuChoices.includes('troubles') ) {
            this._currentMenu = 'troubles';
        }
        this.render(true);
    }

/*------------------------------------------
    Building data for HTML sheet
--------------------------------------------*/

    /** @override */
    getData() {
        const data = super.getData();
        data.gui = {};
        const menu = headers.customNavigator(this._menuChoices, this._currentMenu, this.position);

        data.gm = game.user.isGM;
        data.gmActor = this.actor.isGMActor;
        data.menu = menu;
        headers.dataForActorHeader(this.actor, data, this._displayData.header);

        if( menu.tabs.attributes )   attributes.dataForAttributes(this.actor, data);
        if( menu.tabs.abilities )    abilities.dataForAbilities(this.actor, data, this._displayData.learnNewSkill);
        if( menu.tabs.equipments )   items.dataForEquipment(this.actor, data);
        if( menu.tabs.bags )         items.dataForBags(this.actor, data);
        if( menu.tabs.melee )        battles.dataForMelee(this.actor, data, this._displayData.melee);
        if( menu.tabs.ranged )       battles.dataForRanged(this.actor, data, this._displayData.ranged);
        if( menu.tabs.improvised )   battles.dataForImprovised(this.actor, data, this._displayData.improvised);
        if( menu.tabs.arcane )       battles.dataForArcaneKnowledge(this.actor, data, this._displayData.arcane);
        if( menu.tabs.divine )       battles.dataForDivinePacts(this.actor, data, this._displayData.divine);
        if( menu.tabs.songs )        battles.dataForSongs(this.actor, data, this._displayData.songs);
        if( menu.tabs.spirit )       spirits.dataForSpirit(this.actor, data, this._displayData.spirit);
        if( menu.tabs.troubles )     others.dataForTroubles(this.actor, data);
        if( menu.tabs.xp )           others.dataForXP(this.actor, data);
        if( menu.tabs.merchant )     merchant.dataForMerchant(this.actor, data, this._displayData.merchant);
        if( menu.tabs.monster )      monster.dataForActorMonster(this.actor, data, this._displayData.monster);

        return data;
    }


/*------------------------------------------
    Managing events and clicks
--------------------------------------------*/

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        generateWALinks(html);

        headers.bindHeaderButtons(this, html);
        attributes.bindAttributeActions(this, html);
        abilities.bindAbilitiesActions(this, html);
        battles.bindBattleActions(this, html);
        items.bindItemsActions(this, html);
        spirits.bindSpiritActions(this, html);
        others.bindTroubleActions(this, html);
        others.bindXPActions(this, html);
        merchant.bindMerchantButtons(this, html);
        monster.bindMonsterButtons(this, html);
    }

    _assertEditPermissions() {

        if( ! this.actor.allowedToEdit ) {
            ui.notifications.info(translate('AESYSTEM.actor.notOwned'));
            throw "You're not this character owner!";
        }
    }

/*------------------------------------------
    Other overridden function
--------------------------------------------*/

    /** @override */
    _onDragStart(event) {

        const a = event.currentTarget;
        const dataset = a.dataset;

        if( dataset.dragged === 'equipment' ) {
            this._onDragStartEquipment(event, dataset.id);

        } else if( dataset.dragged === 'attribute' ) {
            this._onDragStartAttribute(event, a.parentElement.dataset.attribute);

        } else if( dataset.dragged === 'ability' ) {
            this._onDragStartAbility(event, dataset.label, dataset.ability, dataset.skillchoice);

        } else if( dataset.dragged === 'battle' ) {
            this._onDragStartBattle(event, dataset.label, dataset.category, dataset.subchoice);

        } else {
            console.log('onDragStart not found');
        }

    }

    _onDragStartEquipment(event, eqtId) {

        const eqt = this.actor.equipment.all.find(e => e.item.id === eqtId);

        const cmd = "game.aesystem.equipItemAndUseIt({" 
                            + "\n\t//tokenId: '" + this.actor.tokenId + "', "
                            + "\n\t//actorId: '" + this.actor.id + "', "
                            + "\n\titemId: '" + eqtId + "', "
                            + "\n\titemName: " + JSON.stringify(eqt.item.name) + "\n});";

        let macroData = {
            name: eqt?.item.name,
            type: 'script',
            scope: 'global',
            img: eqt?.item.img,
            command: cmd
        };

        const dragData = {
            type: 'Macro',
            data: macroData
        };
        event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
    }

    _onDragStartAttribute(event, attribute) {

        const tokenId = this.actor.tokenId;
        const cmd = "game.aesystem.triggerRollPanel({" 
                 + (tokenId ? "\n\t//tokenId: '" + tokenId + "', " : "")
                            + "\n\t//actorId: '" + this.actor.id + "', "
                            + "\n\tabilityCheck: 'explore', "
                            + "\n\tattribute: '" + attribute + "'\n});";

        let macroData = {
            name: this.actor.name + ' - ' + attributeList().find( a => a.key === attribute )?.name,
            type: 'script',
            scope: 'global',
            img: 'systems/acariaempire/resources/macros/actions/attribute.png',
            command: cmd
        };

        const dragData = {
            type: 'Macro',
            data: macroData
        };
        event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
    }

    _onDragStartAbility(event, label, ability, skillChoice) {

        if( ability == 'combat' ) {
            const category = skillChoice ?? 'melee';

            let subchoice = null;
            if( category == 'improvised') { subchoice = 'improvised'; }
            if( category == 'arcane') { subchoice = 'spell'; }
            if( category == 'prayer') { subchoice = 'miracle'; }
            this._onDragStartBattle(event, label, category, subchoice );
            return;
        }
        
        const icon = 'systems/acariaempire/resources/macros/actions/skill_' + ability + '.png';

        const tokenId = this.actor.tokenId;
        const cmd = "game.aesystem.triggerRollPanel({" 
                 + (tokenId ? "\n\t//tokenId: '" + tokenId + "', " : "")
                            + "\n\t//actorId: '" + this.actor.id + "', "
                            + "\n\tabilityCheck: '" + ability + "', "
             + (skillChoice ? "\n\tskillChoice: '" + skillChoice + "', " : "")
                            + "\n});";
        
        const macroData = {
            name: label,
            type: 'script',
            scope: 'global',
            img: icon,
            command: cmd
        };

        const dragData = {
            type: 'Macro',
            data: macroData
        };
        event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
    }

    _onDragStartBattle(event, label, category, subchoice) {

        const icon = 'systems/acariaempire/resources/macros/actions/' + category + (subchoice ? '_' + subchoice : '' ) + '.png';

        const tokenId = this.actor.tokenId;
        const cmd = "game.aesystem.triggerRollPanel({" 
                 + (tokenId ? "\n\t//tokenId: '" + tokenId + "', " : "")
                            + "\n\t//actorId: '" + this.actor.id + "', "
                            + "\n\tbattle: '" + category + "'"
              + (subchoice ? ",\n\tbattleSubchoice: '" + subchoice + "'" : "")
                            + "\n});";

        const macroData = {
            name: label,
            type: 'script',
            scope: 'global',
            img: icon,
            command: cmd
        };

        const dragData = {
            type: 'Macro',
            data: macroData
        };
        event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
    }

    /** @override */
    async _onDrop(event) {
        items.onDropRetrieveItem(this, event);
    }
}


