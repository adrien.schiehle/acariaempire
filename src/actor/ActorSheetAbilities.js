import {translate} from '../tools/Translator.js';
import {jobList, skillList, skillLevels} from '../tools/WaIntegration.js';
import {removingXPAllowed} from '../tools/gmactions.js';
import * as utils from './ActorSheetUtils.js';

/**
 * Prepare data for Skill area tab in actor sheet
 * @param {AEActor} actor Displayed actor
 * @param {object} updatedData Sheet data. This tab related data will be stored inside updatedData.gui.abilities
 */
 export const dataForAbilities = (actor, updatedData, displayData) => {


    if(actor.abilities.isSet) {

        const tooltipCssList = ['first', 'second', 'third'];

        const abilities = actor.abilities.allAbilities.map( skillArea => {

            const evolveBtn = evolveBtnData(actor, skillArea);
            const removeBtn = removeBtnData(skillArea);

            const data = {
                levelSummary : buildLevelSummary(skillArea),
                xptab: utils.buildXPBubblesForAbility(skillArea.levelUpInfo, {bluePoint: evolveBtn.available, multiplier:2 }),
                button: evolveBtn,
                forgetBtn: removeBtn,
                tooltipCss: tooltipCssList[skillArea.order],
                canLearnNewSkill: actor.abilities.canLearnNewSkill, 
                skills: prepareUnlockedSkillListForArea(skillArea)
            };
            return mergeObject( data, skillArea );
        });

        const selectedAbility = actor.abilities.allAbilities.find( a => a.type === displayData.skillType );
        const availableSkills = skillList().filter(skill => {
            return skill.available;
        }).filter(skill => {
            return skill.skillType === displayData.skillType && ! selectedAbility.unlocked.find( u => u.key === skill.key );
        });

        updatedData.gui.abilities = {
            isSet: true,
            ref: actor.abilities.ref,
            icon : actor.abilities.icon,
            name : actor.abilities.name,
            level : actor.abilities.level,
            learnedOnes: abilities,
            learnNewSkill: displayData,
            availableSkills: availableSkills
        }
    } else {

        const allJobs = jobList().filter(j => {
            return j.forMonsters === actor.isMonster;
        }).map(j => {
            return {
                ref: j.job,
                icon : j.icon,
                name : j.name
            };
        }).sort((a,b) => {
            return a.name.localeCompare(b.name);
        });

        updatedData.gui.abilities = {
            isSet: false,
            learnNewSkill: displayData,
            allJobs: allJobs,
            level: 0
        };
    }
}

/**
 * Bind buttons for the ability tab
 * @param {AEActorSheet} sheet parent
 * @param {HTMLElement} html Sheet content
 */
 export const bindAbilitiesActions = ( sheet, html ) => {

    if ( !sheet.options.editable ) { return; }
    html.find('.ae-abilities .attribute-roll').click(event => onClickRollDice(sheet, event));

    html.find('.ae-abilities .starting-job .remove-job').click(event => onClickStartingJobRemove(sheet, event));
    html.find('.ae-abilities .available-job .job-data').click(event => onClickStartingJobChange(sheet, event));
    html.find('.ae-abilities .available-skill .skill-data').click(event => onClickLearnNewAbilitySkill(sheet, event));
    html.find('.ae-abilities .unlocked-skill .forget-skill').click(event => onClickForgetAbilitySkill(sheet, event));

    html.find('.ae-abilities .ability-col .ability-check').click(event => onClickRollDice(sheet, event));
    html.find('.ae-abilities .ability-col .learn-skill').click(event => onClickToggleNewAbilitySkill(sheet, event));
    html.find('.ae-abilities .cancel-learn-skill').click(event => onClickCancelLearnNewAbilitySkill(sheet, event));

    html.find('.ae-abilities .ability-col .evolve-button.available').click(event => onClickSpendXPOnSkillArea(sheet, event));
    html.find('.ae-abilities .ability-col .evolve-button.forget').click(event => onClickReinitSkillArea(sheet, event));
}


// Convenience function for preparing evolve btn data
const evolveBtnData = (actor, skillArea) => {
    const editAllowed = actor.allowedToEdit;
    const xpLeft = actor.getTotalXPLeft('skill') > 0;

    const result = {
        css: '',
        available: false
    };

    const currentLevel = skillArea.levelUpInfo.level;
    const currentXP = skillArea.levelUpInfo.xp;

    if( skillArea.levelUpInfo.xpCost == 0 ) {
        result.title = 'Maximum atteint';

    } else {
        if( ! editAllowed || ! xpLeft || currentLevel >= skillArea.levelUpInfo.maxLevel ) {
            result.title = 'Impossible d\'augmenter';

        } else {
            result.css = 'available';
            result.available= true;
            const nearLevel = (currentXP >= skillArea.levelUpInfo.xpCost - 1);
            result.title = nearLevel ? 'Progresse d\'un niveau' : 'S\'entrainer';
        }
    }
    return result;
}

const removeBtnData = (skillArea) => {

    const removingXP = removingXPAllowed();
    const xpSpent = skillArea.levelUpInfo.level > 0 || skillArea.levelUpInfo.xp > 0;

    return {
        displayed: removingXP && xpSpent,
        title: 'Réinitialiser'
    };
}

const buildLevelSummary = (skillArea) => {
    return  '' + skillArea.level + ' : ' + skillLevels().find( s => s.level === skillArea.level ).name;
}

const prepareUnlockedSkillListForArea = (skillArea) => {
    const removingXpOk = removingXPAllowed();

    const skills = [];
    for( let i = 0; i < skillArea.unlocked.length; i++ ) {
        const spiritSkill = skillArea.unlocked[i].spiritSkill
        const addData = {
            deleteBtn: removingXpOk && ! spiritSkill && i != 0,
            skillType: skillArea.key,
            css: spiritSkill ? 'blue' : 'notblue'
        };
        skills.push( mergeObject( addData, skillArea.unlocked[i] ));
    }
    skills.sort( (a,b) => a.css.localeCompare(b.css) );
    return skills;
}

/** Event for removing current job **/
const onClickStartingJobRemove = async (sheet, event) => {

    event.preventDefault();
    sheet._assertEditPermissions();

    await sheet.actor.abilities.removeStartingJob();
    sheet.actor.render();
}

const onClickStartingJobChange = async (sheet, event) => {

    event.preventDefault();
    sheet._assertEditPermissions();
    const a = event.currentTarget;
    const newJob = a.dataset.job;

    await sheet.actor.abilities.changeStartingJob(newJob);
    sheet.actor.render();
}

const onClickLearnNewAbilitySkill = async (sheet, event) => {
    event.preventDefault();
    sheet._assertEditPermissions();

    const a = event.currentTarget;
    const skillKey = a.dataset.skill;
    const skillType = a.parentElement.parentElement.dataset.skilltype;
    
    sheet._displayData.learnNewSkill.display = false;
    await sheet.actor.abilities.unlockNewSkill(skillType, skillKey);
    sheet.actor.render()
}

const onClickForgetAbilitySkill = async (sheet, event) => {
    event.preventDefault();
    sheet._assertEditPermissions();

    const a = event.currentTarget;
    const skillKey = a.parentElement.dataset.skill;
    const skillType = a.parentElement.dataset.skilltype;

    await sheet.actor.abilities.forgetUnlockedSkill(skillType, skillKey);
    sheet.actor.render();
}

    
const onClickRollDice = async (sheet, event) => {
    event.preventDefault();
    sheet._assertEditPermissions();
    
    const a = event.currentTarget;
    const skillType = a.parentElement.dataset.skilltype;

    const options = {};
    if( skillType === 'combat' ) {
        // For battle, set interaction GUI on the first learned battle mode
        const unlocked = sheet.actor.abilities.combatAbility.unlocked;
        if( unlocked.length > 0 ) {
            options.battle = unlocked[0].key;
        }
    } else {
        // Simply put interaction GUI on the right settings
        options.abilityCheck = skillType;
    }

    game.aesystem.triggerRollPanel(options);
}

const onClickToggleNewAbilitySkill = async (sheet, event) => {
    event.preventDefault();
    sheet._assertEditPermissions();

    const a = event.currentTarget;
    const skillType = a.parentElement.dataset.skilltype;

    sheet._displayData.learnNewSkill.display = true;
    sheet._displayData.learnNewSkill.skillType = skillType;
    sheet._displayData.learnNewSkill.label = translate('AESYSTEM.actor.sheet.abilities.chooseNewSkill.' + skillType);
    sheet.actor.render();
}

const onClickCancelLearnNewAbilitySkill = async (sheet, event) => {
    event.preventDefault();
    sheet._assertEditPermissions();
    
    sheet._displayData.learnNewSkill.display = false;
    sheet.actor.render();
}

const onClickSpendXPOnSkillArea = async (sheet, event) => {
    event.preventDefault();
    sheet._assertEditPermissions();

    const a = event.currentTarget;
    const skillType = a.parentElement.parentElement.dataset.skilltype;
    await sheet.actor.abilities.spendXPToAbility(skillType);

    sheet.actor.render();
}

const onClickReinitSkillArea = async (sheet, event) => {
    event.preventDefault();
    sheet._assertEditPermissions();

    const a = event.currentTarget;
    const skillType = a.parentElement.parentElement.dataset.skilltype;
    await sheet.actor.abilities.reinitAbility(skillType);

    sheet.actor.render();
}
