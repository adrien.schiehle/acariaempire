import { improvisedManeuvers } from '../tools/WaIntegration.js';
import { AEActorBaseDataWithManeuvers } from './ActorBaseDataWithManeuvers.js';

/**
 * For managing improvised attacks
 */
export class AEActorImprovisedData extends AEActorBaseDataWithManeuvers {

    constructor(actor) {
        super(actor, actor.battle.improvised, improvisedManeuvers, 'improvised');
    }
}
