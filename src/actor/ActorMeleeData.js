import { meleeManeuvers } from '../tools/WaIntegration.js';
import { AEActorBaseDataWithManeuvers } from './ActorBaseDataWithManeuvers.js';

/**
 * For managing melee attacks
 */
export class AEActorMeleeData extends AEActorBaseDataWithManeuvers {

    constructor(actor) {
        super(actor, actor.battle.melee, meleeManeuvers, 'melee');
    }

}
