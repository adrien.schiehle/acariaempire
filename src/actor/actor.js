import { translate } from '../tools/Translator.js';
import { allSpellsDefinition, attributeLevels, attributeList, spellCategories } from '../tools/WaIntegration.js';
import { removingXPAllowed } from '../tools/gmactions.js';
import { spellEffects } from '../tools/spells.js';
import { ACTOR_GM_NAME, SPELLEFFECT_STACK_TYPE } from '../tools/constants.js';
import { FATIGUE_EFFECT } from '../tools/activeEffects.js';

import  {AEActorBattle } from './ActorBattle.js';
import { AEActorArcaneData } from './ActorArcaneData.js';
import { AEActorDivineData } from './ActorDivineData.js';
import { AECardsData } from './ActorCardsData.js';
import { AEActorEquipmentData } from './ActorEquipmentData.js';
import { AEActorSongsData } from './ActorSongsData.js';
import { AEActorMeleeData } from './ActorMeleeData.js';
import { AEActorRangedData } from './ActorRangedData.js';
import { AEActorImprovisedData } from './ActorImprovisedData.js';
import { AEActorAbilities } from './ActorAbilities.js';
import { AEActorGuardianSpiritData } from './ActorGuardianSpiritData.js';
import { AEActorStatus } from './ActorStatus.js';
import { AEActorMerchantData } from './ActorMerchantData.js';
import { AEActorMonsterData } from './ActorMonsterData.js';

export const ARCANE_MAX_LEVEL = 5;
export const MELODY_MAX_LEVEL = 5;
export const PACT_MAX_LEVEL = 5;

export const handleActorsDefaultData = (actor) => {
    const isMonster = actor.type == 'monster';
    const isPlayer = actor.type == 'player';

    const chooseDisposition = (currentDisposition) => { 

        // Special case : An invocated monster can be on player side
        if( actor.flags?.acariaempire?.invocation ) { return currentDisposition; }
        if( isMonster ) return -1;
        if( isPlayer ) return 1;
        return 0;
    }

    // Token customization
    const token = actor._source.prototypeToken;
    token.disposition= chooseDisposition(token.disposition);
    token.displayBars= 20;
    token.bar1= { attribute: 'health' };
    token.bar2= { attribute: 'power' };
    token.sight.enabled = true;
    token.sight.range = 50;
    token.actorLink= isPlayer;

    // Allow other player to see the bio
    if( isPlayer ) {
        const perm = actor._source.ownership ?? {};
        perm.default = CONST.DOCUMENT_PERMISSION_LEVELS.LIMITED
        actor._source.ownership = perm;
    }
}

/** Used for dispaying icons on actor directory for players who still have some XP left.*/
export const handleActorDisplayInSidebar = (element, actor) => {

	if( actor.playersOwningIt.length == 0 ) { // Only add infos for player characters
        return; 
    }

    const iconList = [];
    if( actor.getTotalXPLeft('all') > 0 ) {
        iconList.push('systems/acariaempire/resources/actor/sheet/xp_all.png');
    }

    if( actor.guardianSpirit.energyLeft > 0 ) {
        iconList.push('systems/acariaempire/resources/actor/sheet/xp_energy.png');
    }

    if( iconList.length > 0 ) {
        let addition = '<div class="flexcol additional-infos">';
        iconList.forEach( icon => {
            addition += '<img class="additional-icon" src="' + icon + '">';
        });
        addition += '</div>';
        element.innerHTML += addition;
    }
}

/** Clamp a resource value into min max and store it inside updateData **/
export const setResource = (updateData, resourceName, value, min, max) => {

    if( value < min ) {
        value = min;
    } else if( value > max ) {
        value = max;
    }

    updateData['system.' + resourceName + '.value'] = value;
}

/** Removes one XP from the correct pool and trace it inside updateData **/
export const retrieveXP = (from, xpType, updateData, quantity=1) => {

    const xp = from.system.xpToSpend;
    let needed = quantity;

    if( xpType == 'attribute' && xp.attribute > 0 ) {
        let chunk = xp.attribute >= needed ? needed : xp.attribute;
        needed -= chunk;

        updateData['system.xpToSpend.attribute'] = xp.attribute - chunk;
    }

    if( needed > 0 && xpType == 'skill' && xp.skill > 0 ) {
        let chunk = xp.skill >= needed ? needed : xp.skill;
        needed -= chunk;

        updateData['system.xpToSpend.skill'] = xp.skill - chunk;
    }

    if( needed > 0 && xpType == 'power' && xp.power > 0 ) {
        let chunk = xp.power >= needed ? needed : xp.power;
        needed -= chunk;

        updateData['system.xpToSpend.power'] = xp.power - chunk;
    }

    if( needed > 0 && xp.free > 0 ) {
        let chunk = xp.free >= needed ? needed : xp.free;
        needed -= chunk;

        updateData['system.xpToSpend.free'] = xp.free - chunk;
    }

    if( needed > 0 ) {
        ui.notifications.warn('No XP left');
        throw 'No XP left';
    }
}

/** Give xp of a selected category **/
export const giveXP = (to, xpType, updateData, quantity=1) => {

    const xp = to.system.xpToSpend;
    if(xpType == 'attribute' ) {
        updateData['system.xpToSpend.attribute'] = xp.attribute + quantity;

    } else if(xpType == 'skill' ) {
        updateData['system.xpToSpend.skill'] = xp.skill + quantity;
        
    } else if(xpType == 'power' ) {
        updateData['system.xpToSpend.power'] = xp.power + quantity;
        
    } else {
        if(xpType != 'free' ) {
            console.warn('AEActor | Unknown xp type ' + xpType + '. Was given as free XP');
        }
        updateData['system.xpToSpend.free'] = xp.free + quantity;
    }
}

const BASE_INITIATIVE = 5;
const BASE_INITIATIVE_SURPRISED = 10;

const timeTrackerFlag = (actor, {suffix='initiative'} = {}) => {
    return 'combattracker.' + game.combats.active?.id + '.' + actor.tokenId + '.' + suffix;
}


/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class AEActor extends Actor {

    constructor(...args) {
        super(...args);
    }

    /** @override */
    _onUpdate(data, options, userId) {
        this.clearCache();
        super._onUpdate(data, options, userId);

        // Update battle tracker is necessary
        const gmActor = game.aesystem.actorTools.gmActor;
        const combatId = game.combats.active?.id;
        if( game.user.isGM && gmActor.id === this.id && combatId ) { 

            const combatTrackerFlag = data?.flags?.acariaempire?.combattracker[combatId];
            if( combatTrackerFlag  ) {
                const modifiedTokenIds = Object.keys(combatTrackerFlag);
                game.combats.active?.gmTimetrackersMayHaveChanged(modifiedTokenIds); 
            }
        }

        // When attribute changes, there may be some impact on overload condition
        if( game.user.id === userId ) {
            this.reloadAllEffects();
        }
    }

    /** @override */
    _onCreateEmbeddedDocuments(embeddedName, documents, result, options, userId) {
        this.clearCache();
        super._onCreateEmbeddedDocuments(embeddedName, documents, result, options, userId);
    }

    /** @override */
    _onUpdateEmbeddedDocuments(embeddedName, documents, result, options, userId) {
        this.clearCache();
        super._onUpdateEmbeddedDocuments(embeddedName, documents, result, options, userId);
        if( embeddedName == "Item" ) {
            Hooks.call("updateActorEquipment", this, result, options, userId);
        }

        // When equipment change, need to reload their active effects
        if( game.user.id === userId ) {
            this.reloadAllEffects();
        }
    }

    /** @override */
    _onDeleteEmbeddedDocuments(embeddedName, documents, result, options, userId) {
        this.clearCache();
        super._onDeleteEmbeddedDocuments(embeddedName, documents, result, options, userId);
    }

    clearCache() {
        this._attributes = null;
    }

/*------------------------------------------
    Convenience getters
--------------------------------------------*/

    get isMonster() {
        return this.type == 'monster';
    }

    get isNPC() {
        return this.type == 'character';
    }

    get isGMActor() {
        if( game.aesystem ) {
            return game.aesystem?.actorTools.gmActor.id == this.id; 
        } 
        
        // May be called before aesystem initialization. In that case, degraded check
        return this.name === ACTOR_GM_NAME;
    }

    get mainCharacterPlayer() {

        for ( let u of game.users.contents ) {
            if ( u.isGM || ! u.character ) continue;

            if ( u.character.id == this.id )
                return true;
        }
        return false;
    }

    get playersOwningIt() {

        let users = [];
        if( ! this.isGMActor ) {

            // All players have owner rights on GM Actor so that they can give him cards.
            // But they are not really owning it.
            
            const perm = this.ownership;
            const ownerValue = CONST.DOCUMENT_PERMISSION_LEVELS.OWNER;
    
            for ( let u of game.users.contents ) {
                if ( u.isGM ) continue;
    
                if ( perm["default"] >= ownerValue || perm[u.id] >= ownerValue )
                    users.push(u);
            }
        }
        return users;
    }

    get tokenId() {

        if( this.isToken ) return this.token.id;
        
        const tokens = this.getActiveTokens();
        if( tokens.length > 0 ) return tokens[0].id;
        return null;
    }

    /** Convenience fonction fixing alias and user id. See ChatMessage.create for more information on msgData */
    async sendMessage(msgData, options= {}) {
        msgData.user = game.user.id;
        msgData.speaker = { 
            actor: this.id,
            alias: this.name
        };
        return ChatMessage.create(msgData, options);
    }

    get isInBattle() {
        if( game.combats.active?.getCombatantByToken(this.tokenId) ) {
            return true;
        }
        return false;
    }

    get allowedToEdit() {

        return game.user.isGM || this.isOwner;
    }


    getAttribute(attributeName) {
        return this.allAttributes?.find( a => a.key === attributeName ) ?? null;
    }

    getAttributeLevel(attributeName) {
        return getProperty(this.system.attributes, attributeName)?.level ?? 0;
    }

    get allAttributes() {

        if( ! this._attributes && attributeList() ) { // Meaning that the game is properly initialized

            this._attributes = attributeList().map( attributeDef => {
                const data = mergeObject( {}, attributeDef );
                const attributeName = attributeDef.key;

                const sourceAttribute = getProperty(this._source.system.attributes, attributeName) ?? {level: -2, xp: 0, speciality:null };
                const modifiedAttribute = getProperty(this.system.attributes, attributeName) ?? {level: -2, xp: 0, speciality:null };

                data.levelUpInfo = {
                    xp: sourceAttribute.xp, 
                    minLevel: modifiedAttribute.min,
                    maxLevel: modifiedAttribute.max
                };
                mergeObject( 
                    data.levelUpInfo, 
                    attributeLevels().find( l => l.level === sourceAttribute.level )
                );
        
                data.initialValue = sourceAttribute.level;
                data.value = Math.clamped( 
                    modifiedAttribute.level, 
                    data.levelUpInfo.minLevel, 
                    data.levelUpInfo.maxLevel
                );

                return data;
            } );
        }

        return this._attributes;
    }

    get abilities() {
        return new AEActorAbilities(this);
    }

    get melee() {
        return new AEActorMeleeData(this);
    }

    get ranged() {
        return new AEActorRangedData(this);
    }

    get improvised() {
        return new AEActorImprovisedData(this);
    }

    get arcane() {
        return new AEActorArcaneData(this);
    }

    get divine() {
        return new AEActorDivineData(this);
    }

    get songs() {
        return new AEActorSongsData(this);
    }

    get battle() {
        return new AEActorBattle(this);
    }

    get status() {
        return new AEActorStatus(this);
    }

    get cards() {
        return new AECardsData(this);
    }

    get equipment() {
        return new AEActorEquipmentData(this);
    }

    get guardianSpirit() {
        return new AEActorGuardianSpiritData(this);
    }

/*------------------------------------------
    NPC specific subclass
--------------------------------------------*/

    get merchant() {
        return new AEActorMerchantData(this);
    }

/*------------------------------------------
    Monster specific subclass
--------------------------------------------*/

    get monster() {
        return new AEActorMonsterData(this);
    }

/*------------------------------------------
    Active effects
--------------------------------------------*/

    async reloadAllEffects() {

        // reloadEffects are done when actor is updated, 
        //    and when and embedded element is UPDATED
        //    For now, its not done when an embedded element is created or deleted
        
        // reloadEffects only create and drop new ActiveEffects
        //    It can then be done in one go whitout fear of calling it again many times
        
        // WARNING : If reloadAllEffects become called on enbedded element creation and deletion
        //    We will need to stop after one is done and wait for the next call for the others to be done

        await this.reloadHealthEffects();
        await this.equipment.reloadEffects();
        await this.guardianSpirit.reloadEffects();
        await this.battle.reloadEffects();
        await this.status.reloadEffects();
        if( this.isMonster ) { await this.monster.reloadEffects(); }
    }

    get spellEffects() {
        const effects = this.effects ?? [];
        return effects.filter( e => {
            return e.getFlag('acariaempire', 'spell');
        }).map( e => {
            const spellInFlag = e.getFlag('acariaempire', 'spell');
            const spellDef = allSpellsDefinition().spells.find( s => s.key === spellInFlag.definition);
            const spellCategory = spellCategories().find( sc => sc.key === spellDef?.relatedEffect );
            const spell = mergeObject({
                level: spellInFlag.level,
                levelIcon: allSpellsDefinition().levelIcons[spellInFlag.level]
            }, spellDef);

            return {
                effectId: e.id,
                spellId: spellInFlag.id,
                effect: e,
                spell: spell,
                offensive:  spellCategory?.offensive ?? false,
                mainElement: spellInFlag?.mainElement ?? '',
                stacks: spellInFlag.stacks ?? { type: SPELLEFFECT_STACK_TYPE.none, amount: 0 }
            };
        });
    }

    /**
     * Add a spell effect to this character
     * @param {ActiveEffectData} activeEffectData 
     * @returns The new effect on this character. With the format it would have if retrieved via this.spellEffetcs
     */
    async addSpellEffect(activeEffectData) {

        const spellId = activeEffectData.flags?.acariaempire?.spell?.id;
        if( !spellId ) {
            ui.notifications.error(translate('AESYSTEM.actor.activeEffect.spellWithoutId') );
            return null;
        }

        const alreadHere = this.spellEffects.findIndex( s => s.spellId === spellId ) != -1;
        if( alreadHere ) {
            const msg = translate('AESYSTEM.actor.activeEffect.alreadyAdded').replace( 'ACTOR', this.name );
            ui.notifications.info(msg)
            return null;
        }

        // Build the effect data
        const effectData = Object.assign({}, activeEffectData);
        effectData.changes = [];
        effectData.flags = {};

        const spellRef = activeEffectData.flags.acariaempire.spell.definition;
        const level = activeEffectData.flags.acariaempire.spell.level;
        const mainElement = activeEffectData.flags.acariaempire.spell.mainElement;
        const spellEffect = spellEffects[spellRef];
        if( spellEffect ) {
            const value = spellRef + ':' + level + ':' + mainElement;
            effectData.changes.push({
                mode: CONST.ACTIVE_EFFECT_MODES.CUSTOM, 
                key: spellEffects.CHANGE_KEY, 
                value: value, 
                priority: spellEffect.priority
            });

            effectData.flags = {
                acariaempire: {
                    spell: {
                        stacks: spellEffect.createStacks(level)
                    }
                }
            };

            // Some spells triggers something when created (Like invocation)
            if( spellEffect.onCreateSpell ) {
                await spellEffect.onCreateSpell(this, spellId, level, mainElement);
            }
        }
        mergeObject(effectData.flags, activeEffectData.flags);

        await this.createEmbeddedDocuments("ActiveEffect", [effectData] );
        return this.spellEffects.find( se => se.spellId === spellId );
    }

    /**
     * Remove some spell effects from this character.
     * Do nothing if ids are not present.
     * WARNING: 
     * All spell removal should go through this method.
     * Directly calling  deleteEmbeddedDocuments("ActiveEffect", [...] ) would lead to some triggers not being called
     * @param {object[]} spellIds Spell Ids 
     */
    async removeSpellEffects(spellIds) {

        const spellsToRemove = this.spellEffects.filter( se => spellIds.includes(se.spellId) );
        if( spellsToRemove.length > 0 ) {

            // Some spells triggers something when dropped (Like invocation)
            for( let spellData of spellsToRemove ) {
                const spellEffect = spellEffects[spellData.spell.key];
                if( spellEffect?.onDropSpell ) {
                    await spellEffect.onDropSpell( this, spellData.spellId, spellData.spell.level, spellData.mainElement );
                }
            }

            const effectIds = spellsToRemove.map( se => se.effectId );
            await this.deleteEmbeddedDocuments("ActiveEffect", effectIds );
        }
    }

    /**
     * Some spell effects have stacks. 
     * This will allow you to remove stacks of a given type and remove the effect is no stacks left
     * @param {string} stackType see SPELLEFFECT_STACK_TYPE
     * @param {int} amount Optional. Amount of stacks that should be removed. By default 1
     */
    async consumeSpellEffectStack(stackType, amount=1) {
        const updatedActiveEffects = [];
        const removedSpellIds = [];

        this.spellEffects.filter( se => {
            return se.stacks.type === stackType;
        }).forEach( se => {
            if( se.stacks.amount <= amount ) {
                removedSpellIds.push( se.spellId );
            } else {
                updatedActiveEffects.push({
                    _id: se.effectId,
                    'flags.acariaempire.spell.stacks.amount': se.stacks.amount - amount
                });
            }
        });

        if( removedSpellIds.length > 0 ) { 
            await this.removeSpellEffects(removedSpellIds);
        }
        if( updatedActiveEffects.length > 0 ) { 
            await this.updateEmbeddedDocuments("ActiveEffect", updatedActiveEffects ); 
        }
    }

    /**
     * Change a spell current stacks
     * @param {string} spellId spellId as stored in spellEffects.spellId
     * @param {int} newAmount new stack amount. May be equal to zero. In that case, the spell will be removed
     * @returns 
     */
    async updateSpellStackAmount(spellId, newAmount) {

        if( newAmount <= 0 ) {
            await this.removeSpellEffects([spellId]);
            return;
        }

        const spell = this.spellEffects.find( se => se.spellId === spellId );
        if( spell ) {
            const updateData = [{
                _id: spell.effectId,
                'flags.acariaempire.spell.stacks.amount': newAmount
            }];
            await this.updateEmbeddedDocuments("ActiveEffect", updateData ); 
        }
    }

/*------------------------------------------
    Token manipulation
--------------------------------------------*/

	get currentFatigue() {
	    return this.system.health.fatigue ?? 0;
	}

	get health() {
	    return this.system.health.value ?? 0;
	}

    /**
     * Inflicts damage on actor.
     * Takes into account damage resist.
     * Gives power if compatible ability has been learned.
     * Will also inflict fatigue if the damage taken is above 3
     * @param {int} amount Amount of damage the actor receives.
     * @param {string} [element] Can specify which element the damage is issued from
     * @param {int} [minFatigue] If > 0, replace the 3 damage step for fatigue to occurs. Fatigue is still inflicted only if damage are dealt
     * @returns Real amount of damages that has been inflicted
     */
    async inflictDamage(amount, {element=null, minFatigue=0}={}) {

        this._assertIsOwner();

        const elementNode = element? this.system.elements[element] : {resist: 0, powerGain:0, shielded: 0};
        const realAmount = amount - elementNode.resist - elementNode.shielded;
        const realPowerGain = Math.min( elementNode.powerGain, realAmount );
    
        // Inflict damage
        const updateData = {};
        if( realAmount > 0 ) {
            updateData['system.health.value'] = Math.max(0, this.health - realAmount);
            updateData['system.power.value'] = Math.min( this.power + realPowerGain, this.system.power.max );
        }

        // If you take more than one damage at a time, character also takes fatigue
        const fatigueFromDamage = realAmount >= 3 ? 1 : 0;
        const minFatigueIfDamage = realAmount > 0 ? minFatigue : 0;
        const fatigue = Math.max(fatigueFromDamage, minFatigueIfDamage);
        if( fatigue ) {
            updateData['system.health.fatigue'] = Math.min( this.currentFatigue + fatigue, this.system.health.max );
        }
        if( updateData != {} ) {
            await this.update(updateData);
        }
        
        // Consume shield if needed
        if( amount > elementNode.resist && elementNode.shielded > 0 ) {
            await this.consumeSpellEffectStack(SPELLEFFECT_STACK_TYPE.shield);
        }

        // Logging
        //-------------
        let text = translate('AESYSTEM.targets.sheet.damage.done')
                    .replace('ACTOR', this.name)
                    .replace('AMOUNT', realAmount);
        if( element ) { text += '. Element: ' + element; }
        console.log( 'AE-Actor | ' + text );

        if( fatigue > 0 ) {
            text = translate('AESYSTEM.targets.sheet.damage.fatigue')
                .replace('ACTOR', this.name)
                .replace('AMOUNT', fatigue);
            console.log( 'AE-Actor | ' + text );
        }

        if( realPowerGain > 0 ) {
            text = this.name + ' gains ' + realPowerGain + ' power tokens in the process.';
            console.log( 'AE-Actor | ' + text );
        }
        return realAmount;
    }
    
    /**
     * Heal damage on Actor.
     * Can't go above maxHealth
     * @param {int} amount Heal amont
     * @param {string} [element] Element used for this heal. Set it only if you want to benefit grom elementalResist during heal
     * @param {int} [healedFatigue] If > 0, will also heal fatigue. Happen even if no damage is done
     * @returns Real amount of damages that has been healed
     */
    async healDamage( amount, {element=null, healedFatigue=0}={} ) {

        this._assertIsOwner();

        const elementalResist = element? this.system.elements[element].resist : 0;
        const realAmount = Math.min( 
            this.system.health.max - this.health,
            amount + elementalResist
        );

        const updateData = {};
        if( realAmount > 0 ) {
            updateData['system.health.value'] = this.health + realAmount;
        }
        if( healedFatigue > 0 ) {
            updateData['system.health.fatigue'] = Math.max( this.currentFatigue - healedFatigue, 0 );
        }
        if( updateData != {} ) {
            await this.update(updateData);
        }

        // Logging
        //-------------
        let text = translate('AESYSTEM.targets.sheet.heal.done')
                    .replace('ACTOR', this.name)
                    .replace('AMOUNT', realAmount);
        if( element ) { text += '. Element: ' + element; }
        console.log( 'AE-Actor | ' + text );

        if( healedFatigue ) {
            text = translate('AESYSTEM.targets.sheet.heal.fatigue')
                .replace('ACTOR', this.name)
                .replace('AMOUNT', healedFatigue);
            console.log( 'AE-Actor | ' + text );
        }


        return realAmount;
    }

    async reloadHealthEffects() {
        
        const newActiveEffects = [];
        const removedActiveEffectIds = [];

        const fatigueEffects = ( this.effects ?? [] ).filter( e => e.getFlag('acariaempire', 'fatigue') );
        if( fatigueEffects.length != 1 ) {

            if( fatigueEffects.length > 1 ) {
                removedActiveEffectIds.push( ...fatigueEffects.map(e => e.id ) );
            }

            newActiveEffects.push({
                label: 'Fatigue',
                changes: [{
                    mode: CONST.ACTIVE_EFFECT_MODES.CUSTOM, 
                    key: FATIGUE_EFFECT.key, 
                    value: "",
                    priority: FATIGUE_EFFECT.priority
                }],
                flags: {
                    acariaempire : {
                        fatigue: true
                    }
                }
            });
        }
        if( removedActiveEffectIds.length ) { await this.deleteEmbeddedDocuments("ActiveEffect", removedActiveEffectIds ); }
        if( newActiveEffects.length ) { await this.createEmbeddedDocuments("ActiveEffect", newActiveEffects ); }
    }

    /**
     * Amount of gold the character has
     */
    get coins() {
        return this.system.coins;
    }

    /**
     * Gain some gold
     * @param {int} amount apont to gain
     */
     async gainCoins( amount ) {
        const currentCoins = this.coins;
        if( amount < 0 ) { return; }

        const updateData = {};
        updateData['system.coins'] = currentCoins + amount;
        await this.update(updateData);
    }

    /**
     * Spend some gold
     * @param {int} amount apont to spend
     * @return True if the character has enough
     */
    async spendCoins( amount, spendAnyway = false ) {
        const currentCoins = this.coins;
        if( amount < 0 ) { return false; }

        const hasEnough = currentCoins >= amount;
        const updateData = {};
        if( hasEnough ) {
            updateData['system.coins'] = currentCoins - amount;
        } else if( spendAnyway ) {
            updateData['system.coins'] = 0;
        }

        if( updateData != {} ) {
            await this.update(updateData);
        }
        return hasEnough;
    }

	get advantage() {

	    return this.system.advantage.value;
	}

	get mastery() {

	    return this.system.mastery.value;
	}

	get power() {

	    return this.system.power.value;
	}

	/** Advantage range [-6, 6] */
	async setAdvantage(value) {
        if( value == this.system.advantage.value ) return;

        const updateData = {};
        setResource(updateData, 'advantage', value,
                            this.system.advantage.min,
                            this.system.advantage.max);

        await this.update(updateData);
	}

	/** Mastery range [-6, 6] */
	async setMastery(value) {
        if( value == this.system.mastery.value ) return;

        const updateData = {};
        setResource(updateData, 'mastery', value,
                            this.system.mastery.min,
                            this.system.mastery.max);

        await this.update(updateData);
	}

	/** Power range [0, 3] */
	async setPower(value) {
        if( value == this.system.power.value ) return;
        
        const updateData = {};
        setResource(updateData, 'power', value,
                            this.system.power.min,
                            this.system.power.max);

        await this.update(updateData);
	}

	/** Tokens : advantage, mastery, power */
	async setAllBasicTokens(advantage, mastery, power) {
        const updateData = {};
        setResource(updateData, 'advantage', advantage, this.system.advantage.min, this.system.advantage.max);
        setResource(updateData, 'mastery', mastery, this.system.mastery.min, this.system.mastery.max);
        setResource(updateData, 'power', power, this.system.power.min, this.system.power.max);

        await this.update(updateData);
	}

    async resetAllTokens() {
		await this.setAllBasicTokens(0, 0, 1);
		await this.arcane.deleteCurrentCircle();
		await this.divine.deleteCurrentCommunion();
		await this.songs.stopCurrentPerformance();
    }

/*------------------------------------------
    Timetracking part
--------------------------------------------*/

    /**
     * Needed time for all classic actions the actor can do.
     */
    get neededTime() {
        const speed = this.getAttributeLevel('speed');
        const focus = this.getAttributeLevel('focus');

        const result = {
            battle: {
                enters: BASE_INITIATIVE - speed,
                surprised: BASE_INITIATIVE_SURPRISED - focus
            },
            simpleAction: { // Min: 5
                physical: Math.max( 10 - speed, 5 ),
                mental: Math.max( 10 - focus, 5 ),
                fromBaseSpeed: (baseSpeed) => {
                    return Math.max( baseSpeed - speed, 5 );
                }
            }
        };

        result.fastAction = {
            physical: Math.floor( result.simpleAction.physical / 2 ),
            mental: Math.floor( result.simpleAction.mental / 2 ),
            fromBaseSpeed: (baseSpeed) => {
                return Math.floor( result.simpleAction.fromBaseSpeed(baseSpeed) / 2 );
            }
        };

        return result;
    }

    /**
     * Current stored value of character position in the current battle.
     * Can be updated by others since gmActor is owned by everyone.
     * GMs will listiening the gmActor changed and actually updates the combatTracker (done automatically)
     */
	get timetracker() {

        const gmActor = game.aesystem.actorTools.gmActor;
        return gmActor.getFlag('acariaempire', timeTrackerFlag(this)) ?? null
	}

    /**
     * Modifiy current position of a character inside the current battle.
     * Can be done by players during actions.
     * @param {int} value New initiative value
     */
    async setTimetracker(value) {

        const currentValue = this.timetracker;
        if( currentValue != null && currentValue === value ) return;

        if( !game.combats.active ) { 
            console.log('AEActor | No battle currently running. Can\'t set timetracker');
            return; 
        }
    
        const gmActor = game.aesystem.actorTools.gmActor;
        await gmActor.setFlag('acariaempire', timeTrackerFlag(this), value);
    }

    /**
     * Similar to setTimetracker. Modify current value by adding an amount instead of replacing it.
     * Does nothing if the initiative is not set.
     * @param {int} amount amount to add. (Can be negative)
     */
    async spendTime(amount) {

        let currentValue = this.timetracker;
        if( currentValue == null ) {
            currentValue = this.neededTime.battle.enters;
        }

        const newValue = Math.max( 0, currentValue + amount );
        await this.setTimetracker(newValue);
	}

    /**
     * For tracking new spells in the combat tracker
     * @param {object} spellData See roll.context.spellData
     */
    async trackNewSpellEffect( spellData ) {

        if( !spellData.spell ) { return; } // No spell to track
        if( !spellData.until && spellData.until != 0) { return; } // Instant effect

        const data = mergeObject( {}, spellData );

        const gmActor = game.aesystem.actorTools.gmActor;
        const flagName = timeTrackerFlag(this, {suffix:'spells'}) + '.' + spellData.id;
        await gmActor.setFlag('acariaempire', flagName , data);
    }

    /**
     * Spell tracker can be manually removed from tracker.
     * When its done, spell track inside gmActor is also removed so that it doesn't come back
     * @param {string} spellData When put on gmActor flags, spells get a guid for reference. This is it.
     */
     async stopTrackingSpell( spellId ) {

        const gmActor = game.aesystem.actorTools.gmActor;
        const flagName = timeTrackerFlag(this, {suffix:'spells'}) + '.' + spellId +  '.removed';
        await gmActor.setFlag('acariaempire', flagName, true);
    }


/*------------------------------------------
    XP MANAGEMENT
--------------------------------------------*/

    getTotalXPLeft(xpType) {
        let xp = this.system.xpToSpend;
        let value = xp.free;
        if( xpType == 'attribute' || xpType == 'all' ) {
            value += xp.attribute;
        }

        if( xpType == 'skill' || xpType == 'all' ) {
            value += xp.skill;
        }

        if( xpType == 'power' || xpType == 'all' ) {
            value += xp.power;
        }
        return value;
    }

    get xp() {
        let xp = this.system.xpToSpend;
        return {
            attribute: xp.attribute,
            skill: xp.skill,
            power: xp.power,
            free: xp.free, 
            granted: this.system.xpToSpend.granted,
            all: this.getTotalXPLeft('all')
        };
    }

    async grantXP(xpType, amount) {
        
        if( ! game.user.isGM ) {
            return ui.notifications.warn(translate('AESYSTEM.actor.xp.onlyGMAllocateXp'));
        }
        const xp = this.system.xpToSpend;

        let currentAmount = xp.free;
        let xpTitle = 'free';
        if( xpType == 'attribute') { 
            currentAmount = xp.attribute;
            xpTitle = 'attribute';

        } else if( xpType == 'skill') { 
            currentAmount = xp.skill;
            xpTitle = 'skill';

        } else if( xpType == 'power') { 
            currentAmount = xp.power;
            xpTitle = 'power';
        }

        if( currentAmount + amount < 0 ) {
            return; // We can't remove more XP that the actor already have
        }

        const newAmount = Math.max(0, currentAmount + amount);
        const newAllAmount = Math.max(0, xp.granted + amount);

        const updateData = {};
        updateData['system.xpToSpend.' + xpTitle] = newAmount;
        updateData['system.xpToSpend.granted'] = newAllAmount;
        await this.update(updateData);
    }

    async spendXPToAttribute(attributeName) {

        this._assertIsOwner();

        const levelInfo = this.getAttribute(attributeName).levelUpInfo;
        
        if(levelInfo.level >= levelInfo.maxLevel ) {
            return ui.notifications.info(translate('AESYSTEM.actor.xp.attribute.capped'));
        }

        let newXP = levelInfo.xp + 1;
        let newLevel = levelInfo.level;
        if( newXP >= levelInfo.xpCost ) {
            newLevel++;
            newXP -= levelInfo.xpCost;
        }

        const updateData = {};
        retrieveXP(this, 'attribute', updateData, 1);
        updateData[`system.attributes.${attributeName}.level`] = newLevel;
        updateData[`system.attributes.${attributeName}.xp`] = newXP;

        await this.update(updateData);
    }

    async reinitAttribute(attributeName) {

        this._assertGMRemovingXP();

        const levelInfo = this.getAttribute(attributeName).levelUpInfo;
        const retrievedAmount = attributeLevels().reduce( (amount, current) => {
            return current.level < levelInfo.level ? amount + current.xpCost : amount;
        }, levelInfo.xp);

        // Remove XP
        const updateData = {};

        updateData['system.xpToSpend.attribute'] = this.system.xpToSpend.attribute + retrievedAmount;
        updateData[`system.attributes.${attributeName}.level`] = levelInfo.minLevel;
        updateData[`system.attributes.${attributeName}.xp`] = 0;

        // Update actor data
        await this.update(updateData);
    }

    /** Throws an exception if you're attempting the remove XP while not being GM **/
    _assertGMRemovingXP() {
        if( ! removingXPAllowed() ) {
            ui.notifications.warn(translate('AESYSTEM.actor.xp.onlyGMRemoveXp'));
            throw 'Only GM can remove XP points';
        }
    }

    /** More lax than _assertGMRemovingXP. Still OK if you're the character owner. **/
    _assertIsOwner() {
        // GM Only action
        if( ! this.allowedToEdit ) {
            ui.notifications.warn(translate('AESYSTEM.actor.notOwned'));
            throw "You're not this character owner!";
        }
    }
}
