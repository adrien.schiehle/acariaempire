import {removingXPAllowed} from '../tools/gmactions.js';
import { translate } from '../tools/Translator.js';
import {attributeLevels, attributeList} from '../tools/WaIntegration.js';
import * as utils from './ActorSheetUtils.js';

export const dataForAttributes = (actor, updatedData) => {

    // All attributes
    const rawAttributes = actor.allAttributes;
    const positions = [
        [0, 70], [225, -150], [450, -270], [0, -230], [225, -340], [450, -570]
    ];
    const tooltipCssList = ['top left', 'top middle', 'top right', 'bottom left', 'bottom middle', 'bottom right'];

    const attributes = rawAttributes.map( attribute => {

        const position = positions.shift();

        const evolveBtn = evolveBtnData(actor, attribute);
        const removeBtn = removeBtnData(actor, attribute);

        const data = {
            left: position[0],
            top: position[1],
            levelSummary : buildLevelSummary(attribute),
            xptab: utils.buildXPBubblesForAbility(attribute.levelUpInfo, {bluePoint: evolveBtn.available, multiplier:2 }),
            button: evolveBtn,
            forgetBtn: removeBtn,
            tooltipCss: tooltipCssList[attribute.order]
        };
        return mergeObject( data, attribute );
    });

    updatedData.gui.attributes = {
        all: attributes
    };
}


/**
 * Bind buttons for the attribute tab
 * @param {AEActorSheet} sheet parent
 * @param {HTMLElement} html Sheet content
 */
export const bindAttributeActions = ( sheet, html ) => {

    if ( !sheet.options.editable ) { return; }
    html.find('.ae-attributes .attribute .evolve-button.available').click(event => onClickSpendXPOnAttribute(sheet, event));
    html.find('.ae-attributes .attribute .evolve-button.forget').click(event => onClickReinitAttribute(sheet, event));
    html.find('.ae-attributes .attribute-roll').click(event => onClickRollDice(sheet, event));
}


// Convenience function for preparing evolve btn data
const evolveBtnData = (actor, attribute) => {
    const editAllowed = actor.allowedToEdit;
    const xpLeft = actor.getTotalXPLeft('attribute') > 0;

    const result = {
        css: '',
        available: false
    };

    const currentLevel = attribute.levelUpInfo.level;
    const currentXP = attribute.levelUpInfo.xp;

    if( attribute.levelUpInfo.xpCost == 0 ) {
        result.title = translate('AESYSTEM.actor.sheet.attribute.button.maxReached');

    } else {
        if( ! editAllowed || ! xpLeft || currentLevel >= attribute.levelUpInfo.maxLevel ) {
            result.title = translate('AESYSTEM.actor.sheet.attribute.button.augmentKO');

        } else {
            result.css = 'available';
            result.available= true;
            const nearLevel = (currentXP >= attribute.levelUpInfo.xpCost - 1);
            result.title = translate('AESYSTEM.actor.sheet.attribute.button.' + (nearLevel ? 'levelUp' : 'train') );
        }
    }
    return result;
}

const removeBtnData = (actor, attribute) => {

    const removingXP = removingXPAllowed();
    const xpSpent = attribute.levelUpInfo.level > attribute.levelUpInfo.minLevel || attribute.levelUpInfo.xp > 0;
    const enoughRights = game.user.isGM || actor.mainCharacterPlayer;

    return {
        displayed: removingXP && xpSpent && enoughRights,
        title: translate('AESYSTEM.actor.sheet.attribute.button.reset')
    };

}

const buildLevelSummary = (attribute) => {
    const result = utils.alteredValue(attribute);
    result.value += ' : ' + attributeLevels().find( a => a.level === attribute.value ).name;
    return result;
}

const onClickSpendXPOnAttribute = async (sheet, event) => {
    event.preventDefault();
    sheet._assertEditPermissions();

    const a = event.currentTarget;
    const attributeName = a.parentElement.parentElement.dataset.attribute;

    await sheet.actor.spendXPToAttribute(attributeName);
    sheet.render();
}

const onClickReinitAttribute = async (sheet, event) => {
    event.preventDefault();
    sheet._assertEditPermissions();

    const a = event.currentTarget;
    const attributeName = a.parentElement.parentElement.dataset.attribute;

    await sheet.actor.reinitAttribute(attributeName);
    sheet.render();
}

const onClickRollDice = async (sheet, event) => {
    event.preventDefault();
    sheet._assertEditPermissions();
    
    const firstKey = attributeList().find( a => a.name ).key;
    game.aesystem.triggerRollPanel({attribute: firstKey});
}
