import { AEItem } from '../items/item.js';

/**
 * Pick some items inside the compendium and store related data inside an array which will be later persisted inside shop.sellables
 * @param {} path compendium path
 * @param {*} amount The amount of items which should be picked inside
 * @returns Promise<object[]> Each element has : {type:<string>, name:<string>, img:<string>, data:<object>}
 */
 const chooseSellablesFromCompendium = async ( path, amount ) => {
    const pack = game.packs.get(path);
    
    const sizePool = [];
    for( let i = 0; i < pack.index.size; i++ ) { 
        sizePool.push(i); 
    }

    const selectedIndexes = [];
    for( let i = 0; i < amount; i++ ) {
        const indexOnPool = Math.floor( Math.random() * sizePool.length );
        selectedIndexes.push( sizePool[indexOnPool] );
        sizePool.splice(indexOnPool, 1);
    }

    const result = [];
    const base = Array.from(pack.index.values() );
    for( const index of selectedIndexes ) {
        const item = await pack.getDocument( base[index]._id );
        const source = item._source;
        result.push( {
            type: source.type,
            name: source.name,
            img: source.img,
            system: source.system,
            ownership:{ default: CONST.DOCUMENT_PERMISSION_LEVELS.LIMITED }
        });
    }
    return result;
}

/**
 * For managing NPC merchant shops
 */
export class AEActorMerchantData {

    constructor(actor) {
        this._actor = actor;
    }

    get base() {
        const data = this._actor.system.shop;
        if( !data ) { 'This character type \'' + this._actor.type + '\' has no access to merchant part'; }
        return data;
    }

    get hasShop() {
        return this.base.set;
    }

    get sellableItems() {

        if( !this._sellableItems ) {
            
            // Retrieve all sellable items
            const fromShop = (this.base.sellables ?? []).map( data => {
                return {source: 'shop', itemData: data};
            });
            const boughtItems = (this.base.bought ?? []).map( data => {
                return {source: 'bought', itemData: data};
            });

            // Filter on bought duplicates
            boughtItems.forEach( data => {
                if( ! fromShop.find( d => d.itemData.name === data.itemData.name ) ) {
                    fromShop.push(data);
                }
            });

            this._sellableItems = fromShop.map( data => {
                const result = {
                    source: data.source
                };

                const item = new AEItem(data.itemData);
                if( item.isWeapon ) { 
                    result.eqt = item.asWeapon(); 
                } else if( item.isArmor ) { 
                    result.eqt = item.asArmor(); 
                } else { 
                    result.eqt = item.asMisc(); 
                }

                return result;
            });

            this._sellableItems.sort( (a,b) => a.eqt.item.name.localeCompare(b.eqt.item.name) );
        }
        return this._sellableItems;
    }

    clearCache() {
        this._sellableItems = null;
    }

    async buildShop() {

        if( this.hasShop ) { return; }

        const updateData = {};
        updateData['system.shop.set'] = true;
        updateData['system.shop.sellables'] = [];
        updateData['system.shop.bought'] = [];

        this.clearCache();
        await this._actor.update(updateData);
    }

    async deleteShop() {

        if( !this.hasShop ) { return; }

        const updateData = {};
        updateData['system.shop.set'] = false;
        updateData['system.shop.sellables'] = [];
        updateData['system.shop.bought'] = [];

        this.clearCache();
        await this._actor.update(updateData);
    }

    /**
     * Update settings and reload sellables
     * @param {object[]} usedCompendiums List of compendium references and amount of items which should be token from it. ({path, amount})
     */
    async reloadShop(usedCompendiums) {

        if( !this.hasShop ) { return; }

        // Save settings
        const updateData = {};
        updateData['system.shop.settings.compendiums'] = usedCompendiums;

        let sellables = [];
        for( const compendium of usedCompendiums ) {
            const newSellables = await chooseSellablesFromCompendium(compendium.path, compendium.amount);
            sellables = sellables.concat( newSellables );
        }
        sellables.sort( (a,b) => a.name.localeCompare(b.name) );
        updateData['system.shop.sellables'] = sellables;

        this.clearCache();
        await this._actor.update(updateData);
    }


    /**
     * Update .shop.bought by adding a new element to it
     * @param {object} itemData Data needed for recreating item
     */
     async addBoughtItem(itemData) {

        if( !this.hasShop ) { return; }

        const current = this.base.bought ?? [];
        const boughtItems = current.concat( [itemData] );

        // Save bought items
        const updateData = {};
        updateData['system.shop.bought'] = boughtItems;

        this.clearCache();
        await this._actor.update(updateData);
    }

    /**
     * Update .shop.bought by removing an element from it
     * @param {string} itemName The name of the item that has been sold back
     */
     async removeBoughtItem(itemName) {

        if( !this.hasShop ) { return; }

        const current = this.base.bought ?? [];
        const newList = duplicate(current);
        
        const index = newList.findIndex( d => d.name === itemName );
        if( index == -1 ) {
            console.warn( 'AE-Mercant | Bought item named ' + itemName + ' not found');
        }
        newList.splice( index, 1 );

        // Save bought items
        const updateData = {};
        updateData['system.shop.bought'] = newList;

        this.clearCache();
        await this._actor.update(updateData);
    }


}

