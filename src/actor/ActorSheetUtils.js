import {translate} from '../tools/Translator.js';
import { removingXPAllowed } from '../tools/gmactions.js';

const CSS_VALUE_BUFFED = 'buffed-value';
const CSS_VALUE_DEBUFFED = 'debuffed-value';

const ACTOR_XP = {
	'empty': 'systems/acariaempire/resources/actor/xp/xp_empty.png',
	'full': 'systems/acariaempire/resources/actor/xp/xp_full.png',
	'red': 'systems/acariaempire/resources/actor/xp/xp_red.png',
	'blue': 'systems/acariaempire/resources/actor/xp/xp_blue.png',
	'disabled': 'systems/acariaempire/resources/actor/xp/xp_disabled.png',
	'-2': 'systems/acariaempire/resources/actor/xp/xp_-2.png',
	'-1': 'systems/acariaempire/resources/actor/xp/xp_-1.png',
	'0': 'systems/acariaempire/resources/actor/xp/xp_0.png',
	'1': 'systems/acariaempire/resources/actor/xp/xp_1.png',
	'-1_disabled': 'systems/acariaempire/resources/actor/xp/xp_-1_disabled.png',
	'0_disabled': 'systems/acariaempire/resources/actor/xp/xp_0_disabled.png',
	'1_disabled': 'systems/acariaempire/resources/actor/xp/xp_1_disabled.png'
};

export const alteredValue = (base, {valueKey='value', initialKey='initialValue', smallIsBetter=false} = {}) => {
    
    const value = base[valueKey];
    const initial = base[initialKey];
    let diff = value - initial;
    if( smallIsBetter ) { diff = -1 * diff; }

    const result = {
        value: value,
        css: diff < 0 ? CSS_VALUE_DEBUFFED : ( diff > 0 ? CSS_VALUE_BUFFED : '')
    };
    if( diff != 0 ) {
        result.css += ' with-tooltip';
        result.title = '' + initial + '⇒ ' + value;
    }

    return result;
}

export const prepareManeuverData = ( actor, rawManeuvers, currentManeuverKey ) => {

    // Convenience function for preparing evolve btn data
    const maneuverBtnData = (actor, maneuver) => {
        const editAllowed = actor.allowedToEdit;
        const xpLeft = actor.getTotalXPLeft('power') > 0;
    
        const result = {
            css: '',
            title: translate('AESYSTEM.actor.sheet.maneuver.button.mastered')
        };
    
        const currentLevel = maneuver.level.level;
        const currentXP = maneuver.level.xp;
    
        if( currentLevel != 6 ) {
            if( ! editAllowed || ! xpLeft || currentLevel >= maneuver.level.maxLevel ) {
                result.title = translate('AESYSTEM.actor.sheet.maneuver.button.augmentKO');
    
            } else {
                result.css = 'available';
                const nearLevel = (currentXP >= currentLevel);
                result.title = translate('AESYSTEM.actor.sheet.maneuver.button.' + (nearLevel ? 'levelUp' : 'train') );
            }
        }
        return result;
    }
    

    const removingXP = removingXPAllowed();
    const maneuvers = rawManeuvers.map( ([key, maneuver]) => {

        const selected = currentManeuverKey === key;

        const data = {
            cssForFrame: selected ? 'selected' : 'selectable',
            displayDetails: selected,
            summary : maneuver.name + ' - ' + maneuver.level.name,
            xptab: buildXPBubblesForAbility(maneuver.level, {bluePoint: true}),
            button: maneuverBtnData(actor, maneuver),
            forgetBtn: {
                displayed: removingXP && maneuver.level.level > 0,
                title: translate('AESYSTEM.actor.sheet.maneuver.button.reset')
            }
        };
        return mergeObject( data, maneuver );
    });
    maneuvers.sort( (a, b) => b.level.level - a.level.level );
    return maneuvers;
}

/**
 * Fill an xp bubble panel
 * @param {*} ability Must have {level: , xp: , maxLevel: }
 */
 export const buildXPBubblesForAbility = (ability, {bluePoint=false, redPoint=false, multiplier=1, hideUnusedBox=false} = {} ) => {

    const xpCostSet = ability.xpCost || ability.xpCost == 0;
    const xpNeeded = xpCostSet ? ability.xpCost : (ability.level + 1) * multiplier;
    const maxBubbles = ability.maxLevel * multiplier;

    let xptab = [];
    if( ability.level < ability.maxLevel  ) {
        
        for( let i = 0; i < ability.xp; i++ ) {
            xptab.push(ACTOR_XP['full']);
        }

        if( bluePoint ) {
            xptab.push(ACTOR_XP['blue']);
        }

        if( redPoint ) {
            xptab.push(ACTOR_XP['red']);
        }

        for( let i = xptab.length; i < xpNeeded; i++ ) {
            xptab.push(ACTOR_XP['empty']);
        }

        if( !hideUnusedBox ) {
            for( let i = xptab.length; i < maxBubbles; i++ ) {
                xptab.push(ACTOR_XP['disabled']);
            }
        }
        
    }
    return xptab;
}

export const buildXPBubblesForAlternateCapacities = (level, maxLevel) => {
    let xptab = [];
    for( let i = 1; i <= level; i++ ) {
        xptab.push(ACTOR_XP['full']);
    }

    for( let i = level + 1; i <= maxLevel; i++ ) {
        xptab.push(ACTOR_XP['empty']);
    }
    return xptab;
}

export const battleCapacity = (actor, attributeName, skillKey) => {

    
    const tokens = [];

    // Calculate score
    const base = actor.battle.forOffense(skillKey);
    const score = alteredValue(base, {valueKey:'advantage', initialKey:'initialAdvantage'});
    score.icon = 'systems/acariaempire/resources/interaction/tokens/action_score.png';
    score.details = translate('AESYSTEM.actor.sheet.battleCapacity.score.details');
    tokens.push(score);

    // Calculate Card balance
    const mastery = alteredValue(base, {valueKey:'mastery', initialKey:'initialMastery'});
    if( mastery.value != 0 || mastery.css != '' ) {
        const iconSuffix = mastery.value < 0 ? 'negative.png' : 'positive.png';
        mastery.value = Math.abs(mastery.value);
        mastery.icon = 'systems/acariaempire/resources/interaction/tokens/card_balance_' + iconSuffix;
        mastery.details = translate('AESYSTEM.actor.sheet.battleCapacity.cardBalance.details');
        tokens.push(mastery);
    }

    // Having details
    const attribute = actor.getAttribute(attributeName);
    const attributeDetails = translate('AESYSTEM.actor.sheet.battleCapacity.attribute.details')
                              .replace('ADVANTAGE', (attribute.value>0 ? '+' : '') + attribute.value);

    const combatBaseAdvantage = base.initialAdvantage - attribute.value;
    const combatBaseMastery = base.initialMastery;

    const mastered = actor.abilities.combatAbility.unlocked.findIndex( s => s.key === skillKey ) != -1;
    const skillLabel = translate( 'AESYSTEM.actor.sheet.battleCapacity.skill.' + (mastered? 'mastered' : 'unknown' ) );
    const skillDetails = translate( 'AESYSTEM.actor.sheet.battleCapacity.skill.details' )
                            .replace('ADVANTAGE', (combatBaseAdvantage>0 ? '+' : '') + combatBaseAdvantage)
                            .replace('MASTERY', (combatBaseMastery>0 ? '+' : '') + combatBaseMastery);


    return {
        attribute: {
            name: attribute.name,
            details: attributeDetails
        },
        combat: {
            mastered: skillLabel,
            details: skillDetails
        },
        tokens: tokens
    };
}

