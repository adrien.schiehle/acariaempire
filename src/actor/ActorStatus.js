import { translate } from '../tools/Translator.js'

const STATUS_ICONS = {
    overloaded: 'systems/acariaempire/resources/actor/conditions/overload.png'
};


/**
 * Done after each actor change.
 * Reload active effect list related du to statuses.
 * (Simple effects with icons)
 * @param {AEActorEquipmentData} parent 
 * @returns TRUE if some effects have changed
 */
const refreshActorDataWithCurrentStatuses = async (parent) => {

    const currentEffects = parent.effects;
    const newActiveEffects = [];
    const removedActiveEffectIds = [];
    const actor = parent._actor;

    // Removing effects that arent present anymore
    currentEffects.forEach( sf => {
        const key = sf.status;
        if( ! actor.system.status[key] ) {
            removedActiveEffectIds.push( sf.effectId );
        }
    });

    // Adding new status effects
    Object.entries( STATUS_ICONS ).forEach( ([key, icon]) => {
        if( actor.system.status[key] && ! currentEffects.find( sf => sf.status === key ) ) {

            newActiveEffects.push({
                label: translate('AESYSTEM.actor.activeEffect.status.' + key),
                icon: icon,
                changes: [],
                flags: {
                    core: {
                        statusId: key
                    },
                    acariaempire : {
                        status: key
                    }
                }
            });

        };
    });

    // Refresh active effects
    const removingEffects = removedActiveEffectIds.length > 0;
    const addingNewffects = newActiveEffects.length > 0;
    if( removingEffects ) { await actor.deleteEmbeddedDocuments("ActiveEffect", removedActiveEffectIds ); }
    if( addingNewffects ) { await actor.createEmbeddedDocuments("ActiveEffect", newActiveEffects ); }

    return removingEffects || addingNewffects;
}


/**
 * For managing arcane circles during scenes
 */
export class AEActorStatus {

    constructor(actor) {

        this._actor = actor;
    }

    get actorData() {

        return this._actor.system;
    }

    get overloaded() {
        return this.actorData.status.overloaded;
    }

    /*------------------------------------------
        Equipment active effects (mainly overloads for now)
    --------------------------------------------*/

    async reloadEffects() {
        return refreshActorDataWithCurrentStatuses(this);
    }

    get effects() {
        const allEffects = this._actor.effects ?? [];
        return allEffects.filter( e => {
            return e.getFlag('acariaempire', 'status');
        }).map( e => {
            const dataInFlag = e.getFlag('acariaempire', 'status');
            return {
                effectId: e.id,
                effect: e,
                status: dataInFlag
            };
        });
    }


}
