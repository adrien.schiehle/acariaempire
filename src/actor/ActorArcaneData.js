import { translate } from '../tools/Translator.js';
import { arcaneCirclePenalties, arcaneManeuvers, arcaneSpellCategories, elementList } from '../tools/WaIntegration.js';
import { retrieveXP, giveXP } from './actor.js';
import { AEActorBaseDataWithManeuvers } from './ActorBaseDataWithManeuvers.js';

const mergeCircleData = (data) => {
    return {
        harmonizations: data.harmonizations ?? [],
        caracs: data.caracs ?? [],
        fragility: data.fragility ?? 0
    };
}

/**
 * For managing arcane circles during scenes
 */
export class AEActorArcaneData extends AEActorBaseDataWithManeuvers {

    constructor(actor) {
        super(actor, actor.battle.arcane, arcaneManeuvers, 'arcane');
    }

    get knowledges() {
        return Object.entries(this.actorData.arcane.knowledge ?? {} );
    }

    /**
     * @returns Simple numeric value
     */
    getKnowledge(knowledgeName) {
        for( let [key, knowledge] of this.knowledges ) {
            if( key === knowledgeName && Number.isNumeric(knowledge) ) {
                return knowledge;
            }
        }
        throw knowledgeName + ' is not a valid arcane knowledge';
    }

    async spendXPToKnowledge(knowledge) {
 
        this._actor._assertIsOwner();

        if( this._actor.getTotalXPLeft('power') <= 0 ) {
            ui.notifications.info(translate('AESYSTEM.actor.xp.power.noXpLeft'));
            return; // Not an error since there is no control prior to this.
        }

        const updateData = {};
        const currentKnowledge = this.getKnowledge(knowledge);
        if(currentKnowledge >= this._actor.battle.arcane.maneuverMaxLevel ) {
            ui.notifications.info(translate('AESYSTEM.actor.xp.arcane.cappedByMastery'));
            return; // Not an error since there is no control prior to this.
        }

        retrieveXP(this._actor, 'power', updateData, 1);
        updateData['system.arcane.knowledge.' + knowledge] = currentKnowledge + 1;
        await this._actor.update(updateData);
    }

    async removeXPToKnowledge(knowledge) {
 
        this._actor._assertIsOwner();

        const updateData = {};

        // rebuild knownPowers list
        const currentKnowledge = this.getKnowledge(knowledge);

        if(currentKnowledge <= 0) {
            ui.notifications.info(translate('AESYSTEM.actor.xp.arcane.alreadyLevel0'));
            return; // Not an error since there is no control prior to this.
        }

        giveXP(this._actor, 'power', updateData, 1);
        updateData['system.arcane.knowledge.' + knowledge] = currentKnowledge - 1;
        await this._actor.update(updateData);
    }

    get circleHarmonizations() {

        const harmonizationKeys = this.actorData.arcane.circle?.harmonizations ?? [];
        return harmonizationKeys.map( h => elementList().find( e => e.key === h ));
    }

    get circleCaracteristics() {

        const caracs = this.actorData.arcane.circle?.caracs ?? [];
        return caracs.map(c => {
            let result = {shortcut: c.name.startsWith('effect') ? `magic.effects.${c.name}` : `magic.modifiers.${c.name}`};
            return mergeObject(result, c);
        });
    }

    get circleFragility() {
        return this.actorData.arcane.circle?.fragility ?? 0;
    }

    get isCircleHarmonized() {
        return this.circleHarmonizations.length != 0;
    }

    get isCircleAbsent() {
        return this.circleCaracteristics.length == 0;
    }

    async buildNewCircle(effectName) {
        this._actor._assertIsOwner();

        const updateData = {};
        updateData['system.arcane.circle'] = baseCircleStructure(effectName);
        await this._actor.update(updateData);
    }

    /**
     * Modify the arcane circle.
     * Data will be merged with existing one to be sure nothing is incorrect
     */
    async alterArcaneCircle(circle) {
        this._actor._assertIsOwner();

        const updateData = {};
        updateData['system.arcane.circle'] = mergeCircleData(circle);
        await this._actor.update(updateData);
    }

    /**
     * Modify the arcane circle.
     * Data will be merged with existing one to be sure nothing is incorrect
     */
     async deleteCurrentCircle() {
        this._actor._assertIsOwner();

        const updateData = {};
        updateData['system.arcane.circle'] = mergeCircleData({});
        await this._actor.update(updateData);
    }


    /**
     * Circle structure :
     * {
     *    harmonizations : [],
     *    caracs : [
     *        {name: 'carac1', level: 0 },
     *        {name: 'carac2', level: 0 },
     *        {name: 'carac3', level: 0 },
     *        {name: 'carac4', level: 0 }
     *    ]
     * }
     */
    baseCircleStructure (effectName) {

        // Choose circle caracteristics from effectName
        let effect = arcaneSpellCategories().find(e => e.effect === effectName);
        if(! effect) throw 'Wrong effet name ' + effectName;

        const caracs = effect.caracteristics.map(elem => {
            return {
                name: elem,
                level: 0
            }
        });

        // Circle starts with Light harmonizations.
        // -> Every available harmonisations are put as light
        const lightElementKey = 'primary';;
        const nbHarmonisations = this.actorData.arcane.maxCircleHarmonizations;
        const harmonizations = [];
        for( let index = 0; index < nbHarmonisations; index++ ) {
            harmonizations.push(lightElementKey);
        }

        return {
            harmonizations : harmonizations,
            caracs : caracs,
            fragility: 0
        };
    }

    /**
     * Calculate the effect penalty due to used elements inside circle.
     * @param {string} mainElement mainElement for this circle
     * @returns {object} Structure {from:elementId, value:int}
     */
    getAttunementPenalty( mainElement ) {

        const harmonizations = this.circleHarmonizations.map( h => h.element );
        const attunements = Object.entries(this.actorData.elements).filter( ([key,value]) => {
            return value.attuned;
        }).map( ([key, value]) => {
            return key;
        });

        // Calculate penalty
        const primaryElementKey = 'primary';
        const wholePenalty = { from: null, value: 0 };
        harmonizations.forEach( element => {

            // Retrieve the minimum penalty among the attunements
            let penaltyForThisElement = arcaneCirclePenalties().get(primaryElementKey).get(element);
            if( attunements.includes(primaryElementKey) ) {
                // Special case for primary element : base penalty is reduced by one
                penaltyForThisElement = Math.max( 0, penaltyForThisElement - 1 );
            }
            attunements.filter( a => a != primaryElementKey).forEach( attunement => {
                penaltyForThisElement = Math.min( 
                    penaltyForThisElement,
                    arcaneCirclePenalties().get(attunement).get(element)
                );
            });

            // Secondary circle elements only gve a reduced penalty
            if( element != mainElement ) {
                penaltyForThisElement = Math.max( 0, penaltyForThisElement - 1 );
            }

            if( penaltyForThisElement > wholePenalty.value ) {
                wholePenalty.from = element;
                wholePenalty.value =  penaltyForThisElement;
            }
        });
        return wholePenalty;
    }
}
