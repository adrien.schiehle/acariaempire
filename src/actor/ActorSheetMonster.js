import {translate} from '../tools/Translator.js';

export const dataForActorMonster = (actor, updatedData, monsterData) => {

    const allAugments = actor.monster.allAugments;
    const tooltip = translate('AESYSTEM.actor.sheet.monster.journalTooltip');
    const editAllowed = game.user.isGM;

    const knownAugments = allAugments.filter(a => a.level > 0).map(a => {
        return foundry.utils.mergeObject( {tooltip: tooltip, editAllowed: editAllowed}, a);
    });
    const availableMenus = allAugments.filter(a => a.level == 0).map(a => {
        return foundry.utils.mergeObject( {tooltip: tooltip, editAllowed: editAllowed}, a);

    }).reduce( (res, augment) => {
        const entry = res.find( m => m.menu.key === augment.menu.key );
        if( entry ) {
            entry.augments.push(augment);

        } else {
            res.push( {
                menu: augment.menu,
                augments: [augment]
            });
        }
        return res;
    }, []);
    availableMenus.sort( (a,b) => a.menu.displayOrder - b.menu.displayOrder );

    updatedData.gui.monster = {
        knownAugments: knownAugments,
        availableMenus: availableMenus,
        labels: {
            threatlevel: translate('AESYSTEM.actor.sheet.monster.threatLevel').replace('LEVEL', '' + actor.monster.threat),
            knownAugmentTitle: translate('AESYSTEM.actor.sheet.monster.knownAugmentTitle')
        }
    };
}

/**
 * Bind Monster sheet actions.
 * @param {AEActorSheet} sheet parent
 * @param {HTMLElement} html Sheet content
 */
export const bindMonsterButtons = ( sheet, html ) => {

    if( !game.user.isGM ) { return; }
    html.find('.ae-monster .augment-modif').click(event => onClickUpgradeAugment(sheet, event));
    html.find('.ae-monster .available-augment').click(event => onClickLearnAugment(sheet, event));
}

const onClickUpgradeAugment = async (sheet, event) => {
    event.preventDefault();
    sheet._assertEditPermissions();

    const a = event.currentTarget;
    const action = a.dataset.action;
    const augmentKey = a.parentElement.dataset.augmentId;

    const augment = sheet.actor.monster.allAugments.find( a => a.key === augmentKey );
    const level = augment.level + (action == 'plus' ? 1 : -1 );

    await sheet.actor.monster.setAugment( augmentKey, Math.min(level, augment.maxLevel) );
}

const onClickLearnAugment = async (sheet, event) => {
    event.preventDefault();
    sheet._assertEditPermissions();

    const a = event.currentTarget;
    const augmentKey = a.dataset.augmentId;

    const augment = sheet.actor.monster.allAugments.find( a => a.key === augmentKey );
    if( augment?.level == 0 ) {
        await sheet.actor.monster.setAugment( augmentKey, 1 );
    }
}
