import { AEActorTools } from '../actor/ActorTool.js';

/**
 * Overriden PlayerConfig.getData by filtering gmActor in the available actors
 */
export const overrideUserConfigPopup = () => {

    UserConfig.prototype.getData = (arg) => {

        const gmActor = new AEActorTools().gmActor;

        const controlled = game.users.contents.map(e => e.character).filter(a => a);
        const actors = game.actors.contents.filter(a => {
            return a.testUserPermission(game.user, "OWNER") && !controlled.includes(a._id);
        }).filter(a => {
            return gmActor.id != a.id;
        });

        return {
            user: game.user,
            actors: actors
        };
    };
}