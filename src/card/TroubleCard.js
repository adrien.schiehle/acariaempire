import { ALL_CARDS_TYPES, TROUBLE_STATUS } from '../tools/constants.js';
import { guardianSpiritTroubleEffectList, guardianSpiritTroubleLabelList, guardianSpiritTroubleQuantityList, guardianSpiritTroubleStatusList } from '../tools/WaIntegration.js';
import { rtucStack } from './CardTools.js';

const AVAILABLE_EFFECTS = {
    power_health: 'power_health', 
    power_missing: 'power_missing', 
    event_confused: 'event_confused', 
    initiative_slow: 'initiative_slow', 
    success_minimum: 'success_minimum', 
    skull_augmented: 'skull_augmented'
};
const AVAILABLE_COSTS = {
    positive: 'once', 
    negative: 'all_dice'
};

const checkIntegrity = (card) => {

    let isOk = true;
    if(card.type != ALL_CARDS_TYPES.trouble) {
        isOk = false;
        console.error(card.name + ' is not a guardian spirit trouble. (' + card.type + ');');

    } else if( ! Object.values(AVAILABLE_EFFECTS).includes(card.system.effect) ) {
        isOk = false;
        console.error(card.name + ' has invalid effect part');

    } else if( ! Object.values(AVAILABLE_COSTS).includes(card.system.quantity) ) {
        isOk = false;
        console.error(card.name + ' has invalid cost part');

    } else if( ! Object.values(TROUBLE_STATUS).includes(card.system.status) ) {
        isOk = false;
        console.error(card.name + ' has invalid status part');

    } else if( card.system.step < 1 ) {
        isOk = false;
        console.error(card.name + ' has invalid step part');
    }
    
    if(! isOk ) {
        throw 'Invalid guardian spirit trouble item';
    }
}

// Will store templates fetched from files. Loaded only once so that there is no async needed later
const templates = {}

export class AETroubleCard {

    /*-------------------------------------------------------- */

    /**
     * CardContent will be dynamically added during CardsDisplay.addListeners().
     * This method being not async, we load the HTML content during initialisation phase.
     */
     static async loadTemplatesInMemory() {
        templates.cardContent = await getTemplate('systems/acariaempire/resources/cards/trouble/card-content.hbs');
    }

    /*-------------------------------------------------------- 
    Structure :
        item : {
            name: "",                     Card Number
            img : "",                     Simple card background
            data : {
                "effect" :                see guardianSpiritTroubleEffectList(),
                "quantity" :              see guardianSpiritTroubleQuantityList(),
                "step" :                  Hunger or Dissatisfaction step,
                "status" :                see TROUBLE_STATUS
            }
        }

      -------------------------------------------------------- */

    constructor(card) {
        checkIntegrity(card);
        this._card = card;
        this.actionService = game.modules.get('ready-to-use-cards').actionService;
    }

    get card() {
        return this._card;
    }

    get cardData() {
        return this._card.system;
    }

    get effect() {
        if( !this._effect ) {
            this._effect = guardianSpiritTroubleEffectList().find( e => e.key === this.cardData.effect);
        }
        return this._effect;
    }

    get quantity() {
        if( !this._quantity ) {
            this._quantity = guardianSpiritTroubleQuantityList().find( e => e.key === this.cardData.quantity);
        }
        return this._quantity;
    }

    get step() {
        return parseInt( this.cardData.step );
    }

    get status() {
        return guardianSpiritTroubleStatusList().find( e => e.key === this.cardData.status);
    }

    get isPositive() {
        return this.status.key === TROUBLE_STATUS.positive;
    }

    get isNegative() {
        return this.status.key === TROUBLE_STATUS.negative;
    }

    impactOnRollResult(faces, fromSpirit) {

        if( this.status.key === TROUBLE_STATUS.ignored ) { return; }

        const multiplier = this.isPositive ? 1 : -1;
        if( this.effect.key === AVAILABLE_EFFECTS.success_minimum ) {
            fromSpirit.success += multiplier * Math.min( faces.doubleSuccess, this.quantity.max );

        } else if( this.effect.key === AVAILABLE_EFFECTS.power_missing ) {
            fromSpirit.powers += multiplier * Math.min( faces.powers, this.quantity.max );
            
        } else if( this.effect.key === AVAILABLE_EFFECTS.power_health ) {
            fromSpirit.health += multiplier * Math.min( faces.powers, this.quantity.max );
            
        } else if( this.effect.key === AVAILABLE_EFFECTS.initiative_slow ) {
            fromSpirit.initiative -= multiplier * Math.min( faces.powers, this.quantity.max ); // Positive trouble removes UT
            
        } else if( this.effect.key === AVAILABLE_EFFECTS.event_confused ) {
            const nbFaces = this.isPositive ? faces.badEvents : faces.events;
            fromSpirit.events += multiplier * Math.min( nbFaces, this.quantity.max )
            
        } else if( this.effect.key === AVAILABLE_EFFECTS.skull_augmented ) {
            const nbFaces = this.isPositive ? faces.doubleSkulls : faces.simpleSkulls;
            fromSpirit.skulls -= multiplier * Math.min( nbFaces, this.quantity.max ); // Positive trouble removes skulls
        }
    }

    /*-------------------------------------------------------- 
        Methods called by the GUI Wrapper
      -------------------------------------------------------- */

    /**
     * Retrieving RTUCards CustomCardStack for the card related deck
     * @return {CustomCardStack} See RTUCards module
     */
     get sourceStack() {
        if( !this._sourceStack ) {
            this._sourceStack = rtucStack(this.card.source);
        }
        return this._sourceStack;
    }

    /**
     * Retrieving RTUCards CustomCardStack for the current card parent.
     * No cache, may change
     * @return {CustomCardStack} See RTUCards module
     */
     get parentStack() {
        return  rtucStack(this.card.parent);
    }

    /**
     * Retrieving RTUCards stacks definition
     * @return {object} See RTUCards CARD_STACKS_DEFINITION
     */
     get stacksDef() {
        if( !this._stacksDef ) {
            this._stacksDef = game.modules.get('ready-to-use-cards').stacksDefinition;
        }
        return this._stacksDef;
    }

    /**
     * When filling .card-slot, a css class will be added to the HtmlElement.
     * If this getter is not present in your impl, 'basecard' will be the used cssClass
     * @returns {string} the wanted cssClass
     */
     get guiClass() { return 'troublecard'; }
    
    /**
     * You can alter the .card-slot content when this card is selected and visible.
     * Wrapper has already added this.guiClass as a css class of the element
     * @param {HTMLElement} htmlDiv .card-slot htmlElement
     */
     alterFillCardContent(htmlDiv) {

        const data = { impl: this, gui: {} };
        data.gui.negativeLabel = guardianSpiritTroubleLabelList().find( l => l.key === 'apply_negative').name;
        data.gui.positiveLabel = guardianSpiritTroubleLabelList().find( l => l.key === 'apply_positive').name;

        data.token = {
            displayed: !this.parentStack.stackOwner.forNobody
        };
        if( data.token.displayed ) {
            const marginTop = this.isPositive ? -80 : ( this.isNegative ? -470 : -270);
            data.token.style = "margin-top: " + marginTop + "px;";
        }

        
        
        // Substituting data from card-content.hbs
        const content = templates.cardContent(data, {
            allowProtoMethodsByDefault: true,
            allowProtoPropertiesByDefault: true
        });
        htmlDiv.innerHTML = content;
    }

    /**
     * You can modify the content which will be displayed on chat when manipulating this card.
     * Will be used on several actions, like play, discard or reveal.
     * You should directly alter the result param if you want to add changes. (returns nothing)
     * @param {object} result What will be used if you change nothing.
     * @param {CustomCardStack} from Where the card was previously
     * @param {boolean} addCardDescription : If description should be added for each card
     */
     alterBuildCardInfoForListing(result, from, addCardDescription) {

        if( addCardDescription ) {
            result.description = [this.quantity.name];
        }
    }


    /**
     * You can modify the available actions when this selected card is inside your revealed cards
     * You should directly alter the result actions if you want to add changes. (returns nothing)
     * @param {object[]} actions Action computed by the wrapper. (default result base on stack settings)
     * @param {boolean} stackOwnedByUser if this is the current user hand
     */
     alterLoadActionsWhileInRevealedCards(actions, stackOwnedByUser) {
        
        actions.length = 0; // Clearing the action list

        // Add actions related to trouble status change
        if( game.user.isGM || stackOwnedByUser ) {
            
            const currentStatus = this.status;
            guardianSpiritTroubleStatusList().filter( s => s.key != currentStatus.key).forEach( s => {

                const action = this.actionService.customGUIAction(
                    game.i18n.localize('AESYSTEM.cards.trouble.sheet.actions.updateTrouble.actionButton'), 
                    s.key
                );
                action.label = action.label.replace('STATUS', s.name);
                actions.push(action);
            });
        }
    }

    /**
     * You can add custom behavior when an action is clicked.
     * The wrapper will simply relay the information to the impl class
     */
     async onClickDoCustomAction(action) {

        if( Object.values(TROUBLE_STATUS).find( v => v === action) ) {
            if( this.status.key != action ) {
                await this.changeStatus(action);
            }
        }
        this._card.parent.sheet.render();

        // Also update interaction GUI since it can display current trouble
        // Should normally use a hook for this. But since it's a really simple call...
        const gui = game.aesystem.interactionGUIForActor;
        if(gui.rendered) {
            gui.render();
        }
    }


    async changeStatus(newStatus) {
        if( ! Object.values(TROUBLE_STATUS).includes(newStatus) ) {
            throw 'Invalid trouble status ' + newStatus;
        }
        if( this.card.parent.type != 'pile' && newStatus != TROUBLE_STATUS.ignored ) {
            throw 'Status can only be changed for owned Troubles';
        }

        const updateData = {};
        updateData['system.status'] = newStatus;
        await this.card.update(updateData);
        this.card.sheet.render();
    }
}