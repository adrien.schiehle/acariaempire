import {spiritEffectList, spiritElementList, spiritBonusList, eventTypeList, eventTimingList, eventCostList, eventOnRollList} from '../tools/WaIntegration.js';
import { ALL_CARDS_TYPES } from '../tools/constants.js';
import { rtucStack } from './CardTools.js';

export const CARD_CHOICE = {
    none : 'none', 
    event : 'event', 
    spirit : 'spirit', 
};

const EVENTCARD_COSTS = ['0', '1', '2', '3', 'V'];
const EVENTCARD_WHEN = ['yourturn', 'enemyturn', 'allyturn', 'successenemy', 'cardplayed', 'gmonly', 'actionattempt'];
const EVENTCARD_ONROLL = ['die_success', 'die_power', 'token_success', 'token_cardbalance', 'initiative', 'health'];


const checkIntegrity = (card) => {

    let isOk = true;
    if(card.type != ALL_CARDS_TYPES.event) {
        isOk = false;
        console.error(card.name + ' is not an eventCard. (' + card.type + ');');

    } else {

        if( ! card.system.event ) {
            isOk = false;
            console.error(card.name + ' should have an event part in its data');

        } else {

            if( ! EVENTCARD_COSTS.includes(card.system.event.cost) ) {
                isOk = false;
                console.error(card.name + ' has invalid event cost part');
            }
    
            if( ! EVENTCARD_WHEN.includes(card.system.event.when) ) {
                isOk = false;
                console.error(card.name + ' has invalid event when part');
            }
    
            if( ! EVENTCARD_ONROLL.includes(card.system.event.onroll) ) {
                isOk = false;
                console.error(card.name + ' has invalid event onroll part');
            }
        }


        if( ! card.system.spirit ) {
            isOk = false;
            console.error(card.name + ' should have a spirit part in its data');

        } else {

            if( ! spiritEffectList().find(e => e.effect === card.system.spirit.effect) ) {
                isOk = false;
                console.error(card.name + ' has invalid spirit effect part');
            }
    
            if( ! spiritElementList().find(e => e.element === card.system.spirit.element) ) {
                isOk = false;
                console.error(card.name + ' has invalid spirit element part');
            }
    
            if( ! spiritBonusList().find(b => b.bonus === card.system.spirit.bonus) ) {
                isOk = false;
                console.error(card.name + ' has invalid spirit bonus part');
            }
        }
    }
    
    if(! isOk ) {
        throw 'Invalid event card';
    }
}

// Will store templates fetched from files. Loaded only once so that there is no async needed later
const templates = {}

export class AEEventCard {

    /*-------------------------------------------------------- */

    /**
     * CardContent will be dynamically added during CardsDisplay.addListeners().
     * This method being not async, we load the HTML content during initialisation phase.
     */
     static async loadTemplatesInMemory() {
        templates.cardContent = await getTemplate('systems/acariaempire/resources/cards/event/card-content.hbs');
    }

    /*-------------------------------------------------------- 
    Structure :
    card : {
        name: "",                     Card Number
        img : "",                     Simple card background
        data : {
            "event" : {               When used as an event card
                "ref" : "",           Name of the event
                "cost" : "",          How many cards should be discarded to play this one. See EVENTCARD_COSTS
                "when" : "",          When it can be played. See EVENTCARD_WHEN,
                "onroll": ""          Alternative that can be use instead of the main event while rolling dice
                // "description" will only be link inside translation data
            },
            "spirit" : {              When used as a spirit for a prayer
                "effect" : "",        What the spirit will be able to cast
                "element" : "" ,        Element of the summoned spriti
                "bonus" : ""            Bonus given when miracle is done
            }
        }
    }

      -------------------------------------------------------- */

    constructor(card) {
        checkIntegrity(card);
        this._card = card;
        this.actionService = game.modules.get('ready-to-use-cards').actionService;
    }

    get card() {
        return this._card;
    }

    get cardData() {
        return this._card.system;
    }

    get gmOnlyEvent() {
        return this.cardData.event.when == 'successgmonly';
    }

    get eventDefinition() {
        if( !this._eventDefinition ) {
            this._eventDefinition = {
                type: eventTypeList().find( t => t.key === this.cardData.event.ref ),
                timing: eventTimingList().find( t => t.key === this.cardData.event.when ),
                cost: eventCostList().find( c => c.key === this.cardData.event.cost ),
                onroll: eventOnRollList().find( c => c.key === this.cardData.event.onroll ),
            };
        }
        return this._eventDefinition;
    }

    get spiritDefinition() {
        if( !this._spiritDefinition ) {
            this._spiritDefinition = {
                element: spiritElementList().find( el => el.element === this.cardData.spirit.element ),
                effect: spiritEffectList().find( ef => ef.effect === this.cardData.spirit.effect ),
                bonus: spiritBonusList().find( b => b.bonus === this.cardData.spirit.bonus )
            };
        }
        return this._spiritDefinition;
    }

    get isEvent() {
        return this.card.parent?.type === 'hand';
    }

    get isSpirit() {
        const parent = this.card.parent;
        if( parent?.type != 'pile' ) { return false; }

        const owner = rtucStack(parent).stackOwner;
        return !owner.forNobody;
    }

    get noChoiceMade() {
        return !this.isEvent && !this.isSpirit;
    }

    /*-------------------------------------------------------- 
        Methods called by the GUI Wrapper
      -------------------------------------------------------- */

    /**
     * Retrieving RTUCards CustomCardStack for the card related deck
     * @return {CustomCardStack} See RTUCards module
     */
    get sourceStack() {
        if( !this._sourceStack ) {
            this._sourceStack = rtucStack(this.card.source);
        }
        return this._sourceStack;
    }

    /**
     * Retrieving RTUCards CustomCardStack for the current card parent.
     * No cache, may change
     * @return {CustomCardStack} See RTUCards module
     */
     get parentStack() {
        return  rtucStack(this.card.parent);
    }

    /**
     * Retrieving RTUCards stacks definition
     * @return {object} See RTUCards CARD_STACKS_DEFINITION
     */
     get stacksDef() {
        if( !this._stacksDef ) {
            this._stacksDef = game.modules.get('ready-to-use-cards').stacksDefinition;
        }
        return this._stacksDef;
    }

    /**
     * When filling .card-slot, a css class will be added to the HtmlElement.
     * If this getter is not present in your impl, 'basecard' will be the used cssClass
     * @returns {string} the wanted cssClass
     */
     get guiClass() { return 'eventcard'; }

    /**
     * You can alter your card behavior when a rotation is asked in the GUI.
     * @param {boolean} result Result computed by the wrapper (default result base on stack settings)
     * @param {boolean} rotatingAsked If a rotation has been asked by the GUI
     * @returns {boolean} The new result
     */
     alterShouldBeRotated( result, rotatingAsked ) {
        return result || this.isSpirit;
    }
    
    /**
     * You can alter the .card-slot content when this card is selected and visible.
     * Wrapper has already added this.guiClass as a css class of the element
     * @param {HTMLElement} htmlDiv .card-slot htmlElement
     */
     alterFillCardContent(htmlDiv) {

        const data = { impl: this, gui: {} };

        const spiritDef = this.spiritDefinition;
        data.gui.elementImg = spiritDef.element.icon;
        data.gui.effectImg = spiritDef.effect.icon;
        data.gui.effectDesc = spiritDef.effect.description.reduce( (result, current) => result += ' ' + current );
        data.gui.bonusImg = spiritDef.bonus.icon;
        data.gui.bonusDesc = spiritDef.bonus.description.reduce( (result, current) => result += ' ' + current );
        
        // Substituting data from card-content.hbs
        const content = templates.cardContent(data, {
            allowProtoMethodsByDefault: true,
            allowProtoPropertiesByDefault: true
        });
        htmlDiv.innerHTML = content;
    }


    /**
     * You can modify the content which will be displayed on chat when manipulating this card.
     * Will be used on several actions, like play, discard or reveal.
     * You should directly alter the result param if you want to add changes. (returns nothing)
     * @param {object} result What will be used if you change nothing.
     * @param {CustomCardStack} from Where the card was previously
     * @param {boolean} addCardDescription : If description should be added for each card
     */
     alterBuildCardInfoForListing(result, from, addCardDescription) {

        // For event cards, what will be displayed will differ if a revealcard stact is involved => spirit displayed
        const involvedStacks = [from, this.parentStack ];
        const spiritStack = involvedStacks.some( s => {
            return !s.stackOwner.forNobody && s.stack.type == 'pile';
        });

        // Was on a revealedCards stack ?
        if( spiritStack ) {
            // Spirit data will be displayed
            const spiritDef = this.spiritDefinition;
            const elementIcon = spiritDef.element.icon;
            const cardName = this.card.name + ': ' + spiritDef.effect.name;
            result.icon = elementIcon;
            result.name = cardName;
            result.rotated = 1;
            if( addCardDescription ) {
                result.description = [];
                result.description = result.description.concat( spiritDef.element.description );
                result.description = result.description.concat( spiritDef.effect.description );
                result.description = result.description.concat( spiritDef.bonus.description );
            }
    
        } else {
            // Event data will be displayed
            const cardName = this.card.name + ': ' + this.eventDefinition.type.name;
            result.name = cardName;
            if( addCardDescription ) {
                result.description = this.eventDefinition.type.description;
            }
    
        }
    }

    /**
     * You can modify the available actions when this selected card is inside the your hand
     * You should directly alter the result actions if you want to add changes. (returns nothing)
     * @param {object[]} actions Action computed by the wrapper. (default result base on stack settings)
     * @param {boolean} stackOwnedByUser if this is the current user hand
     * @param {boolean} detailsHaveBeenForced Normally, decks card are not visible. An action exist so that they become visible for gm.
     */
     alterLoadActionsWhileInHand(actions, stackOwnedByUser, detailsHaveBeenForced) {

        actions.length = 0; // Clearing the action list

        const choosingRollCard = game.aesystem.interactionGUIForActor.rendered;

        if( stackOwnedByUser ) {

            if( choosingRollCard ) {
                actions.push( this.actionService.customGUIAction(
                    game.i18n.localize('AESYSTEM.cards.event.sheet.actions.useOnRoll.actionButton'), 
                    'useOnRoll'
                ));
            } else {
                actions.push( this.actionService.customGUIAction(
                    game.i18n.localize('AESYSTEM.cards.event.sheet.actions.playEvent.actionButton'), 
                    'playEvent'
                ));
                actions.push( this.actionService.customGUIAction(
                    game.i18n.localize('RTUCards.action.moveCard.discardOne.label'), 
                    'discardEvent'
                ));
            }
        }

        // No rotate available here 
    }

    /**
     * You can modify the available actions when this selected card is inside your revealed cards
     * You should directly alter the result actions if you want to add changes. (returns nothing)
     * @param {object[]} actions Action computed by the wrapper. (default result base on stack settings)
     * @param {boolean} stackOwnedByUser if this is the current user hand
     */
     alterLoadActionsWhileInRevealedCards(actions, stackOwnedByUser) {
        
        actions.length = 0; // Clearing the action list

        if( stackOwnedByUser ) {
            actions.push( this.actionService.customGUIAction(
                game.i18n.localize('AESYSTEM.cards.event.sheet.actions.callSpirits.actionButton'), 
                'callSpirits'
            ));
            actions.push( this.actionService.customGUIAction(
                game.i18n.localize('AESYSTEM.cards.event.sheet.actions.castMiracle.actionButton'), 
                'castMiracle'
            ));
            actions.push( this.actionService.customGUIAction(
                game.i18n.localize('AESYSTEM.cards.event.sheet.actions.releaseSpirit.actionButton'), 
                'releaseSpirit'
            ));
        }

        // No rotate available here 
    }

    /**
     * You can add custom behavior when an action is clicked.
     * The wrapper will simply relay the information to the impl class
     */
     async onClickDoCustomAction(action) {

        if( action == 'callSpirits' ) {
            return this.triggerPrayerRoll('communion');

        } else if( action == 'castMiracle' ) {
            return this.triggerPrayerRoll('miracle');

        } else if( action == 'releaseSpirit' ) {
            return this.triggerPrayerRoll('release');

        } else if( action == 'playEvent' ) {
            return this.askForCardsToUseEvent();

        } else if( action == 'discardEvent' ) {
            return this.discardEvent();

        } else if( action == 'useOnRoll' ) {
            return this.useOnRoll();
        }
    }

    /**
     * Custom action : Discard the card
     */
    async discardEvent() {
        await this.parentStack.discardCards([this._card.id]);
    }

    /**
     * Custom action : Use the card inside roll
     */
     async useOnRoll() {
        const gui = game.aesystem.interactionGUIForActor;
        gui.chooseCardForRoll(this);
        this._card.parent.sheet.close();
    }

    /**
     * Display the roll panel with the right selected character
     * It's set on calling or releasing spirits
     * @param {*} subchoice 
     */
    async triggerPrayerRoll(subchoice) {

        const args = { battle: 'prayer', battleSubchoice: subchoice };
        if( !game.user.isGM && game.user.character ){
            args.actorId = game.user.character.id;
            args.tokenId = game.user.character.tokenId;
        }
        const success = game.aesystem.triggerRollPanel(args);
        if( success ) {
            this._card.parent.sheet.close();
        }
    }

    /**
     * When the playEvent button is clicked
     */
    async askForCardsToUseEvent() {

        const sheet = this._card.parent.sheet;
        const options = {
            minAmount: this.eventDefinition.cost.min, 
            maxAmount: this.eventDefinition.cost.max,
            buttonLabel: this.sourceStack.localizedLabel('sheet.actions.playEvent.actionButton'),
            fromStacks: [this.parentStack]
        };

        const selectTitle = this.sourceStack.localizedLabel('sheet.actions.playEvent.selectTitle');
        
        if( options.maxAmount == 0 ) {
            // No need to discard cards => Trigger event directly
            await this.parentStack.playCards([sheet.currentSelection.id], {displayedInChat: true});
            sheet.render();
            return;
        }

        // Prepare data for selecting cards to discard
        options.criteria = (card) => { 
            return card.type === ALL_CARDS_TYPES.event; 
        };
        options.callBack = async (selection, from, additionalCards) => {
            const discardedIds = additionalCards.map( c => c.id );
            await this.parentStack.playCards([selection.id], {displayedInChat: true});
            await this.parentStack.discardCards(discardedIds);
        };

        const cls = this.stacksDef.shared.actionParametersClasses.cardSelection;
        sheet._actionParameters = new cls(sheet, selectTitle, options );
        sheet.render();
    }

}