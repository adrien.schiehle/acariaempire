/**
 * Retrieving the CustomCardStack implementing RTU-Cards custom actions
 * @param {Cards} cardStack 
 * @returns {CustomCardStack} From RTU-Cards module
 */
 export const rtucStack = (cardStack) => {
    const def = game.modules.get('ready-to-use-cards').stacksDefinition;
    const cls = def.shared.cardClasses.customCardStack;
    return new cls(cardStack);
}

