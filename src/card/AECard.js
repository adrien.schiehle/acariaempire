import { ALL_CARDS_TYPES } from "../tools/constants.js";
import { AEEventCard } from "./EventCard.js";
import { AETroubleCard } from "./TroubleCard.js";

export class AECard extends Card {

    constructor(...args) {
        super(...args);
    }

    /**
     * Current card impl depending on its type.
     * See SimpleCard for a classic implementation
     */
    get impl() {
        if( !this._impl ) {
            if( this.type === ALL_CARDS_TYPES.event ) {
                this._impl = new AEEventCard(this);
            } else if( this.type === ALL_CARDS_TYPES.trouble ) {
                this._impl = new AETroubleCard(this);
            }
        }
        return this._impl;
    }

}
