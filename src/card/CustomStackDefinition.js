import {translate} from '../tools/Translator.js';
import { TROUBLE_STATUS } from '../tools/constants.js';
import { guardianSpiritTroubleEffectList, guardianSpiritTroubleQuantityList } from '../tools/WaIntegration.js';
import { AEEventCard } from './EventCard.js';
import { AETroubleCard } from './TroubleCard.js';

export const alterCardStacksDefinition = (cardStacksDefinition) => {

    cardStacksDefinition.playerStacks.resourceBaseDir = 'systems/acariaempire/resources/cards/event';
    cardStacksDefinition.gmStacks.resourceBaseDir = 'systems/acariaempire/resources/cards/event';
    
    cardStacksDefinition.core.event = {
        cardClass: AEEventCard,
        labelBaseKey : 'AESYSTEM.cards.event.',
        resourceBaseDir : 'systems/acariaempire/resources/cards/event',
        presetLoader: loadEventCards,
        defaultSettings: {
            actions: {
                'peekOnCards-DEDE': true,
                'peekOnCards-PHPH': true,
                'dealCard-GHDE': true,
                'dealCard-GRDE': true,
                'dealCard-PHDE': true,
                'dealCard-PRDE': true,
                'drawDeckCard-GRDE': true,
                'drawDeckCard-GHDE': true,
                'shuffleDeck-DEDE': true,
                'resetDeck-DEDE': true,
                'resetDiscard-DIDI': true,
                'moveCard-DEGH': true,
                'moveCard-DEGR': true,
                'moveCard-DEPH': true,
                'moveCard-DEPR': true,
                'moveCard-DIDE': true,
            }
        }

    };

    cardStacksDefinition.core.trouble = {
        cardClass: AETroubleCard,
        labelBaseKey : 'AESYSTEM.cards.trouble.',
        resourceBaseDir : 'systems/acariaempire/resources/cards/trouble',
        presetLoader: loadTroubleCards,
        defaultSettings: {
            actions: {
                'peekOnCards-DEDE': true,
                'dealCard-GRDE': true,
                'dealCard-PRDE': true,
                'shuffleDeck-DEDE': true,
                'resetDeck-DEDE': true,
                'resetDiscard-DIDI': true,
                'moveCard-DEGH': true,
                'moveCard-DEGR': true,
                'moveCard-DEPH': true,
                'moveCard-DEPR': true,
                'moveCard-DIDE': true,
            }
        }
    };
};

export const loadCardTemplates = async () => {
    await AEEventCard.loadTemplatesInMemory();
    await AETroubleCard.loadTemplatesInMemory();
}

export const overrideGlobalRTUCardsSettings = async () => {
    await game.settings.set("ready-to-use-cards", 'everyRevealedDiscardAll', false); // Some cards in the discard piles should not be discarded
}


const generateID = () => {
    let id = '';
    while( id.length < 16 ) { id += Math.random().toString(36).substring(2); }
    return id.substring(0, 16); // On 16 characters
}

const loadEventCards = async () => {

    const cards = await fetch('systems/acariaempire/resources/cards/event/cards.json').then(r => r.json());
    const origin = generateID();
    return cards.map( c => {

        const cardData = {
            name: '' + c.id,
            faces: [{
                name: c.id,
                img: "systems/acariaempire/resources/cards/event/background/front.webp",
                text: ''
            }],
            width: 2,
            height: 4,
            rotation: 0,
            type: "event",
            value: 0,
            suit: c.spiritelement,
            id: generateID(),
            face: 0,
            drawn: false,
            sort: 0,
            back: {
                name: translate("AESYSTEM.cards.event.cards.defaultname"),
                text: "",
                img: "systems/acariaempire/resources/cards/event/background/back.webp"
            },
            data: {
                event: {
                    ref: c.eventref,
                    cost: '' + c.eventcost,
                    when: c.eventwhen,
                    onroll: c.onroll
                },
                spirit: {
                    effect: c.spiriteffect,
                    element: c.spiritelement,
                    bonus: c.spiritbonus
                }
            },
            origin: origin
        };
        return cardData;
    });
}

const loadTroubleCards = async () => {

    const cards = await fetch('systems/acariaempire/resources/cards/trouble/cards.json').then(r => r.json());
    const origin = generateID();
    return cards.map( c => {

        const effect = guardianSpiritTroubleEffectList().find( el => el.key === c.effect );
        const quantity = guardianSpiritTroubleQuantityList().find( el => el.key === c.quantity );

        const cardName = '' + c.id + ': ' + effect.name;

        const cardData = {
            name: cardName,
            faces: [{
                name: cardName,
                img: "systems/acariaempire/resources/cards/trouble/background/front.webp",
                text: quantity.name
            }],
            width: 2,
            height: 4,
            rotation: 0,
            type: "trouble",
            value: c.id,
            suit: effect.name,
            id: generateID(),
            face: 0,
            drawn: false,
            sort: 0,
            back: {
                name: translate("AESYSTEM.cards.trouble.cards.defaultname"),
                text: "",
                img: "systems/acariaempire/resources/cards/trouble/background/back.webp"
            },
            data: {
                effect: c.effect,
                quantity: c.quantity,
                step: '' + c.id,
                status: TROUBLE_STATUS.ignored
            },
            origin: origin
        };
        return cardData;
    });
}
