import { AEInteractionGUIForDefense } from "../interaction/InteractionGUIForDefense.js";

export const registerAskingDifficultySocket = () => {
	const socket = socketlib.registerSystem("acariaempire"); // Will simply return it if it's already registered
	socket.register("ask-difficulty", promptDifficultyQuestion);
}

export const askGMForInteractionDifficulty = async (mainActorData, defenseData) => {
	const socket = socketlib.registerSystem("acariaempire"); // Will simply return it since already registered

    const mainActor = mainActorData.impl.mainActor;
    const allTargetsUuid = defenseData.impl.allTargetsUuid;
    const actorObj = mainActorData.impl.forSocket();
    const defenseObj = defenseData.impl.forSocket();

    // Needed : mainActor uuid, mainActorData.impl.forSocket(), defenseData.impl.forSocket()

    return socket.executeAsGM("ask-difficulty", game.user, mainActor.uuid, allTargetsUuid, actorObj, defenseObj);
}


const promptDifficultyQuestion = async (user, actorUuid, allTargetsUuid, actorObj, defenseObj) => {
    
    const result = {};
    result.answered = false;

    // We will need to retrieve necessary data from what was given
    let mainActor = fromUuidSync(actorUuid);
    if( mainActor.actor ) { // Can be a token
        mainActor = mainActor.actor;
    }

    // Retrieve targets
    let targets = allTargetsUuid
                    .map( uuid => fromUuidSync(uuid) )
                    .map( a => a.actor ? a.actor : a );

    const gui = new AEInteractionGUIForDefense(user, mainActor, targets);
    gui.mainActorData.changeParentMethod(actorObj.methodGroup);
    gui.mainActorData.changeMethod(actorObj.method);
    gui.mainActorData.impl.fromSocket(actorObj);

    const availableMethods = gui.mainActorData.impl.availableDefenseMethods;
    gui.defenseData.computeAvailableMethods(availableMethods);
    gui.defenseData.changeMethod(defenseObj.method);
    gui.defenseData.impl.fromSocket(defenseObj);

    gui.render(true);

    return await waitForAnswer(gui);
}

const waitForAnswer = async (gui) => {

    while( true ) {
        // Wait 500 ms between each check
        await new Promise(r => setTimeout(r, 500));

        if( gui._state == Application.RENDER_STATES.CLOSED ) {
            return gui.answered ? gui.defenseData.impl.forSocket() : undefined;
        }
    }

}
