import { translate } from '../tools/Translator.js';

export const registerMerchantTransactionSockets = () => {
	const socket = socketlib.registerSystem("acariaempire"); // Will simply return it if it's already registered
	socket.register("sell-item", gmSellsItem);
    socket.register("buy-item", gmBuysItem);
}

/**
 * Only GM have the rights to edit merchant NPC and their furniture
 * This method allow anyone to sell an item to an NPC through the GM
 * @param {AEActor} toActor Actor wanting to sell an item
 * @param {AEActor} fromMerchant Merchant purchasing it
 * @param {AEItem} item Item the actor wants to sell
 * @param {int} price Price at which the item was sold
 * @returns {string} message to display when the sell is done
 */
export const askGMToSellItem = async (toActor, fromMerchant, item, price) => {

	const socket = socketlib.registerSystem("acariaempire"); // Will simply return it if it's already registered
    const actorUuid = toActor.uuid;
    const merchantUuid = fromMerchant.uuid;
    const itemUuid = item.uuid;

    return socket.executeAsGM("sell-item", actorUuid, merchantUuid, itemUuid, price );
}

/**
 * Only GM have the rights to edit merchant NPC and their furniture
 * This method allow anyone to buy an item from an NPC through the GM
 * @param {AEActor} toActor Actor wanting to buy an item
 * @param {AEActor} fromMerchant Merchant selling it
 * @param {int} itemIndexInShop Index of the item inside the merchant shop
 * @param {int} price Price at which the item was purchased
 * @param {string} source Can be 'shop' or 'bought' for now. 'bought' items are removed from store once sold
 * @returns {string} message to display when the purchase is done
 */
 export const askGMToBuyItem = async (fromActor, toMerchant, itemIndexInShop, price, source) => {

	const socket = socketlib.registerSystem("acariaempire"); // Will simply return it if it's already registered
    const actorUuid = fromActor.uuid;
    const merchantUuid = toMerchant.uuid;

    return socket.executeAsGM("buy-item", actorUuid, merchantUuid, itemIndexInShop, price, source );
}

const gmSellsItem = async ( actorUuid, merchantUuid, itemUuid, price) => {

    const actor = fromUuidSync(actorUuid);
    let merchantActor = fromUuidSync(merchantUuid);
    merchantActor = merchantActor.actor ?? merchantActor; // Could be a token or an actor. We want to manipulate the actor

    const item = fromUuidSync(itemUuid);
    if(!item) {
        return translate('AESYSTEM.actor.sheet.merchant.notif.notFound');
    }

    await item.delete();
    await actor.gainCoins(price);
    
    // Give the item
    const newItemData = {
        type: item.type,
        name: item.name,
        img: item.img,
        system: item._source.system,
        ownership:{ default: CONST.DOCUMENT_OWNERSHIP_LEVELS.LIMITED }
    };
    await merchantActor.merchant.addBoughtItem(newItemData);

    const message = translate('AESYSTEM.actor.sheet.merchant.notif.itemSold').replace('ITEM_NAME', item.name ).replace('ITEM_PRICE', price);
    return message;
}

const gmBuysItem = async ( actorUuid, merchantUuid, itemIndexInShop, price, source) => {

    const actor = fromUuidSync(actorUuid);
    let merchantActor = fromUuidSync(merchantUuid);
    merchantActor = merchantActor.actor ?? merchantActor; // Could be a token or an actor. We want to manipulate the actor

    const item = merchantActor.merchant.sellableItems[itemIndexInShop].eqt?.item;
    if(!item) {
        return translate('AESYSTEM.actor.sheet.merchant.notif.notFound');
    }

    // Pay the price
    const paid = await actor.spendCoins(price);
    if( !paid ) {
        return translate('AESYSTEM.actor.sheet.merchant.notif.notEnough');
    }

    // Give the item
    const newItemData = {
        type: item.type,
        name: item.name,
        img: item.img,
        system: item._source.system
    };
    await actor.createEmbeddedDocuments("Item", [newItemData]);

    // An item that has been sold to the merchant can't be bought back multiple times
    if( source === 'bought' ) {
        await merchantActor.merchant.removeBoughtItem(newItemData.name);
    }
    
    const message = translate('AESYSTEM.actor.sheet.merchant.notif.itemBought').replace('ITEM_NAME', item.name ).replace('ITEM_PRICE', price);
    return message;
}